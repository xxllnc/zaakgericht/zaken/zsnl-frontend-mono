// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTheme } from '@mui/material';
import Button from '@mintlab/ui/App/Material/Button';
import { Theme } from '@mintlab/ui/types/Theme';
import { useNotificationBarStyles } from './NotificationBar.styles';

export type NotificationType = {
  message: string;
  button?: {
    label: string;
    action: () => void;
  };
};

type NotificationBarPropsType = {
  notifications: NotificationType[];
};

const NotificationBar: React.ComponentType<NotificationBarPropsType> = ({
  notifications,
}) => {
  const theme = useTheme<Theme>();
  const classes = useNotificationBarStyles();
  const [hiddenList, setHiddenList] = useState<string[]>([]);
  const hideNotification = (notification: string) => {
    setHiddenList(hiddenList.concat(notification));
  };

  const notificationList = notifications.filter(
    notification => !hiddenList.includes(notification.message)
  );

  if (!notificationList.length) return null;

  return (
    <div className={classes.wrapper}>
      {notificationList.map((notification, index) => (
        <div key={index} className={classes.notificationWrapper}>
          <div className={classes.notification}>
            <span>{notification.message}</span>
            {notification.button && (
              <Button
                name="notificationAction"
                sx={{ marginLeft: '20px' }}
                variant="outlined"
                action={() => notification.button?.action()}
              >
                {notification.button.label}
              </Button>
            )}
          </div>
          <Button
            name="closeNotification"
            icon="close"
            iconSize="extraSmall"
            action={() => hideNotification(notification.message)}
            sx={{ margin: '10px', color: theme.mintlab.greyscale.darkest }}
          />
        </div>
      ))}
    </div>
  );
};

export default NotificationBar;

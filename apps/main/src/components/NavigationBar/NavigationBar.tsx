// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import classnames from 'classnames';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Button from '@mintlab/ui/App/Material/Button';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon/Icon';
import { SessionRootStateType } from '@zaaksysteem/common/src/store/session/session.reducer';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { useNavigationBarStyles } from './NavigationBar.styles';

/* eslint-disable complexity */
export const NavigationBar: React.ComponentType<{}> = () => {
  const classes = useNavigationBarStyles();
  const [t] = useTranslation('main');
  const [open, setOpen] = useState(false);
  const capabilities = useSelector(
    (state: SessionRootStateType) =>
      state.session?.data?.logged_in_user?.capabilities || []
  );
  const internAllowed =
    capabilities.includes('gebruiker') && capabilities.includes('dashboard');

  const handleToggle = () => setOpen(!open);

  return (
    <nav
      className={classnames(
        classes.wrapper,
        open ? classes.open : classes.close
      )}
    >
      <div className={classes.menuButtonWrapper}>
        <Button
          action={handleToggle}
          name="navigationBarMenu"
          icon={open ? 'close' : 'menu'}
        />
      </div>

      <List disablePadding classes={{ root: classes.list }}>
        <div className={classes.topWrapper}>
          {internAllowed ? (
            <ListItem button component="a" href="/intern">
              <ListItemIcon classes={{ root: classes.icon }}>
                <Tooltip disabled={open} title={t('modules.dashboard')}>
                  <Icon size="small">{iconNames.home}</Icon>
                </Tooltip>
              </ListItemIcon>

              <ListItemText>{t('modules.dashboard')}</ListItemText>
            </ListItem>
          ) : null}
          {internAllowed && capabilities.includes('message_intake') ? (
            <ListItem button component="a" href="/main/communication">
              <ListItemIcon classes={{ root: classes.icon }}>
                <Tooltip disabled={open} title={t('modules.communication')}>
                  <Icon size="small">{iconNames.chat_bubble}</Icon>
                </Tooltip>
              </ListItemIcon>
              <ListItemText>{t('modules.communication')}</ListItemText>
            </ListItem>
          ) : null}
          {internAllowed &&
          capabilities.includes('documenten_intake_subject') ? (
            <ListItem button component="a" href="/main/document-intake">
              <ListItemIcon classes={{ root: classes.icon }}>
                <Tooltip disabled={open} title={t('links.intake')}>
                  <Icon size="small">{iconNames.note_add}</Icon>
                </Tooltip>
              </ListItemIcon>
              <ListItemText>{t('links.intake')}</ListItemText>
            </ListItem>
          ) : null}
          {internAllowed && capabilities.includes('search') ? (
            <ListItem button component="a" href="/search">
              <ListItemIcon classes={{ root: classes.icon }}>
                <Tooltip disabled={open} title={t('links.search')}>
                  <Icon size="small">{iconNames.search}</Icon>
                </Tooltip>
              </ListItemIcon>
              <ListItemText>{t('links.search')}</ListItemText>
            </ListItem>
          ) : null}
          {internAllowed && capabilities.includes('contact_search') ? (
            <ListItem button component="a" href="/main/contact-search">
              <ListItemIcon classes={{ root: classes.icon }}>
                <Tooltip disabled={open} title={t('links.contactSearch')}>
                  <Icon size="small">{iconNames.person_search}</Icon>
                </Tooltip>
              </ListItemIcon>
              <ListItemText>{t('links.contactSearch')}</ListItemText>
            </ListItem>
          ) : null}
          {internAllowed ? (
            <ListItem button component="a" href="/exportqueue">
              <ListItemIcon classes={{ root: classes.icon }}>
                <Tooltip disabled={open} title={t('links.export')}>
                  <Icon size="small">{iconNames.save_alt}</Icon>
                </Tooltip>
              </ListItemIcon>
              <ListItemText>{t('links.export')}</ListItemText>
            </ListItem>
          ) : null}

          {capabilities.some(
            (el: string) =>
              el === 'admin' ||
              el === 'useradmin' ||
              el === 'beheer_zaaktype_admin'
          ) ? (
            <React.Fragment>
              <div className={classes.separator} />
              <ListItem button component="a" href="/admin">
                <ListItemIcon classes={{ root: classes.icon }}>
                  <Tooltip disabled={open} title={t('modules.admin')}>
                    <Icon size="small">{iconNames.exit_to_app}</Icon>
                  </Tooltip>
                </ListItemIcon>
                <ListItemText>{t('modules.admin')}</ListItemText>
              </ListItem>
            </React.Fragment>
          ) : null}
        </div>
        <div className={classes.bottomWrapper}>
          {internAllowed ? (
            <ListItem
              button
              component="a"
              href="https://help.zaaksysteem.nl"
              target="_new"
            >
              <ListItemIcon classes={{ root: classes.icon }}>
                <Tooltip disabled={open} title={t('links.help')}>
                  <Icon size="small">{iconNames.support}</Icon>
                </Tooltip>
              </ListItemIcon>
              <ListItemText>{t('links.help')}</ListItemText>
            </ListItem>
          ) : null}
          <ListItem button component="a" href="/auth/logout">
            <ListItemIcon classes={{ root: classes.icon }}>
              <Tooltip disabled={open} title={t('links.logout')}>
                <Icon size="small">{iconNames.power_settings_new}</Icon>
              </Tooltip>
            </ListItemIcon>
            <ListItemText>{t('links.logout')}</ListItemText>
          </ListItem>
          {internAllowed ? (
            <ListItem button component="a" href="/intern/!over" target="_new">
              <span className={classes.textLink}>
                {open ? t('links.about') : t('links.aboutShort')}
              </span>
            </ListItem>
          ) : null}
        </div>
      </List>
    </nav>
  );
};

export default NavigationBar;

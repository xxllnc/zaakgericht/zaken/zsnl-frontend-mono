// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { useTranslation } from 'react-i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import TextField from '@mintlab/ui/App/Material/TextField';
import IconButton from '@mui/material/IconButton';

const WebOdfToolbar: React.ComponentType<{
  controllers: any;
  fileId: string;
  activeFormatting: any;
}> = ({ controllers, fileId, activeFormatting }) => {
  const [t] = useTranslation('WebodfMenu');
  const [fontSize, setFontSize] = React.useState(12);

  const controls = [
    {
      action: () => {
        const odfContainer = controllers.canvas.odfContainer();
        odfContainer.createByteArray((ba: any) => {
          const blob = new Blob([ba], {
            type: 'application/vnd.oasis.opendocument.text',
          });

          window.top?.postMessage(
            {
              type: 'save',
              id: fileId,
              blob,
            },
            '*'
          );
        });
      },
      active: false,
      title: 'save' as const,
    },
    {
      action: () => {
        controllers.sessionController && controllers.sessionController.undo();
      },
      active: false,
      title: 'undo' as const,
    },
    {
      action: () => {
        controllers.sessionController && controllers.sessionController.redo();
      },
      active: false,
      title: 'redo' as const,
    },
    {
      action: () => {},
      title: 'done' as const,
      active: false,
      resizer: (
        <TextField
          value={fontSize}
          id="resizer"
          name="resizer"
          type="number"
          variant="generic1"
          onChange={ev => {
            if (ev.target.value && controllers.formattingController) {
              const num = Math.min(Math.max(Number(ev.target.value), 3), 152);
              controllers.formattingController.setFontSize(num);
              setFontSize(num);
            }
          }}
        />
      ),
    },
    {
      action: () => {
        controllers.formattingController &&
          controllers.formattingController.toggleBold();
      },
      active: activeFormatting?.isBold,
      title: 'format_bold' as const,
    },
    {
      action: () => {
        controllers.formattingController &&
          controllers.formattingController.toggleItalic();
      },
      active: activeFormatting?.isItalic,
      title: 'format_italic' as const,
    },
    {
      action: () => {
        controllers.formattingController &&
          controllers.formattingController.toggleUnderline();
      },
      active: activeFormatting?.hasUnderline,
      title: 'format_underlined' as const,
    },
    {
      action: () => {
        controllers.formattingController &&
          controllers.formattingController.alignParagraphLeft();
      },
      active: activeFormatting?.isAlignedLeft,
      title: 'format_align_left' as const,
    },
    {
      action: () => {
        controllers.formattingController &&
          controllers.formattingController.alignParagraphRight();
      },
      active: activeFormatting?.isAlignedRight,
      title: 'format_align_right' as const,
    },
    {
      action: () => {
        controllers.formattingController &&
          controllers.formattingController.alignParagraphCenter();
      },
      active: activeFormatting?.isAlignedCenter,
      title: 'format_align_center' as const,
    },
    {
      action: () => {
        controllers.formattingController &&
          controllers.formattingController.alignParagraphJustified();
      },
      active: activeFormatting?.isAlignedJustified,
      title: 'format_align_justify' as const,
    },
  ];

  return (
    <React.Fragment>
      {controls.map(({ action, active, title, resizer }) =>
        resizer ? (
          resizer
        ) : (
          <Tooltip key={title} title={t(title)}>
            <div style={active ? { backgroundColor: 'rgba(0,0,0,0.2)' } : {}}>
              <IconButton onClick={action} color="inherit">
                <Icon size="small">{iconNames[title]}</Icon>
              </IconButton>
            </div>
          </Tooltip>
        )
      )}
    </React.Fragment>
  );
};

export default WebOdfToolbar;

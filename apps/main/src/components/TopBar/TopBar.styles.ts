// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useTopBarStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      display: 'flex',
      alignItems: 'center',
      gap: 20,
      padding: '16px 32px',
      minHeight: 75,
      borderBottom: `1px solid ${greyscale.dark}`,
    },
    breadCrumbs: {
      flexGrow: 1,
    },
    search: {
      display: 'flex',
      width: 550,
    },
  })
);

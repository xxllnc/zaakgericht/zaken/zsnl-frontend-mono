// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useBreadcrumbsStyles = makeStyles(
  ({ mintlab: { greyscale }, palette: { common }, typography }: Theme) => ({
    wrapper: {
      display: 'flex',
      alignItems: 'center',
      gap: 10,
      fontSize: typography.h6.fontSize,
      fontWeight: typography.h6.fontWeight,
    },
    dashboardLink: {
      color: common.black,
      opacity: 0.4,
      textDecoration: 'none',
      '&:hover': {
        color: greyscale.darkest,
        opacity: 1,
      },
    },
    separator: {
      fill: common.black,
      opacity: 0.4,
    },
    title: {
      whiteSpace: 'nowrap',
    },
    subTitleWrapper: {
      flexGrow: 1,
      width: 100,
      marginLeft: 20,
    },
    subTitle: {
      opacity: 0.4,
      whiteSpace: 'nowrap',
      overflow: 'hidden',
      textOverflow: 'ellipsis',
    },
  })
);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { useState, useEffect } from 'react';
import { useDebouncedCallback } from 'use-debounce';
import { useTranslation } from 'react-i18next';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import {
  builtInSearchOptions,
  getChoices,
  getFilters,
  MultiLineOptionWithExtension,
} from './library';
import { OptionType, FilterType } from './Search.types';
import { useSearchStyles } from './Search.styles';

const DELAY = 400;

const Search: React.ComponentType<{}> = () => {
  //search select
  const [input, setInput] = useState('');
  const [value, setValue] = useState(null as OptionType | null);
  const [choices, setChoices] = useState([] as OptionType[] | []);

  //filters select
  const [filter, setFilter] = useState(null as FilterType | null);
  const [filterOptions, setFilterOptions] = useState([] as FilterType[] | []);

  const [loading, setLoading] = useState(false);
  const [initializing, setInitializing] = useState(true);

  const [t] = useTranslation();
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const classes = useSearchStyles();

  const [getChoicesDebounced] = useDebouncedCallback(async () => {
    async function getResults() {
      if (!filter) return [];
      let response = await getChoices(input, filter, openServerErrorDialog, t);
      return response;
    }

    setLoading(true);
    setChoices(await getResults());
    setLoading(false);
  }, DELAY);

  useEffect(() => {
    getChoicesDebounced();
  }, [input, filter]);

  useEffect(() => {
    async function start() {
      let response = await getFilters(openServerErrorDialog, t);
      setFilter(response[response.length - 1]);
      setFilterOptions(response);
      setInitializing(false);
    }
    start();
  }, []);

  return initializing ? (
    <Loader />
  ) : (
    <React.Fragment>
      <div
        style={{ width: '100%', minWidth: 0 }}
        className="topBarSearchWrapper"
      >
        <Select
          variant="generic"
          loading={loading}
          choices={choices}
          value={value}
          isClearable={true}
          isMulti={false}
          placeholder={t('common:dialog.search')}
          filterOption={option => Boolean(option)}
          onChange={({ target: { value } }) => {
            if (value && value.url) {
              return;
            } else {
              setValue(value);
            }
          }}
          onInputChange={(ev, value) => {
            setInput(value);
          }}
          startAdornment={
            <Icon style={{ marginRight: '10px' }} size="small">
              {iconNames.search}
            </Icon>
          }
          renderOption={(props, option, state) => (
            <MultiLineOptionWithExtension {...props} {...option} />
          )}
        />
      </div>
      {ServerErrorDialog}

      <div className={classes.filters}>
        <Select
          variant="generic"
          value={filter}
          onChange={({ target: { value } }) => {
            setFilter(value);
            if (value && value.value === 'saved_searches') {
              setChoices(builtInSearchOptions(t));
              const cartet = document.querySelector<HTMLButtonElement>(
                '.topBarSearchWrapper>div>div>div>div>button'
              );
              cartet && cartet.click();
            }
          }}
          choices={filterOptions}
          isMulti={false}
          filterOption={option => Boolean(option)}
        />
      </div>
    </React.Fragment>
  );
};

export default Search;

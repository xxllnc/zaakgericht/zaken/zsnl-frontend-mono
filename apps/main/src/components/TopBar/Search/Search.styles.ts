// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useSearchStyles = makeStyles(() => ({
  filters: {
    marginLeft: 12,
    width: 300,
  },
  extension: {
    width: 30,
    marginTop: '5px',
    border: 'none',
    background: 'none',
    cursor: 'pointer',
    alignSelf: 'baseline',
  },
}));

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import * as i18next from 'i18next';
import { request } from '@zaaksysteem/common/src/library/request/request';
import MultilineOption from '@mintlab/ui/App/Zaaksysteem/Select/Option/MultilineOption';
import { APICaseManagement } from '@zaaksysteem/generated';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { useTranslation } from 'react-i18next';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { FilterType, OptionType } from './Search.types';
import { useSearchStyles } from './Search.styles';

/* eslint complexity: [2, 30] */
export const getLabel = (
  result: APICaseManagement.SearchResponseBody['data'][0]
): OptionType => {
  const { id, meta, attributes, relationships } = result;
  const type = attributes.result_type;

  if (type === 'file' || type === 'document') {
    return {
      value: id,
      label: meta?.summary || '',
      icon: <ZsIcon size="tiny">entityType.inverted.file</ZsIcon>,
      url: `/redirect/case?uuid=${relationships?.case?.data.id}&tab=document`,
    };
  } else if (type === 'case_type') {
    return {
      value: id,
      label: meta?.summary || '',
      icon: <ZsIcon size="tiny">entityType.inverted.case_type</ZsIcon>,
      url: `/admin/zaaktypen/${id}/bewerken`,
    };
  } else if (type === 'case') {
    return {
      value: id,
      label: meta?.summary || '',
      subLabel: `${attributes.description || ''}`,
      icon: <ZsIcon size="tiny">entityType.inverted.case</ZsIcon>,
      url: `/redirect/case?uuid=${id}`,
    };
  } else if (
    type === 'person' ||
    type === 'organization' ||
    type === 'employee'
  ) {
    return {
      value: id,
      label: meta?.summary || '',
      icon: <ZsIcon size="tiny">{`entityType.inverted.${type}`}</ZsIcon>,
      url: `/main/contact-view/${type}/${id}`,
    };
  } else if (type === 'custom_object') {
    return {
      value: id,
      label: meta?.summary || '',
      icon: <ZsIcon size="tiny">entityType.inverted.other</ZsIcon>,
      url: `/main/object/${id}`,
    };
  } else if (type === 'object_v1') {
    return {
      value: id,
      label: meta?.summary || '',
      icon: <ZsIcon size="tiny">entityType.inverted.other</ZsIcon>,
      url: `/object/${id}`,
    };
  } else if (type === 'custom_object_type') {
    return {
      value: id,
      label: meta?.summary || '',
      icon: <ZsIcon size="tiny">entityType.inverted.custom_object_type</ZsIcon>,
      url: `/admin/objecttype/edit/${id}`,
    };
  } else if (type === 'saved_search') {
    return {
      value: id,
      label: meta?.summary || '',
      icon: <ZsIcon size="tiny">search.inverted.savedSearch</ZsIcon>,
      url: `/search/${id}`,
    };
  } else {
    return {
      value: id,
      label: meta?.summary || '',
      icon: <ZsIcon size="tiny">entityType.inverted.other</ZsIcon>,
      url: `/redirect/case?uuid=${relationships?.case?.data?.id || ''}`,
    };
  }
};

export const builtInSearchOptions = (t: i18next.TFunction) => [
  {
    value: 'mine',
    label: t('topBar:savedSearches.mine'),
    icon: <ZsIcon size="tiny">search.inverted.savedSearch</ZsIcon>,
    url: '/search/mine',
    type: 'savedSearch',
  },
  {
    value: 'my-department',
    label: t('topBar:savedSearches.myDepartment'),
    icon: <ZsIcon size="tiny">search.inverted.savedSearch</ZsIcon>,
    url: '/search/my-department',
    type: 'savedSearch',
  },
  {
    value: 'all',
    label: t('topBar:savedSearches.all'),
    icon: <ZsIcon size="tiny">search.inverted.savedSearch</ZsIcon>,
    url: '/search/all',
    type: 'savedSearch',
  },
];

export const getChoices = async (
  input: string,
  filter: FilterType,
  openServerErrorDialog: OpenServerErrorDialogType,
  t: i18next.TFunction
) => {
  if (filter.value === 'saved_searches' && !input) {
    return builtInSearchOptions(t);
  }

  if (!input || input.length < 3) return [];

  const mapping: any = {
    all: 'case,custom_object,custom_object_type,document,employee,object_v1,organization,person,case_type,saved_search',
    saved_searches: 'saved_search',
    contacts: 'employee,organization,person',
    documents: 'document',
    case: 'case',
  };

  try {
    const results = await request<APICaseManagement.SearchResponseBody>(
      'GET',
      buildUrl<APICaseManagement.SearchRequestParams>('/api/v2/cm/search', {
        keyword: input,
        type: mapping[filter.value || 'all'],
        max_results_per_type: 20,
      })
    );

    return results.data.map(getLabel);
  } catch (err: any) {
    openServerErrorDialog(err);
  }

  return [];
};

export const getFilters = async (
  openServerErrorDialog: OpenServerErrorDialogType,
  t: i18next.TFunction
) => {
  try {
    const defaultOptions = [
      {
        value: 'all',
        label: t('topBar:filters.all'),
      },
      {
        value: 'saved_searches',
        label: t('topBar:filters.savedSearches'),
      },
      {
        value: 'contacts',
        label: t('topBar:filters.contacts'),
      },
      {
        value: 'documents',
        label: t('topBar:filters.documents'),
      },
      {
        value: 'case',
        label: t('topBar:filters.case'),
      },
    ];

    return [...defaultOptions];
  } catch (err: any) {
    openServerErrorDialog(err);
  }

  return [];
};

const ExtensionComponent: React.ComponentType<{ url: string }> = ({ url }) => {
  const classes = useSearchStyles();
  const [t] = useTranslation();

  return (
    <button
      className={classes.extension}
      type="button"
      title={t('common:general.openInNewWindow')}
      onClick={event => {
        event.stopPropagation();
        event.preventDefault();
        if (url) {
          window.open(url, '_blank');
        }
      }}
    >
      <Icon size="tiny" color="primary">
        {iconNames.open_in_new}
      </Icon>
    </button>
  );
};

export const MultiLineOptionWithExtension = (props: any) => {
  return <MultilineOption {...props} ExtensionComponent={ExtensionComponent} />;
};

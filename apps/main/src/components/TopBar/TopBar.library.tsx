// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { SectionType } from '@zaaksysteem/common/src/components/dialogs/AddElement/AddElement.types';

const supported = false;

export const getSections = ({
  t,
  contactAllowed,
  setCreateCaseOpen,
  setCreateContactMomentOpen,
  setAddElementOpen,
}: {
  t: i18next.TFunction;
  contactAllowed: boolean;
  setCreateCaseOpen: (open: boolean) => void;
  setCreateContactMomentOpen: (open: boolean) => void;
  setAddElementOpen: (open: boolean) => void;
}): SectionType[][] => [
  [
    {
      type: 'case',
      title: t('common:entityType.case'),
      icon: <ZsIcon style={{ fontSize: 30 }}>entityType.case</ZsIcon>,
      action: () => {
        setCreateCaseOpen(true);
        setAddElementOpen(false);
      },
    },
    {
      type: 'contact_moment',
      title: t('common:entityType.contact_moment'),
      icon: <ZsIcon style={{ fontSize: 30 }}>entityType.contact_moment</ZsIcon>,
      action() {
        setCreateContactMomentOpen(true);
        setAddElementOpen(false);
      },
    },
    ...(contactAllowed && supported
      ? [
          {
            type: 'contact',
            title: t('common:entityType.contact'),
            icon: <ZsIcon style={{ fontSize: 30 }}>entityType.contact</ZsIcon>,
            action() {},
          },
        ]
      : []),
  ],
];

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { useEmailTemplatePreviewerStyle } from './EmailTemplatePreviewer.style';
import emailTemplatePreviewerLocale from './locale';

type PreviewDataMessageType = {
  name: 'previewReload';
  value: {
    attachments: string[];
    body: string;
    imageUuid: string;
    label: string;
    recipient_type: string;
    template: string;
    to: string;
    caseId: number;
  };
};

type EmailTemplateResponseType = {
  to: string;
  body: string;
  from: string;
  subject: string;
  cc: string;
  bcc: string;
};

type EmailPreviewDataType = {
  to: string;
  attachments: string[];
  from: string;
  bcc: string;
  subject: string;
};

/* eslint complexity: [2, 8] */
const EmailTemplatePreviewer = () => {
  const [emailPreviewData, setEmailPreviewData] =
    React.useState<EmailPreviewDataType>();
  const classes = useEmailTemplatePreviewerStyle();
  const [t] = useTranslation();

  React.useEffect(() => {
    window.top?.postMessage({ name: 'emailPreviewerLoaded' }, '*');

    const reloadPreview = (event: MessageEvent<PreviewDataMessageType>) => {
      if (event.data.name === 'previewReload') {
        const val = event.data.value;
        request<EmailTemplateResponseType>(
          'POST',
          buildUrl('/api/mail/preview', { case_id: val.caseId }),
          {
            attachments: val.attachments,
            body: val.body,
            case_id: val.caseId,
            recipient_type: val.recipient_type,
            to: val.to,
          }
        ).then(response => {
          const to = ([] as string[])
            .concat(response.to.split(';'), response.cc)
            .filter(Boolean)
            .join(', ');

          const template = encodeURIComponent(val.template);
          // replace line breaks
          const parsedContent = encodeURIComponent(response.body).replace(
            /%0A/g,
            '<br />'
          );

          const parsedHtml = template
            // {{message}}
            .replace('%7B%7Bmessage%7D%7D', parsedContent)
            // {{image_url}}
            .replace(
              '%7B%7Bimage_url%7D%7D',
              `/api/v1/file/${val.imageUuid}/download?inline=1`
            )
            // <script>
            .replace('%3Cscript%3E', '')
            .replace(
              /%3Cstyle/g,
              //@ts-ignore
              `<style nonce='${nonce}'`
            );

          setEmailPreviewData({
            to,
            attachments: val.attachments,
            from: response.from,
            bcc: response.bcc,
            subject: response.subject,
          });

          setTimeout(() => {
            const shadowEl = document.querySelector('#shadow');
            try {
              (shadowEl as any).attachShadow({ mode: 'open' });
            } catch (err: any) {
              console.log(err);
            }

            const bodyString = decodeURIComponent(
              val.template ? parsedHtml : parsedContent
            );

            (shadowEl as any).shadowRoot.innerHTML =
              bodyString + '' === 'undefined' ? '-' : bodyString;

            setTimeout(() => {
              window.top?.postMessage({ name: 'resizeEmailPreviewer' }, '*');
              const img = (shadowEl as any).shadowRoot.querySelector('img');

              img &&
                img.addEventListener('load', () =>
                  window.top?.postMessage({ name: 'resizeEmailPreviewer' }, '*')
                );
            }, 0);
          }, 0);
        });
      }
    };

    window.addEventListener('message', reloadPreview);
    () => window.removeEventListener('message', reloadPreview);
  }, []);

  return (
    <I18nResourceBundle
      resource={emailTemplatePreviewerLocale}
      namespace="EmailPreview"
    >
      {emailPreviewData ? (
        <div className={classes.container}>
          <div className={classes.row}>
            <span>{t('EmailPreview:from')}: </span>
            {emailPreviewData.from || '-'}
          </div>
          <div className={classes.row}>
            <span>{t('EmailPreview:to')}: </span>
            {emailPreviewData.to || '-'}
          </div>
          <div className={classes.row}>
            <span>{t('EmailPreview:bcc')}: </span>
            {emailPreviewData.bcc || '-'}
          </div>
          <div className={classes.row}>{emailPreviewData.subject || '-'}</div>
          <div className={classes.shadowEl} id="shadow" />
          {emailPreviewData.attachments?.length ? (
            <div className={classes.row}>
              {emailPreviewData.attachments.map(attachment => {
                return (
                  <div key={attachment} className={classes.attachment}>
                    <Icon size="small">{iconNames.attachment}</Icon>
                    <span>{attachment}</span>
                  </div>
                );
              })}
            </div>
          ) : null}
        </div>
      ) : (
        <Loader />
      )}
    </I18nResourceBundle>
  );
};

export default EmailTemplatePreviewer;

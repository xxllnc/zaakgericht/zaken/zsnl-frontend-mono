// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const usePanelLayoutStyles = makeStyles(
  ({ mintlab: { greyscale }, breakpoints }: Theme) => ({
    wrapper: {
      display: 'flex',
      flex: 1,
      height: '100%',
    },
    panel: {
      borderRight: `1px solid ${greyscale.dark}`,
      overflow: 'auto',
    },
    fluid: {
      flex: 1,
    },
    side: {
      width: '85px',
      [breakpoints.up('md')]: {
        width: '244px',
      },
    },
  })
);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import Alert from '@zaaksysteem/common/src/components/dialogs/Alert/Alert';
import { sessionSelector } from './../store/session/session.selectors';

type DetermineAuthorizationType = (
  systemRoles?: string[],
  capabilities?: string[]
) => boolean | undefined;

type UseUnauthorizedBannerType = (
  determineAuthorization: DetermineAuthorizationType
) => JSX.Element | null;

export const useUnauthorizedBanner: UseUnauthorizedBannerType =
  determineAuthorization => {
    const [t] = useTranslation('common');
    const session = useSelector(sessionSelector);
    const loggedInUser = session?.logged_in_user;
    const capabilities = loggedInUser?.capabilities;
    const systemRoles = loggedInUser?.system_roles;

    const onClose = () => {
      window.location.href = '/';
    };

    if (!determineAuthorization(systemRoles, capabilities)) {
      return (
        <Alert
          open={true}
          onClose={onClose}
          title={t('noAccess')}
          primaryButton={{ text: t('dialog.ok'), action: onClose }}
        >
          {t('serverErrors.status.401')}
        </Alert>
      );
    } else {
      return null;
    }
  };

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { IModule } from 'redux-dynamic-modules';
import { dashboardReducer, DashboardRootStateType } from './dashboard.reducer';

export function getDashboardModule(): IModule<DashboardRootStateType> {
  return {
    id: 'dashboard',
    reducerMap: {
      dashboard: dashboardReducer as any,
    },
  };
}

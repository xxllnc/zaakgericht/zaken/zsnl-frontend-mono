// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    title: 'Widget',
    widget: {
      add: 'Widget toevoegen',
      config: 'Widget configureren',
      cancel: 'Annuleren',
      favoriteCasetype: 'Favoriete zaaktypen',
      fields: {
        searchTypeCase: 'Zaken',
        searchTypeObject: 'Objecten',
        chooseSearchType: {
          label: 'Kies een zoekopdracht type',
        },
        savedSearchSelect: {
          label: 'Kies een zoekopdracht',
        },
        title: {
          label: 'Titel',
        },
        url: {
          label: 'URL',
        },
      },
    },
  },
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const locale = {
  nl: {
    titleView: 'Externe URL',
    titleEdit: 'Configureer de widget',
    form: {
      title: 'Titel',
      url: 'URL',
      save: 'Opslaan',
    },
  },
};

export default locale;

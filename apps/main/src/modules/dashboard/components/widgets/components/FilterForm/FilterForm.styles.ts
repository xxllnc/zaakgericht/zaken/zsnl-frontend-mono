// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useFilterFormStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      padding: '5px 5% 10px 5%',
      display: 'flex',
      flexDirection: 'column',
    },
    row: {
      marginBottom: '15px',
      display: 'flex',
      alignItems: 'center',

      '&>div:nth-child(1)': {
        width: '158px',
      },
      '&>div:nth-child(2)': {
        flex: 1,
        borderBottom: `1px solid ${greyscale.darker}`,
      },
    },
    fields: {
      marginTop: 12,
    },
    bottom: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: 20,
      '&>*:nth-child(1)': {
        marginRight: 16,
      },
    },
  })
);

export type ClassesType = ReturnType<typeof useFilterFormStyles>;

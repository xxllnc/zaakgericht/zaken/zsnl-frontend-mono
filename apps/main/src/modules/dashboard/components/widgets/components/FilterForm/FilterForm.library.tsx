// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { Theme } from '@mintlab/ui/types/Theme';
import { renderTagsWithIcon } from '@mintlab/ui/App/Zaaksysteem/Select';
import {
  FilterType,
  TasksFormDefinitionType,
  FormValuesType,
  Types,
} from '../../Tasks/types/Tasks.types';

const noValue = (filters: FilterType[]) => !filters || !filters.length;

export const getMultiValueValue = ({
  filters,
  type,
}: {
  filters: null | FilterType[];
  type: Types | 'employee';
}) => {
  if (!filters || noValue(filters)) return null;
  const results = filters
    .map(filter => (filter.type === type ? filter : null))
    .filter(Boolean);
  return results && results.length ? results : null;
};

export const getFormDefinition = ({
  filters,
  t,
}: {
  filters: null | FilterType[];
  t: i18next.TFunction;
  theme: Theme;
}): FormDefinition<TasksFormDefinitionType> => {
  return [
    {
      name: 'assignee',
      label: t('fields.assignee.label'),
      type: fieldTypes.CONTACT_FINDER,
      value: getMultiValueValue({ filters, type: 'employee' }),
      multiValue: true,
      placeholder: t('fields.assignee.choose'),
      config: {
        subjectTypes: ['employee'],
      },
    },
    {
      name: 'case_number',
      label: t('fields.case_nr.label'),
      type: fieldTypes.MULTI_VALUE_TEXT,
      value: getMultiValueValue({ filters, type: 'case_number' }),
      multiValue: true,
      placeholder: t('fields.case_nr.choose'),
      config: {
        createType: 'case_number',
        renderTags: renderTagsWithIcon('hash'),
      },
    },
    {
      name: 'keyword',
      label: t('fields.keyword.label'),
      type: fieldTypes.MULTI_VALUE_TEXT,
      value: getMultiValueValue({ filters, type: 'keyword' }),
      multiValue: true,
      placeholder: t('fields.keyword.choose'),
      config: {
        createType: 'keyword',
        renderTags: renderTagsWithIcon('spellcheck'),
      },
    },
    {
      name: 'department',
      label: t('fields.department.label'),
      type: fieldTypes.DEPARTMENT_FINDER,
      value: getMultiValueValue({ filters, type: 'department' }),
      multiValue: true,
      placeholder: t('fields.department.choose'),
    },
    {
      name: 'case_type',
      label: t('fields.case_type.label'),
      value: getMultiValueValue({ filters, type: 'case_type' }),
      multiValue: true,
      type: fieldTypes.CASE_TYPE_FINDER,
    },
  ];
};

const hasValue = (value: FilterType[]) => {
  if (Array.isArray(value)) {
    return value && value.length;
  }

  return Boolean(value);
};

export const getReturnValues = (values: FormValuesType): FilterType[] => {
  return [
    ...(hasValue(values.assignee) ? values.assignee : []),
    ...(hasValue(values.department) ? values.department : []),
    ...(hasValue(values.case_type) ? values.case_type : []),
    ...(hasValue(values.case_number) ? values.case_number : []),
    ...(hasValue(values.keyword) ? values.keyword : []),
  ];
};

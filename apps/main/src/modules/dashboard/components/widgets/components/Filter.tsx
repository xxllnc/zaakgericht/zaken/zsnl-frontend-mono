// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { FormikProps } from 'formik';
import { useTheme } from '@mui/styles';
// import Select from '@mintlab/ui/App/Zaaksysteem/Select/Form/CreatableSelect';
import Select, { renderTagsWithIcon } from '@mintlab/ui/App/Zaaksysteem/Select';
// import { Chip, useTheme } from '@mui/material';
import { Theme } from '@mintlab/ui/types/Theme';
import Button from '@mintlab/ui/App/Material/Button';
import { FilterType } from '../Tasks/types/Tasks.types';
import { useWidgetFilterStyles, useWidgetHeaderStyles } from '../Widget.style';
import WidgetHeader from './WidgetHeader';
import FilterForm from './FilterForm/FilterForm';

type FilterPropsType = {
  title: string;
  filters: null | FilterType[];
  setFilters: (values: FilterType[]) => void;
};

const Filter = ({ title, filters, setFilters }: FilterPropsType) => {
  const [filterForm, setFilterForm] = useState(false);
  const [formik, setFormik] = useState(null as any);
  const [t] = useTranslation('widget');
  const theme = useTheme<Theme>();
  const classes = useWidgetFilterStyles();
  const headerClasses = useWidgetHeaderStyles();

  const filterHeaderOptions = (
    <>
      <div className={headerClasses.flexibleSpace} />
      <Button
        className={classes.deleteLink}
        name="removeWidgetFilters"
        action={() => {
          if (!formik) return;
          formik.setValues({
            assignee: [],
            case_number: [],
            keyword: [],
            department: [],
            case_type: [],
          });
          setFilters([]);
        }}
      >
        {t('removeFilters')}
      </Button>
    </>
  );

  return (
    <>
      <Select
        isMulti
        freeSolo
        isClearable={false}
        openMenuOnClick={false}
        placeholder=""
        value={filters}
        onChange={({ target: { value } }: any) => {
          setFilters(value);
        }}
        sx={{
          overflow: 'auto',

          '& .MuiAutocomplete-inputRoot': {
            padding: '9px 8px 7px 8px',
          },
          '& .MuiInputBase-input': {
            display: 'none',
          },
          '& .MuiInputBase-root': {
            flexWrap: 'nowrap',
            cursor: 'default',
          },
          '& .MuiTextField-root': {
            backgroundColor: 'transparent',
            '&:hover': {
              backgroundColor: 'transparent',
            },
          },
        }}
        renderTags={renderTagsWithIcon(option =>
          option.type === 'employee'
            ? 'person'
            : option.type === 'department'
            ? 'people'
            : option.type === 'case_type'
            ? 'label'
            : option.type === 'keyword'
            ? 'spellcheck'
            : 'hash'
        )}
      />
      <Button
        name="openWidgetFilter"
        icon="add"
        iconSize="small"
        action={() => setFilterForm(true)}
        sx={{
          marginLeft: '11px',
          backgroundColor: theme.mintlab.greyscale.dark,
          borderRadius: 20,
          boxShadow: theme.mintlab.shadows.sharp,
          color: theme.mintlab.greyscale.darkest,
          marginRight: 'auto',
          padding: '0 10px',

          '&:hover': {
            backgroundColor: theme.mintlab.greyscale.darker,
          },
        }}
      >
        {t('filter')}
      </Button>
      {filterForm && (
        <div className={classes.overlay}>
          <WidgetHeader title={title}>{filterHeaderOptions}</WidgetHeader>

          <FilterForm
            filters={filters}
            onSetFilters={setFilters}
            setFilterForm={setFilterForm}
            onMount={(formik: FormikProps<any>) => setFormik(formik)}
          />
        </div>
      )}
    </>
  );
};

export default Filter;

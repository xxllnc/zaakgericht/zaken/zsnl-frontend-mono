// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
//@ts-ignore
import Pagination from '@mintlab/ui/App/Material/Pagination';
import ResultsView from '../../../../advancedSearch/components/Main/Results/ResultsView';
import WidgetHeader from '../components/WidgetHeader';
import Widget from '../Widget';
import './paginationFix.css';
import { KindType } from '../../../../advancedSearch/AdvancedSearch.types';

const AdvancedSearch = ({
  page_size,
  saved_search_id,
  title,
  kind,
  onClose,
  onParamsChange,
}: {
  page_size: number;
  title: string;
  saved_search_id: string;
  kind?: KindType;
  onParamsChange: (params: any) => {};
  onClose: () => {};
}) => {
  const [page, setPage] = useState(1);
  const [keyword, setKeyword] = useState('');
  const [savedSearchName, setName] = useState('');
  const [resultsPerPage, setResultsPerPage] = useState(page_size);
  const [totalResults, setTotalResults] = useState(0);

  return (
    <Widget
      header={
        <WidgetHeader
          title={
            totalResults
              ? `${title || savedSearchName} (${totalResults})`
              : title || savedSearchName
          }
          onClose={onClose}
          onSearch={setKeyword}
        />
      }
    >
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          width: '100%',
          height: '100%',
        }}
      >
        <div style={{ flex: 1, display: 'flex', height: '100%' }}>
          <ResultsView
            keyword={keyword}
            kind={kind || 'custom_object'}
            identifier={saved_search_id}
            view={'table'}
            page={page}
            resultsPerPage={resultsPerPage}
            setTotalResults={setTotalResults}
            setName={setName}
          />
        </div>
        <div style={{ height: 100 }}>
          <Pagination
            changeRowsPerPage={(event: any) => {
              setResultsPerPage(event?.target?.value);
              onParamsChange({ page_size: event?.target?.value });
            }}
            component={'div'}
            count={totalResults}
            getNewPage={(newPage: number) => setPage(newPage + 1)}
            labelDisplayedRows={({ count: newCount, from, to }: any) =>
              `${from}–${to} van ${newCount}`
            }
            labelRowsPerPage={'Per pagina:'}
            page={page !== 0 ? page - 1 : page}
            rowsPerPage={resultsPerPage}
            rowsPerPageOptions={[5, 10, 25, 50]}
          />
        </div>
      </div>
    </Widget>
  );
};

export default AdvancedSearch;

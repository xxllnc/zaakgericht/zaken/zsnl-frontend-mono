// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import IconButton from '@mui/material/IconButton';
import { ItemValueType } from '@zaaksysteem/common/src/components/form/fields/Options/Options.types';
import WidgetHeader from '../components/WidgetHeader';
import Filter from '../components/Filter';
import { useWidgetHeaderStyles } from '../Widget.style';
import ColumnsConfigDialog from './components/ColumnsConfigDialog';
import { FilterType } from './types/Tasks.types';

type TasksHeaderPropsType = {
  title: string;
  widgetUuid: string;
  onChange?: (value: string) => void;
  filters: null | FilterType[];
  setFilters: (filters: FilterType[]) => void;
  columns: ItemValueType[];
  setColumns: (columns: ItemValueType[]) => void;
  onClose?: () => {};
};

const TasksHeader: React.ComponentType<TasksHeaderPropsType> = ({
  title,
  widgetUuid,
  onChange,
  filters,
  setFilters,
  columns,
  setColumns,
  onClose,
}) => {
  const classes = useWidgetHeaderStyles();
  const [configOpen, setConfigOpen] = useState(false);

  const tasksHeaderOptions = (
    <>
      <Filter title={title} filters={filters} setFilters={setFilters} />
      <div className={classes.flexibleSpace} />
      <IconButton onClick={() => setConfigOpen(true)}>
        <Icon size="small" color="inherit">
          {iconNames.settings}
        </Icon>
      </IconButton>
      {configOpen ? (
        <ColumnsConfigDialog
          value={columns}
          onClose={() => setConfigOpen(false)}
          onSubmit={(values: any) => {
            setConfigOpen(false);
            setColumns(values['columns']);
          }}
        />
      ) : null}
    </>
  );

  return (
    <WidgetHeader
      title={title}
      widgetUuid={widgetUuid}
      onClose={onClose}
      onSearch={onChange}
    >
      {tasksHeaderOptions}
    </WidgetHeader>
  );
};

export default TasksHeader;

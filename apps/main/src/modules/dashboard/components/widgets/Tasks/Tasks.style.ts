// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useTasksStyles = makeStyles(({ palette: { primary } }: Theme) => ({
  daysLeft: {
    backgroundColor: '#a5c620',
    padding: '0 4px',
    fontWeight: 500,
    color: '#fff',
    borderRadius: 8,
    minWidth: 26,
    lineHeight: '18px',
    display: 'inline-block',
    textAlign: 'center',
    fontSize: '.85em',
    boxSizing: 'border-box',
  },
  hasPassed: {
    backgroundColor: '#d8503f',
  },
  link: {
    justifyContent: 'left',
    padding: 0,
    margin: 0,
    color: primary.main,
    textDecoration: 'underline',
    '&:focus, &:hover, &:active': {
      textDecoration: 'underline',
      backgroundColor: 'transparent',
    },
  },
  tableCellHeader: {
    padding: '20px 0 20px 20px',
    borderBottom: 'none',
  },
  tableWrapper: {
    height: 'calc(100% - 60px)',
    flex: '1 1 auto',
  },
}));

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FC } from 'react';
import { useTranslation } from 'react-i18next';
import { useTheme } from '@mui/material/styles';
import {
  Button,
  useMediaQuery,
  Fab,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import {
  availableWidgets,
  Widget,
  widgetTypes,
} from '../widgets/Widget.library';

interface AddWidgetProps {
  addWidget: (widgetType: keyof typeof widgetTypes, params: any) => void;
}

const AddWidgetDialog: FC<AddWidgetProps> = ({ addWidget }) => {
  const [t] = useTranslation();
  const [open, setOpen] = React.useState(false);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [activeWidget, setActiveWidget] = React.useState<Widget>();

  const handleItemClick = (widget: Widget) => {
    switch (widget.action) {
      case 'add':
        addWidget(widget.type, {});
        setOpen(false);
        break;
      case 'config':
        setActiveWidget(widget);
        break;
    }
  };

  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'right',
        margin: theme.spacing(1),
      }}
    >
      <Fab
        color="primary"
        sx={{
          margin: 0,
          top: 'auto',
          right: 20,
          bottom: 20,
          left: 'auto',
          position: 'fixed',
          zIndex: 9,
        }}
        aria-label={t('dashboard:widget.add')}
        onClick={() => {
          setOpen(true);
        }}
      >
        <AddIcon />
      </Fab>
      {activeWidget ? (
        <FormDialog
          minHeight={activeWidget.minHeight}
          formDefinitionT={t}
          formDefinition={activeWidget.form?.formDefinitions}
          rules={activeWidget.form?.rules}
          onClose={() => setActiveWidget(undefined)}
          title={t('dashboard:widget.config')}
          onSubmit={(values: any) => {
            addWidget(activeWidget.type, values);
            setActiveWidget(undefined);
            setOpen(false);
          }}
        />
      ) : (
        <Dialog
          fullScreen={fullScreen}
          open={open}
          onClose={() => setOpen(false)}
          aria-labelledby="widget-dialog-title"
        >
          <DialogTitle id="widget-dialog-title">
            {t('dashboard:widget.add')}
          </DialogTitle>
          <DialogContent>
            <List component="nav" aria-label="widget lijst">
              {availableWidgets.map(widget => (
                <ListItem
                  button
                  key={widget.name}
                  onClick={() => handleItemClick(widget)}
                >
                  <ListItemIcon>{widget.icon}</ListItemIcon>
                  <ListItemText primary={widget.name} />
                </ListItem>
              ))}
            </List>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => setOpen(false)} color="primary">
              {t('dashboard:widget.cancel')}
            </Button>
          </DialogActions>
        </Dialog>
      )}
    </div>
  );
};

export default AddWidgetDialog;

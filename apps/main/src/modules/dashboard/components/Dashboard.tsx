// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
/* eslint-disable id-length */

import React, { FC, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import GridLayout from 'react-grid-layout';
import { useStyles } from './Dashboard.style';
import AddWidgetDialog from './dialogs/AddWidgetDialog';
import { widgetTypes } from './widgets/Widget.library';
import {
  fetchDashboard,
  fitToLayout,
  updateDashboardWidgets,
} from './Dashboard.library';
import { DashboardState, Widget } from './Dashboard.types';
import './rgl.css';
import { defaultTasksWidgetData } from './widgets/Tasks/Tasks.library';
import { sessionSelector } from './../../../store/session/session.selectors';

const GridComponent = GridLayout.WidthProvider(GridLayout);

const Dashboard: FC = () => {
  const classes = useStyles();
  const session = useSelector(sessionSelector);
  const [fetchingDashboardState, setFetchingDashboardState] = useState(true);
  const [{ widgets, layout, components }, setDashboardState] =
    React.useState<DashboardState>({
      widgets: [] as any,
      layout: [],
      components: [],
    });

  useEffect(() => {
    fetchDashboard()
      .then(setDashboardState)
      .then(() => setFetchingDashboardState(false));

    document.body.classList.add('dashboardBg');

    return () => {
      document.body.classList.remove('dashboardBg');
    };
  }, []);

  const removeWidget = (rIx: number) => {
    const updatedWidgets = widgets.filter((_, ix) => ix !== rIx);
    setDashboardState({
      components: components.filter((_, ix) => ix !== rIx),
      layout: layout.filter((_, ix) => ix !== rIx),
      widgets: updatedWidgets,
    });
  };

  const updateLayout = (layout: GridLayout.Layout[]) => {
    const updatedWidgetsAndLayout = widgets.map((widget, ix) => {
      const { minW, minH } = widgetTypes[widget.parameters.type].gridDefaults;
      const height =
        layout[ix].h < minH ? widget.dimensions.height : layout[ix].h;
      const width =
        layout[ix].w < minW ? widget.dimensions.width : layout[ix].w;

      return [
        {
          ...widget,
          dimensions: {
            height,
            width,
            x: layout[ix].x,
            y: layout[ix].y,
          },
        },
        { ...layout[ix], h: height, w: width },
      ] as [Widget, GridLayout.Layout];
    });

    const updatedWidgets = updatedWidgetsAndLayout.map(xs => xs[0]);
    const updatedLayout = updatedWidgetsAndLayout.map(xs => xs[1]);
    setDashboardState({
      widgets: updatedWidgets,
      components,
      layout: updatedLayout,
    });
    updateDashboardWidgets(session?.logged_in_user.uuid || '', updatedWidgets);
  };

  const updateParams = (tIx: number, params: any) => {
    const updatedWidgets = widgets.map((widget, ix) =>
      tIx === ix
        ? { ...widget, parameters: { ...widget.parameters, ...params } }
        : widget
    );
    setDashboardState({ widgets: updatedWidgets, components, layout });
    updateDashboardWidgets(session?.logged_in_user.uuid || '', updatedWidgets);
  };

  const addWidget = (type: any, params: any) => {
    //@ts-ignore
    const { component, gridDefaults } = widgetTypes[type];
    const item = fitToLayout(layout, 12, {
      i: components.length.toString(),
      ...gridDefaults,
    });
    const parameters = {
      type,
      ...params,
      ...(type === 'saved_search'
        ? {
            saved_search_id: params.savedSearchSelectObject.value,
            title: params.savedSearchSelectObject.label,
            kind: 'custom_object',
            page_size: 10,
          }
        : type === 'tasks'
        ? defaultTasksWidgetData
        : {}),
    };
    const widget = {
      dimensions: {
        height: item.h,
        width: item.w,
        x: item.x,
        y: item.y,
      },
      parameters,
    };

    setDashboardState({
      components: [...components, { component, params: parameters }],
      widgets: [...widgets, widget],
      layout: [...layout, item],
    });
  };

  if (!session || fetchingDashboardState) {
    return <Loader />;
  }

  return (
    <div className={classes.wrapper}>
      <div className={classes.content}>
        <AddWidgetDialog addWidget={addWidget} />
        <GridComponent
          //@ts-ignore
          onLayoutChange={updateLayout}
          isResizable={true}
          isDraggable={true}
          layout={layout}
          margin={[20, 20]}
          measureBeforeMount={true}
          draggableHandle={'.widget-draggable-handle'}
          cols={12}
        >
          {components.map((comp, ix) => (
            <div className="grid-item" key={ix}>
              <comp.component
                {...comp.params}
                width={widgets[ix].dimensions.width}
                height={widgets[ix].dimensions.height}
                onClose={() => removeWidget(ix)}
                onParamsChange={(params: any) => updateParams(ix, params)}
              />
            </div>
          ))}
        </GridComponent>
      </div>
    </div>
  );
};

export default Dashboard;

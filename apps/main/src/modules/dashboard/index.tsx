// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/* eslint-disable id-length */
import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import Dashboard from './components/Dashboard';
import locale from './Dashboard.locale';

export default function DashboardModule() {
  return (
    <I18nResourceBundle resource={locale} namespace="dashboard">
      <Dashboard />
    </I18nResourceBundle>
  );
}

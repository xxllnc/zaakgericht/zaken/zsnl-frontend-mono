// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { Route, Routes, Navigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import Snackbar from '@mintlab/ui/App/Material/Snackbar/Snackbar.cmp';
import { SessionType } from '@zaaksysteem/common/src/store/session/session.reducer';
import TopBar from '../../components/TopBar/TopBar';
import NotificationBar from '../../components/NotificationBar/NotificationBar';
import Relations from './views/relations';
import Timeline from './views/timeline';
import Communication from './views/communication';
import Phases from './views/phases';
import SideMenu from './sideMenu/SideMenu';
import { CaseObjType, CaseTypeType, DialogsType, JobType } from './Case.types';
import { useCaseStyles } from './Case.style';
import {
  dialogIsCaseAction,
  formatBreadcrumbLabel,
  getNotifications,
  getUnfinishedJobs,
} from './Case.library';
import AboutDialog from './sideMenu/info/AboutDialog';
import Documents from './views/documents';
import ConfidentialityDialog from './sideMenu/info/ConfidentialityDialog';
import CaseActionsDialog from './sideMenu/caseActions/CaseActionsDialog';
import CaseActionDialog from './sideMenu/caseActions/CaseActionDialog';

export interface CasePropsType {
  session: SessionType;
  caseObj: CaseObjType;
  refreshCaseObj: () => void;
  caseType: CaseTypeType;
  jobs: JobType[];
}

/* eslint complexity: [2, 9] */
const Case: React.ComponentType<CasePropsType> = ({
  session,
  caseObj,
  refreshCaseObj,
  caseType,
  jobs,
}) => {
  const classes = useCaseStyles();
  const [t] = useTranslation('case');
  const [dialog, setDialog] = useState<DialogsType>();
  const [snackOpen, setSnackOpen] = useState(false);

  const closeDialog = () => setDialog(undefined);

  const notifications = getNotifications(
    t,
    session,
    caseObj,
    refreshCaseObj,
    jobs,
    setSnackOpen
  );

  return (
    <div className={classes.wrapper}>
      <Snackbar
        handleClose={(event: React.ChangeEvent, reason: string) => {
          if (reason === 'clickaway') return;
          setSnackOpen(false);
        }}
        message={`${t('snack.jobs')}: (${getUnfinishedJobs(jobs).length}/${
          jobs.length
        })`}
        open={snackOpen}
      />
      <TopBar
        title={formatBreadcrumbLabel(t, caseObj, caseType)}
        subTitle={caseObj.summary}
      />
      <NotificationBar notifications={notifications} />
      <div className={classes.content}>
        <SideMenu
          caseObj={caseObj}
          setDialog={setDialog}
          refreshCaseObj={refreshCaseObj}
        />
        <div className={classes.view}>
          <Routes>
            <Route
              path=""
              element={<Navigate to={'phases'} replace={true} />}
            />
            <Route
              path={'phases/*'}
              element={<Phases caseObj={caseObj} caseType={caseType} />}
            />
            <Route
              path={`documents`}
              element={<Documents caseId={caseObj.number} />}
            />
            <Route
              path={`communication/*`}
              element={<Communication caseObj={caseObj} caseType={caseType} />}
            />
            <Route path={`timeline`} element={<Timeline caseObj={caseObj} />} />
            <Route
              path={`relations`}
              element={
                <Relations
                  session={session}
                  caseObj={caseObj}
                  caseType={caseType}
                />
              }
            />
          </Routes>
        </div>
      </div>
      <AboutDialog
        caseObj={caseObj}
        caseType={caseType}
        onClose={closeDialog}
        open={dialog === 'about'}
      />
      <ConfidentialityDialog
        caseObj={caseObj}
        onClose={closeDialog}
        open={dialog === 'confidentiality'}
        refreshCaseObj={refreshCaseObj}
      />
      <CaseActionsDialog
        caseObj={caseObj}
        onClose={closeDialog}
        open={dialog === 'caseActions'}
        setDialog={setDialog}
      />
      {dialog && dialogIsCaseAction(dialog) && (
        <CaseActionDialog
          caseObj={caseObj}
          caseType={caseType}
          session={session}
          onClose={closeDialog}
          caseAction={dialog}
          refreshCaseObj={refreshCaseObj}
        />
      )}
    </div>
  );
};

export default Case;

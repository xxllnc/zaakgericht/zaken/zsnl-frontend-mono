// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useCaseActionButtonStyles = makeStyles(
  ({ mintlab: { shadows }, palette: { common, primary } }: Theme) => ({
    wrapper: {
      padding: 8,
      height: 55,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    caseActions: {
      width: '100%',
    },
    caseActionsIcon: {
      borderRadius: 8,
      backgroundColor: primary.main,
      color: common.white,
      '&:hover': {
        backgroundColor: primary.dark,
      },
      boxShadow: shadows.sharp,
    },
  })
);

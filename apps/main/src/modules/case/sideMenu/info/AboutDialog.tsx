// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { capitalize } from '@mintlab/kitchen-sink/source';
import DialogContent from '@mui/material/DialogContent';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import { CaseObjType, CaseTypeType } from '../../Case.types';
import { useAboutDialogStyles } from './AboutDialog.styles';
import { formatDate } from './../../Case.library';
import { formatLeadTime } from './Info.library';

type AboutDialogPropsType = {
  caseObj: CaseObjType;
  caseType: CaseTypeType;
  open: boolean;
  onClose: () => void;
};

const AboutDialog: React.ComponentType<AboutDialogPropsType> = ({
  caseObj,
  caseType,
  open,
  onClose,
}) => {
  const [t] = useTranslation('case');
  const classes = useAboutDialogStyles();
  const {
    number,
    summary,
    contactChannel,
    department,
    role,
    requestor,
    recipient,
    assignee,
    coordinator,
    registrationDate,
    targetDate,
    completionDate,
    destructionDate,
    payment,
  } = caseObj;
  const {
    name,
    metaData: {
      legalBasis,
      localBasis,
      designationOfConfidentiality,
      responsibleRelationship,
      processDescription,
    },
    leadTimeLegal,
    leadTimeService,
  } = caseType;

  const items = [
    { id: 'caseNumber', value: number },
    { id: 'caseType', value: name },
    { id: 'summary', value: summary },
    { id: 'contactChannel', value: capitalize(contactChannel) },
    {
      id: 'departmentAndRole',
      value: `${department?.name || '-'} / ${role?.name || '-'}`,
    },
    { id: 'requestor', value: requestor?.name },
    ...(recipient ? [{ id: 'recipient', value: recipient.name }] : []),
    { id: 'assignee', value: assignee?.name },
    { id: 'coordinator', value: coordinator?.name },
    { id: 'registrationDate', value: formatDate(registrationDate) },
    { id: 'targetDate', value: formatDate(targetDate) },
    { id: 'completionDate', value: formatDate(completionDate) },
    { id: 'leadTimeLegal', value: formatLeadTime(leadTimeLegal) },
    { id: 'leadTimeService', value: formatLeadTime(leadTimeService) },
    { id: 'archiveClassificationCode', value: '' },
    { id: 'destructionDate', value: formatDate(destructionDate) },
    {
      id: 'paymentStatus',
      value: payment?.status ? t(`paymentStatus.${payment.status}`) : '',
    },
    { id: 'paymentAmount', value: payment?.amount },
    { id: 'legalBasis', value: legalBasis },
    { id: 'localBasis', value: localBasis },
    { id: 'designationOfconfidentiality', value: designationOfConfidentiality },
    { id: 'responsibleRelationship', value: responsibleRelationship },
    { id: 'processDescription', value: processDescription },
    // { id: 'requestorSnapshot', value: '-' }, not supported yet by v2 or v1
  ];

  return (
    <Dialog
      disableBackdropClick={true}
      open={open}
      onClose={onClose}
      scope="case-about-dialog"
    >
      <DialogTitle
        elevated={true}
        icon="info"
        title={t('info.about')}
        onCloseClick={onClose}
      />

      <DialogContent>
        <div className={classes.wrapper}>
          {items.map(item => (
            <div key={item.id} className={classes.itemWrapper}>
              <div className={classes.label}>{t(`about.${item.id}`)}</div>
              <div className={classes.value}>{item.value || '-'}</div>
            </div>
          ))}
        </div>
      </DialogContent>
    </Dialog>
  );
};

export default AboutDialog;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { CaseObjType } from '../../Case.types';
import Timeline from './Timeline';
import locale from './Timeline.locale';

export type TimelineViewPropsType = {
  caseObj: CaseObjType;
};

const TimelineView: React.ComponentType<TimelineViewPropsType> = props => (
  <I18nResourceBundle resource={locale} namespace="caseTimeline">
    <Timeline {...props} />
  </I18nResourceBundle>
);

export default TimelineView;

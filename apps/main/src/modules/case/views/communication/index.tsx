// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import CommunicationModule from '@zaaksysteem/communication-module/src';
import { CaseObjType, CaseTypeType } from '../../Case.types';

export type CommunicationPropsType = {
  caseObj: CaseObjType;
  caseType: CaseTypeType;
};

const Communication: React.ComponentType<CommunicationPropsType> = ({
  caseObj,
  caseType,
}) => {
  const canEdit = caseObj.canEdit;
  const canCreatePipMessage =
    caseObj.requestor?.type !== 'employee' &&
    caseType.settings.disable_pip_for_requestor !== true;

  return (
    <CommunicationModule
      capabilities={{
        allowSplitScreen: true,
        canAddAttachmentToCase: canEdit,
        canAddSourceFileToCase: canEdit,
        canAddThreadToCase: false,
        canCreateContactMoment: true,
        canCreatePipMessage: canEdit && canCreatePipMessage,
        canCreateEmail: canEdit,
        canCreateNote: true,
        canCreateMijnOverheid: false,
        canDeleteMessage: canEdit,
        canImportMessage: canEdit,
        canSelectCase: false,
        canSelectContact: true,
        canFilter: true,
        canOpenPDFPreview: true,
      }}
      context="case"
      caseUuid={caseObj.uuid}
      contactUuid={caseObj.requestor?.uuid}
      contactName={caseObj.requestor?.name}
      htmlEmailTemplateName={caseObj.htmlEmailTemplateName}
    />
  );
};

export default Communication;

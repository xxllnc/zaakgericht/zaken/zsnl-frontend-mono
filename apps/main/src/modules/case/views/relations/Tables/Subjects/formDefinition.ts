// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  Rule,
  hideFields,
  showFields,
  hasValue,
  updateFields,
} from '@zaaksysteem/common/src/components/form/rules';
import { CaseObjType } from '../../../../Case.types';
import { SubjectType } from '../../Relations.types';
import { generateMagicString } from './library';

/* eslint complexity: [2, 7] */
export const getFormDefinition = (
  t: i18next.TFunction,
  caseObj: CaseObjType,
  subject?: SubjectType
) => {
  const permissionChoices = ['none', 'search', 'read', 'write'].map(option => ({
    value: option,
    label: t(`subjects.dialog.values.permission.${option}`),
  }));
  const roleChoices = caseObj.caseRoles;
  const currentSubject = {
    label: subject?.subject.name,
    value: subject?.subject.uuid,
    key: subject?.subject.uuid,
    type: subject?.subject.type,
  };

  return [
    {
      name: 'subject',
      type: fieldTypes.CONTACT_FINDER_WITH_CONTACT_IMPORTER,
      value: subject ? currentSubject : null,
      disabled: false,
      required: true,
      label: t('subjects.dialog.labels.subject'),
      placeholder: t('subjects.dialog.placeholders.subject'),
    },
    {
      name: 'role',
      type: fieldTypes.SELECT,
      value: subject ? { label: subject.role, value: subject.role } : null,
      required: true,
      label: t('subjects.dialog.labels.role'),
      choices: roleChoices,
    },
    {
      name: 'magic_string_prefix',
      type: fieldTypes.TEXT,
      value: subject?.magic_string_prefix || '',
      disabled: true,
      label: t('subjects.dialog.labels.magic_string_prefix'),
    },
    {
      name: 'authorized',
      type: fieldTypes.CHECKBOX,
      value: subject?.authorized || false,
      label: t('subjects.dialog.labels.authorized'),
      suppressLabel: true,
    },
    ...(subject
      ? []
      : [
          {
            name: 'send_confirmation_email',
            type: fieldTypes.CHECKBOX,
            value: false,
            label: t('subjects.dialog.labels.send_confirmation_email'),
            suppressLabel: true,
          },
        ]),
    {
      name: 'permission',
      type: fieldTypes.SELECT,
      value: subject?.permission || 'none',
      label: t('subjects.dialog.labels.permission'),
      choices: permissionChoices,
    },
  ];
};

export const getRules = (subjects: SubjectType[], subject?: SubjectType) => [
  new Rule()
    .when('role', field => hasValue(field))
    .then(fields => {
      const magicString = generateMagicString(fields, subjects, subject);

      return fields.map(
        updateFields(['magic_string_prefix'], { value: magicString })
      );
    }),
  new Rule()
    .when('subject', (field: any) => field.value?.type === 'employee')
    .then(showFields(['permission']))
    .else(hideFields(['permission'])),
  new Rule()
    .when('subject', (field: any) =>
      ['person', 'organization'].includes(field.value?.type)
    )
    .then(showFields(['authorized', 'send_confirmation_email']))
    .else(hideFields(['authorized', 'send_confirmation_email'])),
];

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { SubjectType } from '../../Relations.types';
import {
  fetchSubjects,
  relateSubject,
  editSubject,
  unrelateSubject,
} from '../requests';

const systemRoles = ['Aanvrager', 'Behandelaar', 'Coordinator'];
export const isSystemRole = (role: string) => systemRoles.includes(role);

const formatMagicString = (magicString: any) =>
  magicString ? magicString.toLowerCase().replace(/\s/g, '') : undefined;

type GetSubjectsType = (caseUuid: string) => Promise<SubjectType[]>;

export const getSubjects: GetSubjectsType = async caseUuid => {
  const response = await fetchSubjects(caseUuid);
  const subjects = response.data.map(
    ({
      id: uuid,
      attributes,
      relationships: {
        subject: {
          data: {
            id,
            meta: { display_name },
            type,
          },
        },
      },
    }) => ({
      // id and type will always be defined
      uuid,
      name: uuid,
      subject: {
        uuid: id || '',
        name: display_name,
        type: type || '',
      },
      ...attributes,
      magic_string_prefix: formatMagicString(attributes.magic_string_prefix),
    })
  );

  return subjects;
};

type GetTakenMagicStrings = (subjects: SubjectType[]) => string[];

export const getTakenMagicStrings: GetTakenMagicStrings = subjects =>
  subjects.map(subject => subject.magic_string_prefix);

type RelateSubjectActionType = (
  caseUuid: string,
  values: any
) => Promise<boolean>;

export const relateSubjectAction: RelateSubjectActionType = async (
  caseUuid,
  values
) => {
  const { subject, role, permission } = values;
  const body = {
    case_uuid: caseUuid,
    ...values,
    subject: {
      id: subject.value,
      type: subject.type,
    },
    permission: permission.value,
    role: role.value,
  };

  const result = await relateSubject(caseUuid, body);

  return Boolean(result.data?.success);
};

type EditSubjectActionType = (
  caseUuid: string,
  relation_uuid: string,
  values: any
) => Promise<boolean>;

export const editSubjectAction: EditSubjectActionType = async (
  caseUuid,
  relation_uuid,
  values
) => {
  const { permission, role, magic_string_prefix, authorized } = values;
  const body = {
    magic_string_prefix,
    role: role.value,
    permission: permission.value,
    relation_uuid,
    authorized,
  };

  const result = await editSubject(caseUuid, body);

  return Boolean(result.data?.success);
};

type UnrelateSubjectActionType = (
  caseUuid: string,
  relation_uuid: string
) => Promise<boolean>;

export const unrelateSubjectAction: UnrelateSubjectActionType = async (
  caseUuid,
  relation_uuid
) => {
  const body = {
    relation_uuid,
  };

  const result = await unrelateSubject(caseUuid, body);

  return Boolean(result.data?.success);
};

type GenerateMagicString = (
  fields: AnyFormDefinitionField[],
  subjects: SubjectType[],
  subject?: SubjectType
) => string;

export const generateMagicString: GenerateMagicString = (
  fields,
  subjects,
  subject
) => {
  const takenMagicStrings = getTakenMagicStrings(subjects);
  const currentRole = subject?.role;
  const currentMagicString = subject?.magic_string_prefix;

  const roleField = fields.find(field => field.name === 'role');
  //@ts-ignore
  const roleValue = roleField?.value.value;
  const magicString = formatMagicString(roleValue);

  // if you're not changing the role, keep the magicString you had
  if (roleValue === currentRole) {
    return currentMagicString;
  }

  // if the base magicString is not taken, take it
  if (!takenMagicStrings.includes(magicString)) {
    return magicString;
  }

  // if it's already taken, iterate upward to the first that's not taken
  //   i've chosen this classic approach, because it matches well with the scenario where
  //   assignee_1 was removed, while assignee and assignee_2 still exist
  let iterator = 1;
  let generatedMagicString = magicString;

  do {
    generatedMagicString = `${magicString}${iterator}`;
    iterator += 1;
  } while (takenMagicStrings.includes(generatedMagicString));

  return generatedMagicString;
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { FormFieldPropsType } from '@zaaksysteem/common/src/components/form/types';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { FormFieldComponentType } from '@zaaksysteem/common/src/components/form/types/Form2.types';
import { useObjectV1ChoicesQuery } from '../requests';

export const OBJECT_FINDER = 'objectFinder';

export interface ObjectFinderPropsType extends FormFieldPropsType {}

export const ObjectFinder: FormFieldComponentType<
  ValueType<string>,
  { objectTypeName: string }
> = ({ config, name, ...restProps }) => {
  const [selectProps, ServerErrorDialog] = useObjectV1ChoicesQuery(
    config?.objectTypeName
  );

  return (
    <>
      <Select {...restProps} {...selectProps} name={name} isClearable={false} />
      {ServerErrorDialog}
    </>
  );
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useQuery } from '@tanstack/react-query';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICaseManagement } from '@zaaksysteem/generated';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { CaseRelationType } from '../Relations.types';

const getRelations = ({
  data,
  included,
}: APICaseManagement.GetCaseRelationsResponseBody): CaseRelationType[] => {
  return (data || []).map(relation => {
    const caseUuid = relation?.relationships?.other_case?.data?.id;
    const caseData = (included || []).find(caseObj => caseObj.id === caseUuid);

    return {
      relation_type: relation?.relationships?.other_case?.meta?.relation_type,
      sequence_number: relation?.attributes?.sequence_number,
      uuid: relation?.id,
      case_uuid: caseUuid,
      name: caseUuid,
      assignee: caseData?.relationships?.assignee?.meta?.display_name,
      progressStatus: caseData?.attributes.progress_status,
      ...caseData?.attributes,
    };
  });
};

export const fetchCaseRelations = async (caseUuid: string) => {
  const result = await request<APICaseManagement.GetCaseRelationsResponseBody>(
    'GET',
    buildUrl<APICaseManagement.GetCaseRelationsRequestParams>(
      '/api/v2/cm/case/get_case_relations',
      {
        case_uuid: caseUuid,
        include: 'this_case,other_case',
      }
    )
  );

  return getRelations(result);
};

export const createCaseRelation = async (
  caseUuid: string,
  otherCaseUuid: string
) => {
  const result =
    await request<APICaseManagement.CreateCaseRelationResponseBody>(
      'POST',
      '/api/v2/cm/case/create_case_relation',
      {
        uuid1: caseUuid,
        uuid2: otherCaseUuid,
      }
    );

  return result.data?.success;
};

export const deleteCaseRelation = async (
  caseUuid: string,
  relationUuid: string
) => {
  const result =
    await request<APICaseManagement.DeleteCaseRelationResponseBody>(
      'POST',
      '/api/v2/cm/case/delete_case_relation',
      {
        case_uuid: caseUuid,
        relation_uuid: relationUuid,
      }
    );

  return result.data?.success;
};

export const reorderCaseRelation = async (
  caseUuid: string,
  relationUuid: string,
  newIndex: number
) => {
  const result =
    await request<APICaseManagement.ReorderCaseRelationResponseBody>(
      'POST',
      '/api/v2/cm/case/reorder_case_relation',
      {
        case_uuid: caseUuid,
        relation_uuid: relationUuid,
        new_index: newIndex,
      }
    );

  return result.data?.success;
};

export const fetchPlannedCases = async (caseNumber: number) => {
  const result = await request('GET', `/api/case/${caseNumber}/planned-cases`);

  return result;
};

export const addPlannedCase = async (body: any) => {
  const result = await request('POST', '/api/v1/scheduled_job/create', body);

  return result;
};

export const editPlannedCase = async (relationUuid: string, body: any) => {
  const result = await request(
    'POST',
    `/api/v1/scheduled_job/${relationUuid}/update`,
    body
  );

  return result;
};

export const removePlannedCase = async (relationUuid: string) => {
  const result = await request(
    'POST',
    `/api/v1/scheduled_job/${relationUuid}/delete`
  );

  return result;
};

export const fetchSubjects = async (caseUuid: string) => {
  const result =
    await request<APICaseManagement.GetSubjectRelationsResponseBody>(
      'GET',
      buildUrl<APICaseManagement.GetSubjectRelationsRequestParams>(
        '/api/v2/cm/case/get_subject_relations',
        {
          case_uuid: caseUuid,
        }
      )
    );

  return result;
};

export const relateSubject = async (
  caseUuid: string,
  body: APICaseManagement.CreateSubjectRelationRequestBody
) => {
  const result =
    await request<APICaseManagement.CreateSubjectRelationResponseBody>(
      'POST',
      buildUrl<APICaseManagement.GetCaseRelationsRequestParams>(
        '/api/v2/cm/case/create_subject_relation',
        {
          case_uuid: caseUuid,
        }
      ),
      body
    );

  return result;
};

export const editSubject = async (
  caseUuid: string,
  body: APICaseManagement.UpdateSubjectRelationResponseBody
) => {
  const result =
    await request<APICaseManagement.UpdateSubjectRelationResponseBody>(
      'POST',
      buildUrl(
        /*<APICaseManagement.DeleteSubjectRelationRequestParams>*/
        '/api/v2/cm/case/update_subject_relation',
        {
          case_uuid: caseUuid,
        }
      ),
      body
    );

  return result;
};

export const unrelateSubject = async (
  caseUuid: string,
  body: APICaseManagement.DeleteSubjectRelationRequestBody
) => {
  const result =
    await request<APICaseManagement.DeleteSubjectRelationResponseBody>(
      'POST',
      buildUrl(
        /*<APICaseManagement.DeleteSubjectRelationRequestParams>*/
        '/api/v2/cm/case/delete_subject_relation',
        {
          case_uuid: caseUuid,
        }
      ),
      body
    );

  return result;
};

export const fetchObjects = async (caseNumber: number) => {
  const result = await request<any>(
    'GET',
    buildUrl(`/api/case/${caseNumber}/related-objects`, {
      zapi_num_rows: 100,
    })
  );

  return result.result;
};

export const useObjectV1ChoicesQuery = (type: string) => {
  const [input, setInput] = React.useState<string | undefined>(undefined);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const enabled = Boolean(input);

  const data = useQuery(
    ['objectsV1', type, input],
    async ({ queryKey: [__, object_type, query] }) => {
      const result = await request(
        'GET',
        buildUrl('/objectsearch', {
          object_type,
          query,
        })
      ).catch(openServerErrorDialog);

      return result.json.entries.map((obj: any) => ({
        value: obj.id,
        label: obj.label,
      }));
    }
  );

  const selectProps = {
    onInputChange: (ev: any, val: any, reason: any) =>
      ['input', 'clear'].includes(reason) && setInput(val),
    loading: data.status === 'loading' && enabled,
    choices: data.data || [],
  };

  return [selectProps, ServerErrorDialog] as const;
};

export const relateObject = async (
  caseUuid: string,
  objectUuid: string,
  copyValues: boolean
) => {
  const result = await request(
    'POST',
    `/api/v1/case/${caseUuid}/relation/add_object`,
    {
      related_id: objectUuid,
      copy_attribute_values: copyValues,
    }
  );

  return Boolean(result.result.instance);
};

export const fetchCustomObjects = async (caseUuid: string) => {
  const result = await request<APICaseManagement.GetCustomObjectsResponseBody>(
    'GET',
    buildUrl<any>('/api/v2/cm/custom_object/get_custom_objects', {
      filter: {
        'relationships.cases.id': caseUuid,
      },
    })
  );

  return result;
};

export const relateCustomObject = async (cases: string[], uuid: string) => {
  const result =
    await request<APICaseManagement.RelateCustomObjectToResponseBody>(
      'POST',
      '/api/v2/cm/custom_object/relate_custom_object_to',
      {
        cases,
        custom_object_uuid: uuid,
      }
    );

  return result;
};

export const unrelateCustomObject = async (cases: string[], uuid: string) => {
  const result =
    await request<APICaseManagement.UnrelateCustomObjectFromResponseBody>(
      'POST',
      '/api/v2/cm/custom_object/unrelate_custom_object_from',
      {
        cases,
        custom_object_uuid: uuid,
      }
    );

  return result.data?.success;
};

export const fetchPlannedEmails = async (caseNumber: number) => {
  const result = await request(
    'GET',
    `/zaak/${caseNumber}/scheduled_jobs`,
    {},
    undefined,
    { ['x-client-type']: 'web' }
  );

  return result.result;
};

export const reschedulePlannedEmails = async (caseNumber: number) => {
  const result = await request(
    'POST',
    `/zaak/${caseNumber}/reschedule/?action=update`,
    {},
    undefined,
    { ['x-client-type']: 'web' }
  );

  return result.result;
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { CaseTypeType } from '../../../../Case.types';
import { CustomObjectType } from '../../Relations.types';
import { fetchCustomObjects } from '../requests';

const getCustomFieldName = (
  relationships: any,
  caseUuid: string,
  caseType: CaseTypeType
): string => {
  const caseRelationships = relationships.cases;

  if (!caseRelationships) return '-';

  const relationshipToThisCase = caseRelationships.find(
    (caseRelation: any) => caseRelation.data.id === caseUuid
  );
  const customFieldUuid =
    relationshipToThisCase?.meta?.source_custom_field_uuid;
  const customFields = caseType.phases.reduce(
    (acc: any[], phase: any) => [...acc, ...phase.custom_fields],
    []
  );
  const customField = customFields.find(
    (field: any) => field.uuid === customFieldUuid
  );

  if (!customField) return '-';

  return customField.name;
};

export const getCustomObjects = async (
  caseUuid: string,
  setCustomObjects: (customObjects: CustomObjectType[]) => void,
  caseType: CaseTypeType
) => {
  const response = await fetchCustomObjects(caseUuid);
  const customObjects = response.data.map(
    ({ id, attributes: { name, title, status }, relationships }) => ({
      uuid: id,
      // this is not a mistake. An object is based on an object type
      // title is the name of the specific object
      // name is the name of the object type
      name: title,
      type: name,
      active: status === 'active',
      customFieldName: getCustomFieldName(relationships, caseUuid, caseType),
    })
  );

  setCustomObjects(customObjects);
};

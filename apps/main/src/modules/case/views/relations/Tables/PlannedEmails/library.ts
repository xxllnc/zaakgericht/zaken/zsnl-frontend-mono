// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import fecha from 'fecha';
import { PlannedEmailType } from '../../Relations.types';
import { fetchPlannedEmails, reschedulePlannedEmails } from '../requests';

const formatEmail = ({
  instance: { reference, date, recipient, label },
}: any) => ({
  uuid: reference,
  name: reference,
  template: label,
  date,
  recipient,
});

type SetPlannedEmailsType = (emails: PlannedEmailType[]) => void;

type GetPlannedEmailsType = (
  caseNumber: number,
  setPlannedEmails: SetPlannedEmailsType
) => void;

export const getPlannedEmails: GetPlannedEmailsType = async (
  caseNumber,
  setPlannedEmails
) => {
  const response = await fetchPlannedEmails(caseNumber);
  const emails = response.map(formatEmail);

  setPlannedEmails(emails);
};

type RescheduleEmailsType = (
  caseNumber: number,
  setPlannedEmails: SetPlannedEmailsType
) => void;

export const rescheduleEmails: RescheduleEmailsType = async (
  caseNumber,
  setPlannedEmails
) => {
  const response = await reschedulePlannedEmails(caseNumber);
  const emails = response.map(formatEmail);

  setPlannedEmails(emails);
};

type FormatDateType = (date: string) => string;

export const formatDate: FormatDateType = date =>
  fecha.format(new Date(date), 'DD-MM-YYYY HH:mm');

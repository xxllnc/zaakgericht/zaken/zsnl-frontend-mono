// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import Typography from '@mui/material/Typography';
import Button from '@mintlab/ui/App/Material/Button';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { useSortableTableStyles } from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable.style';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { SubjectType, SubjectTablePropsType } from '../../Relations.types';
import { useRelationsStyles } from '../../Relations.style';
import {
  getSubjects,
  relateSubjectAction,
  editSubjectAction,
  unrelateSubjectAction,
} from './library';
import Dialog from './Dialog';
import { getColumns } from './ColumnDefinition';

const SubjectsTable: React.ComponentType<SubjectTablePropsType> = ({
  caseObj,
  canEdit,
}) => {
  const [t] = useTranslation('caseRelations');
  const classes = useRelationsStyles();
  const tableStyles = useSortableTableStyles();
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [saving, setSaving] = useState(false);
  const [subjects, setSubjects] = useState<SubjectType[]>([]);
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [currentSubject, setCurrentSubject] = useState<SubjectType>();
  const closeDialog = () => {
    setCurrentSubject(undefined);
    setIsDialogOpen(false);
  };

  useEffect(() => {
    getSubjects(caseObj.uuid).then(setSubjects);
  }, []);

  const startEdit = (subject: SubjectType) => {
    setCurrentSubject(subject);
    setIsDialogOpen(true);
  };

  const add = async (formValues: any) => {
    await relateSubjectAction(caseObj.uuid, formValues)
      .then(async () => {
        await getSubjects(caseObj.uuid).then(setSubjects);

        closeDialog();
      })
      .catch(openServerErrorDialog);
  };

  const edit = async (formValues: any, updatedSubject?: SubjectType) => {
    await editSubjectAction(
      caseObj.uuid,
      updatedSubject?.uuid || '',
      formValues
    )
      .then(() => {
        const updatedSubjects = subjects.map(subject =>
          subject.uuid !== updatedSubject?.uuid
            ? subject
            : {
                ...subject,
                role: formValues.role.value,
                permission: formValues.permission,
                authorized: formValues.authorized,
                magic_string_prefix: formValues.magic_string_prefix,
              }
        );

        setSubjects(updatedSubjects);
        closeDialog();
      })
      .catch(openServerErrorDialog);
  };

  const unrelate = async (relationUuid: string) => {
    setSaving(true);

    const result = await unrelateSubjectAction(caseObj.uuid, relationUuid);

    if (result) {
      const remainingSubjects = subjects.filter(
        subject => subject.uuid !== relationUuid
      );

      setSubjects(remainingSubjects);
    }

    setSaving(false);
  };

  const columns = getColumns(t, canEdit, saving, startEdit, unrelate);
  const rows = subjects;

  return (
    <div className={classes.section}>
      <Typography variant="h2" classes={{ root: classes.header }}>
        {t('subjects.title')}
      </Typography>
      <div
        style={{ flex: '1 1 auto', height: `calc(${rows.length + 1} * 53px)` }}
      >
        <SortableTable
          rows={rows}
          //@ts-ignore
          columns={columns}
          loading={false}
          rowHeight={53}
          noRowsMessage={t('noRowsMessage')}
          styles={tableStyles}
          sorting="none"
        />
      </div>
      {canEdit && (
        <div className={classes.actionFooter}>
          <Button name="addSubject" action={() => setIsDialogOpen(true)}>
            {t('subjects.add')}
          </Button>
          <Dialog
            caseObj={caseObj}
            isOpen={isDialogOpen}
            close={closeDialog}
            add={add}
            edit={edit}
            subject={currentSubject}
            subjects={subjects}
          />
        </div>
      )}
      {ServerErrorDialog}
    </div>
  );
};

export default SubjectsTable;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
//@ts-ignore
import Typography from '@mui/material/Typography';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { useSortableTableStyles } from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable.style';
import { reorder } from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable.library';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { CaseRelationType } from '../../Relations.types';
import { useRelationsStyles } from '../../Relations.style';
import {
  createCaseRelation,
  deleteCaseRelation,
  reorderCaseRelation,
} from '../requests';
import { getColumns, resequence } from './library';

type RelatedTablePropsType = {
  cases: CaseRelationType[];
  caseUuid: string;
  refresh: () => void;
};

const RelatedTable: React.ComponentType<RelatedTablePropsType> = ({
  cases,
  caseUuid,
  refresh,
}) => {
  const [saving, setSaving] = useState(false);
  const [rows, setRows] = useState([] as CaseRelationType[]);
  const [t] = useTranslation('caseRelations');
  const classes = useRelationsStyles();
  const tableStyles = useSortableTableStyles();
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  useEffect(() => {
    if (cases) setRows(cases);
  }, [cases]);

  const columns = getColumns(
    t,
    [
      {
        name: 'relation_type',
        width: 110,
        minWidth: 110,
        // eslint-disable-next-line
        cellRenderer: ({
          rowData: { relation_type },
        }: {
          rowData: CaseRelationType;
        }) => <>{t(`cases.values.relation_type.${relation_type}`)}</>,
      },
    ],
    [
      {
        name: 'delete',
        width: 70,
        minWidth: 70,
        // eslint-disable-next-line
        cellRenderer: ({
          rowData: { uuid, case_uuid },
        }: {
          rowData: CaseRelationType;
        }) => (
          <Button
            name="deleteCaseRelation"
            icon="close"
            iconSize="extraSmall"
            disabled={saving}
            action={async () => {
              setSaving(true);

              const result = await deleteCaseRelation(case_uuid, uuid);

              setSaving(false);

              if (result) {
                refresh();
              }
            }}
          />
        ),
      },
    ]
  );

  const formDefinition: FormDefinition<{ case_uuid: string }> = [
    // this field is always available
    // - to those with read rights
    // - when the case is closed
    {
      name: 'case_uuid',
      type: fieldTypes.CASE_FINDER,
      value: null,
      required: true,
      nestedValue: true,
      isClearable: false,
      placeholder: t('relatedCases.new.placeholder'),
      label: t('relatedCases.new.label'),
    },
  ];

  const { fields } = useForm({
    formDefinition: formDefinition,
  });

  const sortedRows = rows.sort((rowA, rowB) =>
    rowA.sequence_number > rowB.sequence_number ? 1 : -1
  );

  return (
    <div className={classes.section}>
      {ServerErrorDialog}
      <Typography variant="h2" classes={{ root: classes.header }}>
        {t('relatedCases.title')}
      </Typography>
      <div
        style={{
          flex: '1 1 auto',
          height: `calc(${sortedRows.length + 1} * 53px)`,
        }}
      >
        <SortableTable
          rows={sortedRows}
          //@ts-ignore
          columns={columns}
          loading={false}
          rowHeight={53}
          noRowsMessage={t('noRowsMessage')}
          styles={tableStyles}
          sorting="dragdrop"
          onDragEnd={({ draggableId, source, destination }) => {
            if (!destination || source.index === destination.index) {
              return;
            }

            const resequencedRows = resequence(
              reorder(sortedRows, source.index, destination.index)
            );
            setRows(resequencedRows);
            reorderCaseRelation(
              caseUuid,
              draggableId,
              destination.index + 1
            ).catch(openServerErrorDialog);
          }}
          draggingText={t('relatedCases.draggingText')}
        />
      </div>
      <div className={classes.actionFooter}>
        {fields.map(({ FieldComponent, name, ...rest }) => {
          const props = cloneWithout(rest, 'definition', 'mode');

          return (
            <FormControlWrapper {...props} key={name}>
              <FieldComponent
                {...props}
                name={name}
                key={name}
                onBlur={() => {}}
                onChange={async ({ target: { value } }) => {
                  if (!value) {
                    return;
                  }

                  setSaving(true);

                  const result = await createCaseRelation(
                    caseUuid,
                    value
                  ).catch(openServerErrorDialog);

                  setSaving(false);

                  if (result) {
                    refresh();
                  }
                }}
              />
            </FormControlWrapper>
          );
        })}
      </div>
    </div>
  );
};

export default RelatedTable;

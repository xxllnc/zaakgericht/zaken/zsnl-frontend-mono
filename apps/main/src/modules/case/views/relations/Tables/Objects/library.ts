// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { CaseTypeType } from '../../../../Case.types';
import { ObjectType } from '../../Relations.types';
import { fetchObjects } from '../requests';

export const getObjects = async (
  caseNumber: number,
  setObjects: (objects: ObjectType[]) => void
) => {
  const response = await fetchObjects(caseNumber);
  const objects = response.map(({ object_class_name, uuid, name }: any) => ({
    uuid,
    name,
    type: object_class_name,
  }));

  setObjects(objects);
};

export const getChoices = (
  caseType: CaseTypeType
): { label: string; value: string }[] => {
  const choices = caseType.objectFields
    .filter(({ object_metadata: { relate_object } }: any) => relate_object)
    .map(({ label, object_type_prefix }: any) => ({
      label,
      value: object_type_prefix,
    }));

  return choices;
};

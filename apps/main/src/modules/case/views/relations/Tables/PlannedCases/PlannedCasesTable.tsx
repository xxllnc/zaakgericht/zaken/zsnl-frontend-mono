// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import Typography from '@mui/material/Typography';
import Button from '@mintlab/ui/App/Material/Button';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { useSortableTableStyles } from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable.style';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import {
  PlannedCaseType,
  PlannedCasesTablePropsType,
} from '../../Relations.types';
import { useRelationsStyles } from '../../Relations.style';
import {
  getPlannedCases,
  addPlannedCaseAction,
  editPlannedCaseAction,
  removePlannedCaseAction,
} from './library';
import Dialog from './Dialog';
import { getColumns } from './ColumnDefinition';

const PlannedCasesTable: React.ComponentType<PlannedCasesTablePropsType> = ({
  caseUuid,
  caseNumber,
  canEdit,
}) => {
  const [t] = useTranslation('caseRelations');
  const classes = useRelationsStyles();
  const tableStyles = useSortableTableStyles();
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [saving, setSaving] = useState(false);
  const [plannedCases, setPlannedCases] = useState<PlannedCaseType[]>([]);
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [currentPlannedCase, setCurrentPlannedCase] =
    useState<PlannedCaseType>();
  const closeDialog = () => {
    setCurrentPlannedCase(undefined);
    setIsDialogOpen(false);
  };

  useEffect(() => {
    getPlannedCases(caseNumber).then(setPlannedCases);
  }, []);

  const startEdit = (plannedCase: PlannedCaseType) => {
    setCurrentPlannedCase(plannedCase);
    setIsDialogOpen(true);
  };

  // we need to get the truth from backend to get the (new) uuid's
  const refresh = async () => {
    await getPlannedCases(caseNumber).then(setPlannedCases);

    closeDialog();
  };

  const add = (formValues: any) =>
    addPlannedCaseAction(caseUuid, formValues)
      .then(refresh)
      .catch(openServerErrorDialog);

  const edit = (formValues: any, updatedPlannedCase?: PlannedCaseType) =>
    editPlannedCaseAction(updatedPlannedCase?.uuid || '', formValues)
      .then(refresh)
      .catch(openServerErrorDialog);

  const remove = async (relationUuid: string) => {
    setSaving(true);

    const result = await removePlannedCaseAction(relationUuid);

    if (result) {
      const remainingPlannedCases = plannedCases.filter(
        plannedCase => plannedCase.uuid !== relationUuid
      );

      setPlannedCases(remainingPlannedCases);
    }

    setSaving(false);
  };

  const columns = getColumns(t, canEdit, saving, startEdit, remove);
  const rows = plannedCases;

  return (
    <div className={classes.section}>
      <Typography variant="h2" classes={{ root: classes.header }}>
        {t('plannedCases.title')}
      </Typography>
      <div
        style={{ flex: '1 1 auto', height: `calc(${rows.length + 1} * 53px)` }}
      >
        <SortableTable
          rows={rows}
          //@ts-ignore
          columns={columns}
          loading={false}
          rowHeight={53}
          noRowsMessage={t('noRowsMessage')}
          styles={tableStyles}
          sorting="none"
        />
      </div>
      <div className={classes.actionFooter}>
        {canEdit && (
          <>
            <Button name="addPlannedCase" action={() => setIsDialogOpen(true)}>
              {t('plannedCases.add')}
            </Button>
            <Dialog
              isOpen={isDialogOpen}
              close={closeDialog}
              add={add}
              edit={edit}
              plannedCase={currentPlannedCase}
            />
          </>
        )}
      </div>
      {ServerErrorDialog}
    </div>
  );
};

export default PlannedCasesTable;

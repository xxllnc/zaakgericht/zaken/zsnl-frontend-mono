// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SessionType } from '@zaaksysteem/common/src/store/session/session.reducer';
import { CaseObjType, CaseTypeType } from '../../Case.types';

type TablesPropsType = {
  session: SessionType;
  caseUuid: string;
  caseNumber: number;
  caseObj: CaseObjType;
  caseType: CaseTypeType;
  canEdit: boolean;
};

export type CaseTablesPropsType = Pick<TablesPropsType, 'caseObj' | 'caseType'>;

export type RelationsPropsType = Pick<
  TablesPropsType,
  'session' | 'caseObj' | 'caseType'
>;

export type PlannedCasesTablePropsType = Pick<
  TablesPropsType,
  'caseUuid' | 'caseNumber' | 'canEdit'
>;

export type SubjectTablePropsType = Pick<
  TablesPropsType,
  'caseObj' | 'canEdit'
>;

export type ObjectsTablePropsType = Pick<
  TablesPropsType,
  'caseUuid' | 'canEdit' | 'caseNumber' | 'caseType'
>;

export type CustomObjectsTablePropsType = Pick<
  TablesPropsType,
  'caseUuid' | 'canEdit' | 'caseType'
>;

export type PlannedEmailsTablePropsType = Pick<
  TablesPropsType,
  'caseNumber' | 'canEdit'
>;

export type LevelType = 'A' | 'B' | 'C';

export type RelationType =
  | 'initiator'
  | 'continuation'
  | 'plain'
  | 'child'
  | 'parent'
  | 'self';

export interface CaseRelationType
  extends Pick<
    CaseObjType,
    | 'uuid'
    | 'name'
    | 'caseOpen'
    | 'result'
    | 'summary'
    | 'number'
    | 'progressStatus'
  > {
  relation_type: RelationType;
  case_uuid: string;
  level?: LevelType;
  assignee?: string;
  casetype_title?: string;
  sequence_number: number;
}

export type PlannedEmailType = {
  uuid: string;
  name: string;
  date: string;
  template: string;
  recipient: string;
};

export type PlannedCaseType = {
  uuid: string;
  name: string;
  casetype_uuid: string;
  casetype_title: string;
  next_run: string;
  runs_left: string;
  interval_value: string;
  interval_period: string;
};

export type SubjectType = {
  uuid: string;
  name: string;
  subject: {
    uuid: string;
    // type: SubjectTypeType;
    type: string;
    name: string;
  };
  role: string;
  authorized: boolean;
  magic_string_prefix: string;
  permission: 'none' | 'search' | 'read' | 'write';
};

export type ObjectType = {
  uuid: string;
  type: string;
  name: string;
};

export type CustomObjectType = {
  uuid: string;
  type: string;
  name: string;
  customFieldName: string;
};

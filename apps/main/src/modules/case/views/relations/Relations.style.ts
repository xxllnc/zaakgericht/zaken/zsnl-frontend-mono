// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useRelationsStyles = makeStyles(() => ({
  wrapper: {
    height: '100%',
    overflow: 'scroll',
    padding: '10px 20px',
    display: 'flex',
    flexDirection: 'column',
  },
  section: {
    margin: '10px 0px',
  },
  header: {
    margin: '0 20px',
  },
  actionFooter: {
    width: 400,
    margin: 20,
  },
}));

export type ClassesType = ReturnType<typeof useRelationsStyles>;

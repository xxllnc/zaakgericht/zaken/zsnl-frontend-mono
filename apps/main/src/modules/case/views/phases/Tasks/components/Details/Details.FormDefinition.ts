// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  FormDefinition,
  FormDefinitionField,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { ContactFinderConfigType } from '@zaaksysteem/common/src/components/form/fields/ContactFinder/ContactFinder.types';
import { TaskType } from './../../Tasks.types';

const getFormDefinition: (
  task: TaskType
) => FormDefinition<TaskType> = task => [
  {
    name: 'title',
    type: fieldTypes.TEXT,
    required: true,
    label: 'caseTasks:fields.title.placeholder',
    suppressLabel: true,
    placeholder: 'caseTasks:fields.title.placeholder',
    value: task.title,
  },
  {
    name: 'description',
    type: fieldTypes.TEXTAREA,
    isMultiline: true,
    required: false,
    placeholder: 'caseTasks:fields.description.placeholder',
    rows: 5,
    value: task.description,
  },
  <FormDefinitionField<TaskType, ContactFinderConfigType>>{
    name: 'assignee',
    type: fieldTypes.CONTACT_FINDER,
    required: false,
    label: 'caseTasks:fields.assignee.label',
    placeholder: 'caseTasks:fields.assignee.placeholder',
    value: task.assignee,
    config: {
      subjectTypes: ['employee'],
    },
  },
  {
    name: 'due_date',
    type: fieldTypes.DATEPICKER,
    variant: 'inline',
    required: false,
    label: 'caseTasks:fields.due_date.label',
    fullWidth: true,
    placeholder: 'caseTasks:fields.due_date.placeholder',
    value: task.due_date,
  },
  {
    name: 'completed',
    type: 'hidden',
    value: task.completed,
  },
];

export default getFormDefinition;

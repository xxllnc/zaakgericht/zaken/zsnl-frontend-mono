// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { Button } from '@mintlab/ui/App/Material/Button';
import PlusButtonSpaceWrapper from '@zaaksysteem/common/src/components/PlusButtonSpaceWrapper/PlusButtonSpaceWrapper';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import { useDetailStyles } from './Details.style';
import Toolbar from './Toolbar/Toolbar';
import getFormDefinition from './Details.FormDefinition';
import {
  TaskType,
  UpdateTaskType,
  DeleteTaskType,
  SetTaskCompletionType,
  ExitEditModeType,
} from './../../Tasks.types';

export type DetailsPropsType = {
  task: TaskType;
  canEdit: boolean;
  updateTask: UpdateTaskType;
  deleteTask: DeleteTaskType;
  setTaskCompletion: SetTaskCompletionType;
  exitEditMode: ExitEditModeType;
};

/* eslint complexity: [2, 7] */
const Details: React.ComponentType<DetailsPropsType> = ({
  task,
  canEdit,
  updateTask,
  deleteTask,
  setTaskCompletion,
  exitEditMode,
}) => {
  const classes = useDetailStyles();
  const [t] = useTranslation();
  const formDefinition = getFormDefinition(task);
  const { fields, formik } = useForm({
    t,
    formDefinition,
    enableReinitialize: true,
    isInitialValid: true,
  });

  const { task_uuid, is_editable, can_set_completion } = task;

  return (
    <div className={classes.wrapper}>
      <Toolbar
        task={task}
        completed={formik.values.completed as any}
        t={t}
        deleteTask={deleteTask}
        exitEditMode={exitEditMode}
      />

      <div className={classes.scrollWrapper}>
        <PlusButtonSpaceWrapper>
          {fields.map(
            ({
              FieldComponent,
              name,
              error,
              touched,
              value,
              suppressLabel,
              ...rest
            }) => {
              const props = {
                ...cloneWithout(
                  rest,
                  'setFieldValue',
                  'setFieldTouched',
                  'type',
                  'classes'
                ),
                disabled: !canEdit || !is_editable || formik.values.completed,
                ...(name === 'due_date' && {
                  onClose: () => formik.setFieldValue(name, null),
                }),
              };

              return (
                <FormControlWrapper
                  {...props}
                  label={suppressLabel ? null : props.label}
                  compact={true}
                  classes={{
                    wrapper: classes.formControlWrapper,
                  }}
                  error={error}
                  touched={touched}
                  key={name}
                >
                  <FieldComponent
                    name={name}
                    value={value}
                    key={name}
                    {...props}
                  />
                </FormControlWrapper>
              );
            }
          )}

          {formik.values.completed && can_set_completion && canEdit && (
            <Button
              name="reopenTask"
              action={() => setTaskCompletion(task_uuid, false)}
              disabled={!formik.isValid}
              fullWidth
            >
              {t('caseTasks:form.reOpen')}
            </Button>
          )}

          {!formik.values.completed && is_editable && canEdit && (
            <Button
              name="saveTask"
              action={() => updateTask(task_uuid, formik.values)}
              disabled={!formik.isValid}
              fullWidth
            >
              {t('common:dialog.save')}
            </Button>
          )}
        </PlusButtonSpaceWrapper>
      </div>
    </div>
  );
};

export default Details;

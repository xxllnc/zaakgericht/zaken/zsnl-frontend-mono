// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useDetailStyles = makeStyles(() => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    padding: '13px 13px 0 13px',
  },
  formControlWrapper: {
    marginBottom: 20,
  },
  scrollWrapper: {
    flex: '1 1 auto',
  },
}));

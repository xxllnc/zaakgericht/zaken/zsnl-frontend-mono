// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import { AutoSizer, List } from 'react-virtualized';
import { useTranslation } from 'react-i18next';
import { sortTasks } from '../../Tasks.library';
import {
  TasksType,
  AddTaskType,
  SetTaskCompletionType,
  EnterEditModeType,
} from './../../Tasks.types';
import { TaskItem } from './TaskItem/TaskItem';
import { useTaskListStyles } from './Tasklist.style';
import TaskCreator from './TaskCreator';

export type TaskListPropsType = {
  tasks: TasksType;
  canEdit: boolean;
  addTask: AddTaskType;
  setTaskCompletion: SetTaskCompletionType;
  enterEditMode: EnterEditModeType;
};

export const TaskList: React.ComponentType<TaskListPropsType> = ({
  tasks,
  canEdit,
  addTask,
  setTaskCompletion,
  enterEditMode,
}) => {
  const classes = useTaskListStyles();
  const [t] = useTranslation('caseTasks');
  const sortedTasks = sortTasks(tasks);

  const rowRenderer = ({ index, style }: any) => {
    const task = sortedTasks[index];

    return (
      <TaskItem
        task={task}
        style={style}
        key={task.task_uuid}
        setTaskCompletion={setTaskCompletion}
        enterEditMode={enterEditMode}
      />
    );
  };

  return (
    <div className={classes.wrapper}>
      <React.Fragment>
        <TaskCreator canEdit={canEdit} addTask={addTask} />
        {sortedTasks.length ? (
          <AutoSizer>
            {({ width }) => (
              <List
                height={85 * sortedTasks.length}
                rowCount={sortedTasks.length}
                rowHeight={85}
                rowRenderer={rowRenderer}
                width={width}
                style={{ outline: 'none' }}
              />
            )}
          </AutoSizer>
        ) : (
          <div className={classes.placeholder}>{t('placeholder')}</div>
        )}
      </React.Fragment>
    </div>
  );
};

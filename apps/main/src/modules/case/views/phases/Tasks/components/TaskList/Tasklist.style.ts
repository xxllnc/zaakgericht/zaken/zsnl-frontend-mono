// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useTaskListStyles = makeStyles(() => ({
  wrapper: {
    padding: '13px 13px 0 13px',
    height: '100%',
    overflowY: 'auto',
    overflowX: 'hidden',
  },
  placeholder: {
    paddingTop: 20,
  },
}));

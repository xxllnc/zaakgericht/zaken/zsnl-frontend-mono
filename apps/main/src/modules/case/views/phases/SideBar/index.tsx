// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import SideBar, { SideBarPropsType } from './SideBar';

const SideBarWrapper: React.ComponentType<SideBarPropsType> = props => {
  return (
    <Routes>
      <Route path="" element={<Navigate to={'actions'} replace={true} />} />
      <Route path={':sideBarType/*'} element={<SideBar {...props} />} />
    </Routes>
  );
};

export default SideBarWrapper;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { CaseObjType, CaseTypeType } from '../../../Case.types';

export interface FormPropsType {
  caseObj: CaseObjType;
  caseType: CaseTypeType;
  phaseNumber: string;
}

export const Form: React.ComponentType<FormPropsType> = ({ phaseNumber }) => {
  return <div>form phase {phaseNumber}</div>;
};

export default Form;

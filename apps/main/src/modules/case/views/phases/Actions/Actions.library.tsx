// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
//@ts-ignore
import { filterProperties } from '@mintlab/kitchen-sink/source';
import { CaseTypeType } from '../../../Case.types';
import {
  ActionType,
  UpdateActionType,
  MergeActionType,
  ActionRelationTypeType,
} from './Actions.types';
import {
  fetchPhaseActions,
  checkCall,
  untaintCall,
  saveCall,
} from './Actions.requests';
import Icons from './icons/Icons';
import { copyKeys } from './Dialog/PhaseActionsDialog.library';

const getActionIcon = (action: ActionType) => {
  const ActionIcon = Icons[action.type];

  return <ActionIcon action={action} />;
};

const supportedActionTypes = [
  'allocation',
  'case',
  'email',
  'object_action',
  'subject',
  // 'object_mutation,
  'template',
];

const isSupportedType = (action: ActionType) =>
  supportedActionTypes.includes(action.type);

const formatAction = (action: any): ActionType => ({
  ...action,
  icon: getActionIcon(action),
});

export const getActions = async (caseNumber: number, phaseNumber: number) => {
  const actionsByPhase = await fetchPhaseActions(caseNumber);
  const actions = actionsByPhase[phaseNumber - 1].items;
  const supportedActions = actions.filter(isSupportedType);
  const formattedActions = supportedActions.map(formatAction);

  return formattedActions;
};

export const getTitle = (t: i18next.TFunction, action: ActionType) => {
  const verb = t(`${action.type}.verb`).toLowerCase();
  let noun = t(`${action.type}.noun`);

  if (action.type === 'case') {
    noun = t(`case.noun.${action.data.relatie_type}`);
  }

  return t('titleTooltip', {
    noun,
    verb,
  });
};

export const isSubcaseInLastPhase = (
  relationType: ActionRelationTypeType,
  caseType: CaseTypeType,
  phaseNumber: number
) => caseType.phases.length === phaseNumber && relationType === 'deelzaak';

export const isCheckDisabled = (
  action: ActionType,
  caseType: CaseTypeType,
  phaseNumber: number
): boolean => {
  const hasInterface = Boolean(action.data.interface_id); // e.g. xential

  return (
    hasInterface ||
    isSubcaseInLastPhase(action.data.relatie_type, caseType, phaseNumber)
  );
};

export const mergeAction: MergeActionType = (actions, actionToMerge) =>
  actions.map(action =>
    action.id === actionToMerge.id ? actionToMerge : action
  );

const getCaseSaveData = (action: any): any => {
  const basicFormValues = filterProperties(
    action,
    'relaties_eigenaar_type',
    'relaties_eigenaar_uuid',
    'relaties_eigenaar_role',
    'relaties_relatie_type',
    'relaties_start_delay_date',
    'relaties_start_delay',
    'relaties_required',
    'relaties_group_uuid',
    'relaties_role_uuid',
    'relaties_automatisch_behandelen',
    'relaties_subject_role',
    'relaties_betrokkene_role',
    'relaties_betrokkene_prefix',
    'relaties_betrokkene_authorized',
    'relaties_betrokkene_notify'
  );

  const copyValues = copyKeys.reduce((acc: any, key: string) => {
    const value = action.copy.includes(key) ? 1 : 0;

    return { ...acc, [key]: value };
  }, {});

  return {
    ...basicFormValues,
    ...copyValues,
    relaties_eigenaar_type: action.relaties_eigenaar_type?.value || null,
    relaties_betrokkene_role_set: action.relaties_eigenaar_uuid ? 1 : 0,
    relaties_eigenaar_uuid: action.relaties_eigenaar_uuid?.value?.value || null,
    relaties_eigenaar_id: action.relaties_eigenaar_uuid?.value?.label || null,
    relaties_betrokkene_uuid: action.relaties_betrokkene_uuid?.value || null,
  };
};

/* eslint complexity: [2, 7] */
const getSaveData = (action: any): any => {
  switch (action.type) {
    case 'allocation': {
      return filterProperties(action, 'group_uuid', 'role_uuid');
    }
    case 'case': {
      return getCaseSaveData(action);
    }
    case 'email': {
      return filterProperties(
        action,
        'notificaties_rcpt',
        'notificaties_behandelaar',
        'notificaties_email',
        'notificaties_betrokkene_role',
        'notificaties_cc',
        'notificaties_bcc',
        'notificaties_subject',
        'notificaties_body'
      );
    }
    case 'object_action': {
      return {};
    }
    case 'subject': {
      return {};
    }
    case 'template': {
      return filterProperties(
        action,
        'filename',
        'target_format',
        'bibliotheek_kenmerken_id'
      );
    }
    default: {
      return {};
    }
  }
};

export const updateAction: UpdateActionType = async (
  caseNumber,
  type,
  action
) => {
  if (type === 'toggleCheck') {
    const { automatic, id } = action;
    const response = await checkCall(caseNumber, { automatic, id });

    return formatAction(response.result[0]);
  } else if (type === 'untaint') {
    const { id } = action;
    const response = await untaintCall(caseNumber, { id });

    return formatAction(response.result[0]);
  } else {
    const data = getSaveData(action);
    const response = await saveCall(caseNumber, action.type, {
      ...data,
      trigger: type,
      update: 1,
      id: action.id,
    });

    if (type === 'commit') {
      return formatAction(response.result[0]);
    } else {
      // type 'execute' doesn't return the (complete) updated action
      return null;
    }
  }
};

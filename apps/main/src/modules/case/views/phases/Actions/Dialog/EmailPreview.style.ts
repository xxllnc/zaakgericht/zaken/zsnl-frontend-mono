// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useEmailPreviewStyle = makeStyles(({ typography }: Theme) => ({
  wrapper: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    gap: 20,
  },
  label: {
    fontWeight: typography.fontWeightBold,
    marginBottom: 10,
  },
  attachmentWrapper: {
    display: 'flex',
    flexDirection: 'column',
  },
}));

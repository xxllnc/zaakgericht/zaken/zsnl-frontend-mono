// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { InfiniteLoader, SizeInfo } from 'react-virtualized';
import { SortDirectionType } from 'react-virtualized';
import { hasNoValue } from '@zaaksysteem/common/src/library/normalizeValue';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { useDebouncedCallback } from 'use-debounce';
// @ts-ignore
import TextField from '@mintlab/ui/App/Material/TextField';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import useInfiniteScroll from '@zaaksysteem/common/src/hooks/useInfiniteScroll';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { useSortableTableStyles } from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable.style';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { useCaseTableStyles } from './Cases.style';
import { getColumns, getData, getSelectOptions } from './library';
import { CasesRowType, DataParams, SelectFilterType } from './Cases.types';
import CaseTableLoader from './Cases.loader';
import { SubjectType } from './../ContactView.types';
import locale from './Cases.locale';

const PAGE_LENGTH = 20;
const THRESHOLD = 5;
const REMOTE_ROW_COUNT = 99999;

type CasesPropsType = {
  subject: SubjectType;
  params?: {
    [key: string]: string;
  };
};

const Cases: React.FunctionComponent<CasesPropsType> = ({
  subject,
  params,
}) => {
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  const [sortBy, setSortBy] = useState<string | null>(null);
  const [sortDirection, setSortDirection] = useState<SortDirectionType | null>(
    null
  );
  const [selectFilter, setSelectFilter] = useState<SelectFilterType>('owned');
  const [textFilter, setTextFilter] = useState<string>('');
  const [initialLoading, setInitialLoading] = useState<boolean>(true);
  const [tableDimensions, setTableDimensions] = useState<SizeInfo | null>(null);

  const infiniteLoaderRef = useRef<InfiniteLoader>(null);
  const classes = useCaseTableStyles();
  const [t] = useTranslation('cases');
  const tableStyles = useSortableTableStyles();
  const columns = getColumns(t, classes);
  const selectOptions = getSelectOptions(t, subject);

  const { list, isRowLoaded, loadMoreRows, resetList, loading, totalResults } =
    useInfiniteScroll<CasesRowType, DataParams>({
      ref: infiniteLoaderRef,
      pageLength: PAGE_LENGTH,
      getData,
      getDataParams: {
        params,
        selectFilter,
        textFilter,
        pageLength: PAGE_LENGTH,
        uuid: subject.id,
        sortBy,
        sortDirection,
        openServerErrorDialog,
      },
    });

  const [debouncedResetList] = useDebouncedCallback(() => {
    if (!textFilter.length || textFilter.length >= 3) resetList();
  }, 300);

  useEffect(() => {
    (async function () {
      if (initialLoading) {
        await loadMoreRows({ startIndex: 1 });
        setInitialLoading(false);
      }
    })();
  }, []);

  useEffect(() => {
    debouncedResetList();
  }, [selectFilter, textFilter, sortBy, sortDirection]);

  return (
    <I18nResourceBundle resource={locale} namespace="cases">
      <div className={classes.wrapper}>
        <div className={classes.filterBar}>
          {!hasNoValue(totalResults) ? (
            <div className={classes.totalResults}>
              {totalResults === 1
                ? t('contactView:totalResult', { count: totalResults })
                : t('contactView:totalResults', { count: totalResults })}
            </div>
          ) : null}
          <TextField
            value={textFilter}
            onChange={event => {
              setTextFilter(event.target.value);
            }}
            placeholder={t('search')}
            closeAction={() => setTextFilter('')}
            startAdornment={<Icon size="small">{iconNames.search}</Icon>}
            variant="generic1"
          />
          <Select
            variant="generic"
            choices={selectOptions}
            onChange={({ target: { value } }) => {
              setSelectFilter(value.value);
            }}
            placeholder={t('selectPlaceholder')}
            value={selectFilter}
          />
        </div>
        <div style={{ flex: '1 1 auto' }} className={classes.table}>
          <CaseTableLoader
            loading={loading}
            tableDimensions={tableDimensions}
          />
          {ServerErrorDialog}
          <InfiniteLoader
            isRowLoaded={isRowLoaded}
            loadMoreRows={loadMoreRows}
            rowCount={REMOTE_ROW_COUNT}
            threshold={THRESHOLD}
            ref={infiniteLoaderRef}
          >
            {({ onRowsRendered, registerChild }) => (
              <SortableTable
                externalRef={registerChild}
                onRowsRendered={onRowsRendered}
                rows={list}
                //@ts-ignore
                columns={columns}
                noRowsMessage={
                  initialLoading || loading
                    ? t('loadingMessage')
                    : t('noRowsMessage')
                }
                styles={tableStyles}
                sortDirectionDefault="DESC"
                sortInternal={false}
                sorting="column"
                onSort={(sortBy: string, sortDirection: SortDirectionType) => {
                  setSortBy(sortBy);
                  setSortDirection(sortDirection);
                }}
                onResize={(table: SizeInfo) => setTableDimensions(table)}
              />
            )}
          </InfiniteLoader>
        </div>
      </div>
    </I18nResourceBundle>
  );
};

export default Cases;

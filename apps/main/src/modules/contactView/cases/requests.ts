// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICaseManagement } from '@zaaksysteem/generated';
import { hasNoValue } from '@zaaksysteem/common/src/library/normalizeValue';
import { DataParams } from './Cases.types';

const sortMapping: any = {
  status: 'attributes.status',
  number: 'attributes.number',
  progress: 'attributes.progress',
  casetype: 'relationships.case_type.name',
  daysLeft: 'attributes.days_left',
};

/* eslint complexity: [2, 13] */
export const fetchCases = async ({
  uuid,
  pageNum,
  pageLength,
  params,
  openServerErrorDialog,
  selectFilter,
  textFilter,
  sortBy,
  sortDirection,
}: Pick<
  DataParams,
  | 'uuid'
  | 'pageNum'
  | 'pageLength'
  | 'params'
  | 'openServerErrorDialog'
  | 'selectFilter'
  | 'textFilter'
  | 'sortBy'
  | 'sortDirection'
>) => {
  const queryParams: any = {
    contact_uuid: uuid,
    page: pageNum,
    page_size: pageLength,
    // extra param to make the call fast
    ['filter[attributes.is_authorized]']: false,
    ...(params || {}),
  };

  switch (selectFilter) {
    case 'owned':
      queryParams['filter[attributes.roles.contains]'] = 'Aanvrager';
      break;
    case 'ownedOpen':
      queryParams['filter[attributes.roles.contains]'] = 'Aanvrager';
      queryParams['filter[attributes.status]'] = 'new,open,stalled';
      break;
    case 'authorized':
      queryParams['filter[attributes.is_authorized]'] = true;
      break;
    case 'involdedIn':
      queryParams['filter[attributes.roles.not_contains]'] = 'Aanvrager';
      break;
    case 'shared_address':
      queryParams['include_cases_on_location'] = true;
      queryParams['filter[attributes.is_authorized]'] = undefined;
      break;
  }

  if (textFilter) {
    queryParams['filter[keyword]'] = textFilter;
  }

  if (sortBy && sortDirection) {
    let sorting = sortMapping[sortBy];
    if (sortDirection === 'DESC') sorting = '-' + sorting;
    queryParams['sort'] = sorting;
  }

  const url = buildUrl<APICaseManagement.GetContactRelatedCasesRequestParams>(
    '/api/v2/cm/get_contact_related_cases',
    queryParams
  );

  const result =
    await request<APICaseManagement.GetContactRelatedCasesResponseBody>(
      'GET',
      url
    ).catch(openServerErrorDialog);

  return {
    rows: result?.data || [],
    totalResults: !hasNoValue(result?.meta?.total_results)
      ? result?.meta?.total_results
      : undefined,
  };
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SubjectType } from './../ContactView.types';
import { AltAuthIntegrationType, AltAuthDataType } from './Information.types';
import { transformPerson } from './transformers/person';
import { transformOrganization } from './transformers/organization';
import { transformEmployee } from './transformers/employee';
import {
  fetchSubjectSettings,
  fetchObjectType,
  fetchRelatedObject,
  fetchAltAuthIntegration,
  fetchAltAuth,
  checkHasPhoneIntegration,
} from './Information.requests';

type GetAltAuthSupportType = (
  integration: AltAuthIntegrationType,
  type: string
) => boolean;

const getAltAuthSupport: GetAltAuthSupportType = (integration, type) => {
  const enableFor = integration.interface_config.enable_for;

  return (
    enableFor === 'both' ||
    (enableFor === 'persons' && type === 'person') ||
    (enableFor === 'companies' && type === 'organization')
  );
};

type FetchAltAuthDataType = (
  type: string,
  uuid: string
) => Promise<AltAuthDataType | undefined>;

const getAltAuthData: FetchAltAuthDataType = async (subjectType, uuid) => {
  const integration: AltAuthIntegrationType = await fetchAltAuthIntegration();
  const altAuthSupported = getAltAuthSupport(integration, subjectType);

  if (!altAuthSupported) return;

  const result = await fetchAltAuth(uuid);

  const { id, type, is_active, username, phone_number } = result;

  return {
    id,
    subject_type: type === 'person' ? 'natuurlijk_persoon' : 'bedrijf',
    active: is_active ? '1' : '0',
    username,
    phone: phone_number,
  };
};

const getObject = async (uuid?: string) => {
  if (!uuid) {
    return {};
  }

  const object = await fetchRelatedObject(uuid);

  // for when the user is given the uuid of the related object in the get_contact call
  // but is not allowed to know anything about the object
  if (!object) {
    return {};
  }

  const objectType = await fetchObjectType(
    object.relationships.custom_object_type.data.id
  );

  return { object, objectType };
};

export const fetchContactInformation = async (
  fetchedSubject: SubjectType,
  altAuthActive: boolean
) => {
  const { type, id: uuid } = fetchedSubject;

  switch (type) {
    case 'person':
    case 'organization': {
      const subject =
        type === 'person'
          ? transformPerson(fetchedSubject)
          : transformOrganization(fetchedSubject);
      const { object, objectType } = await getObject(subject.relatedObjectUuid);
      const altAuthData = altAuthActive
        ? await getAltAuthData(type, uuid)
        : undefined;

      return {
        subject,
        object,
        objectType,
        altAuthData,
      };
    }
    case 'employee': {
      const settings = await fetchSubjectSettings(uuid);
      const hasPhoneIntegration = await checkHasPhoneIntegration();

      const subject = transformEmployee(fetchedSubject, settings);
      const { object, objectType } = await getObject(subject.relatedObjectUuid);

      return { subject, object, objectType, hasPhoneIntegration };
    }
    default:
      break;
  }
};

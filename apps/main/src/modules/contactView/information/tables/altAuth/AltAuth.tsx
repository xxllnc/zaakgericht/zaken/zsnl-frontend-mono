// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import Button from '@mintlab/ui/App/Material/Button';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { saveAltAuth, sendAltAuth } from '../../Information.requests';
import { AltAuthDataType } from '../../Information.types';
import { getFormDefinition } from './altAuth.formDefinition';

type AltAuthPropsType = {
  altAuthData: AltAuthDataType;
};

const AltAuth: React.FunctionComponent<AltAuthPropsType> = ({
  altAuthData,
}) => {
  const [t] = useTranslation('information');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  const formDefinition = getFormDefinition(t, altAuthData);

  const {
    fields,
    formik: { isValid, values, dirty },
  } = useForm({
    formDefinition,
  });

  return (
    <>
      {ServerErrorDialog}
      <SubHeader
        title={t('altAuth.title')}
        description={t('altAuth.subTitle')}
      />
      <div>
        {fields.map(
          ({ FieldComponent, name, error, touched, value, ...rest }) => {
            const restValues = {
              ...cloneWithout(rest, 'type', 'classes'),
              disabled: values.completed,
            };

            return (
              <FormControlWrapper
                {...restValues}
                error={error}
                touched={touched}
                key={name}
              >
                <FieldComponent
                  name={name}
                  value={value}
                  key={name}
                  {...restValues}
                />
              </FormControlWrapper>
            );
          }
        )}
        <Button
          action={() =>
            saveAltAuth(altAuthData.id, values).catch(openServerErrorDialog)
          }
          name="saveAltAuth"
          disabled={!isValid || !dirty}
        >
          {t('common:verbs.save')}
        </Button>
        <Button
          action={() =>
            sendAltAuth(altAuthData.id, values).catch(openServerErrorDialog)
          }
          sx={{ marginLeft: '20px' }}
          name="sendAltAuth"
        >
          {t('altAuth.sendActivationEmail')}
        </Button>
      </div>
    </>
  );
};

export default AltAuth;

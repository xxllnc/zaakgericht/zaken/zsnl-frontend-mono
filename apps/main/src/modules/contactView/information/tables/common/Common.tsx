// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { SessionType } from '@zaaksysteem/common/src/store/session/session.reducer';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import { generateValidationMap } from '@zaaksysteem/common/src/components/form/validation/library/generateValidationMap';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import Button from '@mintlab/ui/App/Material/Button';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { saveCommonValues } from '../../Information.requests';
import { SeperatorLabel } from '../../Information.library';
import { useInformationStyles } from '../../Information.style';
import { hasEditRightsFromState } from '../../../ContactView.library';
import { SubjectType } from './../../../ContactView.types';
import { getPersonFormDefinition, personRules } from './formDefinition.person';
import {
  getOrganizationFormDefinition,
  organizationRules,
} from './formDefinition.organization';
import { getEmployeeFormDefinition } from './formDefinition.employee';
import { getCommonTitleSuffix } from './../../Information.library';

const getFormDefinition = {
  person: getPersonFormDefinition,
  organization: getOrganizationFormDefinition,
  employee: getEmployeeFormDefinition,
};

const getRules = {
  person: personRules,
  organization: organizationRules,
  employee: [],
};

type CommonPropsType = {
  subject: SubjectType;
  refreshSubject: () => void;
  setSnackOpen: any;
  session: SessionType;
};

const Common: React.FunctionComponent<CommonPropsType> = ({
  subject,
  refreshSubject,
  setSnackOpen,
  session,
}) => {
  const hasEditRights = useSelector(hasEditRightsFromState);
  const isAuthenticated = subject.authenticated;
  const canEdit = hasEditRights && !isAuthenticated;
  const [t] = useTranslation('information');
  const [busy, setBusy] = useState<boolean>(false);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const classes = useInformationStyles();
  const formDefinition = getFormDefinition[subject.type]({
    t,
    subject,
    canEdit,
    session,
  });
  const rules = getRules[subject.type];
  const validationMap = generateValidationMap(formDefinition);

  const {
    fields,
    formik: { values, isValid, dirty },
  } = useForm({
    rules,
    formDefinition,
    validationMap,
    enableReinitialize: true,
  });

  return (
    <>
      {ServerErrorDialog}

      <SubHeader
        title={t('common.title')}
        description={t('common.subTitle')}
        titleSuffix={getCommonTitleSuffix(subject, t)}
        titleSuffixClass={classes.titleSuffix}
      />
      <div>
        {fields.map(
          ({ FieldComponent, name, error, touched, value, ...rest }) => {
            const restValues = {
              ...cloneWithout(rest, 'type', 'classes'),
              disabled: values.completed,
            };

            if (rest.isLabel)
              return <SeperatorLabel key={rest.label} str={rest.label} />;

            return (
              <FormControlWrapper
                {...restValues}
                error={error}
                touched={touched}
                key={name}
              >
                <FieldComponent
                  name={name}
                  value={value}
                  key={name}
                  {...restValues}
                />
              </FormControlWrapper>
            );
          }
        )}

        {canEdit && (
          <Button
            action={() => {
              setBusy(true);
              saveCommonValues(t, subject.uuid, values, subject.type)
                .then(() => {
                  refreshSubject();
                  setSnackOpen(true);
                })
                .catch(openServerErrorDialog)
                .finally(() => {
                  setBusy(false);
                });
            }}
            name="saveCommonValues"
            disabled={busy || !isValid || !dirty}
          >
            {t('common:verbs.save')}
          </Button>
        )}
      </div>
    </>
  );
};

export default Common;

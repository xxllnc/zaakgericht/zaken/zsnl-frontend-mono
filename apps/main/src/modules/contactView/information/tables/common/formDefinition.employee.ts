// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { SubjectType } from './../../../ContactView.types';

export const getEmployeeFormDefinition = ({
  t,
  subject,
}: {
  t: i18next.TFunction;
  subject: SubjectType;
  canEdit?: boolean;
}): AnyFormDefinitionField[] => {
  return [
    {
      name: 'firstName',
      type: fieldTypes.TEXT,
    },
    {
      name: 'surname',
      type: fieldTypes.TEXT,
    },
    {
      name: 'phoneNumber',
      type: fieldTypes.TEXT,
    },
    {
      name: 'email',
      type: fieldTypes.TEXT,
    },
    {
      name: 'department',
      type: fieldTypes.TEXT,
    },
    {
      name: 'roles',
      type: fieldTypes.MULTI_VALUE_TEXT,
      value: subject.roles.map((value: string) => ({ value, label: value })),
    },
  ].map(field => ({
    label: t(`common.employee.${field.name}`),
    value: subject[field.name],
    readOnly: true,
    ...field,
  }));
};

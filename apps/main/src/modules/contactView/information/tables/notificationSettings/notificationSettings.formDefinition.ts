// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { SubjectType } from './../../../ContactView.types';

export const getFormDefinition = (
  t: i18next.TFunction,
  subject: SubjectType
): AnyFormDefinitionField[] => {
  return [
    {
      name: 'newAssignedCase',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'newDocument',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'newAttributeProposal',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'newExtPipMessage',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'caseTermExceeded',
      type: fieldTypes.CHECKBOX,
    },
    {
      name: 'caseSuspensionTermExceeded',
      type: fieldTypes.CHECKBOX,
    },
  ].map(field => ({
    label: t(`notificationSettings.${field.name}`),
    value: subject.settings[field.name],
    ...field,
  }));
};

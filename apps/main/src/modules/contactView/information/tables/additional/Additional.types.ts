// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { SubjectType } from '../../../ContactView.types';

export type GetCommonFormDefinitionType = {
  t: i18next.TFunction;
  hasEditRights?: boolean;
  subject: SubjectType;
  contactChannelEnabled: boolean;
};

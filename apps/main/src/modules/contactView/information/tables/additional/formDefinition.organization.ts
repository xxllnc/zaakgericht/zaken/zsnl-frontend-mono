// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { SubjectType } from './../../../ContactView.types';

export type FormValuesType = {
  phoneNumber: string;
  mobileNumber: string;
  email: string;
  preferredContactChannel: string;
  internalNote: string;
  anynymousUser: boolean;
};

const getChoiceWithTranslation =
  (t: i18next.TFunction, name: string) => (value: string) => {
    return {
      value,
      label: t(`additional.${name}Value.${value}`),
    };
  };

export const getOrganizationFormDefinition = ({
  t,
  subject,
  contactChannelEnabled,
}: {
  t: i18next.TFunction;
  subject: SubjectType;
  contactChannelEnabled: boolean;
}): AnyFormDefinitionField[] =>
  [
    {
      name: 'phoneNumber',
      type: fieldTypes.PHONE_NUMBER,
    },
    {
      name: 'mobileNumber',
      type: fieldTypes.PHONE_NUMBER,
    },
    {
      name: 'email',
      type: fieldTypes.EMAIL,
    },
    ...(contactChannelEnabled
      ? [
          {
            name: 'preferredContactChannel',
            type: fieldTypes.FLATVALUE_SELECT,
            defaultValue: 'pip',
            choices: ['pip', 'email', 'mail', 'phone'].map(
              getChoiceWithTranslation(t, 'preferredContactChannel')
            ),
          },
        ]
      : []),
    {
      name: 'internalNote',
      type: fieldTypes.TEXT,
    },
  ].map(field => ({
    label: t(`additional.${field.name}`),
    value: subject[field.name],
    ...field,
  }));

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { SubjectType } from './../../../ContactView.types';

export type FormValuesType = {
  insideMunicipality: string;
  authenticated: string;
  source: string;
  uuid: string;
};

export const getPersonFormDefinition = ({
  t,
  subject,
}: {
  t: i18next.TFunction;
  subject: SubjectType;
}): AnyFormDefinitionField[] => {
  const { source, externalIdentifier, authenticated } = subject;
  const sourceValue = source
    ? `${source} (${t('special.externalIdentifier')} (${t(
        'special.keySending'
      )}) [${externalIdentifier}])`
    : '-';

  const authenticatedValue = authenticated ? t('common:yes') : t('common:no');

  return [
    {
      name: 'authenticated',
      type: fieldTypes.TEXT,
      value: authenticatedValue,
    },
    {
      name: 'source',
      type: fieldTypes.TEXT,
      value: sourceValue,
    },
    {
      name: 'uuid',
      type: fieldTypes.TEXT,
    },
  ].map(field => ({
    label: t(`special.${field.name}`),
    value: subject[field.name],
    readOnly: true,
    ...field,
  }));
};

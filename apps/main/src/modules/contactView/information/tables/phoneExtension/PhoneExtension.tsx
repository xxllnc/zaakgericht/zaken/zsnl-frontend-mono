// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import { generateValidationMap } from '@zaaksysteem/common/src/components/form/validation/library/generateValidationMap';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import Button from '@mintlab/ui/App/Material/Button';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { savePhoneExtension } from '../../Information.requests';
import { getFormDefinition } from './phoneExtension.formDefinition';
import { SubjectType } from './../../../ContactView.types';

type SignaturePropsType = {
  subject: SubjectType;
};

const PhoneExtension: React.FunctionComponent<SignaturePropsType> = ({
  subject,
}) => {
  const [t] = useTranslation('information');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const formDefinition = getFormDefinition(t, subject);
  const validationMap = generateValidationMap(formDefinition);

  const {
    fields,
    formik: { values, isValid, dirty },
  } = useForm({
    formDefinition,
    validationMap,
  });

  return (
    <>
      {ServerErrorDialog}
      <SubHeader
        title={t('phoneExtension.title')}
        description={t('phoneExtension.subTitle')}
      />
      <div>
        {fields.map(
          ({ FieldComponent, name, error, touched, value, ...rest }) => {
            const restValues = {
              ...cloneWithout(rest, 'type', 'classes'),
              disabled: values.completed,
            };

            return (
              <FormControlWrapper
                {...restValues}
                error={error}
                touched={touched}
                key={name}
              >
                <FieldComponent
                  name={name}
                  value={value}
                  key={name}
                  {...restValues}
                />
              </FormControlWrapper>
            );
          }
        )}
        <Button
          action={() =>
            savePhoneExtension(subject.uuid, values.phoneExtension).catch(
              openServerErrorDialog
            )
          }
          name="savePhoneExtension"
          disabled={!isValid || !dirty}
        >
          {t('common:verbs.save')}
        </Button>
      </div>
    </>
  );
};

export default PhoneExtension;

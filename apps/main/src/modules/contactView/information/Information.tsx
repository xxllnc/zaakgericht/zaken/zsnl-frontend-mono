// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useQuery } from '@tanstack/react-query';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
//@ts-ignore
import Snackbar from '@mintlab/ui/App/Material/Snackbar/Snackbar.cmp';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { SessionType } from '@zaaksysteem/common/src/store/session/session.reducer';
import { fetchContactInformation } from './Information.actions';
import { useInformationStyles } from './Information.style';
import Person from './views/Person';
import Organization from './views/Organization';
import Employee from './views/Employee';
import { SubjectType } from './../ContactView.types';
import locale from './Information.locale';

const TableDictionary: { [key: string]: any } = {
  person: Person,
  organization: Organization,
  employee: Employee,
};

type InformationPropsType = {
  subject: SubjectType;
  session: SessionType;
  refreshSubject: () => {};
};

const Information: React.FunctionComponent<InformationPropsType> = ({
  subject,
  session,
  refreshSubject,
}) => {
  const classes = useInformationStyles();
  const View = TableDictionary[subject.type];
  const altAuthActive = session.active_interfaces.includes('auth_twofactor');
  const [snackOpen, setSnackOpen] = useState<boolean>(false);

  const data = useQuery(
    ['contactInformation', subject, altAuthActive] as const,
    ({ queryKey: [__, sub, active] }) => fetchContactInformation(sub, active)
  );

  return (
    <I18nResourceBundle resource={locale} namespace="information">
      <Snackbar
        autoHideDuration={5000}
        handleClose={(event: React.ChangeEvent, reason: string) => {
          if (reason === 'clickaway') return;
          setSnackOpen(false);
        }}
        message="De gegevens zijn succesvol opgeslagen."
        open={snackOpen}
      />
      <div className={classes.wrapper}>
        {data.status === 'loading' ? (
          <Loader />
        ) : (
          data.data && (
            <View
              data={data.data}
              session={session}
              refreshSubject={refreshSubject}
              setSnackOpen={setSnackOpen}
            />
          )
        )}
      </div>
    </I18nResourceBundle>
  );
};

export default Information;

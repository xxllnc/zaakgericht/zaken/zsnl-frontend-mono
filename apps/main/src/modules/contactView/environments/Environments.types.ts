// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type IdType = string;

export type CustomerTypeType =
  | 'commercial'
  | 'government'
  | 'lab'
  | 'development'
  | 'staging'
  | 'acceptance'
  | 'preprod'
  | 'testing'
  | 'production';

export type OtapType = 'development' | 'testing' | 'accept' | 'production';

export type SoftwareVersionType = 'master';

type StatusType = 'active' | 'processing' | 'disabled';

export type ControlPanelType = {
  uuid: string;
  template: string;
  shortname: string;
  readOnly: 0 | 1 | null;
  customerType: CustomerTypeType;
  allowedDiskspace: number;
  allowedEnvironments: number;
  availableTemplates: string[];
};

export type EnvironmentType = {
  name: string;
  uuid: string;
  label: string;
  fqdn: string;
  diskspace: number;
  hosts: HostType[];
  status: StatusType;
  webAddress: string;
  template: string;
  otap: OtapType;
  customerType: CustomerTypeType;
  softwareVersion: string;
  fallbackUrl: string;
  provisionOn: string;
  servicesDomain: string;
  apiDomain: string;
  database: string;
  databaseHost: string;
  filestore: string;
  freeformReference: string;
  protected: boolean;
  disabled: boolean;
};

export type HostType = {
  uuid: string;
  label: string;
  fqdn: string;
  sslCert: string;
  sslKey: string;
  template: string;
};

export type GetIdType = (uuid: string, setId: (id: string) => void) => void;

export type GetControlPanelType = (id: string) => Promise<ControlPanelType>;

export type GetEnvironmentsType = (
  controlPanelUuid: string,
  setEnvironmentsType: (environments: EnvironmentType[]) => void
) => void;

export type GetHostsType = (
  controlPanelUuid: string,
  setHostsType: (hosts: HostType[]) => void
) => void;

export type SaveEnvironmentType = (
  controlPanelUuid: string,
  values: any,
  environmentUuid?: string
) => Promise<EnvironmentType>;

export type EnvironmentsActionTypeType =
  | 'create'
  | 'edit'
  | 'toggle'
  | 'protect';

export type UpdateEnvironmentsType = (
  actionType: EnvironmentsActionTypeType,
  values: any,
  environment: EnvironmentType | null,
  onClose?: () => void
) => void;

export type HostsActionTypeType = 'create' | 'update';

export type UpdateHostsType = (
  actionType: HostsActionTypeType,
  values: any,
  host: HostType | null,
  onClose: () => void
) => void;

export type SaveHostType = (
  controlPanelUuid: string,
  actionType: HostsActionTypeType,
  values: any,
  hostUuid?: string
) => Promise<HostType>;

export type RowDataType = {
  dataKey:
    | 'status'
    | 'label'
    | 'webAddress'
    | 'hosts'
    | 'template'
    | 'otap'
    | 'actions';
  rowData: EnvironmentType;
};

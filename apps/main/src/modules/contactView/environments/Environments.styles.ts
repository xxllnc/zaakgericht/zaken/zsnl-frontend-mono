// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => ({
  wrapper: { height: '100%' },
  content: { padding: '20px 20px 0 20px' },
  sidePanel: { padding: 20 },
}));

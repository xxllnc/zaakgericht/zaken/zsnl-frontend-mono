// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useStyles = makeStyles(
  ({ typography, palette: { elephant, primary } }: Theme) => ({
    wrapper: {},
    hostsPlaceholder: {
      color: elephant.dark,
    },
    host: {
      display: 'flex',
      alignItems: 'center',
      '&:hover': {
        color: primary.main,
      },
    },
    label: {
      flexGrow: 1,
    },
    formWrapper: {
      width: 500,
      fontFamily: typography.fontFamily,
      display: 'flex',
      flexDirection: 'column',
    },
  })
);

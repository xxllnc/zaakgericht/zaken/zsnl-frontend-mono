// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useStyles = makeStyles(({ typography }: Theme) => ({
  wrapper: { display: 'flex', flexDirection: 'column', gap: 10 },
  item: {
    display: 'flex',
    flexDirection: 'row',
    gap: 10,
    '&>*:nth-child(odd)': { width: 100 },
  },
  button: {
    marginTop: 10,
  },
  formWrapper: {
    width: 500,
    fontFamily: typography.fontFamily,
    display: 'flex',
    flexDirection: 'column',
  },
}));

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { ControlPanelType } from '../Environments.types';
import { useStyles } from './Overview.styles';
import OverviewDialog from './OverviewDialog';
import { getOverviewItems } from './SidePanel.library';

type OverviewPropsType = {
  controlPanel: ControlPanelType;
};

const Overview: React.FunctionComponent<OverviewPropsType> = ({
  controlPanel,
}) => {
  const classes = useStyles();
  const [t] = useTranslation('environments');
  const [dialogOpen, setDialogOpen] = useState<boolean>(false);

  const overviewItems = getOverviewItems(t, controlPanel);

  return (
    <div className={classes.wrapper}>
      {overviewItems.map(item => (
        <div key={item.label} className={classes.item}>
          <div>{item.label}:</div>
          <div>{item.value}</div>
        </div>
      ))}
      {/* backend not implemented */}
      {/* <Button
        name="edit???"
        variant="outlined"
        action={() => setDialogOpen(true)}
      >
        {t('common:verbs.edit')}
      </Button> */}
      {dialogOpen && (
        <OverviewDialog
          controlPanel={controlPanel}
          onClose={() => {
            setDialogOpen(false);
          }}
          open={dialogOpen}
        />
      )}
    </div>
  );
};

export default Overview;

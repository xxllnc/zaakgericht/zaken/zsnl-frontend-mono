// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useStyles = makeStyles(({ palette: { elephant } }: Theme) => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    paddingRight: 20,
    minWidth: 250,
    '&>*:nth-child(odd)': { marginBottom: 10 },
    '&>*:nth-child(even)': { marginBottom: 40 },
  },
}));

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import DialogActions from '@mui/material/DialogActions';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import {
  ControlPanelType,
  EnvironmentType,
  HostType,
  UpdateHostsType,
} from '../Environments.types';
import { getHostsDialogFormDefinition } from './HostsDialog.formDefinition';
import { useStyles } from './Hosts.styles';

type HostsDialogPropsType = {
  controlPanel: ControlPanelType;
  environments: EnvironmentType[];
  host: HostType | null;
  updateHosts: UpdateHostsType;
  onClose: () => void;
  open: boolean;
};

const HostsDialog: React.FunctionComponent<HostsDialogPropsType> = ({
  controlPanel,
  environments,
  host,
  updateHosts,
  onClose,
  open,
}) => {
  const classes = useStyles();
  const [t] = useTranslation('environments');
  const dialogEl = useRef();

  const actionType = host ? 'update' : 'create';
  const title = t('hosts.dialog.title', {
    verb: t(`common:verbs.${actionType}`).toLowerCase(),
  });
  const formDefinition = getHostsDialogFormDefinition(
    t,
    host,
    controlPanel,
    environments
  );

  let {
    fields,
    formik: { values, isValid },
  } = useForm({
    formDefinition,
  });

  return (
    <>
      <Dialog
        disableBackdropClick={true}
        open={open}
        onClose={onClose}
        scope={'environments-hosts-dialog'}
        ref={dialogEl}
      >
        <DialogTitle
          elevated={true}
          icon="settings"
          title={title}
          onCloseClick={onClose}
        />
        <DialogContent>
          <div className={classes.formWrapper}>
            {fields.map(
              ({ FieldComponent, key, type, suppressLabel, ...rest }) => {
                const props = cloneWithout(rest, 'mode');

                return (
                  <FormControlWrapper
                    {...props}
                    label={suppressLabel ? false : props.label}
                    compact={true}
                    key={`${props.name}-formcontrol-wrapper`}
                  >
                    <FieldComponent
                      {...props}
                      t={t}
                      containerRef={dialogEl.current}
                    />
                  </FormControlWrapper>
                );
              }
            )}
          </div>
        </DialogContent>
        <>
          <Divider />
          <DialogActions>
            {createDialogActions(
              [
                {
                  text: title,
                  disabled: !isValid,
                  onClick: () => {
                    updateHosts(actionType, values, host, onClose);
                  },
                },
                {
                  text: t('common:dialog.cancel'),
                  onClick: onClose,
                },
              ],
              'environments-hosts-dialog'
            )}
          </DialogActions>
        </>
      </Dialog>
    </>
  );
};

export default HostsDialog;

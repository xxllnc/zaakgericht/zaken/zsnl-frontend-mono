// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  GetIdType,
  GetEnvironmentsType,
  GetHostsType,
  SaveEnvironmentType,
  GetControlPanelType,
  SaveHostType,
  HostType,
  HostsActionTypeType,
  EnvironmentType,
  EnvironmentsActionTypeType,
} from './Environments.types';
import {
  fetchControlPanel,
  fetchEnvironments,
  fetchHosts,
  fetchSubjectV1,
  saveHost,
  updateEnvironment,
} from './Environments.requests';

export const getId: GetIdType = async (uuid, setId) => {
  const subjectV1 = await fetchSubjectV1(uuid);
  const id = subjectV1.old_subject_identifier;

  setId(id);
};

// @ts-ignore
export const getControlPanels: GetControlPanelType = async id => {
  const response = await fetchControlPanel(id);

  const controlPanels = response.result.instance.rows;

  if (!controlPanels.length) {
    return {};
  } else {
    const controlPanel = controlPanels[0].instance;
    const {
      id,
      template,
      shortname,
      read_only: readOnly,
      customer_type: customerType,
      allowed_diskspace,
      allowed_instances,
      available_templates,
    } = controlPanel;

    return {
      uuid: id,
      template,
      shortname,
      readOnly,
      customerType,
      allowedDiskspace: allowed_diskspace,
      allowedEnvironments: allowed_instances,
      availableTemplates: available_templates,
    };
  }
};

const formatHost = ({
  reference,
  instance: { fqdn, label, ssl_cert, ssl_key, template },
}: any) => ({
  uuid: reference,
  fqdn,
  label,
  sslCert: ssl_cert,
  sslKey: ssl_key,
  template,
});

const formatEnvironment = ({ instance }: any) => ({
  name: instance.id,
  uuid: instance.id,
  fqdn: instance.fqdn,
  label: instance.label,
  diskspace: Number(instance.stat_diskspace),
  hosts: instance.hosts.rows.map(formatHost),
  status: instance.status,
  otap: instance.otap,
  template: instance.template,
  customerType: instance.customer_type,
  softwareVersion: instance.software_version,
  fallbackUrl: instance.fallback_url,
  provisionedOn: instance.provisioned_on,
  servicesDomain: instance.services_domain,
  apiDomain: instance.api_domain,
  database: instance.database,
  databaseHost: instance.database_host,
  filestore: instance.filestore,
  freeformReference: instance.freeform_reference,
  protected: instance.protected,
  disabled: instance.disabled,
});

export const getEnvironments: GetEnvironmentsType = async (
  controlPanelUuid,
  setEnvironments
) => {
  const response = await fetchEnvironments(controlPanelUuid);
  const environments = response.map(formatEnvironment);

  setEnvironments(environments);
};

export const getHosts: GetHostsType = async (controlPanelUuid, setHosts) => {
  const response = await fetchHosts(controlPanelUuid);
  const hosts = response.map(formatHost);

  setHosts(hosts);
};

/* eslint complexity:[2,11] */
export const saveHostAction: SaveHostType = async (
  controlPanelUuid,
  actionType,
  values,
  hostUuid
) => {
  const data = {
    label: values.label || null,
    fqdn: values.fqdn || null,
    instance: values.environment?.value || values.environment || null,
    ssl_cert: values.sslCert || null,
    ssl_key: values.sslKey || null,
    template: values.template?.value || values.template || null,
  };
  const response = await saveHost(controlPanelUuid, actionType, data, hostUuid);

  try {
    if (response.result.instance) {
      return formatHost(response.result);
    }
  } catch (error: any) {
    return error;
  }
};

export const mergeHosts = (
  actionType: HostsActionTypeType,
  hosts: HostType[],
  newHost: HostType
): HostType[] => {
  if (actionType === 'create') return [...hosts, newHost];

  return hosts.map(host => (host.uuid === newHost.uuid ? newHost : host));
};

export const saveEnvironmentAction: SaveEnvironmentType = async (
  controlPanelUuid,
  values,
  environmentUuid
) => {
  const data = {
    ...values,
    password: values.password || undefined,
  };

  const response = await updateEnvironment(
    controlPanelUuid,
    data,
    environmentUuid
  );

  try {
    if (response.result.instance) {
      return formatEnvironment(response.result);
    }
  } catch (error: any) {
    return error;
  }
};

export const mergeEnvironments = (
  actionType: EnvironmentsActionTypeType,
  environments: EnvironmentType[],
  newEnvironment: EnvironmentType
): EnvironmentType[] => {
  if (actionType === 'create') return [...environments, newEnvironment];

  return environments.map(environment =>
    environment.uuid === newEnvironment.uuid ? newEnvironment : environment
  );
};

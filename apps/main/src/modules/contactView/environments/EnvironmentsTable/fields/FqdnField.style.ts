// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useStyles = makeStyles(
  ({ typography, palette: { elephant } }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
    },
    fieldWrapper: {
      flexGrow: 1,
    },
    domain: {
      padding: '0 20px',
      fontSize: typography.fontSize,
    },
  })
);

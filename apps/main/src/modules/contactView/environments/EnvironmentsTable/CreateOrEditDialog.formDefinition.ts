// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { ControlPanelType, EnvironmentType } from '../Environments.types';
import {
  customerTypes,
  otapTypes,
  softwareVersions,
} from './EnvironmentsTable.library';

type GetFormDefinitionType = (
  t: i18next.TFunction,
  controlPanel: ControlPanelType,
  environment: EnvironmentType | null
) => AnyFormDefinitionField[];

/* eslint complexity: [2, 8] */
export const getCreateOrEditDialogFormDefinition: GetFormDefinitionType = (
  t,
  controlPanel,
  environment
) => {
  const formDefinition = [
    {
      name: 'label',
      type: fieldTypes.TEXT,
      value: environment?.label || '',
      required: true,
      label: t('table.createOrEdit.labels.label'),
    },
    {
      name: 'fqdn',
      type: 'fqdn',
      value: environment?.fqdn?.split('.')[0] || '',
      required: true,
      label: t('table.createOrEdit.labels.fqdn'),
      config: {
        customerType: controlPanel.customerType,
      },
    },
    ...(environment
      ? []
      : [
          {
            name: 'customerType',
            type: fieldTypes.SELECT,
            value: controlPanel.customerType,
            required: false,
            label: t('table.createOrEdit.labels.customerType'),
            choices: customerTypes.map(customerType => ({
              label: t(`customerTypes.${customerType}`),
              value: customerType,
            })),
          },
        ]),
    {
      name: 'otap',
      type: fieldTypes.SELECT,
      value: environment?.otap || 'development',
      required: false,
      label: t('table.createOrEdit.labels.otap'),
      choices: otapTypes.map(otap => ({
        label: t(`otapTypes.${otap}`),
        value: otap,
      })),
    },
    ...(environment
      ? []
      : [
          {
            name: 'softwareVersion',
            type: fieldTypes.SELECT,
            value: 'master',
            required: false,
            label: t('table.createOrEdit.labels.softwareVersion'),
            choices: softwareVersions.map(softwareVersion => ({
              label: t(`softwareVersions.${softwareVersion}`),
              value: softwareVersion,
            })),
          },
        ]),
    {
      name: 'template',
      type: fieldTypes.SELECT,
      value: environment?.template || null,
      required: false,
      isClearable: true,
      label: t('table.createOrEdit.labels.template'),
      choices: controlPanel.availableTemplates.map(template => ({
        label: template,
        value: template,
      })),
      placeholder: t('table.createOrEdit.placeholders.template'),
    },
    {
      name: 'password',
      type: fieldTypes.TEXT,
      displayType: 'password',
      value: '',
      label: t('table.createOrEdit.labels.password'),
    },
  ];

  return formDefinition;
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => ({
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    height: 'calc(100% - 1px)',
    gap: 20,
  },
  createButton: {
    width: 200,
  },
  hostList: {
    display: 'flex',
    flexDirection: 'column',
    gap: 5,
    listStyleType: 'none',
    margin: 0,
    padding: 0,
  },
  actionWrapper: {
    display: 'flex',
    flexDirection: 'row',
  },
}));

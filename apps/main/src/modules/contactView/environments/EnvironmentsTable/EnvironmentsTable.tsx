// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { useSortableTableStyles } from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable.style';
import { useStyles } from './EnvironmentsTable.styles';
import {
  ControlPanelType,
  EnvironmentType,
  UpdateEnvironmentsType,
} from './../Environments.types';
import { getColumns } from './EnvironmentsTable.library';
import { cellRenderer } from './EnvironmentsTableCell';
import DetailsDialog from './DetailsDialog';
import CreateOrEditDialog from './CreateOrEditDialog';

type EnvironmentsTablePropsType = {
  controlPanel: ControlPanelType;
  environments: EnvironmentType[];
  updateEnvironments: UpdateEnvironmentsType;
};

const EnvironmentsTable: React.FunctionComponent<
  EnvironmentsTablePropsType
> = ({ controlPanel, environments, updateEnvironments }) => {
  const classes = useStyles();
  const tableStyles = useSortableTableStyles();
  const [t] = useTranslation('environments');
  const [environmentToView, setEnvironmentToView] =
    useState<EnvironmentType | null>(null);
  const [createEnvironment, setCreateEnvironment] = useState<boolean>(false);
  const [environmentToEdit, setEnvironmentToEdit] =
    useState<EnvironmentType | null>(null);

  const cellRender = (rowData: any) =>
    cellRenderer(
      rowData,
      setEnvironmentToView,
      setEnvironmentToEdit,
      updateEnvironments,
      t,
      classes
    );

  return (
    <div className={classes.wrapper}>
      <Button
        className={classes.createButton}
        action={() => {
          setCreateEnvironment(true);
        }}
        name="createEnvironment"
      >
        {t('table.createOrEdit.createEnvironment')}
      </Button>
      <div
        style={{
          flex: '1 1 auto',
          height: '100%',
          width: '100%',
          paddingRight: '16px',
        }}
      >
        <SortableTable
          styles={tableStyles}
          rows={environments}
          columns={getColumns(t, cellRender)}
          noRowsMessage={t('table.noRows')}
        />
        {environmentToView && (
          <DetailsDialog
            environment={environmentToView}
            onClose={() => setEnvironmentToView(null)}
            open={Boolean(environmentToView)}
          />
        )}
        {(createEnvironment || environmentToEdit) && (
          <CreateOrEditDialog
            controlPanel={controlPanel}
            environment={environmentToEdit}
            updateEnvironments={updateEnvironments}
            onClose={() => {
              setCreateEnvironment(false);
              setEnvironmentToEdit(null);
            }}
            open={createEnvironment || Boolean(environmentToEdit)}
          />
        )}
      </div>
    </div>
  );
};

export default EnvironmentsTable;

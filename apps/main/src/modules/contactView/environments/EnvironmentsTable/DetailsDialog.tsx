// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import DialogContent from '@mui/material/DialogContent';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import { EnvironmentType } from './../Environments.types';
import { useDetailsDialogStyles } from './DetailsDialog.style';
import { getDetailsDialogFormDefinition } from './DetailsDialog.formDefinition';

type DetailsDialogPropsType = {
  environment: EnvironmentType;
  onClose: () => void;
  open: boolean;
};

const DetailsDialog: React.ComponentType<DetailsDialogPropsType> = ({
  environment,
  onClose,
  open,
}) => {
  const [t] = useTranslation('environments');
  const classes = useDetailsDialogStyles();
  const dialogEl = useRef();

  const title = environment.label;
  const formDefinition = getDetailsDialogFormDefinition(t, environment);

  let { fields } = useForm({
    formDefinition,
  });

  return (
    <>
      <Dialog
        disableBackdropClick={true}
        open={open}
        onClose={onClose}
        scope={'environments-details-dialog'}
        ref={dialogEl}
      >
        <DialogTitle
          elevated={true}
          icon="eye"
          title={title}
          onCloseClick={onClose}
        />
        <DialogContent>
          <div className={classes.formWrapper}>
            {fields.map(({ FieldComponent, key, type, ...rest }) => {
              const props = cloneWithout(rest, 'mode');

              return (
                <FormControlWrapper
                  {...props}
                  key={`${props.name}-formcontrol-wrapper`}
                >
                  <FieldComponent {...props} containerRef={dialogEl.current} />
                </FormControlWrapper>
              );
            })}
          </div>
        </DialogContent>
      </Dialog>
    </>
  );
};

export default DetailsDialog;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import ContactView from './ContactView';
import locale from './ContactView.locale';

export default function ContactViewModule() {
  return (
    <I18nResourceBundle resource={locale} namespace="contactView">
      <ContactView />
    </I18nResourceBundle>
  );
}

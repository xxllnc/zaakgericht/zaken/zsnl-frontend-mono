// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent, useEffect } from 'react';
import * as i18next from 'i18next';
import { FieldInputProps } from 'formik';
import ObjectTypeFinder from '@zaaksysteem/common/src/components/form/fields/ObjectTypeFinder/ObjectTypeFinder';
import { isUUID } from '../../../library/library';

type ObjectTypeSelectPropsType = {
  field: FieldInputProps<any>;
  t: i18next.TFunction;
};

const ObjectTypeSelect: FunctionComponent<ObjectTypeSelectPropsType> = ({
  field,
  t,
}) => {
  const { onChange, value, name } = field;
  useEffect(() => {
    if (value && value?.value && isUUID(value?.value)) {
      onChange({
        target: { name, type: 'select', value },
      });
    }
  }, []);

  return (
    <>
      {
        //@ts-ignore
        <ObjectTypeFinder
          {...field}
          variant="generic"
          placeholder={t('editForm.fields.objectType.placeholder')}
        />
      }
    </>
  );
};

export default ObjectTypeSelect;

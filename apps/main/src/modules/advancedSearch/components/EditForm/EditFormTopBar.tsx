// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import classNames from 'classnames';
import { getLabel } from '../../components/Main/Main.library';
import { ModeType } from '../../AdvancedSearch.types';

type EditFormTopBarPropsType = {
  classes: any;
  mode: ModeType;
  currentQuery?: any;
  t: i18next.TFunction;
};

const EditFormTopBar: FunctionComponent<EditFormTopBarPropsType> = ({
  classes,
  mode,
  currentQuery,
  t,
}) => {
  const label = getLabel({ mode, currentQuery, t });

  return (
    <div
      className={classNames(
        classes.mainTopBar,
        classes.editFormCentered,
        classes.editFormTitle
      )}
    >
      {label}
    </div>
  );
};

export default EditFormTopBar;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent, useEffect, useState, useRef } from 'react';
import * as i18next from 'i18next';
import { FormikProps, FieldInputProps, Field } from 'formik';
import classNames from 'classnames';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import {
  DragDropContext,
  Draggable,
  Droppable,
  DropResult,
} from 'react-beautiful-dnd';
import {
  ColumnType,
  EditFormStateType,
  ClassesType,
  KindType,
  ModeType,
  IdentifierType,
  CustomFieldType,
} from '../../../AdvancedSearch.types';
import { getColumns } from '../EditForm.library';
import { getUniqueColumnIdentifier } from '../../../library/library';
import { getCategories } from '../../../library/config';
import ColumnSelector from './ColumnSelector';
import ColumnEntry from './ColumnEntry';

const FIELD_NAME = 'columns';

type ColumnsPropsType = {
  classes: ClassesType;
  show: Boolean;
  t: i18next.TFunction;
  kind: KindType;
  mode: ModeType;
  identifier?: IdentifierType | null;
  customFields: CustomFieldType[];
  openServerErrorDialog: OpenServerErrorDialogType;
};

/* eslint complexity: [2, 12] */
const validateFunc = (t: i18next.TFunction) => (values: ColumnType[]) => {
  return values && Array.isArray(values) && values.length > 0
    ? undefined
    : t('editForm.fields.columns.errorMessage');
};

const Columns: FunctionComponent<
  ColumnsPropsType & {
    form: FormikProps<EditFormStateType>;
    field: FieldInputProps<any>;
  }
> = ({
  form,
  field,
  classes,
  show,
  t,
  customFields,
  kind,
  mode,
  identifier,
  openServerErrorDialog,
}) => {
  const { name } = field;
  const {
    setFieldValue,
    values: { columns, sortColumn, sortOrder },
  } = form;

  const [allColumns, setAllColumns] = useState<ColumnType[]>([]);

  const columnsRef = useRef(null as ColumnType[] | null);
  useEffect(() => {
    columnsRef.current = columns;
  }, [columns]);

  const onDragEnd = (result: DropResult) => {
    if (!result.destination) return;
    const newItems = reorder(result.source.index, result.destination.index);
    setFieldValue(name, newItems);
  };

  const reorder = (startIndex: number, endIndex: number) => {
    const result = Array.from(columns);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);
    return result;
  };

  const onDelete = (index: number) => {
    //@ts-ignore
    const newItems = columnsRef.current.filter(
      (thisItem, thisIndex) => thisIndex !== index
    );
    setFieldValue(name, newItems);
  };

  const handleSelectOnChange = ({ target: value }: any) => {
    const data = value.value.data;
    setFieldValue(name, [...[data], ...columns]);
  };

  const init = () => {
    const allColumns = getColumns(
      kind,
      kind === 'custom_object' ? customFields : null,
      t
    );
    const categories = getCategories(kind, t);
    const whichCategory =
      kind === 'custom_object' ? 'default' : 'systemAttributes';
    const defaultColumns = allColumns.filter(column =>
      (categories as any)[whichCategory].includes(column.type)
    );

    if (kind === 'custom_object') setAllColumns(allColumns);
    if (columnsRef.current && columnsRef.current.length === 0 && mode === 'new')
      setFieldValue(name, defaultColumns);
  };

  useEffect(() => {
    if (kind === 'custom_object' && customFields) init();
  }, [customFields]);

  useEffect(() => {
    if (kind === 'case') {
      setTimeout(() => init(), 0);
    }
  }, [mode, identifier]);

  return (
    <>
      <div
        className={classNames(
          classes.editFormSection,
          classes.columnsOuterWrapper,
          {
            [classes.show]: show,
            [classes.hide]: !show,
          }
        )}
      >
        <div className={classes.columnsInnerWrapper}>
          <div className={classes.columnsSelector}>
            <ColumnSelector
              kind={kind}
              name={name}
              columns={columns}
              handleSelectOnChange={handleSelectOnChange}
              classes={classes}
              t={t}
              allColumns={allColumns}
              openServerErrorDialog={openServerErrorDialog}
            />
          </div>
          <div>
            <DragDropContext onDragEnd={onDragEnd}>
              <Droppable droppableId={`${name}-droppable`}>
                {provided => (
                  <div {...provided.droppableProps} ref={provided.innerRef}>
                    {(columns || []).map((item, index) => {
                      const uniqueIdentifier = getUniqueColumnIdentifier(item);

                      return (
                        <Draggable
                          key={uniqueIdentifier}
                          draggableId={uniqueIdentifier}
                          index={index}
                        >
                          {(draggableProvided, draggableSnapshot) => (
                            <ColumnEntry
                              item={item}
                              index={index}
                              provided={draggableProvided}
                              //@ts-ignore
                              snapshot={draggableSnapshot}
                              classes={classes}
                              onDelete={onDelete}
                              setFieldValue={setFieldValue}
                              sortColumn={sortColumn}
                              sortOrder={sortOrder}
                              t={t}
                            />
                          )}
                        </Draggable>
                      );
                    })}
                    {provided.placeholder}
                  </div>
                )}
              </Droppable>
            </DragDropContext>
          </div>
        </div>
      </div>
    </>
  );
};

export default (props: ColumnsPropsType) => {
  const { t } = props;
  return (
    <Field
      {...props}
      name={FIELD_NAME}
      component={Columns}
      validate={validateFunc(t)}
      key={FIELD_NAME}
    />
  );
};

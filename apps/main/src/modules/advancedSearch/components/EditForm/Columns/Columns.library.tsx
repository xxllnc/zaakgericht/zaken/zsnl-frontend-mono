// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import { ColumnType } from '../../../AdvancedSearch.types';

export const mapColumnToChoice = (column: ColumnType): ValueType<string> => ({
  value: column.type,
  label: column.label,
  //@ts-ignore
  data: column,
});

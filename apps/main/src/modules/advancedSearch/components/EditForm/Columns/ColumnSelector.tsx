// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent, useState } from 'react';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import * as i18next from 'i18next';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import Button from '@mintlab/ui/App/Material/Button';
import { getCategories, getDefaultColumns } from '../../../library/config';
import { ColumnType, KindType } from '../../../AdvancedSearch.types';
import AttributeFinder from '../AttributeFinder';
import { getUniqueColumnIdentifier } from '../../../library/library';
import { mapColumnToChoice } from './Columns.library';

type ColumnSelectorPropsType = {
  kind: KindType;
  name: string;
  columns: ColumnType[];
  handleSelectOnChange: (event: any) => void;
  classes: any;
  t: i18next.TFunction;
  allColumns: ColumnType[];
  openServerErrorDialog: OpenServerErrorDialogType;
};

type MethodType = 'systemAttributes' | 'searchAttributes';

/* eslint complexity: [2, 12] */
const ColumnSelector: FunctionComponent<ColumnSelectorPropsType> = ({
  kind,
  name,
  columns,
  handleSelectOnChange,
  classes,
  t,
  allColumns,
  openServerErrorDialog,
}) => {
  const [method, setMethod] = useState<MethodType>('systemAttributes');
  const [category, setCategory] = useState<string>('systemAttributes');
  //const [selectedColumn, setSelectedColumn] = useState<string | null>(null);

  const getCategoriesChoices = () => {
    const categories = getCategories('case', t);
    return Object.keys(categories).map((category: string) => ({
      label: t(`editForm.fields.columns.categories.${category}`),
      value: category,
    }));
  };

  const getAttributesChoices = () => {
    const defaults = getDefaultColumns('case', t);
    const categories = getCategories('case', t);
    const choices = defaults.filter(def =>
      (categories as any)[category].includes(def.type)
    );
    return choices.map(mapColumnToChoice);
  };

  const filterOption = (option: ValueType<string, any>, input?: string) => {
    const identifier = getUniqueColumnIdentifier(option.data as ColumnType);
    const columnPresent = columns.find(
      col => getUniqueColumnIdentifier(col) === identifier
    );

    if (columnPresent) {
      return false;
    } else if (input && option.label) {
      //@ts-ignore
      return option.label.toLowerCase().indexOf(input.toLowerCase()) !== -1;
    }

    return true;
  };

  if (kind === 'custom_object') {
    const choices: ValueType<String>[] = (allColumns || []).map(
      mapColumnToChoice
    );

    return (
      <div className={classes.columnsSingleSelectWrapper}>
        <Select
          variant="generic"
          name={`${name}-add-select`}
          value={null}
          choices={choices}
          isClearable={true}
          loading={false}
          filterOption={filterOption}
          onChange={handleSelectOnChange}
          placeholder={t('editForm.fields.columns.selectColumnPlaceholder')}
        />
      </div>
    );
  } else {
    return (
      <>
        <div className={classes.columnsMethodChoice}>
          {['systemAttributes', 'searchAttributes'].map(thisMethod => (
            <Button
              name="editFormMethod"
              key={thisMethod}
              variant={thisMethod === method ? 'contained' : 'outlined'}
              action={() => setMethod(thisMethod as MethodType)}
            >
              {t(`editForm.methods.${thisMethod}`)}
            </Button>
          ))}
        </div>
        {method === 'systemAttributes' && (
          <div className={classes.columnsSystemAttributeSelectors}>
            <Select
              variant="generic"
              name={`${name}-categories`}
              value={category}
              choices={getCategoriesChoices()}
              onChange={(event: React.ChangeEvent<any>) =>
                setCategory(event.target.value.value)
              }
            />
            <Select
              variant="generic"
              name={`${name}-add-select`}
              value={null}
              choices={getAttributesChoices()}
              filterOption={filterOption}
              onChange={(event: React.ChangeEvent<any>) =>
                handleSelectOnChange(event)
              }
              placeholder={t('editForm.fields.columns.selectColumnPlaceholder')}
            />
          </div>
        )}

        {method === 'searchAttributes' && (
          <div className={classes.columnsSingleSelectWrapper}>
            <AttributeFinder
              name={`${name}-attribute-finder`}
              handleSelectOnChange={(event: React.ChangeEvent<any>) => {
                const { data } = event.target.value;
                const { type, label, magicString } = data;
                const columnObj: ColumnType = {
                  type,
                  label,
                  source: ['attributes', 'custom_fields', magicString],
                  magicString,
                };

                handleSelectOnChange({
                  target: {
                    ...event.target,
                    value: {
                      ...event.target.value,
                      data: columnObj,
                    },
                  },
                });
              }}
              t={t}
              openServerErrorDialog={openServerErrorDialog}
              filterOption={(option: any) =>
                !columns.find(
                  col => col.magicString === option.data?.magicString
                )
              }
              selectKey={`attribute-finder-${columns?.length}`}
            />
          </div>
        )}
      </>
    );
  }
};

export default ColumnSelector;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent, useState } from 'react';
import * as i18next from 'i18next';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import IconButton from '@mui/material/IconButton';
import { SORTABLE_COLUMNS } from '../../../library/config';
import {
  ColumnType,
  SortDirectionType,
  ClassesType,
} from '../../../AdvancedSearch.types';
import SetSortDialog from './SetSortDialog';

type ColumnEntryPropsType = {
  index: number;
  provided: any;
  item: ColumnType;
  classes: ClassesType;
  onDelete: any;
  setFieldValue: any;
  sortColumn?: String;
  sortOrder?: SortDirectionType;
  t: i18next.TFunction;
};

/* eslint complexity: [2, 12] */
const ColumnEntry: FunctionComponent<ColumnEntryPropsType> = ({
  index,
  provided,
  item,
  classes,
  onDelete,
  setFieldValue,
  sortColumn,
  sortOrder,
  t,
}) => {
  const [settingSort, setSettingSort] = useState(false);
  const { type } = item;
  const isSortable = SORTABLE_COLUMNS.some(
    (identifier: string) => identifier === type
  );

  const iconType =
    sortColumn && sortColumn === type
      ? sortOrder === 'asc'
        ? iconNames.arrow_drop_up
        : iconNames.arrow_drop_down
      : iconNames.sort;

  return (
    <>
      {settingSort && (
        <SetSortDialog
          onConfirm={({ sort }: any) => {
            setFieldValue('sortColumn', type);
            setFieldValue('sortOrder', sort.value);
            setSettingSort(false);
          }}
          hide={() => setSettingSort(false)}
          t={t}
        />
      )}
      <div
        ref={provided.innerRef}
        {...provided.draggableProps}
        {...provided.dragHandleProps}
        className={classes.columnsColumnEntry}
        style={provided.draggableProps.style}
      >
        <div className={classes.columnsEntryDragIndicator}>
          <Icon size="small" color="inherit">
            {iconNames.drag_indicator}
          </Icon>
        </div>
        <div>{item.label}</div>
        {isSortable && (
          <div className={classes.columnsEntrySorting}>
            <IconButton
              onClick={() => setSettingSort(true)}
              disableRipple={true}
              size="small"
            >
              <Tooltip
                enterDelay={200}
                title={t('editForm.fields.columns.sortOn', {
                  column: item.label,
                })}
              >
                <Icon size="small" color="inherit">
                  {iconType}
                </Icon>
              </Tooltip>
            </IconButton>
          </div>
        )}

        <IconButton
          onClick={() => onDelete(index)}
          disableRipple={true}
          size="small"
          classes={{
            root: classes.rightCornerDeleteButton,
          }}
        >
          <Icon size="extraSmall" color="inherit">
            {iconNames.delete}
          </Icon>
        </IconButton>
      </div>
    </>
  );
};

export default ColumnEntry;

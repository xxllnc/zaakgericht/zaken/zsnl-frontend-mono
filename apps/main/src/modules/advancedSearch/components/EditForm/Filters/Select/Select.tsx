// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import { Field } from 'formik';
import UISelect from '@mintlab/ui/App/Zaaksysteem/Select';
import { useStyles } from './Select.styles';

type FilterSelectPropsType = {
  filter: any;
  name: string;
  validate: any;
  t: i18next.TFunction;
  choices: any[];
};

const PassThrough = (props: any) => {
  const { field } = props;
  return <UISelect variant="generic" {...props} {...field} />;
};

const Select: FunctionComponent<FilterSelectPropsType> = ({
  filter,
  name,
  validate = null,
  t,
  choices,
}) => {
  const classes = useStyles();
  return (
    <div className={classes.wrapper}>
      <Field
        name={name}
        validate={validate}
        component={PassThrough}
        choices={choices}
        nestedValue={true}
      />
    </div>
  );
};

export default Select;

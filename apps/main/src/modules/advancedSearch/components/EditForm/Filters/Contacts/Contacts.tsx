// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent, useState } from 'react';
import * as i18next from 'i18next';
import { Field } from 'formik';
import Typography from '@mui/material/Typography';
//@ts-ignore
import Select, {
  renderOptionWithIcon,
  renderTagsWithIcon,
} from '@mintlab/ui/App/Zaaksysteem/Select';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import { ContactType, FilterType } from '../../../../AdvancedSearch.types';
import { getTypeChoices, useContactsChoicesQuery } from './Contacts.library';
import { useStyles } from './Contacts.styles';
import { validateFunc } from './Contacts.library';

type FilterSelectPropsType = {
  filter: FilterType;
  name: string;
  validate: any;
  t: i18next.TFunction;
  choices: any[];
  translations: { [key: string]: string };
};

type ContactsCmpPropsType = {
  showContactTypes: boolean;
  key: string;
  field: any;
} & Pick<FilterSelectPropsType, 'translations' | 't'>;

export type SearchType = ContactType | 'all';

const Contacts: FunctionComponent<FilterSelectPropsType> = ({
  filter,
  name,
  validate = null,
  t,
  translations,
  ...rest
}) => (
  <div style={{ width: '100%' }}>
    <Field
      name={name}
      key={filter.uuid}
      validate={validateFunc(filter, translations)}
      component={ContactsCmp}
      translations={translations}
      t={t}
      {...rest}
    />
  </div>
);

const ContactsCmp: FunctionComponent<ContactsCmpPropsType> = ({
  showContactTypes,
  translations,
  field,
  t,
  key,
}) => {
  const classes = useStyles();
  const typeChoices = getTypeChoices(t);
  const [searchType, setSearchType] = useState<SearchType>(
    typeChoices[0].value
  );

  const [selectProps] = useContactsChoicesQuery({
    searchType: showContactTypes ? searchType : 'employee',
    field,
  });

  const getIcon = (option: any) =>
    option.type === 'organization' ? iconNames.domain : iconNames.person;

  return (
    <div className={classes.wrapper}>
      {showContactTypes && (
        <div className={classes.typesWrapper}>
          <div className={classes.label}>
            <Typography variant="subtitle1">
              {t('searchIn') as string}
            </Typography>
          </div>
          <div className={classes.typeSelect}>
            <Select
              name="searchType"
              variant="generic"
              choices={typeChoices}
              onChange={(event: any) => setSearchType(event.target.value)}
              value={searchType}
              nestedValue={true}
            />
          </div>
        </div>
      )}
      <div>
        <Select
          {...field}
          {...selectProps}
          renderOption={renderOptionWithIcon(getIcon)}
          renderTags={renderTagsWithIcon(getIcon)}
          placeholder={translations['form:choose']}
        />
      </div>
    </div>
  );
};

export default Contacts;

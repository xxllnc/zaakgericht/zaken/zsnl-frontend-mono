// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import TextField from '@mintlab/ui/App/Material/TextField';
import { makeStyles } from '@mui/styles';
import { Field } from 'formik';
import { FilterNamesType, FilterType } from '../../../../AdvancedSearch.types';

type FilterTextPropsType = {
  filter: FilterType;
  name: FilterNamesType;
  t: i18next.TFunction;
  placeholder: any;
  validate: any;
};

const TextFieldPassThrough = (props: any) => {
  const { field } = props;
  return <TextField {...props} {...field} />;
};

const Text = ({ name, placeholder, validate }: FilterTextPropsType) => {
  const theseStyles = useStyles();
  return (
    <div className={theseStyles.wrapper}>
      <Field
        name={name}
        component={TextFieldPassThrough}
        {...(validate && { validate })}
        {...(placeholder && { placeholder })}
      />
    </div>
  );
};

const useStyles = makeStyles(({ mintlab: { greyscale } }: any) => {
  return {
    wrapper: {
      width: '100%',
      backgroundColor: greyscale.light,
    },
  };
});

export default Text;

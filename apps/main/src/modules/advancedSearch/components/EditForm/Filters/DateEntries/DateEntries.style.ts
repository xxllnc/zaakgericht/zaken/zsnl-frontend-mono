// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useStyles = makeStyles(
  ({ typography, mintlab: { greyscale }, palette: { error } }: Theme) => ({
    dialogWrapper: {
      '& .MuiPickersBasePicker-pickerView': {
        alignSelf: 'center',
      },
      height: 450,
      width: 480,
      display: 'flex',
      flexDirection: 'column',
      overflow: 'hidden',
    },
    actionButtons: {
      display: 'flex',
      padding: 12,
      justifyContent: 'flex-end',
      '&>:nth-child(1)': {
        marginRight: 12,
      },
    },
    rangeSwitch: {
      height: 50,
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
      '&>:nth-child(1)': {
        marginRight: 12,
      },
    },
    rangeWrapper: {
      height: 600,
      width: 740,
      display: 'flex',
      padding: 20,
      flexDirection: 'column',
    },
    rangeSplits: {
      display: 'flex',
      flexDirection: 'row',
      flex: 1,
      '&>:nth-child(1)': {
        marginRight: 22,
      },
      justifyContent: 'center',
    },
    rangeSplit: {
      display: 'flex',
      flexDirection: 'column',
    },
    rangeErrorWrapper: {
      height: 60,
      padding: 10,
      display: 'flex',
    },
    rangeError: {
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
      padding: 8,
      backgroundColor: error.main,
      color: 'white',
      borderRadius: 8,
      marginLeft: 'auto',
    },
    rangeKindLabel: {
      flex: 1,
    },
    rangeTime: {
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'center',
      marginBottom: 20,
      height: 50,
    },
    rangeTimeWrapper: {
      backgroundColor: greyscale.light,
      width: 140,
      borderRadius: 8,
    },
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      margin: '20px 0px 20px 0px',
      width: '100%',
    },
    entryWrapper: {
      display: 'flex',
      marginBottom: 8,
    },
    entryContent: {
      flex: 1,
      display: 'flex',
      '&>:nth-child(2)': {
        flex: 1,
      },
    },
    entryClose: {
      width: 30,
      marginLeft: 6,
      alignItems: 'center',
      justifyContent: 'center',
      display: 'flex',
    },
    entryDateTimeDuration: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    operator: {
      padding: 6,
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      '&>:nth-child(1)': {
        width: 200,
      },
    },
    relativeWrapper: {
      display: 'flex',
      height: '100%',
      flexDirection: 'column',
      padding: 24,
      '& h6': {
        fontWeight: typography.fontWeightBold,
      },
    },
    manual: {
      height: 60,
      display: 'flex',
      alignItems: 'center',
      justifyItems: 'flex-end',
      marginBottom: 24,
    },
    common: {
      padding: '8px 0px 0px 0px',
      flex: 1,
      display: 'flex',
      '& ul': {
        listStyleType: 'none',
        margin: 0,
        padding: 0,
        width: '100%',
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'wrap',
        height: '230px',
      },
      '& li': {
        marginBottom: 14,
        width: '45%',
      },
    },
    nr: {
      width: 60,
      marginRight: 12,
    },
    type: {
      width: 174,
    },
    in: {
      width: 40,
      textAlign: 'center',
    },
    period: {
      width: 180,
    },
    okButton: {
      flex: 1,
    },
    addDropdownMenu: {
      marginBottom: 20,
      display: 'flex',
      justifyContent: 'flex-end',
    },
  })
);

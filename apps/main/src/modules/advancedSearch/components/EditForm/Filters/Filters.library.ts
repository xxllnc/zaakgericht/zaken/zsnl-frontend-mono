// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import {
  FilterNamesType,
  KindType,
  FilterType,
  ErrorType,
} from '../../../AdvancedSearch.types';
import { hasTextValue } from '../../../library/validations';

// to Select choices
type OptionType = {
  label: string;
  value: FilterNamesType;
};

export const hasNoValue = (value: any) =>
  typeof value === 'undefined' ||
  value === null ||
  JSON.stringify(value) === '{}' ||
  value === '';

// Determines which filters are allowed to be added

const TRANSLATION_BASE = 'editForm.fields.filters.';

export const getFiltersChoices = ({
  t,
  kind,
}: {
  t: i18next.TFunction;
  kind: KindType;
}): OptionType[] => {
  let options: any;
  if (kind === 'custom_object') {
    options = [
      [t(`${TRANSLATION_BASE}fields.keyword.label`), 'keyword'],
      [
        t(`${TRANSLATION_BASE}fields.modified.label`),
        'attributes.last_modified',
      ],
      [t(`${TRANSLATION_BASE}fields.status.label`), 'attributes.status'],
      [
        t(`${TRANSLATION_BASE}fields.archiveStatus.label`),
        'attributes.archive_status',
      ],
    ];
  } else if (kind === 'case') {
    options = [
      [t(`${TRANSLATION_BASE}fields.keyword.label`), 'keyword'],
      [
        t(`${TRANSLATION_BASE}fields.requestor.label`),
        'relationship.requestor.id',
      ],
      [
        t(`${TRANSLATION_BASE}fields.assignee.label`),
        'relationship.assignee.id',
      ],
    ];
  } else {
    options = [];
  }

  const defaultOptions: OptionType[] = options.map((entry: any[]) => ({
    label: entry[0],
    value: entry[1],
  }));

  return [...defaultOptions];
};

const validateText =
  (filter: FilterType, errorMessage: string) =>
  (value: any): ErrorType | boolean => {
    const { uuid } = filter;
    if (value === null || value === undefined) return false;
    return hasTextValue(value)
      ? false
      : {
          uuid,
          value: errorMessage,
        };
  };

/* eslint complexity: [2, 12] */
export const getFieldProps = ({
  t,
  filter,
}: {
  t: any;
  filter: FilterType;
}) => {
  if (!filter) return {};
  const { type, uuid, parameters } = filter;

  switch (type) {
    case 'keyword': {
      const translatedLabel = t(t(`${TRANSLATION_BASE}fields.keyword.label`));
      return {
        validate: validateText(
          filter,
          t(`${TRANSLATION_BASE}fields.keyword.errorMessage`, {
            name: translatedLabel,
          })
        ),
      };
    }
    case 'attributes.status': {
      return {
        choices: ['active', 'inactive', 'draft'].map((entry: string) => ({
          label: t(`${TRANSLATION_BASE}fields.status.choices.${entry}`),
          value: entry,
        })),
        validate: (value: any): ErrorType | undefined =>
          hasNoValue(value)
            ? {
                value: t(`${TRANSLATION_BASE}fields.status.errorMessage`),
                uuid,
              }
            : undefined,
        placeholder: t(`${TRANSLATION_BASE}fields.status.placeholder`),
      };
    }
    case 'attributes.archive_status': {
      const choices = [
        [
          t(`${TRANSLATION_BASE}fields.archiveStatus.choices.archived`),
          'archived',
        ],
        [
          t(`${TRANSLATION_BASE}fields.archiveStatus.choices.toDestroy`),
          'to destroy',
        ],
        [
          t(`${TRANSLATION_BASE}fields.archiveStatus.choices.toPreserve`),
          'to preserve',
        ],
      ];
      return {
        choices: choices.map((entry: any[]) => ({
          label: entry[0],
          value: entry[1],
        })),
        validate: (value: any): ErrorType | undefined =>
          hasNoValue(value)
            ? {
                value: t(
                  `${TRANSLATION_BASE}fields.archiveStatus.errorMessage`
                ),
                uuid,
              }
            : undefined,
        placeholder: `${TRANSLATION_BASE}fields.archiveStatus.placeholder`,
      };
    }
    case 'attributes.archival_state': {
      const choices = [
        [
          t(`${TRANSLATION_BASE}fields.archivalState.choices.vernietigen`),
          'vernietigen',
        ],
        [
          t(`${TRANSLATION_BASE}fields.archivalState.choices.overdragen`),
          'overdragen',
        ],
      ];
      return {
        choices: choices.map((entry: any[]) => ({
          label: entry[0],
          value: entry[1],
        })),
        validate: (value: any) =>
          !value
            ? {
                uuid,
                value: t(
                  `${TRANSLATION_BASE}fields.archiveStatus.errorMessage`
                ),
              }
            : undefined,
        placeholder: `${TRANSLATION_BASE}fields.archiveStatus.placeholder`,
        nestedValue: true,
      };
    }
    case 'relationship.assignee.id': {
      return {
        showContactTypes: false,
        translations: {
          'form:choose': t(`${TRANSLATION_BASE}fields.assignee.placeholder`),
          errorMessage: t(`${TRANSLATION_BASE}fields.assignee.errorMessage`),
        },
      };
    }
    case 'relationship.requestor.id': {
      return {
        showContactTypes: true,
        translations: {
          'form:choose': t(`${TRANSLATION_BASE}fields.requestor.placeholder`),
          errorMessage: t(`${TRANSLATION_BASE}fields.requestor.errorMessage`),
        },
      };
    }
    case 'custom_field':
      {
        const { type } = parameters;

        if (type === 'text') {
          return {
            placeholder: t(`${TRANSLATION_BASE}types.text.placeholder`),
            validate: validateText(
              filter,
              t(`${TRANSLATION_BASE}types.text.errorMessage`)
            ),
          };
        }
      }
      break;
    default:
      return {};
  }
};

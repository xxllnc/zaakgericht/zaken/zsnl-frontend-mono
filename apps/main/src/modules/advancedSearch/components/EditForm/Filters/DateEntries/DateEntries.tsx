// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef, FunctionComponent, useState } from 'react';
import { FormikProps } from 'formik';
import * as i18next from 'i18next';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { Field } from 'formik';
import {
  DateEntryType,
  DateEntryTypeAbsolute,
  DateEntryTypeRelative,
  DateEntryTypeRange,
  ErrorType,
  FilterType,
} from '../../../../AdvancedSearch.types';
import { validateForm } from '../../EditForm.library';
import { EditFormStateType } from '../../../../AdvancedSearch.types';
import { hasNoValue } from '../Filters.library';
import { useStyles } from './DateEntries.style';
import AbsoluteDialog from './components/AbsoluteDialog';
import RelativeDialog from './components/RelativeDialog';
import RangeDialog from './components/RangeDialog';
import {
  hasValue,
  defaultDialogProperties,
  TRANSLATION_BASE,
} from './DateEntries.library';
import Entries from './components/Entries';
import AddDropdownMenu from './components/AddDropdownMenu';
import { DialogPropertiesType } from './DateEntries.types';

type DateEntriesPropsType = {
  values: any;
  index: number;
  formik: FormikProps<EditFormStateType>;
  t: i18next.TFunction;
  filter: FilterType;
  name: string;
};

const DateEntries: FunctionComponent<DateEntriesPropsType> = ({
  values,
  index,
  formik,
  t,
  filter,
  name,
}) => {
  const { uuid } = filter;
  const classes = useStyles();
  const helpersRef = useRef<HTMLInputElement | null>(null);
  const [dialogProperties, setDialogProperties] = useState(
    defaultDialogProperties as DialogPropertiesType
  );

  const insertOrSetFieldValue = (valuesObj: DateEntryType) => {
    if (hasValue(dialogProperties.index)) {
      formik.setFieldValue(`${name}.[${dialogProperties.index}]`, valuesObj);
    } else {
      //@ts-ignore
      helpersRef.current.insert(0, valuesObj);
    }
  };

  const isOpen = (type: DateEntryType['type']) =>
    dialogProperties?.open === true && dialogProperties?.entry?.type === type;

  return (
    <>
      <AbsoluteDialog
        open={isOpen('absolute')}
        t={t}
        entry={dialogProperties?.entry as DateEntryTypeAbsolute}
        onClose={() => setDialogProperties(defaultDialogProperties)}
        onSubmit={entry => insertOrSetFieldValue(entry)}
      />
      <RelativeDialog
        open={isOpen('relative')}
        t={t}
        entry={dialogProperties?.entry as DateEntryTypeRelative}
        onClose={() => setDialogProperties(defaultDialogProperties)}
        onSubmit={entry => insertOrSetFieldValue(entry)}
      />
      <RangeDialog
        open={isOpen('range')}
        t={t}
        entry={dialogProperties?.entry as DateEntryTypeRange}
        onClose={() => setDialogProperties(defaultDialogProperties)}
        onSubmit={entry => insertOrSetFieldValue(entry)}
      />
      <div className={classes.wrapper}>
        <AddDropdownMenu
          t={t}
          setDialogProperties={setDialogProperties}
          classes={classes}
        />
        <Field
          component={Entries}
          name={name}
          validate={(value: any): ErrorType | undefined => {
            const isUnpopulatedArray = !isPopulatedArray(value);
            return hasNoValue(value) || isUnpopulatedArray
              ? {
                  value: t(`${TRANSLATION_BASE}errors.minimumEntries`),
                  uuid,
                }
              : undefined;
          }}
          values={values}
          classes={classes}
          t={t}
          setDialogProperties={setDialogProperties}
          validateForm={validateForm}
          index={index}
          helpersRef={helpersRef}
          setHelpersRef={(ref: HTMLInputElement | null) => {
            if (ref) helpersRef.current = ref;
          }}
        />
      </div>
    </>
  );
};

export default DateEntries;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import { APICaseManagement } from '@zaaksysteem/generated';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { useQuery } from '@tanstack/react-query';
import { useDebouncedCallback } from 'use-debounce';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl } from '@mintlab/kitchen-sink/source';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
//@ts-ignore
import { hasNoValue } from '../Filters.library';
import { FilterType } from '../../../../AdvancedSearch.types';
import { SearchType } from './Contacts';

export const getTypeChoices = (
  t: i18next.TFunction
): ValueType<SearchType>[] => [
  {
    label: t('contacts.all') as string,
    value: 'all',
  },
  {
    label: t('contacts.employee') as string,
    value: 'employee',
  },
  {
    label: t('contacts.person') as string,
    value: 'person',
  },
  {
    label: t('contacts.organization') as string,
    value: 'organization',
  },
];

export const validateFunc =
  (filter: FilterType, translations: any) => (value: any) => {
    const isUnpopulatedArray = !isPopulatedArray(value);
    return hasNoValue(value) || isUnpopulatedArray
      ? {
          value: translations.errorMessage,
          uuid: filter.uuid,
        }
      : undefined;
  };

export const useContactsChoicesQuery = ({ searchType, field }: any) => {
  const [input, setInput] = React.useState<string | undefined>(undefined);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const enabled = Boolean(input && input?.length > 2);

  const data = useQuery(
    ['contactFinder', input, searchType],
    async ({ queryKey: [__, keyword, searchType] }) => {
      const result = await request<APICaseManagement.SearchResponseBody>(
        'GET',
        buildUrl<any>('/api/v2/cm/search', {
          keyword,
          type:
            searchType === 'all'
              ? ['person', 'organization', 'employee']
              : [searchType],
          page: 1,
          page_size: 50,
        })
      ).catch(openServerErrorDialog);

      if (!result || !result.data || result.data.length === 0) return [];

      return result.data.map(result => ({
        label: result?.meta?.summary || '',
        value: result.id,
        type: (result.attributes.result_type as SearchType) || '',
      }));
    },
    { enabled }
  );

  const [setInputDebounced] = useDebouncedCallback(
    async (val: any) => setInput(val),
    400
  );

  const selectProps = {
    onInputChange: (ev: any, val: any) => setInputDebounced(val),
    onChange: (event: any) => {
      field.onChange({
        ...event,
        target: {
          ...event.target,
          value: event.target.value.map((val: any) =>
            cloneWithout(val, 'icon')
          ),
        },
      });
    },
    loading: data.status === 'loading' && enabled,
    choices: data.data || [],
    isMulti: true,
  };

  return [selectProps, ServerErrorDialog] as const;
};

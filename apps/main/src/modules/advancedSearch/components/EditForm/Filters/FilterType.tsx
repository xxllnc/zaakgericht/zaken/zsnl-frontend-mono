// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import { FormikProps } from 'formik';
//@ts-ignore
import objectScan from 'object-scan';
//@ts-ignore
import { get } from '@mintlab/kitchen-sink/source';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import IconButton from '@mui/material/IconButton';
import {
  FilterNamesType,
  FilterType,
  EditFormStateType,
  ClassesType,
  ErrorType,
} from '../../../AdvancedSearch.types';
import { filterTranslationKeys } from '../../../library/config';
import { validateForm } from '../EditForm.library';
import Text from './Text/Text';
import DateEntries from './DateEntries/DateEntries';
import Select from './Select/Select';
import CustomField from './CustomField/CustomField';
import { getFieldProps } from './Filters.library';
import Contacts from './Contacts/Contacts';

type KeysType = Partial<Record<FilterNamesType, any>>;
type FilterTypePropsType = {
  filter: FilterType;
  index: number;
  arrayHelpers: any;
  values: EditFormStateType;
  errors: any;
  classes: ClassesType;
  t: i18next.TFunction;
  formik: FormikProps<EditFormStateType>;
  name: string;
};

const getComponent = (filter: FilterType) => {
  if (filter.type === 'custom_field') {
    return CustomField;
  } else {
    const typeMap: KeysType = {
      keyword: Text,
      'attributes.last_modified': DateEntries,
      'attributes.status': Select,
      'attributes.archive_status': Select,
      'attributes.archival_state': Select,
      'relationship.assignee.id': Contacts,
      'relationship.requestor.id': Contacts,
    };

    //@ts-ignore
    return typeMap[filter.type];
  }
};

// Main selection/entrypoint for a main level filter
const FilterTypeComponent: FunctionComponent<FilterTypePropsType> = ({
  filter,
  index,
  arrayHelpers,
  values,
  errors,
  classes,
  t,
  formik,
  name,
}) => {
  const { type, uuid } = filter;
  const Component = getComponent(filter);
  const baseName = `${name}.[${index}]`;

  let errorObj: ErrorType | undefined;

  const errorPath = objectScan(['**', 'uuid'], {
    joined: true,
    filterFn: ({ value }: any) => value?.uuid === uuid,
  })(errors);
  if (errorPath && errorPath.length) {
    errorObj = get(errors, errorPath[0]);
  }

  const fieldProps = getFieldProps({ t, filter });

  const getName = () => {
    switch (type) {
      case 'custom_field':
        return baseName;
      case 'relationship.assignee.id':
      case 'relationship.requestor.id':
        return `${baseName}.parameters`;
      default:
        return `${baseName}.parameters.value`;
    }
  };

  const FilterComponent = Component ? (
    <Component
      name={getName()}
      index={index}
      filter={filter}
      values={values}
      arrayHelpers={arrayHelpers}
      t={t}
      formik={formik}
      {...fieldProps}
    />
  ) : (
    <div>type not supported yet: {filter.type}</div>
  );

  const getLabel = (): string => {
    if (filter.type === 'custom_field') {
      return filter.parameters.magicString;
    } else {
      //@ts-ignore
      const translatedLabel = filterTranslationKeys[filter.type];
      return t(
        `editForm.fields.filters.fields.${translatedLabel || filter.type}.label`
      );
    }
  };

  return (
    <div className={classes.filterWrapper}>
      <p className={classes.filterName}>{getLabel()}</p>
      {FilterComponent}
      <IconButton
        onClick={() => {
          arrayHelpers.remove(index);
          validateForm(formik);
        }}
        disableRipple={true}
        size="small"
        classes={{
          root: classes.rightCornerDeleteButton,
        }}
      >
        <Icon size="extraSmall" color="inherit">
          {iconNames.delete}
        </Icon>
      </IconButton>
      {errorObj && <div className={classes.filterError}>{errorObj.value}</div>}
    </div>
  );
};

export default FilterTypeComponent;

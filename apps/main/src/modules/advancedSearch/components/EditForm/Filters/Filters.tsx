// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import * as i18next from 'i18next';
import { FieldArray, FormikProps } from 'formik';
import classNames from 'classnames';
import { FiltersSelect } from '../FiltersSelect/FiltersSelect';
import {
  FilterType,
  KindType,
  ClassesType,
  EditFormStateType,
} from '../../../AdvancedSearch.types';
import FilterTypeComponent from './FilterType';
import CustomFieldsOperatorSelect from './CustomFieldsOperatorSelect/CustomFieldsOperatorSelect';

type FiltersPropsType = {
  kind: KindType;
  formik: FormikProps<EditFormStateType>;
  classes: ClassesType;
  show: boolean;
  t: i18next.TFunction;
  [key: string]: any;
};

// This is the main FieldArray
/* eslint complexity: [2, 12] */
const Filters = ({
  kind,
  formik,
  classes,
  show = true,
  t,
  openServerErrorDialog,
}: FiltersPropsType) => {
  const { errors, values } = formik;
  const [arrayHelpersRef, setArrayHelpersRef] = useState<any | null>(null);
  const filters = values.filters?.filters;
  const hasFilters = Boolean(filters && filters.length > 0);
  const objectTypeSelected = Boolean(values.selectedObjectType);
  const showFilters =
    (kind === 'custom_object' && objectTypeSelected) || kind === 'case';
  const showFiltersSelect =
    (kind === 'custom_object' && Boolean(values.selectedObjectType)) ||
    kind === 'case';
  const name = 'filters.filters';

  const hasCustomFieldFilters =
    hasFilters && filters?.some(filter => filter.type === 'custom_field');
  const showOperatorSelect = showFilters && hasCustomFieldFilters;

  return (
    <div
      className={classNames(classes.filtersWrapper, {
        [classes.show]: show,
        [classes.hide]: !show,
      })}
    >
      {showFiltersSelect && (
        <FiltersSelect
          classes={classes}
          arrayHelpersRef={arrayHelpersRef}
          t={t}
          kind={kind}
          formik={formik}
          openServerErrorDialog={openServerErrorDialog}
        />
      )}
      {showOperatorSelect && (
        <CustomFieldsOperatorSelect classes={classes} t={t} />
      )}

      {showFilters && (
        <div className={classes.editFormSection}>
          <span
            className={classNames(classes.filterRowLabel, classes.selfAlignTop)}
          >
            {t('editForm.fields.filters.label') + ':'}
          </span>
          <div className={classes.filterRowContent}>
            <FieldArray
              name={name}
              render={arrayHelpers => {
                if (!arrayHelpersRef) setArrayHelpersRef(arrayHelpers);
                return (
                  <div>
                    {hasFilters ? (
                      values?.filters?.filters?.map(
                        (filter: FilterType, index: number) => {
                          return (
                            <FilterTypeComponent
                              key={`filter-${index}`}
                              name={name}
                              filter={filter}
                              index={index}
                              arrayHelpers={arrayHelpers}
                              values={values}
                              formik={formik}
                              errors={errors}
                              classes={classes}
                              t={t}
                            />
                          );
                        }
                      )
                    ) : (
                      <div>
                        {t('editForm.fields.filters.noFiltersLabel') as string}
                      </div>
                    )}
                  </div>
                );
              }}
            />
          </div>
        </div>
      )}
    </div>
  );
};

export default Filters;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent, useState, useEffect } from 'react';
import set from 'date-fns/set';
import * as i18next from 'i18next';
import compareAsc from 'date-fns/compareAsc';
import fecha from 'fecha';
import Button from '@mintlab/ui/App/Material/Button';
import Typography from '@mui/material/Typography';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import {
  Dialog as UIDialog,
  DialogTitle,
} from '@mintlab/ui/App/Material/Dialog';
//@ts-ignore
import Switch from '@mintlab/ui/App/Material/Switch';
//@ts-ignore
import { StaticDatePicker } from '@mintlab/ui/App/Material/DatePicker';
//@ts-ignore
import { TimePicker } from '@mintlab/ui/App/Material/DatePicker';
import { useStyles } from '../DateEntries.style';
import { TRANSLATION_BASE } from '../DateEntries.library';
import {
  DateEntryType,
  DateEntryTypeRange,
} from '../../../../../AdvancedSearch.types';

type RangeDialogPropsType = {
  onSubmit: (entry: DateEntryTypeRange) => void;
  onClose: () => void;
  entry: DateEntryType;
  open: boolean;
  t: i18next.TFunction;
};

const RangeDialog: FunctionComponent<RangeDialogPropsType> = ({
  onSubmit,
  entry,
  open,
  onClose,
  t,
}) => {
  const [includeTime, setIncludeTime] = useState<boolean>(false);
  const [startTime, setStartTime] = useState<string | null>(null); // full iso string
  const [startDate, setStartDate] = useState<string | null>(null); //date string
  const [endTime, setEndTime] = useState<string | null>(null); // full iso string
  const [endDate, setEndDate] = useState<string | null>(null); // date string
  const classes = useStyles();

  useEffect(() => {
    if (open === true) {
      if (entry?.type === 'range' && entry?.startValue && entry?.endValue) {
        const startDateParsed = new Date(Date.parse(entry.startValue));
        const endDateParsed = new Date(Date.parse(entry.endValue));

        setIncludeTime(entry?.timeSetByUser === true);
        setStartDate(fecha.format(startDateParsed, 'YYYY-MM-DD'));
        setEndDate(fecha.format(endDateParsed, 'YYYY-MM-DD'));
        setStartTime(entry.startValue);
        setEndTime(entry.endValue);
      } else {
        setStartDate(new Date().toISOString());
        setEndDate(new Date().toISOString());
        setStartTime(null);
        setEndTime(null);
        setIncludeTime(false);
      }
    }
  }, [open]);

  const getValues = (): DateEntryTypeRange => {
    const startDateObj = new Date(startDate as string);
    const endDateObj = new Date(endDate as string);
    let startValue;
    let endValue;
    if (startTime) {
      const startTimeObj = new Date(startTime);
      startValue = set(new Date(startDateObj), {
        hours: startTimeObj.getHours(),
        minutes: startTimeObj.getMinutes(),
        seconds: 0,
        milliseconds: 0,
      }).toISOString();
    } else {
      startValue = set(new Date(startDateObj), {
        hours: 0,
        minutes: 0,
        seconds: 0,
        milliseconds: 0,
      }).toISOString();
    }

    if (endTime) {
      const endTimeObj = new Date(endTime);
      endValue = set(new Date(endDateObj), {
        hours: endTimeObj.getHours(),
        minutes: endTimeObj.getMinutes(),
        seconds: 0,
        milliseconds: 0,
      }).toISOString();
    } else {
      endValue = set(new Date(endDateObj), {
        hours: 0,
        minutes: 0,
        seconds: 0,
        milliseconds: 0,
      }).toISOString();
    }

    return {
      type: 'range',
      startValue,
      endValue,
      timeSetByUser: includeTime,
    };
  };

  /* eslint complexity: [2, 12] */
  const getError = (t: i18next.TFunction): Boolean | String => {
    if (!startDate || !endDate) return true;
    if (includeTime && (!startTime || !endTime)) return true;

    const values = getValues();
    const compare = compareAsc(
      new Date(Date.parse(values.startValue as string)),
      new Date(Date.parse(values.endValue as string))
    );
    if (includeTime && (compare === 0 || compare === 1)) {
      return t(`${TRANSLATION_BASE}errors.timeRange`);
    }
    if (!includeTime && compare === 1) {
      return t(`${TRANSLATION_BASE}errors.dateRange`);
    }
    return false;
  };
  const error = getError(t);

  return (
    <React.Fragment>
      {/* @ts-ignore */}
      <UIDialog onClose={onClose} open={open} maxWidth={false}>
        <DialogTitle
          title={t(`${TRANSLATION_BASE}titles.range`)}
          icon="today"
        />
        <Divider />
        <div className={classes.rangeSwitch}>
          <Typography variant="subtitle1">
            {t(`${TRANSLATION_BASE}includeTimes`) as string}
          </Typography>
          <Switch
            checked={includeTime}
            onChange={(event: React.ChangeEvent<any>) =>
              setIncludeTime(event.target.checked)
            }
          />
        </div>
        <Divider />
        <DialogContent>
          <div className={classes.rangeWrapper}>
            <div className={classes.rangeSplits}>
              <div className={classes.rangeSplit}>
                <div className={classes.rangeTime}>
                  <div className={classes.rangeKindLabel}>
                    <Typography variant="h5">
                      {t(`${TRANSLATION_BASE}from`) as string}
                    </Typography>
                  </div>
                  <div className={classes.rangeTimeWrapper}>
                    {includeTime ? (
                      <TimePicker
                        value={startTime}
                        onChange={(value: any) => {
                          const dateObj = new Date(value);
                          setStartTime(dateObj.toISOString());
                        }}
                        ampm={false}
                        placeholder={t(`${TRANSLATION_BASE}placeholders.time`)}
                      />
                    ) : null}
                  </div>
                </div>
                <div>
                  <StaticDatePicker
                    value={startDate}
                    onChange={(event: any) => setStartDate(event.target.value)}
                  />
                </div>
              </div>
              <div className={classes.rangeSplit}>
                <div className={classes.rangeTime}>
                  <div className={classes.rangeKindLabel}>
                    <Typography variant="h5">
                      {t(`${TRANSLATION_BASE}untill`) as string}
                    </Typography>
                  </div>
                  <div className={classes.rangeTimeWrapper}>
                    {includeTime ? (
                      <TimePicker
                        value={endTime}
                        onChange={(value: any) => {
                          const dateObj = new Date(value);
                          setEndTime(dateObj.toISOString());
                        }}
                        ampm={false}
                        placeholder={t(`${TRANSLATION_BASE}placeholders.time`)}
                      />
                    ) : null}
                  </div>
                </div>
                <div>
                  <StaticDatePicker
                    value={endDate}
                    onChange={(event: any) => setEndDate(event.target.value)}
                    {...(startDate && { minDate: startDate })}
                  />
                </div>
              </div>
            </div>
            <div className={classes.rangeErrorWrapper}>
              {error && typeof error === 'string' ? (
                <div className={classes.rangeError}>{error}</div>
              ) : null}
            </div>
          </div>
        </DialogContent>
        <Divider />
        <div className={classes.actionButtons}>
          <Button
            name="cancelDateEntiries"
            action={() => {
              onClose();
            }}
          >
            {t(`${TRANSLATION_BASE}cancelButton`)}
          </Button>
          <Button
            name="submitDateEntiries"
            disabled={Boolean(error)}
            action={() => {
              onSubmit(getValues());
              onClose();
            }}
          >
            {t(`${TRANSLATION_BASE}okButton`)}
          </Button>
        </div>
      </UIDialog>
    </React.Fragment>
  );
};

export default RangeDialog;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import { useDebouncedCallback } from 'use-debounce';
import { useQuery } from '@tanstack/react-query';
import { APICaseManagement } from '@zaaksysteem/generated';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { replaceKeysInJSON } from '../../library/library';
import { filtersKeyNamesReplacements } from '../../library/config';

type AttributeFinderPropsType = {
  handleSelectOnChange: (event: any) => void;
  name: string;
  t: i18next.TFunction;
  openServerErrorDialog: OpenServerErrorDialogType;
  filterOption?: any;
  selectKey: string;
};

type SearchResultAPIType = APICaseManagement.EntityAttributeSearchList;

const useAttributeChoicesQuery = () => {
  const [input, setInput] = React.useState<string | undefined>(undefined);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const enabled = Boolean(input && input.length > 2);

  const data = useQuery(
    ['attributes', input],
    async ({ queryKey: [__, keyword] }) => {
      const body = await request<SearchResultAPIType>(
        'GET',
        buildUrl<APICaseManagement.SearchAttributeRequestParams>(
          '/api/v2/cm/attribute/search',
          {
            'filter[keyword]': keyword,
            page: 1,
            page_size: 50,
          }
        )
      ).catch(openServerErrorDialog);

      return body
        ? (body.data || []).map(result => ({
            label: result.attributes.label,
            value: [
              'attributes',
              'custom_fields',
              result.attributes.magic_string,
            ].join('.'),
            data: replaceKeysInJSON(
              result.attributes,
              filtersKeyNamesReplacements.map((element: string[]) =>
                [...element].reverse()
              )
            ),
          }))
        : [];
    },
    { enabled }
  );

  const [setInputDebounced] = useDebouncedCallback(
    async (val: any) => setInput(val),
    400
  );

  const selectProps = {
    onInputChange: (ev: any, val: any, reason: any) =>
      ['input', 'clear'].includes(reason) && setInputDebounced(val),
    loading: data.status === 'loading' && enabled,
    choices: data.data || [],
  };

  return [selectProps, ServerErrorDialog] as const;
};

const AttributeFinder: FunctionComponent<AttributeFinderPropsType> = ({
  handleSelectOnChange,
  name,
  t,
  filterOption,
  selectKey,
}) => {
  const [selectProps, ServerErrorDialog] = useAttributeChoicesQuery();

  return (
    <>
      <Select
        {...selectProps}
        variant="generic"
        name={name}
        key={selectKey}
        isClearable={false}
        onChange={(event: React.ChangeEvent<any>) => {
          const data = event.target.value.data as SearchResultAPIType['data'];
          handleSelectOnChange({
            target: {
              ...event.target,
              value: {
                ...event.target.value,
                data,
              },
            },
          });
        }}
        placeholder={t('editForm.attributeFinder.placeholder')}
        {...(filterOption && { filterOption })}
      />
      {ServerErrorDialog}
    </>
  );
};

export default AttributeFinder;

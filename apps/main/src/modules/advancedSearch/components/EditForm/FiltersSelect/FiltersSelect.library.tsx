// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const getDefaultParameters = (type: string) => {
  switch (type) {
    case 'keyword':
      return {
        label: '',
        value: '',
      };
    case 'attributes.last_modified':
      return {
        label: '',
        value: null,
      };
    case 'relationship.assignee.id':
    case 'relationship.requestor.id':
      return [];
    default:
      return '';
  }
};

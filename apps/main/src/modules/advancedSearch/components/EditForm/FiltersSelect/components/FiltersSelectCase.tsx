// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent, useState } from 'react';
import * as i18next from 'i18next';
import { v4 } from 'uuid';
import Button from '@mintlab/ui/App/Material/Button';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { Field, FormikProps, FieldInputProps } from 'formik';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { getFiltersChoices } from '../../Filters/Filters.library';
import { getDefaultParameters } from '../FiltersSelect.library';
import AttributeFinder from '../../AttributeFinder';
import {
  FilterType,
  EditFormStateType,
} from '../../../../AdvancedSearch.types';
import { validateForm } from '../../EditForm.library';

type FiltersSelectCasePropsType = {
  t: i18next.TFunction;
  classes: any;
  arrayHelpers: any;
  formik: FormikProps<EditFormStateType>;
  openServerErrorDialog: OpenServerErrorDialogType;
};

type MethodType = 'systemAttributes' | 'searchAttributes';

const FiltersSelectCase: FunctionComponent<FiltersSelectCasePropsType> = ({
  t,
  classes,
  arrayHelpers,
  formik,
  openServerErrorDialog,
}) => {
  const [method, setMethod] = useState<MethodType>('systemAttributes');
  //  const { values } = formik;

  return (
    <>
      <div className={classes.filtersSelectCaseWrapper}>
        <div className={classes.filtersSelectCaseMethod}>
          {['systemAttributes', 'searchAttributes'].map(methodType => (
            <Button
              name="setMethod"
              key={methodType}
              action={() => setMethod(methodType as MethodType)}
              variant={methodType === method ? 'contained' : 'outlined'}
            >
              {t(`editForm.methods.${methodType}`)}
            </Button>
          ))}
        </div>
        <div className={classes.filtersSelectCaseSelectors}>
          <span className={classes.filterRowLabel}>
            {t('editForm.fields.filtersSelect.label') as string}
          </span>
          <div className={classes.filterRowContent}>
            {method === 'systemAttributes' && (
              <Field
                name="selectedFilter"
                component={SelectSystemAttributes}
                choices={getFiltersChoices({ t, kind: 'case' })}
                arrayHelpers={arrayHelpers}
                t={t}
              />
            )}
            {method === 'searchAttributes' && (
              <AttributeFinder
                selectKey={`filters-select-case-attribute-finder`}
                name={`filters-select-case-attribute-finder`}
                handleSelectOnChange={(event: React.ChangeEvent<any>) => {
                  const {
                    data: { magicString, type },
                  } = event.target.value;

                  const filterObj: FilterType = {
                    type: 'custom_field',
                    uuid: v4(),
                    parameters: {
                      operator: 'eq',
                      magicString,
                      type,
                      value: getDefaultParameters(type),
                    },
                  };
                  arrayHelpers.insert(0, filterObj);
                  validateForm(formik);
                }}
                t={t}
                openServerErrorDialog={openServerErrorDialog}
              />
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default FiltersSelectCase;

type SelectSystemAttributesPropsType = {
  arrayHelpers: any;
  name: string;
  choices: any;
  value: any;
  form: FormikProps<EditFormStateType>;
  field: FieldInputProps<any>;
  t: i18next.TFunction;
};
const SelectSystemAttributes: FunctionComponent<
  SelectSystemAttributesPropsType
> = ({ arrayHelpers, name, choices, value, form, field, t }) => {
  const { values } = form;
  const { onChange } = field;

  return (
    <div style={{ width: '100%' }}>
      <Select
        variant="generic"
        name={name}
        choices={choices}
        value={value}
        isClearable={true}
        onChange={(event: React.ChangeEvent<any>) => {
          onChange(event);
          const targetValue = event.target?.value?.value;
          if (!targetValue) return;
          const defaultParameters = getDefaultParameters(targetValue);
          arrayHelpers.insert(0, {
            type: targetValue,
            uuid: v4(),
            parameters: defaultParameters,
          });

          validateForm(form);
        }}
        filterOption={(option: any) => {
          if (!values.filters) return true;
          const existing = values.filters.filters.reduce(
            (acc: any, current: any) => {
              acc[acc.length] = current.type;
              return acc;
            },
            []
          );
          return !existing.includes(option.value);
        }}
        placeholder={t('editForm.fields.filters.addFilters.systemAttributes')}
      />
    </div>
  );
};

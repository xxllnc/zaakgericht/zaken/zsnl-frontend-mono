// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { FormikProps } from 'formik';
import { UseQueryResult } from '@tanstack/react-query';
import * as i18next from 'i18next';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { V2ServerErrorType } from '@zaaksysteem/common/src/types/ServerError';
import {
  ModeType,
  SavedSearchType,
  EditFormStateType,
  KindType,
  CustomFieldType,
  ColumnType,
} from '../../AdvancedSearch.types';
import { getDefaultColumns } from '../../library/config';
import { flat } from '../../library/library';

type GetInitialStateArgsType = {
  mode: ModeType;
  currentQuery: UseQueryResult<SavedSearchType, V2ServerErrorType>;
};

const newState: EditFormStateType = {
  name: '',
  selectedObjectType: null,
  selectedFilter: null,
  filters: {
    customFieldsOperator: 'and',
    filters: [],
  },
  permissions: [],
  columns: [],
  sortColumn: '',
  sortOrder: 'desc',
};

/* eslint complexity: [2, 12] */
export const getInitialState = ({
  mode,
  currentQuery,
}: GetInitialStateArgsType): EditFormStateType => {
  if (mode === 'edit') {
    const savedSearch = currentQuery.data as SavedSearchType;
    const { name, permissions, columns, sortColumn, sortOrder } = savedSearch;

    // ObjectType is not treated as a filter in the form, but rather
    // as a (seperate) required field, so they get split up here.
    const sanitizedFilters = savedSearch?.filters
      ? savedSearch.filters?.filters.filter(
          el => el.type !== 'relationship.custom_object_type'
        )
      : [];
    const objectTypeFilter = savedSearch?.filters?.filters.find(
      el => el.type === 'relationship.custom_object_type'
    );

    return savedSearch
      ? {
          name: name || '',
          selectedObjectType: objectTypeFilter
            ? (objectTypeFilter.parameters as ValueType<string>)
            : null,
          selectedFilter: null,
          filters: {
            ...savedSearch.filters,
            filters: sanitizedFilters,
          },
          permissions,
          columns,
          sortColumn,
          sortOrder,
        }
      : newState;
  } else {
    return newState;
  }
};

export const getColumns = (
  kind: KindType,
  customFields: CustomFieldType[] | null,
  t: i18next.TFunction
): ColumnType[] => {
  if (kind === 'custom_object') {
    // Custom fields columns
    const customFieldColumns =
      customFields && customFields.length
        ? customFields.map(field => ({
            label: field.label,
            type: field.type,
            source: ['attributes', 'custom_fields', field.magicString],
          }))
        : [];

    return [...getDefaultColumns(kind, t), ...customFieldColumns];
  } else {
    return [...getDefaultColumns(kind, t)];
  }
};

//TODO: from newstate
export const resetForm = (formik: FormikProps<EditFormStateType>) => {
  formik.setValues({
    ...formik.values,
    selectedFilter: null,
    filters: {
      customFieldsOperator: 'and',
      filters: [],
    },
    permissions: [],
    columns: [],
    sortColumn: newState.sortColumn,
    sortOrder: newState.sortOrder,
  });
};

export const validateForm = (formik: FormikProps<EditFormStateType>) => {
  setTimeout(() => {
    formik.validateForm();
  }, 75);
};

export const isFormValid = ({ saveMutation, formik }: any) => {
  if (saveMutation.isLoading) return true;
  const flattened = {};
  flat(formik.errors, flattened);
  return !Object.entries(flattened).length;
};

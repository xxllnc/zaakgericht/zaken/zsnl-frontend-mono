// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source';

export const getFormikPassThrough = (Component: any) => {
  return (props: any) => {
    const newProps = cloneWithout(props, 'field');
    const thisError = props?.form?.errors[props?.field?.name];
    return (
      <Component
        {...newProps}
        {...props?.field}
        {...(thisError && { error: thisError })}
      />
    );
  };
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent, Dispatch, SetStateAction } from 'react';
import { useNavigate } from 'react-router-dom';
import { matchPath } from 'react-router';
import * as i18next from 'i18next';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { useTheme } from '@mui/material';
import { QueryClient } from '@tanstack/react-query';
import Button from '@mintlab/ui/App/Material/Button';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { KindType, ClassesType, SnackType } from '../../AdvancedSearch.types';
import { useSavedSearchesQuery } from '../../query/useList';
import Row from './Row';

type LeftBarPropsType = {
  kind: KindType;
  classes: ClassesType;
  client: QueryClient;
  setSnack: Dispatch<SetStateAction<SnackType | null>>;
  t: i18next.TFunction;
  openServerErrorDialog: OpenServerErrorDialogType;
  prefix: string;
  params: any;
};

/*eslint complexity: [2, 12] */
const LeftBar: FunctionComponent<LeftBarPropsType> = ({
  kind,
  classes,
  client,
  setSnack,
  t,
  openServerErrorDialog,
  prefix,
  params,
}) => {
  const theme = useTheme();
  const navigate = useNavigate();
  const listQuery = useSavedSearchesQuery({
    kind,
    openServerErrorDialog,
  });
  const { data, hasNextPage, fetchNextPage } = listQuery;
  const match = matchPath(
    { path: `${prefix}/:kind/:mode/:identifier` },
    location.pathname
  );
  const isNew = params['*'] === 'new';

  return (
    <section className={classes.leftBar}>
      {listQuery.isLoading ? <Loader /> : null}
      <div className={classes.newSavedSearchWrapper}>
        <div>
          <Tooltip enterDelay={250} title={t('newSavedSearch')}>
            <Button
              action={() => {
                navigate('new');
              }}
              name="newSavedSearch"
              icon="add_circle_outline"
              sx={{ color: theme.palette.primary.dark }}
              disabled={isNew}
            />
          </Tooltip>
        </div>
      </div>

      {(data?.pages || []).map((pageGroup: any) =>
        pageGroup.data.map((entry: any) => (
          <Row
            data={entry}
            key={entry.uuid}
            classes={classes}
            client={client}
            setSnack={setSnack}
            t={t}
            identifier={match?.params?.identifier}
          />
        ))
      )}
      {listQuery.isFetching ? <Loader /> : null}
      {hasNextPage ? (
        <div className={classes.loadMore}>
          <Button
            // variant="semiContained"
            // color="primary"
            name="loadMore"
            iconSize="small"
            action={() => fetchNextPage()}
            sx={{
              backgroundColor: theme.palette.primary.main,
              '&:hover': {
                backgroundColor: theme.palette.primary.main,
              },
              color: theme.palette.common.white,
            }}
            disabled={listQuery.isFetching}
          >
            {t('loadMore')}
          </Button>
        </div>
      ) : null}
    </section>
  );
};

export default LeftBar;

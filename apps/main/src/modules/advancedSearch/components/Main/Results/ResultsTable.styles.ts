// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => {
  return {
    resultsTableWrapper: {
      display: 'flex',
      flexDirection: 'column',
      width: '100%',
    },
    resultsTable: {
      flex: '1 1 auto',
    },
  };
});

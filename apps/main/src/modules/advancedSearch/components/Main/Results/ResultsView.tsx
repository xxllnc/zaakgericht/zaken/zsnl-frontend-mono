// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent, useEffect, useState } from 'react';
import { useQueryClient } from '@tanstack/react-query';
import { createPortal } from 'react-dom';
import { useSelectionBehaviour } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import { Geojson } from '@mintlab/ui/types/MapIntegration';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { useSingleQuery } from '../../../query/useSingle';
import { useResultsQuery } from '../../../query/useResults';
import {
  IdentifierType,
  KindType,
  ViewType,
  ColumnType,
  ExportFormatType,
} from '../../../AdvancedSearch.types';
import {
  useResultsCountQuery,
  GetResultsReturnType,
} from '../../../query/useResults';
import resultsLocale from '../../../locale/results.locale';
import ExportButton from './components/ExportButton';
import { getColumns, startExport } from './Results.library';
import ResultsTable from './ResultsTable';
import ResultsMap from './ResultsMap';

type ResultsViewPropsType = {
  identifier: IdentifierType;
  kind: KindType;
  page: number;
  view: ViewType;
  resultsPerPage: number;
  setTotalResults?: any;
  setIsFetching?: any;
  keyword?: string;
  setName?: (name: string) => void;
  portalRef?: React.RefObject<HTMLDivElement>;
  setSnack?: any;
};

/* eslint complexity: [2, 14] */
const ResultsView: FunctionComponent<ResultsViewPropsType> = ({
  identifier,
  kind,
  page,
  resultsPerPage,
  setTotalResults,
  setIsFetching,
  view = 'table',
  keyword,
  setName,
  portalRef,
  setSnack,
}) => {
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [exporting, setExporting] = useState<boolean>(false);
  const queryClient = useQueryClient();
  const currentQuery = useSingleQuery({
    identifier,
    openServerErrorDialog,
  });
  const columns = currentQuery?.data?.columns;
  const sortColumn = currentQuery?.data?.sortColumn;
  const sortOrder = currentQuery?.data?.sortOrder;
  const filters = currentQuery?.data?.filters;
  const [t] = useTranslation('results');

  const queryParams = {
    kind,
    identifier,
    page,
    resultsPerPage,
    openServerErrorDialog,
    columns,
    sortColumn,
    sortOrder,
    filters,
    geo: view === 'map',
    currentSuccess: currentQuery.isSuccess,
    keyword,
  };

  const resultsQuery = useResultsQuery(queryParams, t);
  const resultsQueryCount = useResultsCountQuery(
    cloneWithout(
      queryParams,
      'page',
      'resultsPerPage',
      'sortColumn',
      'sortOrder'
    ),
    t
  );

  useEffect(() => {
    if (setTotalResults) setTotalResults(resultsQueryCount.data);
  }, [resultsQueryCount.data]);

  useEffect(() => {
    if (setIsFetching) setIsFetching(resultsQuery.isFetching);
  }, [resultsQuery.isFetching]);

  useEffect(() => {
    if (setName && currentQuery?.data) setName(currentQuery.data.name);
  }, [currentQuery.data]);

  const resultsQueryData = resultsQuery?.data as GetResultsReturnType;
  const rows = resultsQueryData || [];
  const features: Geojson[] = rows?.data?.reduce<Geojson[]>((acc, current) => {
    if (current?.geoFeatures && current?.geoFeatures.length) {
      acc = [...acc, ...current.geoFeatures];
    }
    return acc;
  }, []);

  const selectionProps = useSelectionBehaviour({
    rows: rows.data,
    page,
    resultsPerPage,
    selectEverythingTranslations: t('selectable', {
      returnObjects: true,
    }),
  });
  const { everythingSelected, selectedRows } = selectionProps;

  if (currentQuery?.status === 'loading' || resultsQuery?.status === 'loading')
    return <Loader />;

  if (!resultsQuery?.data) {
    return null;
  }

  const initiateExport = async (format: ExportFormatType) => {
    try {
      setExporting(true);
      await startExport({
        queryClient,
        format,
        kind,
        exportItems: everythingSelected ? 'all' : selectedRows,
      });
      setSnack({ message: t('export.successMessage'), open: true });
      setExporting(false);
    } catch (errors: any) {
      openServerErrorDialog(errors);
      setExporting(false);
    }
  };

  const showExportButton =
    portalRef &&
    portalRef.current &&
    setSnack &&
    (isPopulatedArray(selectedRows) || everythingSelected);

  return (
    <>
      {ServerErrorDialog}
      {showExportButton &&
        createPortal(
          <ExportButton
            t={t}
            exportAction={(format: ExportFormatType) => initiateExport(format)}
            loading={exporting}
          />,
          portalRef.current
        )}
      {view === 'table' && (
        <ResultsTable
          columns={getColumns(columns as ColumnType[])}
          rows={rows.data}
          sortColumn={sortColumn}
          sortOrder={sortOrder}
          kind={kind}
          t={t}
          selectionProps={selectionProps}
        />
      )}

      {view === 'map' && <ResultsMap features={features} />}
    </>
  );
};

export default (props: ResultsViewPropsType) => (
  <I18nResourceBundle resource={resultsLocale} namespace="results">
    <ResultsView {...props} />
  </I18nResourceBundle>
);

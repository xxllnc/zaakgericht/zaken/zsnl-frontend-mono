// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
// @ts-ignore
import Pagination from '@mintlab/ui/App/Material/Pagination';
import { useStyles } from '../Pagination.styles';

const ResultsPagination = ({
  totalResults,
  page,
  resultsPerPage,
  setResultsPerPage,
  setPage,
}: any) => {
  const classes = useStyles();

  return (
    <div className={classes.resultsTablePaging}>
      <Pagination
        changeRowsPerPage={(event: any) =>
          setResultsPerPage(event?.target?.value)
        }
        component={'div'}
        count={totalResults || 0}
        getNewPage={(newPage: number) => setPage(newPage + 1)}
        labelDisplayedRows={({ count: newCount, from, to }: any) =>
          `${from}–${to} van ${newCount}`
        }
        labelRowsPerPage={'Per pagina:'}
        page={page !== 0 ? page - 1 : page}
        rowsPerPage={resultsPerPage}
        rowsPerPageOptions={[5, 10, 25]}
      />
    </div>
  );
};

export default ResultsPagination;

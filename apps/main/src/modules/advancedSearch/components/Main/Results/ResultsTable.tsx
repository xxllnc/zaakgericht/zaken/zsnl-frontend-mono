// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import * as i18next from 'i18next';
import { useSelectionBehaviourReturnType } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { ColumnType } from '@mintlab/ui/App/Zaaksysteem/SortableTable/types/SortableTableTypes';
import {
  SortDirectionType,
  ResultRowType,
  KindType,
} from '../../../AdvancedSearch.types';
import { useStyles } from './ResultsTable.styles';

type ResultsTablePropsType = {
  columns: ColumnType[];
  rows: ResultRowType[];
  sortColumn?: any;
  sortOrder?: SortDirectionType;
  kind: KindType;
  t: i18next.TFunction;
  selectionProps: useSelectionBehaviourReturnType;
};

/* eslint complexity: [2, 10] */
const ResultsTable: FunctionComponent<ResultsTablePropsType> = ({
  columns,
  rows,
  sortColumn,
  sortOrder,
  t,
  kind,
  selectionProps,
}) => {
  const classes = useStyles();

  if (!rows) return null;

  const handleDoubleClick = ({ event, rowData }: any) => {
    event.stopPropagation();
    event.preventDefault();
    const url =
      kind === 'custom_object'
        ? `/main/object/${rowData.uuid}`
        : `/redirect/case?uuid=${rowData.uuid}`;
    top?.window.open(url, '_new');
  };

  const parsedRows = rows.map(({ columns, uuid }) => ({
    ...columns,
    uuid,
  }));

  return (
    <div className={classes.resultsTableWrapper}>
      <div className={classes.resultsTable}>
        <SortableTable
          rows={parsedRows}
          columns={columns}
          noRowsMessage={t('noResultsFound')}
          rowHeight={50}
          loading={false}
          onRowDoubleClick={handleDoubleClick}
          // No dynamic table sorting, providing sortBy and sortDirection as props
          sorting="none"
          sortInternal={false}
          headerHeight={75}
          {...selectionProps}
          {...(sortColumn && { sortColumn })}
          {...(sortOrder && { sortOrder: sortOrder.toUpperCase() })}
        />
      </div>
    </div>
  );
};

export default React.memo(ResultsTable);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import * as i18next from 'i18next';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { ColumnType as ColumnTypeTable } from '@mintlab/ui/App/Zaaksysteem/SortableTable/types/SortableTableTypes';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { getLabel } from '../Main.library';
import {
  ColumnType,
  ExportFormatType,
  KindType,
  ModeType,
} from '../../../AdvancedSearch.types';
//import { buildSearchOrExportQuery } from '../../../query/library';
import { QUERY_KEY_SAVEDSEARCH } from '../../../query/constants';
import { getUniqueColumnIdentifier } from '../../../library/library';

export const getColumns = (columns: ColumnType[]): ColumnTypeTable[] => {
  return (columns || []).map(column => {
    const getSizeProps = () => {
      if (column.type === 'id' || column.type === 'uuid') {
        return {
          width: 150,
        };
      } else {
        return {
          width: 1,
          flexGrow: 1,
          flexShrink: 0,
        };
      }
    };

    const uniqueIdentifier = getUniqueColumnIdentifier(column);
    return {
      label: column.label,
      name: uniqueIdentifier,
      cellRenderer: ({ rowData }: any) => {
        const thisRowData = rowData[uniqueIdentifier];
        return (
          <div>
            {thisRowData?.['$$typeof'] ? (
              thisRowData
            ) : (
              <Tooltip title={thisRowData} enterNextDelay={750}>
                {thisRowData}
              </Tooltip>
            )}
          </div>
        );
      },
      ...getSizeProps(),
    };
  });
};

export const Label = ({
  classes,
  t,
  mode,
  currentQuery,
}: {
  classes: any;
  t: i18next.TFunction;
  mode: ModeType;
  currentQuery: any;
}) => {
  const topLabel = getLabel({ mode, currentQuery, t });
  return <span className={classes.mainTopBarName}>{topLabel}</span>;
};

export const startExport = async ({
  queryClient,
  format,
  kind,
  exportItems,
}: {
  queryClient: any;
  format: ExportFormatType;
  kind: KindType;
  exportItems: string[] | 'all';
}) => {
  const [, data] = queryClient.getQueriesData([QUERY_KEY_SAVEDSEARCH])[0];
  if (data) {
    //const { columns, filters } = data;
    // const url = buildSearchOrExportQuery({
    //   columns,
    //   filters,
    //   kind,
    //   mode: 'export',
    //   count: false,
    //   format,
    //   exportItems,
    // });
    const url =
      '/api/v2/cm/saved_search/list?page_size=20&page=1&filter[kind]=case';
    return request<any>('GET', url);
  }
};

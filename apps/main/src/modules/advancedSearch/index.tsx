// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import locale from './locale/AdvancedSearch.locale';
import AdvancedSearch from './AdvancedSearch';

const moduleConfig = {
  id: 'advanced-search',
  reducerMap: {},
};
type AdvancedSearchModulePropsType = {
  prefix: string;
};

const AdvancedSearchModule: FunctionComponent<
  AdvancedSearchModulePropsType
> = ({ prefix }) => {
  return (
    //@ts-ignore
    <DynamicModuleLoader modules={[moduleConfig]}>
      <I18nResourceBundle resource={locale} namespace="search">
        <ReactQueryDevtools />
        <Routes>
          <Route
            path=""
            element={<Navigate to="custom_object" replace={true} />}
          />
          <Route path=":kind/*" element={<AdvancedSearch prefix={prefix} />} />
        </Routes>
      </I18nResourceBundle>
    </DynamicModuleLoader>
  );
};

export default AdvancedSearchModule;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Routes, Route, useParams } from 'react-router-dom';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { useQueryClient } from '@tanstack/react-query';
import { useStyles } from './styles/AdvancedSearch.styles';
import TopBar from './components/TopBar/TopBar';
import SelectMessage from './components/Main/SelectMessage';
import LeftBar from './components/LeftBar/LeftBar';
import Main from './components/Main/Main';
import { useTransition } from './hooks/useTransition';
import {
  KindType,
  AdvancedSearchParamsType,
  SnackType,
} from './AdvancedSearch.types';
import { invalidateAllQueries } from './query/library';

type AdvancedSearchPropsType = {
  prefix: string;
};

/* eslint complexity: [2, 10] */
const AdvancedSearch = ({ prefix }: AdvancedSearchPropsType) => {
  const classes = useStyles();
  const [t] = useTranslation('search');
  const params = useParams<
    keyof AdvancedSearchParamsType
  >() as AdvancedSearchParamsType;
  const { kind } = params;
  const [snack, setSnack] = useState<SnackType | null>(null);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const client = useQueryClient();

  useTransition(
    (prevKind: KindType) => {
      if (prevKind && kind !== prevKind) {
        invalidateAllQueries(client);
      }
    },
    [kind]
  );

  return (
    <>
      <div className={classes.wrapper}>
        {ServerErrorDialog}
        <TopBar kind={kind} classes={classes} t={t} />
        <div className={classes.content}>
          <LeftBar
            kind={kind}
            classes={classes}
            client={client}
            setSnack={setSnack}
            t={t}
            openServerErrorDialog={openServerErrorDialog}
            prefix={prefix}
            params={params}
          />
          <section className={classes.mainContent}>
            <Routes>
              <Route
                path=""
                element={<SelectMessage classes={classes} t={t} />}
              />

              <Route
                path=":mode/*"
                element={
                  <Main
                    classes={classes}
                    client={client}
                    snack={snack}
                    setSnack={setSnack}
                    openServerErrorDialog={openServerErrorDialog}
                    t={t}
                  />
                }
              >
                <Route path=":identifier" />
              </Route>
            </Routes>
          </section>
        </div>
      </div>
    </>
  );
};

export default AdvancedSearch;

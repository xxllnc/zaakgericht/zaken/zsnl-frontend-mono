// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const columnsStyles = ({ greyscale, elephant }: any) => {
  return {
    columnsColumnEntry: {
      position: 'relative',
      padding: 2,
      border: `1px solid ${elephant.lightest}`,
      boxShadow: '0.1px 1px 3px 0.1px rgb(0 0 0 / 12%)',
      borderRadius: 4,
      display: 'flex',
      flexDirection: 'row' as 'row',
      marginBottom: 8,
      alignItems: 'center',
      height: 44,
    },
    columnsOuterWrapper: {
      display: 'flex',
      justifyContent: 'center',
    },
    columnsInnerWrapper: {
      flexDirection: 'column',
      display: 'flex',
      width: '90%',
    },
    columnsSelector: {
      marginBottom: 30,
    },
    columnsMethodChoice: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      marginBottom: 16,
      padding: 10,
      columnGap: 14,
    },
    columnsEntryDragIndicator: {
      color: greyscale.darker,
      marginRight: 12,
    },
    columnsSystemAttributeSelectors: {
      display: 'flex',
      '&>:nth-child(1)': {
        width: '55%',
        marginRight: 20,
      },
      '&>:nth-child(2)': {
        flex: 1,
      },
    },
    columnsEntrySorting: {
      marginLeft: 'auto',
      marginRight: 14,
    },
    columnsSingleSelectWrapper: {
      display: 'flex',
      justifyContent: 'flex-end',
      '&>:nth-child(1)': {
        width: '65%',
      },
    },
  };
};

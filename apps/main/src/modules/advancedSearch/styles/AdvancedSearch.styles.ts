// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';
import { permissionsStyles } from './permissions';
import { columnsStyles } from './columns';
import { editFormStyles } from './editForm';
import { mainStyles } from './main';
import { filtersStyles } from './filters';

export const useStyles = makeStyles(
  // @ts-ignore
  ({
    palette: {
      elephant,
      primary,
      common,
      thundercloud,
      basalt,
      coral,
      cloud,
      error,
      northsea,
    },
    mintlab: { greyscale },
    typography,
    breakpoints,
  }: Theme) => {
    const savedSearchLinkActive = {
      backgroundColor: northsea.dark,
      '& *': {
        color: 'white',
      },
    };
    return {
      wrapper: {
        display: 'flex',
        flexFlow: 'column',
        flexDirection: 'column',
        height: '100vh',
        minHeight: '100vh',
        width: '100vw',
        overflow: 'hidden',
      },
      scrollWrapper: {
        overflow: 'hidden',
        overflowY: 'scroll',
      },
      hide: {
        '&&': {
          display: 'none',
        },
      },
      show: {
        display: 'inherit',
      },
      // main level blocks
      topBar: {
        display: 'flex',
        justifyContent: 'flex-end',
        height: 60,
        overflow: 'hidden',
        alignItems: 'center',
        '&>*': {
          marginRight: 12,
        },
        backgroundColor: northsea.lighter,
        color: common.white,
      },
      typeChip: {
        borderRadius: 12,
        padding: '6px 12px 6px 12px',
        backgroundColor: thundercloud.light,
        fontSize: 12,
        color: common.white,
        textDecoration: 'none',
      },
      typeChipActive: {
        backgroundColor: northsea.dark,
        color: common.white,
        boxShadow: '0.1px 1px 3px 0.1px rgb(0 0 0 / 12%)',
      },
      content: {
        display: 'flex',
        flexDirection: 'row',
        flex: 1,
        overflow: 'hidden',
      },
      leftBar: {
        width: 300,
        overflowY: 'auto',
        backgroundColor: cloud.main,
        position: 'relative',
      },
      leftBarRowWrapper: {
        display: 'flex',
        padding: '8px 12px',
        justifyContent: 'center',
        '&:hover': savedSearchLinkActive,
      },
      savedSearchLinkActive,
      savedSearchLinkInactive: {
        '& *': {
          color: basalt.darkest,
        },
      },
      savedSearchLink: {
        textDecoration: 'none',
        width: '100%',
        display: 'flex',
        alignItems: 'center',
        whiteSpace: 'nowrap',
        overflow: 'hidden',
      },
      tabs: {
        margin: '16px 0px 0px 16px',
      },
      tabsContent: {
        margin: '0px 0px 20px 0px',
      },
      mainContent: {
        padding: 20,
        flex: 1,
        flexDirection: 'column',
        display: 'flex',
        position: 'relative',
      },

      bottomDivider: {},
      dividerTop: {
        borderBottom: 'none',
        borderTop: `1px solid ${greyscale.light}`,
      },
      noDivider: {
        border: 'none',
      },
      selectedTab: {
        backgroundColor: primary.lightest,
        color: primary.main,
      },
      tab: {
        borderTopLeftRadius: 6,
        borderTopRightRadius: 6,
      },
      loadMore: {
        display: 'flex',
        margin: '20px 0px',
        justifyContent: 'center',
        alignItems: 'center',
      },
      newSavedSearchWrapper: {
        display: 'flex',
        justifyContent: 'flex-end',
        marginBottom: 10,
      },
      rightCornerDeleteButton: {
        '&&': {
          position: 'absolute',
          top: -10,
          right: -14,
          color: common.white,
          backgroundColor: primary.dark,
          boxShadow: '0.1px 1px 3px 0.1px rgb(0 0 0 / 12%)',
          '&:hover': {
            backgroundColor: primary.dark,
          },
        },
      },
      selectMessage: {
        textAlign: 'center' as const,
      },
      snack: {
        marginBottom: 70,
      },
      ...mainStyles({ primary, typography }),
      ...editFormStyles({
        breakpoints,
        error,
        elephant,
        coral,
        typography,
        greyscale,
      }),
      ...permissionsStyles({ greyscale, primary }),
      ...columnsStyles({ greyscale, elephant }),
      ...filtersStyles(),
    };
  }
);

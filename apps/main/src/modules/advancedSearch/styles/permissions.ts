// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const permissionsStyles = ({ greyscale, primary }: any) => {
  return {
    permissionsWrapper: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
    },
    permissionsEntries: {
      width: 500,
    },
    newPermission: {
      display: 'flex',
      width: '100%',
      justifyContent: 'flex-end',
      marginRight: 60,
      marginBottom: 36,
    },
    permissionsEntryWrapper: {
      position: 'relative',
      border: `1px solid ${greyscale.light}`,
      boxShadow: '0.1px 1px 3px 0.1px rgb(0 0 0 / 12%)',
      borderRadius: 10,
      padding: '16px 36px 2px 16px',
      marginBottom: 20,
    },
    permissionsEntryDelete: {
      position: 'absolute' as 'absolute',
      top: 0,
      right: 0,
    },
    permissionsEntryRow: {
      display: 'flex',
      flexDirection: 'row',
      '&>:nth-child(1)': {
        width: 100,
        display: 'flex',
        alignItems: 'center',
      },
      '&>:nth-child(2)': {
        flex: 1,
      },
      marginBottom: 12,
    },
    permissionsEntryRowSelect: {
      '&>:nth-child(2)': {
        flex: 1,
        backgroundColor: greyscale.light,
      },
    },
    permissionsAddButton: {
      width: 120,
      marginLeft: 380,
    },
    permissionsInfo: {
      marginBottom: 20,
      display: 'flex',
      alignItems: 'center',
      alignSelf: 'flex-end',
      justifyContent: 'flex-end',
      color: primary.main,
    },
    permissionsText: {
      fontWeight: 400,
      textAlign: 'left' as const,
      '& li': {
        marginBottom: 8,
      },
      '& ul': {
        marginLeft: 16,
        padding: 0,
      },
    },
    mayEditCheckbox: {
      '&&': {
        marginLeft: 42,
      },
    },
  };
};

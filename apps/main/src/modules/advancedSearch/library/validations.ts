// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const hasTextValue = (value: any) => {
  const expr = new RegExp(/^(?!\s*$).+/);
  return value.match(expr);
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import {
  ColumnType,
  KindType,
  RelationshipType,
} from '../AdvancedSearch.types';

export const SAVED_SEARCHES_PAGE_LENGTH = 20;
export const DEFAULT_RESULTS_PER_PAGE = 25;

export const getCategories = (kind: KindType, t: i18next.TFunction) => {
  return kind === 'custom_object'
    ? {
        default: [
          'uuid',
          'title',
          'subtitle',
          'externalReference',
          'dateCreated',
          'lastModified',
        ],
      }
    : {
        systemAttributes: getSystemAttributeFields(t).map(fields => fields[0]),
        requestor: getRelationshipFields('requestor', t).map(
          fields => fields[0]
        ),
        assignee: getRelationshipFields('assignee', t).map(fields => fields[0]),
        case: getCaseFields(t).map(fields => fields[0]),
      };
};

const getCaseFields = (t: i18next.TFunction) => [
  ['caseNumber', t('columns.caseNumber'), ['attributes', 'number']],
  ['caseTypeTitle', t('columns.caseType'), ['attributes', 'case_type_title']],
  [
    'caseStatus',
    `${t('columns.subcategories.case')} ${t('columns.status')}`,
    ['attributes', 'status'],
  ],
  [
    'registrationDate',
    `${t('columns.subcategories.case')} ${t('columns.registrationDate')}`,
    ['attributes', 'registration_date'],
  ],
  [
    'completionDate',
    `${t('columns.subcategories.case')} ${t('columns.completionDate')}`,
    ['attributes', 'completion_date'],
  ],
  [
    'destructionDate',
    `${t('columns.subcategories.case')} ${t('columns.destructionDate')}`,
    ['attributes', 'destruction_date'],
  ],
];

const getSystemAttributeFields = (t: i18next.TFunction) => [
  ['id', 'UUID', ['id']],
  ['title', t('columns.title'), ['title']],
];

const getRelationshipFields = (
  type: RelationshipType,
  t: i18next.TFunction
) => [
  [
    `relationship.${type}.summary`,
    `${t(`relationships.${type}`)} ${t('columns.summary')}`,
    ['relationships', type, 'data', 'meta', 'summary'],
  ],
  [
    `relationship.${type}.contact_information.email`,
    `${t(`relationships.${type}`)} ${t(`columns.subcategories.contact`)} ${t(
      'columns.email'
    )}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'contact_information',
      'email',
    ],
  ],
  [
    `relationship.${type}.contact_information.mobile_number`,
    `${t(`relationships.${type}`)} ${t(`columns.subcategories.contact`)} ${t(
      'columns.mobile_number'
    )}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'contact_information',
      'mobile_number',
    ],
  ],
  [
    `relationship.${type}.contact_information.phone_number`,
    `${t(`relationships.${type}`)} ${t(`columns.subcategories.contact`)} ${t(
      'columns.phone_number'
    )}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'contact_information',
      'phone_number',
    ],
  ],

  [
    `relationship.${type}.contact_information.phone_number`,
    `${t(`relationships.${type}`)} ${t(`columns.subcategories.contact`)} ${t(
      'columns.phone_number'
    )}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'contact_information',
      'phone_number',
    ],
  ],
  [
    `relationship.${type}.contact_person.family_name`,
    `${t(`relationships.${type}`)} ${t(`columns.subcategories.contact`)} ${t(
      'columns.familyName'
    )}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'contact_person',
      'family_name',
    ],
  ],
  [
    `relationship.${type}.contact_person.first_name`,
    `${t(`relationships.${type}`)} ${t(`columns.subcategories.contact`)} ${t(
      'columns.firstName'
    )}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'contact_person',
      'first_name',
    ],
  ],
  [
    `relationship.${type}.contact_person.initials`,
    `${t(`relationships.${type}`)} ${t(`columns.subcategories.contact`)} ${t(
      'columns.initials'
    )}`,
    ['relationships', type, 'data', 'attributes', 'contact_person', 'initials'],
  ],
  [
    `relationship.${type}.contact_person.insertions`,
    `${t(`relationships.${type}`)} ${t(`columns.subcategories.contact`)} ${t(
      'columns.insertions'
    )}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'contact_person',
      'insertions',
    ],
  ],
  [
    `relationship.${type}.contact_information.name`,
    `${t(`relationships.${type}`)} ${t('columns.name')}`,
    ['relationships', type, 'data', 'attributes', 'name'],
  ],
  [
    `relationship.${type}.contact_information.gender`,
    `${t(`relationships.${type}`)} ${t('columns.gender')}`,
    ['relationships', type, 'data', 'attributes', 'gender'],
  ],
  [
    `relationship.${type}.contact_information.date_of_birth`,
    `${t(`relationships.${type}`)} ${t('columns.dateOfBirth')}`,
    ['relationships', type, 'data', 'attributes', 'date_of_birth'],
  ],
  [
    `relationship.${type}.contact_information.date_of_death`,
    `${t(`relationships.${type}`)} ${t('columns.dateOfDeath')}`,
    ['relationships', type, 'data', 'attributes', 'date_of_death'],
  ],
  [
    `relationship.${type}.location_address.city`,
    `${t(`relationships.${type}`)} ${t(`columns.subcategories.location`)} ${t(
      'columns.city'
    )}`,
    ['relationships', type, 'data', 'attributes', 'location_address', 'city'],
  ],
  [
    `relationship.${type}.location_address.country`,
    `${t(`relationships.${type}`)} ${t(`columns.subcategories.location`)} ${t(
      'columns.country'
    )}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'location_address',
      'country',
    ],
  ],
  [
    `relationship.${type}.location_address.street`,
    `${t(`relationships.${type}`)} ${t(`columns.subcategories.location`)} ${t(
      'columns.street'
    )}`,
    ['relationships', type, 'data', 'attributes', 'location_address', 'street'],
  ],
  [
    `relationship.${type}.location_address.street_number`,
    `${t(`relationships.${type}`)} ${t(`columns.subcategories.location`)} ${t(
      'columns.streetNumber'
    )}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'location_address',
      'street_number',
    ],
  ],
  [
    `relationship.${type}.location_address.street_number_letter`,
    `${t(`relationships.${type}`)} ${t(`columns.subcategories.location`)} ${t(
      'columns.streetNumberLetter'
    )}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'location_address',
      'street_number_letter',
    ],
  ],
  [
    `relationship.${type}.location_address.street_number_suffix`,
    `${t(`relationships.${type}`)} ${t(`columns.subcategories.location`)} ${t(
      'columns.streetNumberSuffix'
    )}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'location_address',
      'street_number_suffix',
    ],
  ],
  [
    `relationship.${type}.location_address.zipcode`,
    `${t(`relationships.${type}`)} ${t(`columns.subcategories.location`)} ${t(
      'columns.zipcode'
    )}`,
    [
      'relationships',
      type,
      'data',
      'attributes',
      'location_address',
      'zipcode',
    ],
  ],
  [
    `relationship.${type}.name`,
    `${t(`relationships.${type}`)} ${t('columns.name')}`,
    ['relationships', type, 'data', 'attributes', 'name'],
  ],
  [
    `relationship.${type}.organization_type`,
    `${t(`relationships.${type}`)} ${t('columns.organizationType')}`,
    ['relationships', type, 'data', 'attributes', 'organization_type'],
  ],
  [
    `relationship.${type}.coc_number`,
    `${t(`relationships.${type}`)} ${t('columns.cocNumber')}`,
    ['relationships', type, 'data', 'attributes', 'coc_number'],
  ],
  [
    `relationship.${type}.coc_location_number`,
    `${t(`relationships.${type}`)} ${t('columns.cocLocationNumber')}`,
    ['relationships', type, 'data', 'attributes', 'coc_location_number'],
  ],
  [
    `relationship.${type}.rsin`,
    `${t(`relationships.${type}`)} ${t('columns.rsin')}`,
    ['relationships', type, 'data', 'attributes', 'rsin'],
  ],
  [
    `relationship.${type}.correspondence_address`,
    `${t(`relationships.${type}`)} ${t('columns.correspondenceAddress')}`,
    ['relationships', type, 'data', 'attributes', 'correspondence_address'],
  ],
  [
    `relationship.${type}.date_founded`,
    `${t(`relationships.${type}`)} ${t('columns.dateFounded')}`,
    ['relationships', type, 'data', 'attributes', 'date_founded'],
  ],
  [
    `relationship.${type}.date_registered`,
    `${t(`relationships.${type}`)} ${t('columns.dateRegistered')}`,
    ['relationships', type, 'data', 'attributes', 'date_registered'],
  ],
  [
    `relationship.${type}.date_ceased`,
    `${t(`relationships.${type}`)} ${t('columns.dateCeased')}`,
    ['relationships', type, 'data', 'attributes', 'date_ceased'],
  ],
  [
    `relationship.${type}.source`,
    `${t(`relationships.${type}`)} ${t('columns.source')}`,
    ['relationships', type, 'data', 'attributes', 'source'],
  ],
];

export const getDefaultColumns = (
  kind: KindType,
  t: i18next.TFunction
): ColumnType[] => {
  let columns: Array<string | Array<string>>[];

  if (kind === 'custom_object') {
    columns = [
      ['uuid', t('columns.id'), ['id']],
      ['title', t('columns.title'), ['attributes', 'title']],
      ['subtitle', t('columns.subtitle'), ['attributes', 'subtitle']],
      [
        'externalReference',
        t('columns.externalReference'),
        ['attributes', 'external_reference'],
      ],
      ['dateCreated', t('columns.dateCreated'), ['attributes', 'date_created']],
      [
        'lastModified',
        t('columns.lastModified'),
        ['attributes', 'last_modified'],
      ],
    ];
  } else {
    columns = [
      ...getSystemAttributeFields(t),
      ...getCaseFields(t),
      ...getRelationshipFields('assignee', t),
      ...getRelationshipFields('requestor', t),
    ];
  }
  return columns.map(entry => ({
    type: entry[0] as string,
    label: entry[1] as string,
    source: entry[2] as Array<string>,
  }));
};

export const SORTABLE_COLUMNS = [
  'attributes.title',
  'attributes.subtitle',
  'attributes.external_reference',
  'attributes.date_created',
  'attributes.last_modified',
];

export const filterTranslationKeys = {
  'attributes.last_modified': 'modified',
  'attributes.status': 'status',
  'attributes.archive_status': 'archiveStatus',
  'attributes.archival_state': 'archivalState',
  'relationship.requestor.id': 'requestor',
  'relationship.assignee.id': 'assignee',
};

export const filtersKeyNamesReplacements = [
  ['startValue', 'start_value'],
  ['endValue', 'end_value'],
  ['timeSetByUser', 'time_set_by_user'],
  ['magicString', 'magic_string'],
  ['customFieldsOperator', 'custom_fields_operator'],
];

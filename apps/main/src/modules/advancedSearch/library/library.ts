// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { AuthorizationsType, ColumnType } from '../AdvancedSearch.types';

export const getUniqueColumnIdentifier = (column: ColumnType) => {
  if (column.magicString) {
    return column.magicString;
  } else if (isPopulatedArray(column.source)) {
    return column.source.join('.');
  } else {
    return column.type;
  }
};

export const isUUID = (str?: string) => {
  if (!str) return false;
  const reg = new RegExp(
    /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i
  );
  return reg.test(str);
};

export const getFromQueryValues = <T = any>(
  values: any,
  name: string
): T | null =>
  Object.entries(values).reduce((acc, current) => {
    if (current[0] === name) {
      acc = current[1] as T;
    }
    return acc;
  }, null as any);

export const hasAccess = (
  authorizations: AuthorizationsType[],
  level: AuthorizationsType
): Boolean => {
  if (!authorizations) return false;
  if (level === 'read')
    return authorizations.some(
      (auth: any) => ['admin', 'readwrite', 'read'].indexOf(auth) >= 0
    );
  if (level === 'readwrite')
    return authorizations.some(
      (auth: any) => ['admin', 'readwrite'].indexOf(auth) >= 0
    );
  if (level === 'admin')
    return authorizations.some((auth: any) => ['admin'].indexOf(auth) >= 0);
  return false;
};

export const flat = (obj: any, out: any) => {
  if (!obj || !Object.keys(obj).length) return out;
  Object.keys(obj).forEach(key => {
    if (typeof obj[key] == 'object') {
      out = flat(obj[key], out);
    } else {
      out[key] = obj[key];
    }
  });
  return out;
};

export const replaceKeysInJSON = (data: any, replaceArr: any[]) => {
  const replaced = replaceArr.reduce(
    (acc: any, current: string[]) =>
      acc.replaceAll(`"${current[0]}"`, `"${current[1]}"`),
    JSON.stringify(data)
  );
  return JSON.parse(replaced);
};

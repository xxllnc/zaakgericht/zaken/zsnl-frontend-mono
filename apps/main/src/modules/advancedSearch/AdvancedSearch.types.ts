// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { Geojson } from '@mintlab/ui/types/MapIntegration';

export type AdvancedSearchParamsType = {
  kind: KindType;
  identifier?: IdentifierType;
  mode: ModeType;
};

export type CustomFieldType = {
  type: string;
  magicString: string;
  label: string;
  name?: string;
  uuid?: string;
  value?: any;
};

export type ColumnType = {
  type: string;
  label: string;
  source: string[];
  magicString?: string;
};

export type AuthorizationsType = 'read' | 'readwrite' | 'admin';

export type SavedSearchType = {
  uuid: string;
  name: string;
  filters: FiltersType | null;
  permissions: PermissionType[];
  columns: ColumnType[];
  authorizations: AuthorizationsType[];
  sortColumn: string;
  sortOrder: 'asc' | 'desc';
  [key: string]: any;
};

export type StatusType = 'active' | 'inactive' | 'draft';
export type ArchiveStatusType = 'archived' | 'to destroy' | 'to preserve';

export type FilterNamesType =
  | 'keyword'
  | 'relationship.custom_object_type'
  | 'attributes.last_modified'
  | 'attributes.status'
  | 'attributes.archive_status'
  | 'attributes.archival_state'
  | 'relationship.assignee.id'
  | 'relationship.requestor.id';

export type FiltersType = {
  customFieldsOperator?: 'and' | 'andor';
  kindType?: any;
  filters: FilterType[];
};

export type ErrorType = {
  uuid: string;
  value: string;
};

type FilterTypeBaseType = { uuid: string };
type CustomFieldTypeTypes = 'text' | 'TODO';

export type FilterTypeTypes =
  | {
      type: 'custom_field';
      parameters: {
        operator: 'eq' | 'ne';
        magicString: string;
        type: CustomFieldTypeTypes;
        value: any;
      };
    }
  | {
      type: 'keyword';
      parameters: {
        label: any;
        value: string | null;
      };
    }
  | {
      type: 'name';
      parameters: {
        label: any;
        value: string | null;
      };
    }
  | {
      type: 'attributes.last_modified';
      parameters: {
        label: any;
        value: DateEntryType[] | null;
      };
    }
  | {
      type: 'attributes.status';
      parameters: {
        label: any;
        value: StatusType | null;
      };
    }
  | {
      type: 'attributes.archive_status';
      parameters: {
        label: any;
        value: ArchiveStatusType | null;
      };
    }
  | {
      type: 'relationship.custom_object_type';
      parameters: {
        label: any;
        value: string | null;
      };
    }
  | {
      type: 'attributes.archival_state';
      parameters: {
        label: any;
        value: string | null;
      };
    }
  | {
      type: 'relationship.assignee.id';
      parameters: {
        label: string;
        value: string;
      }[];
    }
  | {
      type: 'relationship.requestor.id';
      parameters: {
        label: string;
        value: string;
        type?: ContactType;
      }[];
    };

export type ContactType = 'employee' | 'person' | 'organization';
export type FilterType = FilterTypeBaseType & FilterTypeTypes;
export type RelationshipType = 'coordinator' | 'requestor' | 'assignee';

export type KindType = 'custom_object' | 'case';
export type ModeType = 'view' | 'edit' | 'new' | null;
export type ViewType = 'table' | 'map' | null;
export type IdentifierType = string;
export type APIValueContextType = 'results' | 'save';

export type PermissionType = {
  groupID: string | null;
  roleID: string | null;
  writePermission: boolean;
  saved: boolean;
};

export type EditFormStateType = {
  name: string;
  selectedObjectType: ValueType<string> | null;
  selectedFilter: ValueType<string> | null;
} & Pick<
  SavedSearchType,
  'filters' | 'permissions' | 'columns' | 'sortColumn' | 'sortOrder'
>;

export type SortDirectionType = 'asc' | 'desc';

export type ResultRowType = {
  columns: { [key: string]: any };
  uuid: string;
  versionIndependentUuid: string;
  geoFeatures?: Geojson[];
  [key: string]: any;
};

export type ClassesType = {
  [key: string]: any;
};

export type SnackType = {
  message: string | null;
  open: boolean;
};

export type OperatorType = 'lt' | 'gt' | 'le' | 'ge';

export type DateEntryTypeAbsolute = {
  type: 'absolute';
  operator: OperatorType | null;
  value: string | null; // ISO string
};

export type DateEntryTypeRelative = {
  type: 'relative';
  operator: OperatorType | null;
  value: string | null; // Duration string
};

export type DateEntryTypeRange = {
  type: 'range';
  startValue: string | null;
  endValue: string | null;
  timeSetByUser: boolean;
};

export type DateEntryType =
  | DateEntryTypeAbsolute
  | DateEntryTypeRelative
  | DateEntryTypeRange;

export type ExportFormatType = 'CSV';

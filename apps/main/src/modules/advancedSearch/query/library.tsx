// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React, { FunctionComponent } from 'react';
import fecha from 'fecha';
import * as i18next from 'i18next';
import { get } from '@mintlab/kitchen-sink/source';
import { APICaseManagement } from '@zaaksysteem/generated/types/APICaseManagement.types';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Button from '@mintlab/ui/App/Material/Button';
import { QueryClient } from '@tanstack/react-query';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import { ColumnType } from '../AdvancedSearch.types';
import {
  QUERY_KEY_SAVEDSEARCHES,
  QUERY_KEY_SAVEDSEARCH,
  QUERY_KEY_RESULTS,
  QUERY_KEY_RESULTS_COUNT,
  QUERY_KEY_CUSTOM_FIELDS,
} from './constants';

export const invalidateAllQueries = (client: QueryClient) => {
  client.invalidateQueries([QUERY_KEY_SAVEDSEARCHES]);
  client.invalidateQueries([QUERY_KEY_SAVEDSEARCH]);
  client.invalidateQueries([QUERY_KEY_RESULTS]);
  client.invalidateQueries([QUERY_KEY_RESULTS_COUNT]);
  client.invalidateQueries([QUERY_KEY_CUSTOM_FIELDS]);
};

type APISearchRowType =
  APICaseManagement.SearchCustomObjectsResponseBody['data'][0];

type LinkButtonPropsType = {
  title: string;
  url: string;
  t: i18next.TFunction;
};
export const LinkButton: FunctionComponent<LinkButtonPropsType> = ({
  title,
  url,
  t,
}) => (
  <Button
    name="openInNewWindow"
    title={t('results:openInNew')}
    action={(event: any) => {
      event.stopPropagation();
      window.open(url, '_blank');
    }}
    endIcon={<Icon size="extraSmall">{iconNames.open_in_new}</Icon>}
    variant={'text'}
    sx={{
      color: ({ palette: { primary } }: any) => primary.main,
    }}
  >
    {title}
  </Button>
);

/**
 * Currently supports rendering of these attribute types: Geo
  Relatie (object + subject)
  Rekeningnummer
  Datum
  E-mail
  Geocoordinaten (lat/lon)
  Numeriek
  Enkelvoudige keuze
  Keuzelijst
  Tekstveld
  Groot tekstveld
  Webadres
  Valuta
 */
/* eslint complexity: [2, 10] */
export const getResultValue = (
  column: ColumnType,
  row: APISearchRowType,
  t: i18next.TFunction
) => {
  const { source, type, magicString } = column;
  const joined = source.join('.');
  const getType = () => {
    if (magicString) {
      return type;
    } else if (isPopulatedArray(source)) {
      return (get(row, joined, '') as any)?.type || type;
    } else {
      return type;
    }
  };
  const mapType = getType();

  const actualValue = (resultValue?: any) => {
    const target = resultValue || get(row, joined, '');
    if (typeof target === 'string') {
      return target;
    } else if (target?.value && target?.type) {
      return target.value;
    } else {
      return target;
    }
  };
  const safeValue = (value: any) => `${value}`;
  const getDate = (value: any) => {
    if (!value) {
      return null;
    } else if (/^\d{4}-\d{2}-\d{2}$/.test(value)) {
      //@ts-ignore
      return fecha.format(fecha.parse(value, 'YYYY-MM-DD'), 'DD-MM-YYYY');
    } else if (/^\d{4}-\d{2}-\d{2}T/.test(value)) {
      return fecha.format(
        //@ts-ignore
        fecha.parse(value, 'YYYY-MM-DDTHH:mm:SS'),
        'DD-MM-YYYY HH:mm:SS'
      );
    }
    return null;
  };

  type TypeMappingType = () => null | (() => any);
  const typeMapping: TypeMappingType = () => {
    // Add more checks here and functions to typeResolvers
    // to create custom handling for column types that are not
    // directly tied to a field on the backend. This way we
    // can create compound column types, like a combined address.

    if (typeResolvers[mapType]) {
      return typeResolvers[mapType];
    } else {
      return null;
    }
  };

  const typeResolvers: {
    [type: string]: () => any;
  } = {
    text: () => safeValue(actualValue()),
    email: () => (
      <LinkButton
        title={t('results:types.email')}
        url={`mailto:${actualValue()}`}
        t={t}
      />
    ),
    date: () => getDate(actualValue()) || '',
    geojson: () => (
      <LinkButton
        title={t('results:types.geojson')}
        url={`/main/object/${row?.id}/map`}
        t={t}
      />
    ),
    url: () => (
      <LinkButton title={t('results:types.url')} url={actualValue()} t={t} />
    ),
    address_v2: () => actualValue()?.address?.full || '',
    currency: () => {
      return new Intl.NumberFormat('nl-NL', {
        style: 'currency',
        currency: 'EUR',
      }).format(actualValue());
    },
    relationship: () => {
      const getProps = () => {
        if (!actualValue()) return null;
        const objectValue = get(row, joined) as any;
        const { specifics } = objectValue;
        switch (specifics?.relationship_type) {
          case 'custom_object':
            return {
              url: `/main/object/${actualValue()}/`,
              title:
                specifics?.metadata?.summary ||
                t('results:types.relationshipTypes.customObject'),
            };
          case 'subject':
          case 'organization':
            return {
              url: `/redirect/contact_page?uuid=${actualValue()}`,
              title:
                specifics?.metadata?.summary ||
                t('results:types.relationshipTypes.subject'),
            };
          default:
            return {
              url: '/',
              title: t('results:types.relationshipTypes.unknown'),
            };
        }
      };

      const props = getProps();
      return props ? (
        <LinkButton title={props.title} url={props.url} t={t} />
      ) : null;
    },
  };

  if (typeMapping()) {
    //@ts-ignore
    return typeMapping()();
  } else if (getDate(actualValue())) {
    return getDate(actualValue());
  } else if (
    typeof actualValue() === 'string' ||
    actualValue() instanceof String
  ) {
    return actualValue();
  } else if (actualValue() === null || actualValue() === undefined) {
    return '';
  } else {
    return safeValue(actualValue());
  }
};

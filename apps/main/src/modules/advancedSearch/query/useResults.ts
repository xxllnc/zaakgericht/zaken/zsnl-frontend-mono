// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  V2ServerErrorsType,
  V2ServerErrorType,
} from '@zaaksysteem/common/src/types/ServerError';
import * as i18next from 'i18next';
//@ts-ignore
import objectScan from 'object-scan';
//@ts-ignore
import addDuration from 'date-fns-duration';
import { useQuery } from '@tanstack/react-query';
import add from 'date-fns/add';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { get } from '@mintlab/kitchen-sink/source';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICaseManagement } from '@zaaksysteem/generated/types/APICaseManagement.types';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { APIGeo } from '@zaaksysteem/generated/types/APIGeo.types';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source/array';
import {
  ColumnType,
  FilterType,
  IdentifierType,
  KindType,
  SortDirectionType,
  ResultRowType,
  DateEntryType,
  FiltersType,
} from '../AdvancedSearch.types';
import {
  getFromQueryValues,
  getUniqueColumnIdentifier,
} from '../library/library';
import { getResultValue } from './library';
import { QUERY_KEY_RESULTS, QUERY_KEY_RESULTS_COUNT } from './constants';

const COUNT_STALE_TIME = 60 * 1000;
export type GetResultsReturnType = {
  data: ResultRowType[];
};

function updateObject(object: any, newValue: any, path: any) {
  var stack = path.split('.');
  while (stack.length > 1) {
    object = object[stack.shift()];
  }

  const shifted = stack.shift();
  object[shifted] = { ...object[shifted], ...newValue };
}

// Rows from the search API --> Table rows
const APIResultsToResults = (
  rows: APICaseManagement.SearchCustomObjectsResponseBody['data'],
  included: any,
  columns: ColumnType[],
  t: i18next.TFunction
): ResultRowType[] =>
  rows.map(row => {
    if (row.relationships) {
      Object.entries(row.relationships).forEach(val => {
        const keyName = val[0];

        const { id, type } = val[1].data;
        const filterFn = ({ value }: any) =>
          value?.type === type && value?.id === id;

        const includedPath = objectScan(['**'], {
          joined: true,
          filterFn,
        })(included);

        if (isPopulatedArray(includedPath)) {
          updateObject(
            row.relationships,
            get(included, includedPath[0]),
            `${keyName}.data`
          );
        }
      });
    }

    let columnsObj: ResultRowType['columns'] = {};
    const {
      id,
      attributes: { version_independent_uuid = '' },
      geoFeatures,
    } = row;

    columns.map(column => {
      const resultValue = getResultValue(column, row, t);
      if (resultValue)
        columnsObj[getUniqueColumnIdentifier(column)] = resultValue;
    });

    return {
      columns: columnsObj,
      uuid: id,
      versionIndependentUuid: version_independent_uuid,
      geoFeatures,
    };
  });

/* eslint complexity: [2, 20] */
export const filtersToAPIFilters = (
  filters: FiltersType | null,
  keyword?: string | null
) => {
  const lastModifiedToSearchFilters = (
    filterValues: DateEntryType[] | null
  ) => {
    if (!filterValues) return;
    return filterValues.flatMap(filter => {
      if (filter.type === 'absolute') {
        return `${filter.operator} ${filter.value}`;
      } else if (filter.type === 'relative') {
        const modifiedDate = addDuration(new Date(), filter.value);
        return `${filter.operator} ${modifiedDate.toISOString()}`;
      } else if (filter.type === 'range') {
        if (filter.timeSetByUser === true) {
          return [`ge ${filter.startValue}`, `le ${filter.endValue}`];
        } else {
          const dayLater = add(new Date(filter.endValue as string), {
            days: 1,
          });
          return [`gt ${filter.startValue}`, `lt ${dayLater.toISOString()}`];
        }
      }
    });
  };

  const filterValueToAPIValue = (filter: FilterType) => {
    switch (filter.type) {
      case 'keyword':
        return filter.parameters.value;
      case 'relationship.custom_object_type':
      case 'attributes.status':
      case 'attributes.archive_status':
      case 'attributes.archival_state':
        return filter.parameters.value;
      case 'attributes.last_modified':
        return lastModifiedToSearchFilters(filter.parameters.value);
      case 'relationship.requestor.id':
      case 'relationship.assignee.id':
        return filter.parameters.map(parameter => parameter.value);
    }
  };

  if (!filters) return [];
  let dbFilters = filters.filters.reduce((acc: any, filter: FilterType) => {
    const { type } = filter;
    const dbFiltersMap: any = {
      'relationship.custom_object_type': 'relationship.custom_object_type.id',
    };

    if (!type) return acc;
    const mappedName = dbFiltersMap[type] || type;
    acc[mappedName] = filterValueToAPIValue(filter);
    return acc;
  }, {});

  if (keyword) {
    dbFilters.keyword = [dbFilters.keyword, keyword].filter(Boolean).join(' ');
  }

  return dbFilters;
};

type GetResultsPropsType = {
  count: boolean;
  t: i18next.TFunction;
};

const getIncludedParams = (columns: ColumnType[] | null) => {
  if (!columns || !isPopulatedArray(columns)) return null;
  const includes = columns
    .reduce((acc, current) => {
      if (current.source[0].includes('relationship'))
        acc.push(current.source[1]);
      return acc;
    }, [] as string[])
    .filter((value, index, arr) => arr.indexOf(value) === index);
  return isPopulatedArray(includes) ? includes : null;
};

const getCustomFields = (columns: ColumnType[] | null) => {
  if (!columns || !isPopulatedArray(columns)) return null;
  const customFields = columns
    .reduce((acc, current) => {
      if (current.magicString) acc.push(current.magicString);
      return acc;
    }, [] as string[])
    .filter((value, index, arr) => arr.indexOf(value) === index);
  return isPopulatedArray(customFields) ? customFields : null;
};

/* eslint complexity: [2, 20] */
export const getResults =
  ({ count, t }: GetResultsPropsType) =>
  async ({
    queryKey,
  }: {
    queryKey: any;
  }): Promise<GetResultsReturnType | number> => {
    const kind = getFromQueryValues<KindType>(queryKey[1], 'kind');
    const page = getFromQueryValues<number>(queryKey[1], 'page');
    const page_size = getFromQueryValues<number>(queryKey[1], 'resultsPerPage');
    const sortColumn = getFromQueryValues<string>(queryKey[1], 'sortColumn');
    const sortOrder = getFromQueryValues<SortDirectionType>(
      queryKey[1],
      'sortOrder'
    );
    const geo = getFromQueryValues<boolean>(queryKey[1], 'geo');
    const filters = getFromQueryValues<FiltersType | null>(
      queryKey[1],
      'filters'
    );
    const columns = getFromQueryValues<ColumnType[]>(queryKey[1], 'columns');
    const keyword = getFromQueryValues<string>(queryKey[1], 'keyword');
    const includes = getIncludedParams(columns);
    const customFields = getCustomFields(columns);

    const baseUrl = `${
      kind === 'custom_object'
        ? '/api/v2/cm/custom_object/search'
        : '/api/v2/cm/case/search'
    }${count ? '/count' : ''}`;

    const sort =
      sortColumn && sortOrder
        ? `${sortOrder === 'desc' ? '-' : ''}${sortColumn.replace(
            'attributes.',
            ''
          )}`
        : null;

    const parsedFilters = filtersToAPIFilters(filters, keyword);

    const url = buildUrl(baseUrl, {
      ...(page && { page }),
      ...(page_size && { page_size }),
      ...(filters && { filter: parsedFilters }),
      ...(sort && { sort }),
      ...(includes && { includes }),
      ...(customFields && { custom_fields: customFields }),
    });

    let results = await request<any>('GET', url).catch(
      (serverError: V2ServerErrorType) => {
        if (!count) throw serverError;
      }
    );

    if (count) return results?.data?.attributes?.total_results || 0;

    if (geo && results && isPopulatedArray(results?.data)) {
      const geoURL = buildUrl('/api/v2/geo/get_geo_features', {
        uuid: results?.data.map(
          (result: any) =>
            result.attributes.version_independent_uuid || result.id
        ),
      });
      const geoResults = await request<APIGeo.GetGeoFeaturesResponseBody>(
        'GET',
        geoURL
      ).catch((serverError: V2ServerErrorType) => {
        throw serverError;
      });

      results.data = results.data.map((result: any) => {
        const features =
          geoResults.data.find(
            geoResult =>
              geoResult.id === result.attributes.version_independent_uuid
          )?.attributes?.geo_json?.features || null;
        return {
          ...result,
          ...(features &&
            Array.isArray(features) &&
            features.length && { geoFeatures: features }),
        };
      });
    }

    return {
      data:
        results.data && isPopulatedArray(results?.data)
          ? APIResultsToResults(
              results.data,
              results?.included,
              columns || [],
              t
            )
          : [],
    };
  };

type UseResultsQueryParamsType = {
  identifier: IdentifierType;
  page: number;
  resultsPerPage: number;
  openServerErrorDialog: OpenServerErrorDialogType;
  kind: KindType;
  geo: boolean;
  currentSuccess: boolean;
  sortOrder?: SortDirectionType;
  sortColumn?: string;
  filters?: FiltersType | null;
  columns?: ColumnType[];
  keyword?: string;
};

export const useResultsQuery = (
  queryParams: UseResultsQueryParamsType,
  t: i18next.TFunction
) => {
  const {
    identifier,
    page,
    resultsPerPage,
    openServerErrorDialog,
    kind,
    geo,
    sortColumn,
    sortOrder,
    filters,
    columns,
    keyword,
    currentSuccess,
  } = queryParams;

  return useQuery(
    [
      QUERY_KEY_RESULTS,
      {
        identifier,
        page,
        resultsPerPage,
        kind,
        sortColumn,
        sortOrder,
        filters,
        geo,
        columns,
        keyword,
      },
    ],
    getResults({ count: false, t }),
    {
      onError: (error: V2ServerErrorsType) => openServerErrorDialog(error),
      enabled: currentSuccess && Boolean(identifier),
      keepPreviousData: true,
    }
  );
};

export const useResultsCountQuery = (
  queryParams: UseResultsQueryParamsType,
  t: i18next.TFunction
) => {
  const {
    identifier,
    openServerErrorDialog,
    kind,
    geo,
    filters,
    columns,
    keyword,
    currentSuccess,
  } = queryParams;

  return useQuery(
    [
      QUERY_KEY_RESULTS_COUNT,
      {
        identifier,
        kind,
        filters,
        geo,
        columns,
        keyword,
      },
    ],
    getResults({ count: true, t }),
    {
      onError: (error: V2ServerErrorsType) => openServerErrorDialog(error),
      enabled: currentSuccess && Boolean(identifier),
      keepPreviousData: true,
      staleTime: COUNT_STALE_TIME,
    }
  );
};

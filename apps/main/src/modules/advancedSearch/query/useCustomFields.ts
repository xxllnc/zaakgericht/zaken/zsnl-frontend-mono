// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery } from '@tanstack/react-query';
import { V2ServerErrorType } from '@zaaksysteem/common/src/types/ServerError';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICaseManagement } from '@zaaksysteem/generated/types/APICaseManagement.types';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { getFromQueryValues } from '../library/library';
import { CustomFieldType } from '../AdvancedSearch.types';
import { QUERY_KEY_CUSTOM_FIELDS } from './constants';

const getCustomFields = async ({ queryKey }: any) => {
  const uuid = getFromQueryValues(queryKey[1], 'uuid');

  if (!uuid) return [];

  const objectType =
    await request<APICaseManagement.GetPersistentCustomObjectTypeResponseBody>(
      'GET',
      buildUrl<any>(
        '/api/v2/cm/custom_object_type/get_persistent_custom_object_type',
        {
          uuid,
        }
      )
    ).catch((serverError: V2ServerErrorType) => {
      throw serverError;
    });

  const customFieldsData =
    objectType?.data?.attributes?.custom_field_definition?.custom_fields || [];

  const customFields: CustomFieldType[] = customFieldsData.map(
    ({ attribute_uuid, name, custom_field_type, magic_string, label }) => ({
      uuid: attribute_uuid,
      name,
      label,
      type: custom_field_type as string,
      magicString: magic_string as string,
    })
  );

  return customFields || [];
};

export const useCustomFields = ({
  uuid,
  config,
}: {
  uuid: string | null;
  config: {
    [key: string]: any;
  };
}) => {
  return useQuery<any, any>(
    [QUERY_KEY_CUSTOM_FIELDS, { uuid }],
    getCustomFields,
    {
      ...config,
    }
  );
};

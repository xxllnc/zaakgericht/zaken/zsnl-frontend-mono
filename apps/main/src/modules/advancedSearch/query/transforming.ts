// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated/types/APICaseManagement.types';
import { v4 } from 'uuid';
import { SavedSearchType, PermissionType } from '../AdvancedSearch.types';
import { replaceKeysInJSON } from '../library/library';
import { filtersKeyNamesReplacements } from '../library/config';

type ResultType = APICaseManagement.ListSavedSearchResponseBody['data'];

// Converts from API responses ---> Frontend ready Saved Searches objects
export const mapAPISavedSearch = (result: ResultType[0]): SavedSearchType => {
  const parseFilters = (
    filters: ResultType[0]['attributes']['filters']
  ): SavedSearchType['filters'] => ({
    ...filters,
    filters: filters.filters.map((filter: any) => ({
      ...filter,
      uuid: v4(),
    })),
  });

  const mapPermissions = (
    permissions: ResultType[0]['attributes']['permissions']
  ) =>
    permissions.map(
      ({ group_id, role_id, permission }): PermissionType => ({
        groupID: group_id,
        roleID: role_id,
        writePermission: permission.includes('write'),
        saved: true,
      })
    );

  const {
    attributes: {
      filters,
      name,
      permissions,
      columns,
      sort_column,
      sort_order,
    },
    id,
    meta,
  } = result;

  const returnObj = {
    uuid: id,
    name,
    filters: parseFilters(filters),
    permissions: mapPermissions(permissions),
    authorizations: meta?.authorizations || [],
    columns,
    sortColumn: sort_column,
    sortOrder: sort_order,
  };

  return replaceKeysInJSON(
    returnObj,
    filtersKeyNamesReplacements.map((element: string[]) =>
      [...element].reverse()
    )
  );
};

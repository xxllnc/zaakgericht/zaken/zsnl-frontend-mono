// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const QUERY_KEY_SAVEDSEARCHES = 'savedSearches';
export const QUERY_KEY_SAVEDSEARCH = 'savedSearch';
export const QUERY_KEY_RESULTS = 'results';
export const QUERY_KEY_RESULTS_COUNT = 'resultsCount';
export const QUERY_KEY_CUSTOM_FIELDS = 'customFields';

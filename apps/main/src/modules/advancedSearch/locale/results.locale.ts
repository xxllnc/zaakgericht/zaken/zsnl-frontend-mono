// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    tableView: 'Tabelweergave',
    mapView: 'Kaartweergave',
    openInNew: 'Open in nieuw venster',
    noResultsFound: 'Geen resultaten gevonden',
    types: {
      email: 'E-mailadres',
      geojson: 'Geo-json',
      url: 'Webadres',
      relationshipTypes: {
        customObject: 'Objectrelatie',
        subject: 'Contactrelatie',
        unknown: 'Onbekend',
      },
    },
    selectable: {
      select: 'Alleen de rijen op deze pagina zijn geselecteerd.',
      selectButton: 'Selecteer alle rijen',
      deselect: 'Alle rijen zijn geselecteerd.',
      deselectButton: 'Selecteer alleen de rijen op deze pagina',
      selectEverything: 'Selecteer alles',
      deselectEverything: 'Deselecteer alles',
    },
    export: {
      successMessage:
        'De export is succesvol gestart. Na afronding is deze beschikbaar onder "Exportbestanden".',
      buttonLabel: 'Exporteren als…',
      formats: {
        CSV: 'CSV',
      },
    },
  },
};

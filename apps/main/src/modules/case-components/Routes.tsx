// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Route, Routes } from 'react-router-dom';
import { SessionType } from '@zaaksysteem/common/src/store/session/session.reducer';
import Relations from '../case/views/relations';
import Timeline from '../case/views/timeline';
import Communication from '../case/views/communication';
import ObjectForm from '../case/views/phases/Dialogs/ObjectForm';
import { CaseObjType, CaseTypeType } from '../case/Case.types';
import TasksWrapper from './TasksWrapper';
import { refreshTasks } from './library';

export interface RoutesPropsType {
  caseObj: CaseObjType;
  caseType: CaseTypeType;
  session: SessionType;
}

/* eslint complexity: [2, 9] */
const CaseComponentRoutes: React.ComponentType<RoutesPropsType> = ({
  caseObj,
  caseType,
  session,
}) => (
  <Routes>
    <Route
      path={`communication/*`}
      element={<Communication caseObj={caseObj} caseType={caseType} />}
    />
    <Route
      path={`phase/:phaseNumber/tasks/*`}
      element={
        <TasksWrapper
          caseObj={caseObj}
          setOpenTasksCount={refreshTasks}
          iframe={true}
        />
      }
    />
    <Route
      path={`relations`}
      element={
        <Relations session={session} caseObj={caseObj} caseType={caseType} />
      }
    />
    <Route path={`object/:type/*`}>
      <Route
        path=""
        element={
          <ObjectForm
            session={session}
            // we don't pass the caseObj here
            // because it needs to be up to date to prefill values
            // and the case can have changed in the mean time
            caseUuid={caseObj.uuid}
            caseType={caseType}
          />
        }
      />
      <Route
        path=":objectUuid"
        element={
          <ObjectForm
            session={session}
            caseUuid={caseObj.uuid}
            caseType={caseType}
          />
        }
      />
    </Route>
    <Route path={`timeline`} element={<Timeline caseObj={caseObj} />} />
  </Routes>
);

export default CaseComponentRoutes;

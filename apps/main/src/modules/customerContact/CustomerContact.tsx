// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import CommunicationModule from '@zaaksysteem/communication-module/src';
import TopBar from '../../components/TopBar/TopBar';
import { useCustomerContactStyles } from './CustomerContact.styles';

const CustomerContact = () => {
  const classes = useCustomerContactStyles();
  const [t] = useTranslation('customerContact');

  return (
    <div className={classes.wrapper}>
      <TopBar title={t('title')} />
      <CommunicationModule
        capabilities={{
          allowSplitScreen: true,
          canAddAttachmentToCase: false,
          canAddSourceFileToCase: false,
          canAddThreadToCase: true,
          canCreateContactMoment: false,
          canCreatePipMessage: false,
          canCreateEmail: false,
          canCreateNote: false,
          canCreateMijnOverheid: false,
          canDeleteMessage: true,
          canImportMessage: false,
          canSelectCase: false,
          canSelectContact: false,
          canFilter: true,
          canOpenPDFPreview: true,
        }}
        context="inbox"
      />
    </div>
  );
};

export default CustomerContact;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@tanstack/react-query';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import Button from '@mintlab/ui/App/Material/Button';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { handlePromise } from './Profile.actions';
import { ProfileParamsType } from './Profile.types';
import { useProfileStyles } from './Profile.style';
import locale from './profile.locale';
import { Attributes } from './Attributes';

const Profile: React.FunctionComponent = () => {
  const { contactId, contactType } = useParams<
    keyof ProfileParamsType
  >() as ProfileParamsType;
  const [t] = useTranslation();
  const classes = useProfileStyles();

  const data = useQuery(
    ['profile', contactId, contactType] as const,
    ({ queryKey: [__, id, type] }) => handlePromise(t, id, type)
  );

  if (data.status === 'loading' || !data.data) {
    return <Loader />;
  }

  const { title, description, formDefinition, uuid } = data.data;

  const locationHref = (uuid: number) => {
    if (window.top) {
      window.top.location.href = `/main/object/${uuid}`;
    }
  };

  return (
    <div className={classes.wrapper}>
      <SubHeader title={title} description={description} />
      <Attributes formDefinition={formDefinition} />
      <Button name="profileLink" onClick={() => locationHref(uuid)}>
        {t('profile:link')}
      </Button>
    </div>
  );
};

const ProfileModule: React.FunctionComponent = () => (
  <I18nResourceBundle resource={locale} namespace="profile">
    <Profile />
  </I18nResourceBundle>
);

export default ProfileModule;

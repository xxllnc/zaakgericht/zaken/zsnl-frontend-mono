// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import generateCustomFieldFormDefinition from '@zaaksysteem/common/src/components/form/library/generateCustomFieldFormDefinition';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICaseManagement } from '@zaaksysteem/generated';
import { buildUrl } from '@mintlab/kitchen-sink/source';

const fetchSubject = async (contactId: string, contactType: string) => {
  const getFilterParameters = () => {
    switch (contactType) {
      case 'employee':
        return {
          employee_uuid: contactId,
        };

      case 'organization':
        return {
          organization_uuid: contactId,
        };

      case 'person':
        return {
          person_uuid: contactId,
        };

      default:
        return {};
    }
  };

  const url = buildUrl<APICaseManagement.GetSubjectRequestParams>(
    '/api/v2/cm/subject/get_subject',
    getFilterParameters()
  );
  const result = await request('GET', url);

  return result.data || [];
};

const fetchObjects = async (uuid: string) => {
  const url = buildUrl<APICaseManagement.GetCustomObjectRequestParams>(
    '/api/v2/cm/custom_object/get_custom_object',
    {
      uuid,
    }
  );
  const result = await request('GET', url);

  return result.data || [];
};

const fetchObjectTypes = async (uuid: string) => {
  const url = buildUrl<APICaseManagement.GetCustomObjectTypeRequestParams>(
    `/api/v2/cm/custom_object_type/get_custom_object_type`,
    {
      uuid,
    }
  );
  const result = await request('GET', url);

  return result.data || [];
};

export const handlePromise = async (
  t: i18next.TFunction,
  contactId: string,
  contactType: string
) => {
  const subject = await fetchSubject(contactId, contactType);
  const object = await fetchObjects(
    subject.relationships.related_custom_object.data.id
  );
  const objectType = await fetchObjectTypes(
    object.relationships.custom_object_type.data.id
  );

  return {
    title: t('profile:title'),
    description: t('profile:description'),
    uuid: object.attributes.version_independent_uuid,
    formDefinition: generateCustomFieldFormDefinition({
      customFieldsDefinition:
        objectType.attributes.custom_field_definition.custom_fields,
      customFieldsValues: object.attributes.custom_fields,
      readOnly: true,
      t,
    }),
  };
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    title: 'Overige gegevens',
    description: 'Een overzicht van gerelateerde gegevens.',
    link: 'Object openen',
  },
};

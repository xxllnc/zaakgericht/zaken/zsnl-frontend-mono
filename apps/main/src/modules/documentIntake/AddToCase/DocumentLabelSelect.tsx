// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useQuery } from '@tanstack/react-query';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APIDocument } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { FormFieldPropsType } from '@zaaksysteem/common/src/components/form/types';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import useServerErrorDialog, {
  OpenServerErrorDialogType,
} from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { FormFieldComponentType } from '@zaaksysteem/common/src/components/form/types/Form2.types';

export const DOCUMENT_LABEL_SELECT = 'documentLabelSelect';

const fetchDocumentChoices =
  (onError: OpenServerErrorDialogType) =>
  async (case_uuid: string): Promise<ValueType<string>[]> => {
    if (!case_uuid) {
      return [];
    }

    const body = await request<APIDocument.GetDocumentLabelsResponseBody>(
      'GET',
      buildUrl<APIDocument.GetDocumentLabelsRequestParams>(
        '/api/v2/document/get_document_labels',
        { case_uuid }
      ),
      {} as APIDocument.AddDocumentToCaseRequestBody
    ).catch(onError);

    return body && body.data
      ? body.data.map(({ id, attributes: { name, public_name } }) => ({
          label: public_name || name,
          value: id,
        }))
      : [];
  };
export interface DocumentLabelSelectPropsType extends FormFieldPropsType {}

export const DocumentLabelSelect: FormFieldComponentType<
  ValueType<string>,
  { caseUuid: string }
> = ({ config, name, ...restProps }) => {
  const caseUuid = config?.caseUuid;
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  const { data, status } = useQuery(
    ['documentLabels', caseUuid],
    ({ queryKey: [__, uuid] }) =>
      fetchDocumentChoices(openServerErrorDialog)(uuid)
  );

  return (
    <>
      <Select
        {...restProps}
        name={name}
        choices={data || []}
        loading={status === 'loading'}
      />
      {ServerErrorDialog}
    </>
  );
};

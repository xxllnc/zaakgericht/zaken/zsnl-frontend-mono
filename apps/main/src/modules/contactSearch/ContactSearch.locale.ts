// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    contactSearch: 'Contact zoeken',
    sideMenu: {
      persons: 'Personen',
      organizations: 'Organisaties',
      employees: 'Medewerkers',
    },
    title: '{{type}} zoeken',
    subTitle: 'Zoek een {{type}}',
    search: {
      button: 'Zoeken',
      person: {
        bsn: 'Burgerservicenummer',
        zipcode: 'Postcode',
        houseNumber: 'Huisnummer',
        dateOfBirth: 'Geboortedatum',
        prefix: 'Tussenvoegsel',
        familyName: 'Achternaam',
      },
      organization: {
        rsin: 'RSIN',
        coc: 'KvK-nummer',
        cocLocation: 'Vestigingsnummer',
        name: 'Handelsnaam',
        street: 'Straatnaam',
        houseNumber: 'Huisnummer',
        houseNumberLetter: 'Huisletter',
        houseNumberSuffix: 'Huisnummer toevoeging',
        zipcode: 'Postcode',
        city: 'Plaats',
        phoneNumber: 'Telefoonnummer',
        email: 'E-mailadres',
      },
      employee: {
        query: 'Zoekterm',
        inactive: 'Inclusief inactieve medewerkers',
      },
    },
    results: {
      button: 'Opnieuw zoeken',
      columns: {
        name: 'Naam',
        // person & organization
        externalSubscription: '',
        address: 'Adres',
        // person
        dateOfBirth: 'Geboortedatum',
        //organization
        coc: 'KvK-nummer',
        //employee
        department: 'Afdeling',
        phoneNumber: 'Telefoonnummer',
      },
      externalSubscription: {
        true: 'Authentiek',
        false: 'Niet-authentiek',
      },
      noResults: 'Geen resultaten gevonden.',
      tooManyResults:
        'Er zijn meer dan 10 resultaten gevonden. Verklein uw zoekopdracht en probeer het opnieuw.',
    },
  },
};

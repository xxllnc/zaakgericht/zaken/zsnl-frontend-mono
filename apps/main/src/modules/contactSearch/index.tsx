// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import ContactSearch from './ContactSearch';
import locale from './ContactSearch.locale';

export default function ContactSearchModule() {
  return (
    <I18nResourceBundle resource={locale} namespace="contactSearch">
      <Routes>
        <Route path="" element={<Navigate to="person" replace={true} />} />
        <Route path={':type'} element={<ContactSearch />} />
      </Routes>
    </I18nResourceBundle>
  );
}

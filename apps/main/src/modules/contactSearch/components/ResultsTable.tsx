// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { useSortableTableStyles } from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable.style';
import { useStyles } from '../ContactSearch.styles';
import { ContactType, ResultType } from '../ContactSearch.types';
import { getColumns } from './getColumns';

export interface ResultsTablePropsType {
  type: ContactType;
  results: ResultType[];
  setResults: (results: ResultType[] | undefined) => void;
}

const ResultsTable: React.FunctionComponent<ResultsTablePropsType> = ({
  type,
  results,
  setResults,
}) => {
  const classes = useStyles();
  const tableStyles = useSortableTableStyles();
  const [t] = useTranslation('contactSearch');

  return (
    <div className={classes.resultsWrapper}>
      <Button
        action={() => {
          setResults(undefined);
        }}
        name="clearResults"
        sx={{ width: 200 }}
      >
        {t('results.button')}
      </Button>
      {results.length > 10 ? (
        <div>{t('results.tooManyResults')}</div>
      ) : (
        <div
          style={{
            flex: '1 1 auto',
            height: '100%',
            width: '100%',
            paddingRight: '16px',
          }}
        >
          <SortableTable
            styles={tableStyles}
            rows={results}
            columns={getColumns(t, type)}
            noRowsMessage={t('results.noResults')}
          />
        </div>
      )}
    </div>
  );
};

export default ResultsTable;

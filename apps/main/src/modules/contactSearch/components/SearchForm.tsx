// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { generateValidationMap } from '@zaaksysteem/common/src/components/form/validation/library/generateValidationMap';
import Button from '@mintlab/ui/App/Material/Button';
import { useStyles } from '../ContactSearch.styles';
import { ContactType, SearchType } from '../ContactSearch.types';
import { getFormDefinition, getCanSubmit } from './formDefinitions';

export interface SearchFormPropsType {
  type: ContactType;
  search: SearchType;
}

const SearchForm: React.FunctionComponent<SearchFormPropsType> = ({
  type,
  search,
}) => {
  const classes = useStyles();
  const [t] = useTranslation('contactSearch');
  const seperatorLabel = t('common:or').toUpperCase();

  const formDefinition = getFormDefinition[type]({
    t,
  });
  const validationMap = generateValidationMap(formDefinition);

  let {
    fields,
    formik: { isValid, values },
  } = useForm({
    formDefinition,
    validationMap,
  });

  const canSubmit = getCanSubmit(type, isValid, values);

  useEffect(() => {
    const listener = (event: any) => {
      const isSubmitKey = ['Enter', 'NumpadEnter'].includes(event.code);
      const isTextField = event.target.type === 'text';

      if (isSubmitKey && isTextField && canSubmit) {
        search(values);
      }
    };

    document.addEventListener('keydown', listener);

    return () => {
      document.removeEventListener('keydown', listener);
    };
  }, [values]);

  return (
    <div className={classes.searchWrapper}>
      <SubHeader
        title={t('title', {
          type: t(`common:entityType.${type}`),
        })}
        description={t('subTitle', {
          type: t(`common:entityType.${type}`).toLowerCase(),
        })}
      />
      <div>
        {fields.map(
          (
            { FieldComponent, key, type, manualRequired, label, ...rest }: any,
            index: number
          ) =>
            rest.isLabel ? (
              <div
                key={`${rest.name}-${index}-formcontrol-wrapper`}
                className={classes.separator}
              >
                {seperatorLabel}
              </div>
            ) : (
              <FormControlWrapper
                {...rest}
                label={manualRequired ? `${label} *` : label}
                key={`${rest.name}-formcontrol-wrapper`}
              >
                <FieldComponent {...rest} t={t} />
              </FormControlWrapper>
            )
        )}
      </div>
      <Button
        action={() => {
          search(values);
        }}
        disabled={!canSubmit}
        name="contactSearchFormSubmit"
        sx={{ width: '200px' }}
      >
        {t('search.button')}
      </Button>
    </div>
  );
};

export default SearchForm;

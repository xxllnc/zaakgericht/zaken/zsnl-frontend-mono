// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';

export type ContactType = SubjectTypeType;

export type SearchType = (values: any) => void;

export type ColumnType = {
  name: string;
  label: string;
  width: number;
  flexGrow?: number;
  disableSort?: boolean;
};

export type GetColumnsType = (
  t: i18next.TFunction,
  type: ContactType
) => ColumnType[];

export type PersonType = {
  uuid: string;
  name: string;
  address: string;
  dateOfBirth: string;
  externalSubscription: boolean;
};

export type OrganizationType = {
  uuid: string;
  coc: string;
  name: string;
  address: string;
  externalSubscription: boolean;
};

export type EmployeeType = {
  uuid: string;
  name: string;
  department: string;
  phoneNumber: string | null;
};

export type ResultType = PersonType | OrganizationType | EmployeeType;

export type PerformSearchType = (
  type: ContactType,
  values: any
) => Promise<ResultType[]>;
export type SearchPersonType = (values: any) => Promise<ResultType[]>;
export type SearchOrganizationType = (values: any) => Promise<ResultType[]>;
export type SearchEmployeeType = (values: any) => Promise<EmployeeType[]>;

export type FormatResultType = (result: any) => ResultType;

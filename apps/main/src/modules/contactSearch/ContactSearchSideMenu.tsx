// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  SideMenu,
  SideMenuItemType,
} from '@mintlab/ui/App/Zaaksysteem/SideMenu';
import { ContactSearchParamsType } from './ContactSearch';

type ContactViewSideMenuPropsType = Pick<ContactSearchParamsType, 'type'>;

export const ContactSearchSideMenu: React.ComponentType<
  ContactViewSideMenuPropsType
> = ({ type }) => {
  const [t] = useTranslation('contactSearch');

  const MenuItemLink = React.forwardRef<any, SideMenuItemType>(
    ({ href, ...restProps }, ref) => <Link to={href || ''} {...restProps} />
  );
  MenuItemLink.displayName = 'MenuItemLink';

  const menuItems: SideMenuItemType[] = [
    {
      id: 'person',
      label: t('sideMenu.persons'),
      href: '../person',
      icon: <Icon>{iconNames.person}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'organization',
      label: t('sideMenu.organizations'),
      href: '../organization',
      icon: <Icon>{iconNames.domain}</Icon>,
      component: MenuItemLink,
    },
    {
      id: 'employee',
      label: t('sideMenu.employees'),
      href: '../employee',
      icon: <Icon>{iconNames.person_outline}</Icon>,
      component: MenuItemLink,
    },
  ].map<SideMenuItemType>(menuItem => ({
    ...menuItem,
    selected: type === menuItem.id,
  }));

  return (
    <div>
      <SideMenu items={menuItems} />
    </div>
  );
};

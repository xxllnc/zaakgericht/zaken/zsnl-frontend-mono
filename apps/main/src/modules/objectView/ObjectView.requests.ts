// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICaseManagement } from '@zaaksysteem/generated';

export const fetchObject = async (uuid: string) => {
  const url = buildUrl<APICaseManagement.GetCustomObjectRequestParams>(
    '/api/v2/cm/custom_object/get_custom_object',
    {
      uuid,
    }
  );

  const response = await request<APICaseManagement.GetCustomObjectResponseBody>(
    'GET',
    url
  );

  return response.data;
};

export const updateObject = async (
  data: APICaseManagement.UpdateCustomObjectRequestBody
) => {
  const url = '/api/v2/cm/custom_object/update_custom_object';

  const response =
    await request<APICaseManagement.UpdateCustomObjectResponseBody>(
      'POST',
      url,
      data
    );

  return response;
};

export const activateObject = async (
  data: APICaseManagement.UpdateCustomObjectRequestBody
) => {
  const url = '/api/v2/cm/custom_object/update_custom_object';

  const response =
    await request<APICaseManagement.UpdateCustomObjectResponseBody>(
      'POST',
      url,
      data
    );

  return response;
};

export const fetchObjectType = async (uuid: string) => {
  const url = buildUrl<APICaseManagement.GetCustomObjectTypeRequestParams>(
    '/api/v2/cm/custom_object_type/get_custom_object_type',
    {
      uuid,
    }
  );

  const response =
    await request<APICaseManagement.GetCustomObjectTypeResponseBody>(
      'GET',
      url
    );

  return response;
};

export const fetchCaseRelations = async (uuid: string) => {
  const url = buildUrl<APICaseManagement.GetRelatedCasesRequestParams>(
    '/api/v2/cm/custom_object/get_related_cases',
    {
      uuid,
    }
  );

  const response = await request<APICaseManagement.GetRelatedCasesResponseBody>(
    'GET',
    url
  );

  return response;
};

export const fetchObjectRelations = async (uuid: string) => {
  const url = buildUrl<APICaseManagement.GetRelatedObjectsRequestParams>(
    '/api/v2/cm/custom_object/get_related_objects',
    {
      uuid,
    }
  );

  const response =
    await request<APICaseManagement.GetRelatedObjectsResponseBody>('GET', url);

  return response;
};

export const fetchSubjectRelations = async (uuid: string) => {
  const url = buildUrl<APICaseManagement.GetRelatedCasesRequestParams>(
    '/api/v2/cm/custom_object/get_related_subjects',
    {
      uuid,
    }
  );

  const response =
    await request<APICaseManagement.GetRelatedSubjectsResponseBody>('GET', url);

  return response;
};

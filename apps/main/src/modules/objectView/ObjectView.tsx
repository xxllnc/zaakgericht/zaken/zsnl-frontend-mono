// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';
import { PanelLayout } from '../../components/PanelLayout/PanelLayout';
import { Panel } from '../../components/PanelLayout/Panel';
import TopBar from '../../components/TopBar/TopBar';
import SideMenu from './components/Menu/Menu';
import Map from './components/Map/Map';
import { useObjectViewStyles } from './ObjectView.style';
import Attributes from './components/Attributes/Attributes';
import Relationships from './components/Relationships/Relationships';
import Timeline from './components/Timeline';
import { ObjectType, ObjectTypeType } from './ObjectView.types';

export interface ObjectViewPropsType {
  object: ObjectType;
  objectType: ObjectTypeType;
  refreshObject: () => void;
}

const ObjectView: React.FunctionComponent<ObjectViewPropsType> = ({
  object,
  objectType,
  refreshObject,
}) => {
  const classes = useObjectViewStyles();

  return (
    <div className={classes.wrapper}>
      <TopBar title={object.title} />
      <div className={classes.content}>
        <PanelLayout>
          <Panel type="side">
            <SideMenu />
          </Panel>
          <Panel>
            <Routes>
              <Route
                path=""
                element={<Navigate to="attributes" replace={true} />}
              />
              <Route
                path={'attributes'}
                element={
                  <Attributes
                    object={object}
                    objectType={objectType}
                    refreshObject={refreshObject}
                  />
                }
              />
              <Route
                path={'timeline'}
                element={<Timeline uuid={object.versionIndependentUuid} />}
              />
              <Route
                path={'map'}
                element={
                  <Map
                    object={object}
                    objectType={objectType}
                    objectUuid={object.versionIndependentUuid}
                  />
                }
              />
              <Route
                path={'relationships'}
                element={<Relationships object={object} />}
              />
            </Routes>
          </Panel>
        </PanelLayout>
      </div>
    </div>
  );
};

export default ObjectView;

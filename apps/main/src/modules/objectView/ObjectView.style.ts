// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useObjectViewStyles = makeStyles(() => ({
  wrapper: {
    width: '100%',
    height: '100%',
  },
  content: {
    height: 'calc(100% - 75px)',
  },
}));

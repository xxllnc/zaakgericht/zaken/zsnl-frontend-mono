// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import * as i18next from 'i18next';
import { ColumnType } from '@mintlab/ui/App/Zaaksysteem/SortableTable/types/SortableTableTypes';
import Progress from '@mintlab/ui/App/Zaaksysteem/Progress/Progress';
import {
  RelatedCaseRowType,
  RelatedSubjectRowType,
} from '../../ObjectView.types';

export const getCasesColumns = ({
  t,
}: {
  t: i18next.TFunction;
}): ColumnType[] => {
  return [
    {
      name: 'nr',
      width: 100,
      label: t('objectView:relationships.cases.columns.nr'),
    },
    {
      name: 'status',
      width: 150,
      label: t('objectView:relationships.cases.columns.status'),
      /* eslint-disable-next-line */
      cellRenderer: ({ rowData }: { rowData: RelatedCaseRowType }) => {
        return <div>{t(`common:case.status.${rowData.status}`) as string}</div>;
      },
    },
    {
      name: 'progress',
      width: 130,
      label: t('objectView:relationships.cases.columns.progress'),
      /* eslint-disable-next-line */
      cellRenderer: ({ rowData }: { rowData: RelatedCaseRowType }) => {
        return (
          <div>
            <Progress percentage={rowData.progress} />
          </div>
        );
      },
    },
    {
      name: 'caseType',
      width: 1,
      flexGrow: 1,
      label: t('objectView:relationships.cases.columns.caseType'),
    },
    {
      name: 'extra',
      width: 1,
      flexGrow: 1,
      label: t('objectView:relationships.cases.columns.extra'),
    },
    {
      name: 'assignee',
      width: 1,
      flexGrow: 1,
      label: t('objectView:relationships.cases.columns.assignee'),
    },
    {
      name: 'result',
      width: 1,
      flexGrow: 1,
      label: t('objectView:relationships.cases.columns.result'),
    },
  ];
};

export const getObjectsColumns = ({
  t,
}: {
  t: i18next.TFunction;
}): ColumnType[] => {
  return [
    {
      name: 'name',
      width: 400,
      label: t('objectView:relationships.objects.columns.name'),
    },
    {
      name: 'objectType',
      width: 1,
      flexGrow: 1,
      label: t('objectView:relationships.objects.columns.objectType'),
    },
  ];
};

export const getSubjectColumns = ({
  t,
}: {
  t: i18next.TFunction;
}): ColumnType[] => {
  return [
    {
      name: 'name',
      width: 400,
      label: t('objectView:relationships.subjects.columns.name'),
    },
    {
      name: 'subjectType',
      width: 1,
      flexGrow: 1,
      label: t('objectView:relationships.subjects.columns.subjectType'),
      // eslint-disable-next-line
      cellRenderer: ({ rowData }: { rowData: RelatedSubjectRowType }) => (
        <>
          {t(
            `objectView:relationships.subjects.columns.${rowData.subjectType}`
          )}
        </>
      ),
    },
    {
      name: 'rolType',
      width: 1,
      flexGrow: 1,
      label: t('objectView:relationships.subjects.columns.roleType'),
      cellRenderer: ({ rowData }: { rowData: RelatedSubjectRowType }) => {
        if (!rowData || !rowData?.roleType) return '';

        const getItems = rowData.roleType.map((item: any) => {
          return item.meta.summary;
        });

        return getItems.join(', ');
      },
    },
  ];
};

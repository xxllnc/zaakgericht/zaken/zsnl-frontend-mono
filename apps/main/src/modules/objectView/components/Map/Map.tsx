// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { GeoMap } from '@mintlab/ui/App/External/GeoMap';
import { request } from '@zaaksysteem/common/src/library/request/request';
import useServerErrorDialog, {
  OpenServerErrorDialogType,
} from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { APIGeo } from '@zaaksysteem/generated';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { ObjectTypeType, ObjectType } from '../../ObjectView.types';

const getObjectGeoFeatures = async (
  uuid: string,
  onError: OpenServerErrorDialogType
): Promise<GeoJSON.FeatureCollection | undefined> => {
  const body = await request<APIGeo.GetGeoFeaturesResponseBody>(
    'GET',
    buildUrl<APIGeo.GetGeoFeaturesRequestParams>(
      '/api/v2/geo/get_geo_features',
      { uuid: [uuid] }
    )
  ).catch(onError);

  return body?.data && body.data[0]
    ? {
        type: 'FeatureCollection',
        features: body.data.reduce(
          (acc, dataEntry) =>
            acc.concat(dataEntry.attributes?.geo_json.features || []),
          []
        ) as any,
      }
    : undefined;
};

const Map: React.FunctionComponent<{
  objectUuid: string;
  object: ObjectType;
  objectType: ObjectTypeType;
}> = ({ objectUuid, object, objectType }) => {
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [objectGeoFeatures, setObjectGeoFeatures] = useState<
    GeoJSON.FeatureCollection | undefined
  >(undefined);

  React.useEffect(
    () =>
      void getObjectGeoFeatures(objectUuid, openServerErrorDialog).then(
        setObjectGeoFeatures
      ),
    []
  );

  return (
    <React.Fragment>
      {ServerErrorDialog}
      <GeoMap
        geoFeature={objectGeoFeatures || null}
        name="ObjectMap"
        canDrawFeatures={false}
        minHeight="100%"
        context={{
          type: 'ObjectMap',
          data: { object, objectType },
        }}
      />
    </React.Fragment>
  );
};

export default Map;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { SideMenu } from '@mintlab/ui/App/Zaaksysteem/SideMenu';
import { ObjectViewModuleParamsType } from '../..';
import { getItems } from './Menu.library';
import { useSideMenuStyles } from './Menu.styles';

const Menu: React.ComponentType = () => {
  const [t] = useTranslation('objectView');
  const classes = useSideMenuStyles();
  const params = useParams<
    keyof ObjectViewModuleParamsType
  >() as ObjectViewModuleParamsType;
  const view = params['*'];
  const items = getItems(t, view);

  return (
    <div className={classes.wrapper}>
      <SideMenu items={items} />
    </div>
  );
};

export default Menu;

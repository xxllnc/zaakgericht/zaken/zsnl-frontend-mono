// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useSideMenuStyles = makeStyles(() => ({
  wrapper: {
    padding: 12,
  },
}));

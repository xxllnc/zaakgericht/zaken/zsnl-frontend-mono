// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import fecha from 'fecha';
import { APICaseManagement } from '@zaaksysteem/generated';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { getDataFuncType } from '@zaaksysteem/common/src/hooks/useInfiniteScroll';
import { TimelineItemType } from '@zaaksysteem/common/src/components/Timeline/Timeline.types';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { PAGE_LENGTH } from '@zaaksysteem/common/src/components/Timeline/Timeline';

type GetDataType = ({
  openServerErrorDialog,
  uuid,
}: {
  openServerErrorDialog: OpenServerErrorDialogType;
  uuid: string;
}) => getDataFuncType<TimelineItemType, any>;

export const getData: GetDataType = ({ openServerErrorDialog, uuid }) =>
  async function getDataAsync({ pageNum, keyword, startDate, endDate }: any) {
    const urlParams: APICaseManagement.GetCustomObjectEventLogsRequestParams = {
      page: pageNum,
      page_size: PAGE_LENGTH,
      ...(uuid && { custom_object_uuid: uuid }),
      ...(startDate && {
        period_start:
          fecha.format(new Date(startDate), 'YYYY-MM-DD') + 'T00:00:00+01:00',
      }),
      ...(endDate && {
        period_end:
          fecha.format(new Date(endDate), 'YYYY-MM-DD') + 'T23:59:59+01:00',
      }),
    };

    const url =
      buildUrl<APICaseManagement.GetCustomObjectEventLogsRequestParams>(
        `/api/v2/cm/custom_object/get_custom_object_event_logs`,
        urlParams
      );
    const result =
      await request<APICaseManagement.GetCustomObjectEventLogsResponseBody>(
        'GET',
        url
      ).catch((err: V2ServerErrorsType) => {
        openServerErrorDialog(err);
      });

    if (!result || !result.data) return { rows: [] };

    return {
      rows: result.data.map(row => {
        const { attributes } = row;

        return {
          id: attributes?.id || '',
          type: attributes?.type || '',
          author: attributes?.user || '',
          date: new Date(attributes?.date || ''),
          description: attributes?.description || '',
        };
      }),
    };
  };

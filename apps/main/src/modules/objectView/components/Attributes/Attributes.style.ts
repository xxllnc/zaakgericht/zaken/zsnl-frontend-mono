// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useAttributesStyles = makeStyles(() => ({
  wrapper: {
    width: '100%',
    padding: '20px 32px',
    display: 'flex',
    flexDirection: 'column',
    maxWidth: '930px',
    gap: 20,
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  buttons: {
    display: 'flex',
    justifyContent: 'flex-end',
    gap: '20px',
    marginLeft: '30px',
  },
  colContent: {
    flex: 1,
  },
}));

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, Suspense } from 'react';
import { Provider } from 'react-redux';
import { useTranslation } from 'react-i18next';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
//@ts-ignore
import MaterialUiThemeProvider from '@mintlab/ui/App/Material/MaterialUiThemeProvider/MaterialUiThemeProvider';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
//@ts-ignore
import ErrorBoundary from '@zaaksysteem/common/src/components/ErrorBoundary/ErrorBoundary';
import DialogRenderer from '@zaaksysteem/common/src/components/DialogRenderer/DialogRenderer';
import SnackbarRenderer from '@zaaksysteem/common/src/components/SnackbarRenderer/SnackbarRenderer';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import useDialogs from '@zaaksysteem/common/src/library/useDialogs';
import useMessages from '@zaaksysteem/common/src/library/useMessages';
import { configureStore } from './configureStore';
import Routes from './Routes';
import ErrorDialog from './components/ErrorDialog/ErrorDialog';
import { DIALOG_ERROR } from './constants/dialog.constants';
import locale from './locale/main.locale';

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: Infinity,
      refetchOnWindowFocus: false,
      retry: 0,
    },
  },
});

const initialState = {};
const store = configureStore(initialState);

function App() {
  const [t] = useTranslation('common');
  const [, addDialogs] = useDialogs();
  const [, addMessages] = useMessages();

  useEffect(() => {
    addMessages(
      t('serverErrors.status', {
        returnObjects: true,
      })
    );

    addDialogs({
      [DIALOG_ERROR]: ErrorDialog,
    });
  }, []);

  return (
    <Provider store={store}>
      <QueryClientProvider client={queryClient}>
        <MaterialUiThemeProvider>
          <I18nResourceBundle namespace="main" resource={locale}>
            <Suspense fallback={<Loader delay={200} />}>
              <Routes prefix={process.env.APP_CONTEXT_ROOT || '/main'} />
              <ErrorBoundary>
                <DialogRenderer />
                <SnackbarRenderer />
              </ErrorBoundary>
            </Suspense>
          </I18nResourceBundle>
        </MaterialUiThemeProvider>
      </QueryClientProvider>
    </Provider>
  );
}

export default App;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';
//@ts-ignore
import { objectifyParams } from '@mintlab/kitchen-sink/source/url';
import { APIDocument } from '@zaaksysteem/generated';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
// @ts-ignore
import locale from '@zaaksysteem/common/src/components/DocumentExplorer/locale/locale';
import { DocumentUploadDialog } from '@zaaksysteem/common/src/components/dialogs/DocumentUploadDialog/DocumentUploadDialog';
import useDocumentExplorer from '@zaaksysteem/common/src/components/DocumentExplorer/useDocumentExplorer';
import Button from '@mintlab/ui/App/Material/Button';
import FileExplorer from '@zaaksysteem/common/src/components/DocumentExplorer/components/FileExplorer/FileExplorer';
import Search from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/library/Search';
import {
  PresetLocations,
  HandleItemClickType,
  StoreShapeType,
} from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/types/types';
import { DirectoryItemType } from '@zaaksysteem/common/src/components/DocumentExplorer/components/FileExplorer/types/FileExplorerTypes';
import BreadCrumbs from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/library/BreadCrumbs';
import { useDocumentExplorerStyles } from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/DocumentExplorer.style';
import { getColumns } from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/utils/getColumns';
import { buildUrl, ENCODED_NULL_BYTE } from '@mintlab/kitchen-sink/source';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import IconButton from '@mui/material/IconButton';
import { isDocument } from '@zaaksysteem/common/src/components/DocumentExplorer/components/DocumentExplorer/utils/isDocument';

type PipCaseDocumentsPropsType = {
  capabilities: {
    canDownload: boolean;
    canSearch: boolean;
  };
};

type CaseDocumentsParamsType = {
  caseUuid: string;
};

const CaseDocuments: React.ComponentType<PipCaseDocumentsPropsType> = ({
  capabilities,
}) => {
  const { caseUuid } = useParams<
    keyof CaseDocumentsParamsType
  >() as CaseDocumentsParamsType;

  const getURL = (state: StoreShapeType) => {
    const { search, location } = state;

    const getFilter = () =>
      location === PresetLocations.Search
        ? { fulltext: search }
        : {
            'relationships.parent.id':
              location === PresetLocations.Home ? ENCODED_NULL_BYTE : location,
          };

    return buildUrl<APIDocument.GetDirectoryEntriesForCaseRequestParams>(
      `/api/v2/document/get_directory_entries_for_case`,
      {
        case_uuid: caseUuid,
        filter: getFilter(),
        no_empty_folders: '1',
      }
    );
  };

  const { case_status } = objectifyParams(window.location.search);
  const canUpload = case_status === 'open';
  const { canSearch } = capabilities;
  const classes = useDocumentExplorerStyles();
  const [t] = useTranslation();
  const [state, actions, ServerErrorDialog] = useDocumentExplorer({
    t,
    getURL,
  });
  const {
    doRefreshAction,
    doSearchAction,
    doNavigateAction,
    doFileOpenAction,
    doSearchCloseAction,
  } = actions;
  const { items, location, loading, path } = state;
  const [createCaseDocumentsOpen, setCreateCaseDocumentsOpen] = useState(false);
  const [isFiltered, setIsFiltered] = useState(false);

  const handleItemClick: HandleItemClickType = (event, item) => {
    event.stopPropagation();
    event.preventDefault();
    doNavigateAction(item.id);
  };
  const handleFolderOpen = (folder: DirectoryItemType) =>
    doNavigateAction(folder.uuid);
  const handleOpenCreateDocuments = () => setCreateCaseDocumentsOpen(true);
  const handleCreateDocumentsConfirm = () => {
    doRefreshAction();
    setCreateCaseDocumentsOpen(false);
  };
  const handleCreateDocumentsClose = () => setCreateCaseDocumentsOpen(false);

  const { name, mimeType, modified } = getColumns({
    t,
    classes,
  });

  const columns = [
    name({
      showNotAccepted: true,
    }),
    mimeType(),
    modified(),
    {
      label: '',
      name: 'open',
      width: 66,
      disableSort: true,
      /* eslint-disable-next-line */
      cellRenderer: ({ rowData }: any) => {
        if (isDocument(rowData)) {
          return (
            <Tooltip
              title={t('DocumentExplorer:columns.download.tooltip')}
              placement={'top-start'}
              enterDelay={400}
            >
              <IconButton
                name={t('DocumentExplorer:columns.download.tooltip')}
                aria-label={t('DocumentExplorer:columns.download.tooltip')}
                onClick={(event: React.MouseEvent) => {
                  if (rowData.download)
                    window.open(rowData.download.url, '_blank');
                  event.preventDefault();
                  event.stopPropagation();
                }}
                title={t('DocumentExplorer:columns.download.label')}
                color="inherit"
              >
                <Icon size="small" color="primary">
                  {iconNames.save_alt}
                </Icon>
              </IconButton>
            </Tooltip>
          );
        }

        return <Fragment />;
      },
    },
  ];

  const inFolderLocation = ![
    PresetLocations.Home as string,
    PresetLocations.Search as string,
  ].includes(location);

  return (
    <I18nResourceBundle resource={locale} namespace="DocumentExplorer">
      <div className={classes.wrapper}>
        {ServerErrorDialog}
        <div className={classes.topBar}>
          {canSearch ? (
            <Search
              onChange={filter => {
                setIsFiltered(Boolean(filter));
                doSearchAction(filter);
              }}
              onClose={() => {
                setIsFiltered(false);
                doSearchCloseAction();
              }}
            />
          ) : null}
        </div>
        <div className={classes.toolbar}>
          <BreadCrumbs
            path={path}
            location={location}
            handleItemClick={handleItemClick}
            classes={classes}
          />

          {canUpload && location !== PresetLocations.Search ? (
            <div className={classes.actionButtons}>
              <Button
                name="addFiles"
                action={handleOpenCreateDocuments}
                sx={{ fontSize: '17px' }}
              >
                {t('DocumentExplorer:addFiles.button')}
              </Button>
              <DocumentUploadDialog
                onConfirm={handleCreateDocumentsConfirm}
                onClose={handleCreateDocumentsClose}
                open={createCaseDocumentsOpen}
                caseUUID={caseUuid}
                {...(inFolderLocation && { directoryUUID: location })}
              />
            </div>
          ) : null}
        </div>
        <div className={classes.table}>
          <FileExplorer
            items={items}
            columns={columns}
            onFolderOpen={handleFolderOpen}
            onFileOpen={doFileOpenAction}
            loading={loading}
            noRowsMessage={t('DocumentExplorer:table.noRows')}
            noRowsProps={isFiltered ? { role: 'alert' } : {}}
            rowHeight={50}
            selectable={false}
          />
        </div>
      </div>
    </I18nResourceBundle>
  );
};

export default CaseDocuments;

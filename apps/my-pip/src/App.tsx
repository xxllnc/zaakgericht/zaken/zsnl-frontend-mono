// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect } from 'react';
import { Provider } from 'react-redux';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import DialogRenderer from '@zaaksysteem/common/src/components/DialogRenderer/DialogRenderer';
import SnackbarRenderer from '@zaaksysteem/common/src/components/SnackbarRenderer/SnackbarRenderer';
import useDialogs from '@zaaksysteem/common/src/library/useDialogs';
//@ts-ignore
import MaterialUiThemeProvider from '@mintlab/ui/App/Material/MaterialUiThemeProvider/MaterialUiThemeProvider';
import { configureStore } from './configureStore';
import Routes from './Routes';
import { DIALOG_ERROR } from './constants/dialog.constants';
import ErrorDialog from './components/ErrorDialog/ErrorDialog';

const store = configureStore();

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: Infinity,
      refetchOnWindowFocus: false,
      retry: 0,
    },
  },
});

function App() {
  const [, addDialogs] = useDialogs();

  useEffect(() => {
    addDialogs({
      [DIALOG_ERROR]: ErrorDialog,
    });
  }, []);

  return (
    <Provider store={store}>
      <QueryClientProvider client={queryClient}>
        <MaterialUiThemeProvider>
          <Routes prefix={process.env.APP_CONTEXT_ROOT || '/my-pip'} />
          <DialogRenderer />
          <SnackbarRenderer />
        </MaterialUiThemeProvider>
      </QueryClientProvider>
    </Provider>
  );
}

export default App;

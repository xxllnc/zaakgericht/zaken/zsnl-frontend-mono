// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connectRouter, routerMiddleware } from 'connected-react-router';
import { History } from 'history';
import { iframeMiddleware } from './iframe.middleware';

export const getRouterModule = (history: History) => ({
  id: 'router',
  reducerMap: {
    router: connectRouter(history),
  },
  middlewares: [routerMiddleware(history), iframeMiddleware],
});

export default getRouterModule;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { RouterState } from 'connected-react-router';
import { UIState } from '@zaaksysteem/common/src/store/ui/ui.reducer';

export interface State {
  router: RouterState;
  ui: UIState;
}

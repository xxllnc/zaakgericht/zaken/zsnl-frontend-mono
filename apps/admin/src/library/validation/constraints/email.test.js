// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { email } from './email';

/* 
  The 'email-regex' NPM package (https://www.npmjs.com/package/email-regex)
  used only performs a basic check and shouldn't be counted on to perform
  a full validation.
*/

describe('The `email` function', () => {
  test('accepts an empty string', () => {
    expect(email('')).toBe(true);
  });

  test('rejects a (basic) invalid e-mail address', () => {
    expect(email('invalid-address')).toBe(false);
  });

  test('accepts a (basic) valid e-mail address', () => {
    expect(email('info@mintlab.nl')).toBe(true);
  });
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { hasValue } from '../../../library/value';

// ZS-TODO: look for an email validation alternative
// (based on business requirements please).
// The `email-regex` package needs transpilation and
// requires more configuration code than it provides.

/**
 * @type {string}
 */
const expression =
  '[^\\.\\s@:](?:[^\\s@:]*[^\\s@:\\.])?@[^\\.\\s@]+(?:\\.[^\\.\\s@]+)*';

/**
 * @param {Object} [options={}]
 * @return {RegExp}
 */
function emailRegex(options = {}) {
  if (options.exact) {
    return new RegExp(`^${expression}$`);
  }

  return new RegExp(expression, 'g');
}

/**
 * @param {string} userInput
 * @returns {boolean}
 */
export const email = userInput => {
  if (!hasValue(userInput)) {
    return true;
  }

  return emailRegex({ exact: true }).test(userInput);
};

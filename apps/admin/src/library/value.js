// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * @param {*} value
 * @return {boolean}
 */
export const isDefined = value => typeof value !== 'undefined';

/**
 * @param {*} value
 * @return {boolean}
 */
export const hasValue = value => value !== '' && value !== null && value !== [];

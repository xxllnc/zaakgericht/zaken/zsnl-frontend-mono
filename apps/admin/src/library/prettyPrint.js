// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * Join multiple strings with a space. This is useful for
 * keeping long pieces of prose in source code readable
 * and indented with their context.
 *
 * @param {...string} subStrings
 * @return {string}
 */
export const multiLine = (...subStrings) => subStrings.join(' ');

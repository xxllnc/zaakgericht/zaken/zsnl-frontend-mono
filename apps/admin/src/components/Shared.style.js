// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const sharedStylesheet = ({ mintlab: { greyscale, radius } }) => ({
  sheet: {
    border: `2px solid ${greyscale.light}`,
    'background-color': greyscale.light,
    'border-top-left-radius': radius.sheet,
    'border-top-right-radius': radius.sheet,
    overflowY: 'scroll',
  },
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withTranslation } from 'react-i18next';
import Card from '@mintlab/ui/App/Material/Card';

/**
 * Generic error component for unresolvable routes.
 *
 * @return {ReactElement}
 */
export const ErrorNotFound = ({ t }) => (
  <Card title={t('common:routeNotFound')} />
);

export default withTranslation()(ErrorNotFound);

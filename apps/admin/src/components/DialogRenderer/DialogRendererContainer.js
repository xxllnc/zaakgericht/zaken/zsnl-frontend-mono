// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { hideDialog } from '../../store/ui/ui.actions';
import { invoke } from '../../store/route/route.actions';
import { discard } from '../../modules/systemConfiguration/store/systemconfiguration.actions';
import DialogRenderer from './DialogRenderer';

const mapStateToProps = ({ ui: { dialogs }, route }) => ({
  dialogs,
  route,
});

const mapDispatchToProps = dispatch => ({
  hide: payload => dispatch(hideDialog(payload && payload.dialogType)),
  invoke: payload => dispatch(invoke(payload)),
  discard: options => dispatch(discard(options)),
});

const DialogRendererContainer = withTranslation()(
  connect(mapStateToProps, mapDispatchToProps)(DialogRenderer)
);

export default DialogRendererContainer;

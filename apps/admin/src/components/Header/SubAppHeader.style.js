// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * @return {JSS}
 */
export const subAppHeaderStylesheet = () => ({
  header: {
    padding: '0',
    height: '72px',
    minHeight: '72px',
    display: 'flex',
    'align-items': 'center',
    position: 'relative',
  },
});

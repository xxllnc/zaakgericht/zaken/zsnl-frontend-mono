// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { getIframeUrl } from './iframe';

describe('The `iframeRouter` module', () => {
  /**
   * @test {getIframeUrl}
   */
  test('`getIframeUrl`', () => {
    expect(typeof getIframeUrl).toBe('function');
  });
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { getSegment } from '@mintlab/kitchen-sink/source';
import dictionary from '../../routes';

const join = (...rest) => rest.join('');

function expandPath(inputBasePath, actualInputPath, outputBasePath) {
  const expression = new RegExp(`^${inputBasePath}([/?].*)?`);
  const [, tail] = expression.exec(actualInputPath) || [];

  if (tail) {
    return join(outputBasePath, tail);
  }

  return outputBasePath;
}

/**
 * Resolve the iframe path component from the parent window path component.
 *
 * @param {string} parentPath
 * @param {string} root
 * @return {string}
 */
export function getIframeUrl(parentPath, root) {
  const [pathComponent] = parentPath.split('?');
  const segment = getSegment(pathComponent);
  const iframePath = dictionary[segment];

  return expandPath(join(root, segment), parentPath, iframePath);
}

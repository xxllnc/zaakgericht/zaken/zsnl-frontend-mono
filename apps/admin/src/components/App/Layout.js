// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withStyles } from '@mui/styles';
import Layout from '@mintlab/ui/App/Zaaksysteem/Layout';
import { extract, getSegment } from '@mintlab/kitchen-sink/source';
import routes from '../../routes';
import { navigate } from '../../library/url';
import ErrorBoundary from './ErrorBoundary';
import { layoutStylesheet } from './Layout.style';
import ContainersWrapper from './ContainersWrapper';
import SearchContainer from './SearchContainer';

const INTERN_URL = '/intern/';
const ABOUT_URL = '/intern/!over';
const LOGOUT_URL = '/auth/logout';
const SUPPORT_URL = 'https://beheerders.zaaksysteem.nl/';

/**
 * @param {Array} input
 * @param {Function} route
 * @return {Array}
 */
const mapToDrawer = (input, route) =>
  input.map(({ icon, label, path }) => ({
    action() {
      route({
        path,
      });
    },
    href: path,
    icon,
    label,
  }));

/**
 * @param {boolean} legacy
 * @param {Object} classes
 * @return {Object}
 */
const getContentClasses = (legacy, classes) => ({
  content: legacy ? classes.legacy : classes.next,
});

/**
 * Wrap the {@link View} in the `@mintlab/ui` Layout.
 *
 * @param {Object} props
 * @param {Array} props.banners
 * @param {Object} props.classes
 * @param {string} props.company
 * @param {Array} props.navigation
 * @param {Function} props.t
 * @param {Function} props.toggleDrawer
 * @param {string} props.user
 * @return {ReactElement}
 */
export const AdminLayout = props => {
  const { t, classes, children } = props;
  const [
    company,
    navigation,
    isDrawerOpen,
    toggleDrawer,
    banners,
    iframeProps,
  ] = extract(
    'company',
    'userNavigation',
    'isDrawerOpen',
    'toggleDrawer',
    'banners',
    props
  );
  const { requestUrl, route } = iframeProps;
  const segment = getSegment(requestUrl);
  const legacy = typeof routes[segment] === 'string';

  const drawer = {
    primary: mapToDrawer(navigation, route),
    secondary: [
      {
        href: INTERN_URL,
        icon: 'sync',
        label: t('navigation:handle'),
      },
      {
        href: SUPPORT_URL,
        target: '_blank',
        icon: 'help',
        label: t('navigation:support'),
      },
      {
        href: LOGOUT_URL,
        icon: 'exit_to_app',
        label: t('navigation:logout'),
      },
    ],
    about: {
      action: () => navigate(ABOUT_URL),
      label: t('navigation:about'),
    },
  };

  return (
    <Layout
      searchComponent={!segment || legacy ? null : <SearchContainer t={t} />}
      banners={banners}
      classes={getContentClasses(legacy, classes)}
      company={company}
      drawer={drawer}
      identity={`${t('common:title')} ${t('common:admin')}`}
      isDrawerOpen={isDrawerOpen}
      menuLabel={t('aria:mainMenu')}
      toggleDrawer={toggleDrawer}
      scope="admin"
    >
      <ErrorBoundary>
        <ContainersWrapper>{children}</ContainersWrapper>
      </ErrorBoundary>
    </Layout>
  );
};

export default withStyles(layoutStylesheet)(AdminLayout);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { I18nextProvider, useTranslation } from 'react-i18next';
import FormatDateProvider from './FormatDate';

/**
 * @reactProps {Object} locale
 * @reactProps {Object} session
 * @return {ReactElement}
 */
const Locale = ({ children }) => {
  const [, i18n] = useTranslation();

  return (
    <I18nextProvider i18n={i18n}>
      <FormatDateProvider>{children}</FormatDateProvider>
    </I18nextProvider>
  );
};

export default Locale;

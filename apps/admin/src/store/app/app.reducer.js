// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  APP_BOOTSTRAP_VALID,
  APP_BOOTSTRAP_ERROR,
  APP_SECTION_CHANGE,
} from './app.constants';

const initialState = {
  bootstrap: 'pending',
  section: '',
};

/**
 * @param {Object} state
 * @param {Object} action
 * @return {Object}
 */
export function app(state = initialState, action) {
  switch (action.type) {
    case APP_BOOTSTRAP_VALID:
      return {
        ...state,
        bootstrap: 'valid',
      };
    case APP_BOOTSTRAP_ERROR:
      return {
        ...state,
        bootstrap: 'error',
      };
    case APP_SECTION_CHANGE:
      return {
        ...state,
        section: action.payload.section,
      };
    default:
      return state;
  }
}

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { fetchSession } from '@zaaksysteem/common/src/store/session/session.actions';
import { SESSION_FETCH } from '@zaaksysteem/common/src/store/session/session.constants';
import { invoke } from '../route/route.actions';
import { getUrl } from '../../library/url';
import {
  APP_BOOTSTRAP_INIT,
  APP_BOOTSTRAP_VALID,
  APP_MODULE_ADDED,
} from './app.constants';
import { valid, error } from './app.actions';

const handleBootstrapStart = (store, next, action) => {
  next(action);
  store.dispatch(fetchSession());
};

const handleSessionFetchSuccess = (store, next, action) => {
  next(action);
  store.dispatch(valid());
};

const handleSessionFetchError = (store, next, action) => {
  next(action);
  store.dispatch(error());
};

const handleBootstrapValid = (store, next, action) => {
  next(action);
  store.dispatch(
    invoke({
      path: getUrl(),
    })
  );
};

const handleModuleAdded = (store, next, action) => {
  const { payload } = action;
  next(action);
  store.dispatch({
    type: APP_MODULE_ADDED,
    payload,
  });
};

export const bootstrapMiddleware = store => next => action => {
  switch (action.type) {
    case APP_BOOTSTRAP_INIT:
      return handleBootstrapStart(store, next, action);
    case APP_BOOTSTRAP_VALID:
      return handleBootstrapValid(store, next, action);
    case SESSION_FETCH.SUCCESS:
      return handleSessionFetchSuccess(store, next, action);
    case SESSION_FETCH.ERROR:
      return handleSessionFetchError(store, next, action);
    case '@@Internal/ModuleManager/ModuleAdded':
      return handleModuleAdded(store, next, action);
    default:
      return next(action);
  }
};

export default bootstrapMiddleware;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { cloneWithout } from '@mintlab/kitchen-sink/source';
import { UI_BANNER_SHOW, UI_BANNER_HIDE } from '../ui.constants';

const initialState = {};

/* eslint-disable complexity */
/**
 * @param {Object} state
 * @param {Object} action
 * @return {Object}
 */
export function banners(state = initialState, action) {
  const { payload, type } = action;

  switch (type) {
    case UI_BANNER_SHOW:
      return {
        ...state,
        [payload.identifier]: {
          ...payload,
        },
      };
    case UI_BANNER_HIDE:
      return {
        ...cloneWithout(state, payload.identifier),
      };

    default:
      return state;
  }
}

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { UI_DRAWER_OPEN, UI_DRAWER_CLOSE } from '../ui.constants';

const initialState = false;

/* eslint-disable complexity */
/**
 * @param {Object} state
 * @param {Object} action
 * @return {Object}
 */
export function drawer(state = initialState, action) {
  const { type } = action;

  switch (type) {
    case UI_DRAWER_OPEN:
      return true;

    case UI_DRAWER_CLOSE:
      return false;

    default:
      return state;
  }
}

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import DataStore from './DataStore';
import locale from './DataStore.locale';

type DatastoreRouteType = {};

const DataStoreModule: React.ComponentType<DatastoreRouteType> = () => {
  return (
    <I18nResourceBundle resource={locale} namespace="dataStore">
      <DataStore />
    </I18nResourceBundle>
  );
};

export default DataStoreModule;

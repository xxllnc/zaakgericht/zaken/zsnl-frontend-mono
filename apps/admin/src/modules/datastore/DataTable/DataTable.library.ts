// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { updateContact } from '../DataStore.requests';
import {
  ActionTypeType,
  DataTypeType,
  UpdateContactType,
} from '../DataStore.types';
import { getContactType } from '../DataStore.library';
import { cellRenderer } from './DataTableCell';

type ColumnsType = {
  NatuurlijkPersoon: string[];
  Organisatie: string[];
  BagLigplaats: string[];
  BagNummeraanduiding: string[];
  BagOpenbareruimte: string[];
  BagPand: string[];
  BagStandplaats: string[];
  BagVerblijfsobject: string[];
  BagWoonplaats: string[];
};

const columns: ColumnsType = {
  NatuurlijkPersoon: [
    'afnemerindicatie',
    'pending',
    'uuid',
    'id',
    'burgerservicenummer',
    'a_nummer',
    'voorletters',
    'voornamen',
    'geslachtsnaam',
    'voorvoegsel',
    'geslachtsaanduiding',
    'nationaliteitscode1',
    'nationaliteitscode2',
    'nationaliteitscode3',
    'geboorteplaats',
    'geboorteland',
    'geboortedatum',
    'aanhef_aanschrijving',
    'voorletters_aanschrijving',
    'voornamen_aanschrijving',
    'naam_aanschrijving',
    'voorvoegsel_aanschrijving',
    'burgerlijke_staat',
    'indicatie_geheim',
    'land_waarnaar_vertrokken',
    'import_datum',
    'authenticated',
    'authenticatedby',
    'verblijfsobject_id',
    'datum_overlijden',
    'aanduiding_naamgebruik',
    'onderzoek_persoon',
    'onderzoek_huwelijk',
    'onderzoek_overlijden',
    'onderzoek_verblijfplaats',
    'partner_a_nummer',
    'partner_burgerservicenummer',
    'partner_voorvoegsel',
    'partner_geslachtsnaam',
    'datum_huwelijk',
    'datum_huwelijk_ontbinding',
    'in_gemeente',
    'naamgebruik',
    'active',
    'adellijke_titel',
    'preferred_contact_channel',
    'pending',
    'related_custom_object_id',
    'gemeentecode',
    'surname',
    'straatnaam',
    'huisnummer',
    'huisletter',
    'huisnummertoevoeging',
    'nadere_aanduiding',
    'postcode',
    'woonplaats',
    'gemeentedeel',
    'functie_adres',
    'datum_aanvang_bewoning',
    'woonplaats_id',
    'gemeente_code',
    'hash',
    'adres_buitenland1',
    'adres_buitenland2',
    'adres_buitenland3',
    'landcode',
    'bag_id',
    'geo_lat_long',
  ],
  Organisatie: [
    'afnemerindicatie',
    'pending',
    'uuid',
    'id',
    'dossiernummer',
    'subdossiernummer',
    'hoofdvestiging_dossiernummer',
    'hoofdvestiging_subdossiernummer',
    'vorig_dossiernummer',
    'vorig_subdossiernummer',
    'handelsnaam',
    'rechtsvorm',
    'kamernummer',
    'faillisement',
    'surseance',
    'telefoonnummer',
    'email',
    'vestiging_adres',
    'vestiging_straatnaam',
    'vestiging_huisnummer',
    'vestiging_huisnummertoevoeging',
    'vestiging_postcodewoonplaats',
    'vestiging_postcode',
    'vestiging_woonplaats',
    'correspondentie_adres',
    'correspondentie_straatnaam',
    'correspondentie_huisnummer',
    'correspondentie_huisnummertoevoeging',
    'correspondentie_postcodewoonplaats',
    'correspondentie_postcode',
    'correspondentie_woonplaats',
    'hoofdactiviteitencode',
    'nevenactiviteitencode1',
    'nevenactiviteitencode2',
    'werkzamepersonen',
    'contact_naam',
    'contact_aanspreektitel',
    'contact_voorletters',
    'contact_voorvoegsel',
    'contact_geslachtsnaam',
    'contact_geslachtsaanduiding',
    'authenticated',
    'authenticatedby',
    'fulldossiernummer',
    'import_datum',
    'verblijfsobject_id',
    'system_of_record',
    'system_of_record_id',
    'vestigingsnummer',
    'vestiging_huisletter',
    'correspondentie_huisletter',
    'vestiging_adres_buitenland1',
    'vestiging_adres_buitenland2',
    'vestiging_adres_buitenland3',
    'vestiging_landcode',
    'correspondentie_adres_buitenland1',
    'correspondentie_adres_buitenland2',
    'correspondentie_adres_buitenland3',
    'correspondentie_landcode',
    'rsin',
    'oin',
    'main_activity',
    'secondairy_activities',
    'date_founded',
    'date_registration',
    'date_ceased',
    'vestiging_latlong',
    'vestiging_bag_id',
    'preferred_contact_channel',
    'active',
    'pending',
    'related_custom_object_id',
  ],
  BagLigplaats: [
    'afnemerindicatie',
    'pending',
    'identificatie',
    'begindatum',
    'einddatum',
    'officieel',
    'status',
    'hoofdadres',
    'inonderzoek',
    'documentdatum',
    'documentnummer',
    'correctie',
    'id',
  ],
  BagNummeraanduiding: [
    'afnemerindicatie',
    'pending',
    'identificatie',
    'begindatum',
    'einddatum',
    'huisnummer',
    'officieel',
    'huisletter',
    'huisnummertoevoeging',
    'postcode',
    'woonplaats',
    'inonderzoek',
    'openbareruimte',
    'type',
    'documentdatum',
    'documentnummer',
    'status',
    'correctie',
    'id',
    'gps_lat_lon',
  ],
  BagOpenbareruimte: [
    'afnemerindicatie',
    'pending',
    'identificatie',
    'begindatum',
    'einddatum',
    'naam',
    'officieel',
    'woonplaats',
    'type',
    'inonderzoek',
    'documentdatum',
    'documentnummer',
    'status',
    'correctie',
    'id',
    'gps_lat_lon',
  ],
  BagPand: [
    'afnemerindicatie',
    'pending',
    'identificatie',
    'begindatum',
    'einddatum',
    'officieel',
    'bouwjaar',
    'status',
    'inonderzoek',
    'documentdatum',
    'documentnummer',
    'correctie',
    'id',
  ],
  BagStandplaats: [
    'afnemerindicatie',
    'pending',
    'identificatie',
    'begindatum',
    'einddatum',
    'officieel',
    'status',
    'hoofdadres',
    'inonderzoek',
    'documentdatum',
    'documentnummer',
    'correctie',
    'id',
  ],
  BagVerblijfsobject: [
    'afnemerindicatie',
    'pending',
    'identificatie',
    'begindatum',
    'einddatum',
    'officieel',
    'hoofdadres',
    'oppervlakte',
    'status',
    'inonderzoek',
    'documentdatum',
    'documentnummer',
    'correctie',
    'id',
  ],
  BagWoonplaats: [
    'afnemerindicatie',
    'pending',
    'identificatie',
    'begindatum',
    'einddatum',
    'officieel',
    'naam',
    'status',
    'inonderzoek',
    'documentdatum',
    'documentnummer',
    'correctie',
    'id',
  ],
};

export const columnWidth = 300;
export const actionColumnWidth = 67;

export const actionColumns: ActionTypeType[] = ['create', 'delete'];

export const getColumns = (
  dataType: DataTypeType,
  updateContact: (actionType: ActionTypeType, id: number) => void,
  t: i18next.TFunction,
  classes: any
) => {
  const aColumns = actionColumns.map(column => ({
    name: column,
    label: '',
    width: actionColumnWidth,
    minWidth: actionColumnWidth,
    disableSort: true,
    cellRenderer: (rowData: any) =>
      cellRenderer(rowData, dataType, updateContact, t, classes),
  }));

  const vColumns = columns[dataType].map(column => ({
    name: column,
    label:
      column === 'pending'
        ? `${column} (${t('table.column.pending')})`
        : column,
    width: columnWidth,
    minWidth: columnWidth,
    disableSort: column === 'afnemerindicatie' ? true : false,
    cellRenderer: (rowData: any) =>
      cellRenderer(rowData, dataType, updateContact, t, classes),
  }));

  return dataType === 'NatuurlijkPersoon' || dataType === 'Organisatie'
    ? [...aColumns, ...vColumns]
    : vColumns;
};

export const updateContactAction: UpdateContactType = async (
  actionType,
  dataType,
  id
) => {
  const contactType = getContactType(dataType);
  const response = await updateContact(actionType, contactType, id);

  try {
    if (response.result[0].queue_items === '0') {
      return true;
    }
  } catch (error: any) {
    return error;
  }
};

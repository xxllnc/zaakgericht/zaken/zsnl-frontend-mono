// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useFiltersDialogStyles = makeStyles(({ typography }: Theme) => ({
  formWrapper: {
    width: 350,
    fontFamily: typography.fontFamily,
    display: 'flex',
    flexDirection: 'column',
  },
}));

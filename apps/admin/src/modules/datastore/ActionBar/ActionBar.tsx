// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDebouncedCallback } from 'use-debounce';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
//@ts-ignore
import Snackbar from '@mintlab/ui/App/Material/Snackbar/Snackbar.cmp';
import Button from '@mintlab/ui/App/Material/Button';
// @ts-ignore
import TextField from '@mintlab/ui/App/Material/TextField';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import {
  DataType,
  DataTypeType,
  FiltersType,
  SelectedType,
  SetDataTypeType,
  SetFiltersType,
} from '../DataStore.types';
import {
  dataTypeValues,
  initiateExport,
  getActiveFilters,
  defaultFilters,
} from '../DataStore.library';
import { EverythingSelectedType } from '../../transactions/Transactions.types';
import { useActionBarStyles } from './ActionBar.styles';
import FiltersDialog from './FiltersDialog';
import BulkDialog from './BulkDialog';

type ActionBarPropsType = {
  data?: DataType;
  dataType: DataTypeType;
  filters: FiltersType;
  selectedRows: SelectedType;
  everythingSelected: EverythingSelectedType;
  setDataType: SetDataTypeType;
  setFilters: SetFiltersType;
};

const ActionBar: React.ComponentType<ActionBarPropsType> = ({
  data,
  dataType,
  filters,
  selectedRows,
  everythingSelected,
  setDataType,
  setFilters,
}) => {
  const classes = useActionBarStyles();
  const [t] = useTranslation('dataStore');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [snack, setSnack] = useState<string | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [dialogOpen, setDialogOpen] = useState<
    'filters' | 'bulk' | 'cleanUp' | null
  >(null);
  const [searchTerm, setSearchTerm] = useState<string>('');
  const setTextFilter = (value: string) => {
    setFilters({ ...filters, freeform_filter: value, zapi_page: 0 });
  };
  const [debouncedCallback] = useDebouncedCallback(value => {
    if (!value.length || value.length >= 3) setTextFilter(value);
  }, 500);
  const filtersActive =
    dataType === 'NatuurlijkPersoon' || dataType === 'Organisatie';

  return (
    <>
      <div className={classes.wrapper}>
        <div className={classes.actionWrapper}>
          <Button
            disabled={!filtersActive}
            action={() => {
              setDialogOpen('filters');
            }}
            name="datastoreFilters"
          >
            {t('filters.button')}
          </Button>
          <Button
            disabled={loading}
            action={() => {
              setLoading(true);
              initiateExport(dataType, filters)
                .then(() => {
                  setSnack(t('export.snack'));
                })
                .catch(openServerErrorDialog)
                .finally(() => setLoading(false));
            }}
            name="datastoreExport"
          >
            {t('export.button')}
          </Button>
          <Button
            action={() => {
              setDialogOpen('bulk');
            }}
            disabled={!selectedRows.length}
            name="datastoreBulk"
          >
            {t('bulk.button')}
          </Button>
          <div className={classes.dataTypeWrapper}>
            <Select
              value={dataType}
              name="data-type"
              onChange={(event: any) => {
                setFilters({
                  ...defaultFilters,
                  freeform_filter: filters.freeform_filter,
                });
                setDataType(event.target.value?.value);
              }}
              choices={dataTypeValues.map((value: DataTypeType) => ({
                value,
                label: value,
              }))}
              isClearable={false}
            />
          </div>
          <div className={classes.textFilterWrapper}>
            <TextField
              value={searchTerm}
              onChange={(event: any) => {
                const value = event.target.value;
                setSearchTerm(value);
                debouncedCallback(event.target.value);
              }}
              placeholder={t('filters.fields.freeform_filter.placeholder')}
              closeAction={() => {
                setSearchTerm('');
                setTextFilter('');
              }}
            />
          </div>
        </div>
        {filtersActive && (
          <div className={classes.filterTextWrapper}>
            <span>{`${t('activeFilters')}: ${
              getActiveFilters(t, filters, dataType).join(', ') ||
              t('common:none')
            }.`}</span>
          </div>
        )}
      </div>
      {ServerErrorDialog}
      {dialogOpen === 'filters' && (
        <FiltersDialog
          dataType={dataType}
          filters={filters}
          setFilters={setFilters}
          onClose={() => setDialogOpen(null)}
          open={dialogOpen === 'filters'}
        />
      )}
      <BulkDialog
        data={data}
        selectedRows={selectedRows}
        everythingSelected={everythingSelected}
        dataType={dataType}
        filters={filters}
        setSnack={setSnack}
        onClose={() => setDialogOpen(null)}
        open={dialogOpen === 'bulk'}
      />
      <Snackbar
        autoHideDuration={5000}
        handleClose={(event: React.ChangeEvent, reason: string) => {
          if (reason === 'clickaway') return;
          setSnack(null);
        }}
        message={snack}
        open={Boolean(snack)}
        classes={{
          root: classes.snack,
        }}
      />
    </>
  );
};

export default ActionBar;

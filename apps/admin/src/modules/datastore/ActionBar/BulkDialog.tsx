// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import DialogContent from '@mui/material/DialogContent';
import Divider from '@mui/material/Divider';
import DialogActions from '@mui/material/DialogActions';
import { Dialog, DialogTitle } from '@mintlab/ui/App/Material/Dialog';
import { capitalize } from '@mintlab/kitchen-sink/source';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import { getActiveFilters, performBulkAction } from '../DataStore.library';
import {
  DataType,
  DataTypeType,
  FiltersType,
  SelectedType,
  AllSelectedType,
} from '../DataStore.types';
import { useBulkDialogStyles } from './BulkDialog.style';

type FiltersDialogPropsType = {
  data?: DataType;
  selectedRows: SelectedType;
  everythingSelected: AllSelectedType;
  dataType: DataTypeType;
  filters: FiltersType;
  setSnack: (text: string) => void;
  onClose: () => void;
  open: boolean;
};

const BulkDialog: React.ComponentType<FiltersDialogPropsType> = ({
  data,
  selectedRows,
  everythingSelected,
  dataType,
  filters,
  setSnack,
  onClose,
  open,
}) => {
  const [t] = useTranslation('dataStore');
  const classes = useBulkDialogStyles();
  const dialogEl = useRef();

  const title = t('bulk.dialog.title', { dataType });

  const activeFilters =
    getActiveFilters(t, filters, dataType) || t('common:none');

  return (
    <>
      <Dialog
        disableBackdropClick={true}
        open={open}
        onClose={onClose}
        scope={'dataStore-bulk-dialog'}
        ref={dialogEl}
      >
        <DialogTitle
          elevated={true}
          icon="delete"
          title={title}
          onCloseClick={onClose}
        />
        <DialogContent>
          <div className={classes.contentWrapper}>
            {everythingSelected ? (
              <>
                <span>{t('bulk.dialog.explanationAll')}</span>
                {activeFilters.length > 0 && (
                  <ul>
                    {activeFilters.map(filter => (
                      <li key={filter}>{capitalize(filter)}</li>
                    ))}
                  </ul>
                )}
              </>
            ) : (
              <span>{t('bulk.dialog.explanationSelection')}</span>
            )}
            <span>{t('cleanUp.dialog.warning')}</span>
          </div>
        </DialogContent>
        <>
          <Divider />
          <DialogActions>
            {createDialogActions(
              [
                {
                  color: 'primary',
                  text: t('common:verbs.delete'),
                  onClick: () => {
                    performBulkAction({
                      type: 'delete',
                      selectedRows,
                      everythingSelected,
                      data,
                      dataType,
                      filters,
                    }).then(() => {
                      setSnack(t('bulk.snack.delete'));
                      onClose();
                    });
                  },
                },
                {
                  color: 'primary',
                  text: t('common:verbs.create'),
                  onClick: () => {
                    performBulkAction({
                      type: 'create',
                      selectedRows,
                      everythingSelected,
                      data,
                      dataType,
                      filters,
                    }).then(() => {
                      setSnack(t('bulk.snack.create'));
                      onClose();
                    });
                  },
                },
                {
                  text: t('common:dialog.cancel'),
                  onClick: onClose,
                },
              ],
              'datastore-bulk-dialog'
            )}
          </DialogActions>
        </>
      </Dialog>
    </>
  );
};

export default BulkDialog;

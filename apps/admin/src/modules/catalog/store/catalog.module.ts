// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { combineReducers } from 'redux';
import items from './items/items.reducer';
import caseTypeVersions from './caseTypeVersions/caseTypeVersions.reducer';
import details from './details/details.reducer';
import moveItems from './moveItems/moveItems.reducer';
import changeOnlineStatus from './changeOnlineStatus/changeOnlineStatus.reducer';
import attribute from './attribute/attribute.reducer';
import emailTemplate from './emailTemplate/emailTemplate.reducer';
import folder from './folder/folder.reducer';
import search from './search/search.reducer';
import documentTemplate from './documentTemplate/documentTemplate.reducer';
import { catalogMiddleware } from './catalog.middleware';

export const catalog = combineReducers({
  items,
  caseTypeVersions,
  details,
  moveItems,
  changeOnlineStatus,
  attribute,
  emailTemplate,
  folder,
  search,
  documentTemplate,
});

export const getCatalogModule = (uuid?: string) => ({
  id: 'catalog',
  middlewares: [catalogMiddleware as any],
  reducerMap: {
    catalog,
  },
});

export default getCatalogModule;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxConstants } from '../../../../library/redux/ajax/createAjaxConstants';

export const CATALOG_ATTRIBUTE_INIT = 'CATALOG:ATTRIBUTE:INIT';
export const CATALOG_ATTRIBUTE_FETCH = createAjaxConstants(
  'CATALOG:ATTRIBUTE:FETCH'
);
export const CATALOG_ATTRIBUTE_SAVE = createAjaxConstants(
  'CATALOG:ATTRIBUTE:SAVE'
);

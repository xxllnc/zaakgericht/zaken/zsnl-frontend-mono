// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { cloneWithout, buildUrl } from '@mintlab/kitchen-sink/source';
import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import {
  CATALOG_ATTRIBUTE_INIT,
  CATALOG_ATTRIBUTE_FETCH,
  CATALOG_ATTRIBUTE_SAVE,
} from './attribute.constants';

const fetchAjaxAction = createAjaxAction(CATALOG_ATTRIBUTE_FETCH);
const saveAjaxAction = createAjaxAction(CATALOG_ATTRIBUTE_SAVE);

export const initAttribute = payload => ({
  type: CATALOG_ATTRIBUTE_INIT,
  payload,
});

export const fetchAttribute = payload => {
  const attribute_id = payload;

  return fetchAjaxAction({
    url: buildUrl('/api/v2/admin/catalog/get_attribute_detail', {
      attribute_id,
    }),
    method: 'GET',
  });
};

export const saveAttribute = payload => {
  const url = payload.isNew
    ? '/api/v2/admin/catalog/create_attribute'
    : '/api/v2/admin/catalog/edit_attribute';

  const data = cloneWithout(payload, 'isNew');

  return saveAjaxAction({
    url,
    method: 'POST',
    data,
  });
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { handleAjaxStateChange } from '../../../../library/redux/ajax/handleAjaxStateChange';
import { AJAX_STATE_INIT } from '../../../../library/redux/ajax/createAjaxConstants';
import {
  CATALOG_MOVEITEMS_START,
  CATALOG_MOVEITEMS_CONFIRM,
  CATALOG_MOVEITEMS_CLEAR,
} from './moveItems.constants';

export const handleMoveStart = (state, action) => {
  return {
    ...state,
    items: action.payload,
  };
};

export const handleMoveClear = state => {
  return {
    ...state,
    items: [],
  };
};

export const initialState = {
  state: AJAX_STATE_INIT,
  items: [],
};

export function moveItems(state = initialState, action) {
  const handleAjaxState = handleAjaxStateChange(CATALOG_MOVEITEMS_CONFIRM);

  switch (action.type) {
    case CATALOG_MOVEITEMS_START:
      return handleMoveStart(state, action);

    case CATALOG_MOVEITEMS_CLEAR:
      return handleMoveClear(state);

    case CATALOG_MOVEITEMS_CONFIRM.PENDING:
    case CATALOG_MOVEITEMS_CONFIRM.ERROR:
    case CATALOG_MOVEITEMS_CONFIRM.SUCCESS:
      return handleAjaxState(state, action);

    default:
      return state;
  }
}

export default moveItems;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { CATALOG_SEARCH_EXIT, CATALOG_SEARCH } from './search.constants';

export const doCatalogSearch = query => ({
  type: CATALOG_SEARCH,
  payload: {
    query,
  },
});

export const doExitSearch = payload => ({
  type: CATALOG_SEARCH_EXIT,
  payload,
});

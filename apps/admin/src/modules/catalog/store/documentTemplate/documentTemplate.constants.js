// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxConstants } from '../../../../library/redux/ajax/createAjaxConstants';

export const CATALOG_DOCUMENT_TEMPLATE_INIT = 'CATALOG:DOCUMENT_TEMPLATE:INIT';
export const CATALOG_DOCUMENT_TEMPLATE_FETCH = createAjaxConstants(
  'CATALOG:DOCUMENT_TEMPLATE:FETCH'
);
export const CATALOG_DOCUMENT_TEMPLATE_INTEGRATIONS_FETCH = createAjaxConstants(
  'CATALOG:DOCUMENT_TEMPLATE:INTEGRATIONS:FETCH'
);
export const CATALOG_DOCUMENT_TEMPLATE_SAVE = createAjaxConstants(
  'CATALOG:DOCUMENT_TEMPLATE:SAVE'
);

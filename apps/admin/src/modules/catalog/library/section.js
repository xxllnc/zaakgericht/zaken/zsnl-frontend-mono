// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { navigation } from '../fixtures/navigation';

export const getSectionFromPath = path => {
  const found = navigation.find(item => path.includes(item.path));
  return found ? found.section : '';
};

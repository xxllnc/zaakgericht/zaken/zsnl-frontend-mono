// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const LEADING_VALUE_WHITESPACE = /:\s(?!\S+:)/g;
const MULTI_VALUE_WHITESPACE = /\s*,\s*/g;
const VALUE_WHITESPACE = /".*\s+.*"/g;

const parseQueryValue = value => {
  const trimmed = value ? value.trim().replace(/\+/g, ' ') : '';
  return trimmed.indexOf(',') > -1 ? trimmed.split(',') : trimmed;
};

const getKeywordFromQuery = (query, keyValue) => {
  return keyValue
    .reduce((keyword, value) => {
      return keyword.replace(value, '');
    }, query)
    .trim();
};

const sanitizeQuery = query => {
  const sanitizedQuery = query
    .replace(LEADING_VALUE_WHITESPACE, ':')
    .replace(MULTI_VALUE_WHITESPACE, ',');

  const matches = sanitizedQuery.match(VALUE_WHITESPACE);

  return matches
    ? matches.reduce((acc, value) => {
        const joinedValue = value.replace(/"/g, '').replace(/\s/g, '+');

        return acc.replace(value, joinedValue);
      }, query)
    : sanitizedQuery;
};

const quouteWhitespacedValue = value =>
  value.indexOf(' ') > -1 ? `"${value}"` : value;

export const objectToQuery = obj => {
  const { keyword = '', ...rest } = obj;

  const filterString = Object.entries(rest)
    .filter(([, value]) => Boolean(value))
    .map(([key, value]) => {
      const parsedValue = Array.isArray(value)
        ? value.map(quouteWhitespacedValue).join(',')
        : quouteWhitespacedValue(value);

      return `${key}:${parsedValue}`;
    })
    .join(' ');

  return `${filterString} ${keyword}`;
};

export const queryToObject = query => {
  const sanitizedQuery = sanitizeQuery(query);
  const keyValue = sanitizedQuery.match(/[^\s]+:[^:\s]*/g) || [];
  const keyword = getKeywordFromQuery(sanitizedQuery, keyValue);

  return keyValue.reduce(
    (acc, current) => {
      const [key, value] = current.split(':');

      return {
        ...acc,
        [key.trim()]: parseQueryValue(value),
      };
    },
    {
      keyword: keyword || '',
    }
  );
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * @return {JSS}
 */
export const catalogTableStyleSheet = ({
  palette: { primary },
  typography,
  mintlab: { greyscale },
}) => ({
  flexContainer: {
    display: 'flex',
    alignItems: 'center',
    boxSizing: 'border-box',
  },
  tableHeader: {
    fontWeight: typography.fontWeightMedium,
  },
  tableRow: {
    borderBottom: `1px solid ${greyscale.dark}`,
  },
  tableRowHover: {
    '&:hover': {
      cursor: 'pointer',
      backgroundColor: primary.lightest,
    },
  },
  tableRowDisabled: {
    opacity: '0.5',
  },
  tableCell: {
    padding: '20px 0px 20px 20px',
  },
  activeLabel: {
    marginLeft: '10px',
    color: greyscale.evenDarker,
  },
  tableWrapper: {
    display: 'flex',
    height: '100%',
  },
});

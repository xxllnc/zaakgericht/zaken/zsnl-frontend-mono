// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useRef } from 'react';
import classNames from 'classnames';
import { AutoSizer, Column, Table, InfiniteLoader } from 'react-virtualized';
import { getSegments } from '@mintlab/kitchen-sink/source';
import { withStyles } from '@mui/styles';
import Checkbox from '@mintlab/ui/App/Material/Checkbox';
import Render from '@mintlab/ui/App/Abstract/Render';
import { get, preventDefaultAndCall } from '@mintlab/kitchen-sink/source';
import { IconCell, LinkCell } from './../../../../components/Table/cells';
import { catalogTableStyleSheet } from './CatalogTable.style';

const PAGE_SIZE = 20;

/**
 * @reactProps {Object} classes
 * @reactProps {Array<Object>} columns
 * @reactProps {Array<Object>} rows
 * @reactProps {Function} t
 * @reactProps {Function} toggleItem
 * @reactProps {Function} onRowNavigate
 * @reactProps {Function} loadMore
 * @reactProps {Number} pageLoaded
 * @reactProps {Boolean} loadingItems
 * @reactProps {Number} load
 *
 * @returns {ReactElement}
 */
const CatalogTable = ({
  classes,
  columns,
  rows = [],
  t,
  toggleItem,
  onRowNavigate,
  loadMore,
  pageLoaded,
  loadingItems,
  load,
}) => {
  const loadRef = useRef(null);
  const infiniteLoaderRef = useRef(null);

  const loadMoreRows = async ({ startIndex }) => {
    const segments = getSegments(window.location.pathname);
    const [, , id] = segments;
    const params = new URLSearchParams(window.location.search);
    const nextStart = pageLoaded * PAGE_SIZE;
    const nextEnd = nextStart + PAGE_SIZE;
    const nextPage = () => {
      if (startIndex === 0) {
        return 1;
      } else if (startIndex >= nextStart && startIndex <= nextEnd) {
        return pageLoaded + 1;
      } else {
        return pageLoaded;
      }
    };
    const page = nextPage();

    if (page === pageLoaded || loadingItems) {
      return Promise.resolve([]);
    }
    await loadMore({
      id: id || null,
      query: params.get('query'),
      page,
      page_size: PAGE_SIZE,
    });
    return Promise.resolve();
  };

  useEffect(() => {
    (async function () {
      if (load && loadRef.current !== load) {
        loadRef.current = load;
        if (infiniteLoaderRef)
          infiniteLoaderRef.current.resetLoadMoreRowsCache();
        loadMoreRows({ startIndex: 1 });
      }
    })();
  }, [load]);

  const catalogItemScope = 'catalog-item';
  const cellContent = {
    selected(rowData) {
      return (
        <Checkbox
          scope={catalogItemScope}
          disabled={rowData.beingMoved}
          checked={rowData.selected}
        />
      );
    },
    icon(rowData) {
      return <IconCell value={rowData.icon} />;
    },
    name({ active, beingMoved, isNavigable, name, path, type }) {
      const shouldBeLink = isNavigable && !beingMoved;
      return (
        <div data-scope={`${catalogItemScope}:name-wrapper`}>
          <Render condition={!shouldBeLink}>
            <span>{name}</span>
          </Render>
          <Render condition={shouldBeLink}>
            <LinkCell
              handleNavigate={preventDefaultAndCall(event => {
                const tagName = get(event, 'nativeEvent.target.tagName');
                if (tagName && tagName.toLowerCase() === 'a') {
                  event.stopPropagation();
                }
                onRowNavigate({ path, isNavigable });
              })}
              scope={`${catalogItemScope}:${type}`}
              path={path}
              value={name}
            />
          </Render>
          <Render condition={type === 'case_type' && !active}>
            <span className={classes.activeLabel}>
              {t('catalog:items:offline')}
            </span>
          </Render>
        </div>
      );
    },
    type(rowData) {
      return t(`common:entityType:${rowData.type}`);
    },
  };

  const rowClassName = ({ index }) =>
    classNames(classes.flexContainer, {
      [classes.tableHeader]: index === -1,
      [classes.tableRow]: index >= 0,
      [classes.tableRowHover]:
        index >= 0 && rows[index] && !rows[index].beingMoved,
      [classes.tableRowDisabled]:
        index >= 0 && rows[index] && rows[index].beingMoved,
    });

  const onRowClick = ({ event, rowData }) => {
    if (rowData.beingMoved) {
      return;
    }
    const multiSelect = event.target.type === 'checkbox';
    toggleItem(rowData.id, multiSelect);
  };

  const onRowDoubleClick = ({ rowData: { path, isNavigable } }) =>
    onRowNavigate({ path, isNavigable });

  const headerRenderer = ({ label }) => (
    <div className={classes.tableCell}>{label}</div>
  );

  const cellRenderer = ({ dataKey, rowData }) => (
    <div className={classes.tableCell}>{cellContent[dataKey](rowData)}</div>
  );

  const isRowLoaded = ({ index }) => !!rows[index];

  return (
    <div className={classes.tableWrapper}>
      <InfiniteLoader
        isRowLoaded={isRowLoaded}
        loadMoreRows={loadMoreRows}
        rowCount={99999}
        threshold={10}
        ref={infiniteLoaderRef}
      >
        {({ onRowsRendered, registerChild }) => (
          <AutoSizer>
            {table => (
              <Table
                ref={registerChild}
                height={table.height}
                onRowsRendered={onRowsRendered}
                width={table.width}
                headerHeight={61}
                rowHeight={82}
                rowCount={rows.length}
                rowGetter={({ index }) => rows[index]}
                rowClassName={rowClassName}
                onRowClick={onRowClick}
                onRowDoubleClick={onRowDoubleClick}
              >
                {columns.map(({ label, name, width }) => (
                  <Column
                    key={name}
                    width={width}
                    flexGrow={width === 1 ? 1 : 0}
                    dataKey={name}
                    label={label}
                    headerRenderer={headerRenderer}
                    cellRenderer={cellRenderer}
                  />
                ))}
              </Table>
            )}
          </AutoSizer>
        )}
      </InfiniteLoader>
    </div>
  );
};

export default withStyles(catalogTableStyleSheet)(CatalogTable);

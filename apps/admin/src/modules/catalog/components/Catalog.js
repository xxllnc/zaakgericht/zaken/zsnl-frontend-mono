// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classNames from 'classnames';
import deepmerge from 'deepmerge';
import { withStyles } from '@mui/styles';
import Fab from '@mintlab/ui/App/Material/Fab';
import Card from '@mintlab/ui/App/Material/Card';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Render from '@mintlab/ui/App/Abstract/Render';
import { sharedStylesheet } from '../../../components/Shared.style';
import { catalogStyleSheet } from './Catalog.style';
import CatalogHeaderContainer from './CatalogHeader/CatalogHeaderContainer';
import CatalogTable from './table/CatalogTable';
import DetailViewContainer from './DetailView/DetailViewContainer';
import MoveBar from './MoveBar/MoveBar';

/**
 * @reactProps {Object} classes
 * @reactProps {Array} columns
 * @reactProps {Function} doNavigate
 * @reactProps {boolean} loadingTable
 * @reactProps {boolean} loadingDetailView
 * @reactProps {Function} onRowNavigate
 * @reactProps {Array<Object>} rows
 * @reactProps {boolean} showDetailView
 * @reactProps {Function} t
 * @reactProps {Function} toggleDetailView
 * @reactProps {Array} moveItems
 * @reactProps {Function} clearMoveItems
 * @reactProps {Function} confirmMoveItems
 * @reactProps {boolean} movingItems
 * @reactProps {boolean} inSearch
 * @return {ReactElement}
 */
const Catalog = ({
  classes,
  columns,
  doNavigate,
  toggleItem,
  loadingTable,
  loadingDetailView,
  onRowNavigate,
  rows,
  showDetailView,
  t,
  moveItems,
  clearMoveItems,
  confirmMoveItems,
  movingItems,
  addElement,
  inSearch,
  loadMore,
  pageLoaded,
  loadingItems,
  load,
}) => (
  <div className={classes.wrapper}>
    <CatalogHeaderContainer doNavigate={doNavigate} t={t} />
    <div className={classes.contentWrapper}>
      <Render condition={!inSearch}>
        <div
          className={classNames(
            classes.addElementWrapper,
            moveItems.length && !showDetailView ? 'withMoveBanner' : null
          )}
        >
          <Fab action={addElement} scope="catalog-actions" color="primary">
            add
          </Fab>
        </div>
      </Render>

      <div
        className={classNames(classes.sheet, {
          [classes.sheetWidth]: showDetailView,
        })}
      >
        <Card
          className={classNames(classes.tableWrapper, {
            [classes.tableWrapperMovingItems]: moveItems.length,
          })}
          classes={{
            content: classes.card,
          }}
        >
          <CatalogTable
            columns={columns}
            rows={rows}
            onRowNavigate={onRowNavigate}
            toggleItem={toggleItem}
            loadMore={loadMore}
            pageLoaded={pageLoaded}
            loadingItems={loadingItems}
            t={t}
            load={load}
          />
          {loadingTable && <Loader className={classes.loader} />}
        </Card>
        <Render condition={moveItems.length}>
          <MoveBar
            t={t}
            numToMove={moveItems.length}
            moveAction={() => confirmMoveItems()}
            cancelAction={() => clearMoveItems()}
            disabled={movingItems || inSearch}
          />
        </Render>
      </div>
      <Render condition={showDetailView}>
        <DetailViewContainer
          loading={loadingDetailView}
          t={t}
          doNavigate={doNavigate}
          rows={rows}
        />
      </Render>
    </div>
  </div>
);

/**
 * @param {Object} theme
 * @return {JSS}
 */
const mergedStyles = theme =>
  deepmerge(sharedStylesheet(theme), catalogStyleSheet(theme));

export default withStyles(mergedStyles)(Catalog);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Button from '@mintlab/ui/App/Material/Button';
import Render from '@mintlab/ui/App/Abstract/Render';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import DropdownMenu, {
  DropdownMenuList,
} from '@mintlab/ui/App/Zaaksysteem/DropdownMenu';
import { Box } from '@mui/material';
import { useButtonBarStyle } from './ButtonBar.style';

const ButtonBar = ({
  actionButtons,
  advancedActionButtons,
  permanentButtons,
}: {
  actionButtons: any;
  advancedActionButtons: any;
  permanentButtons: any;
}) => {
  const styles = useButtonBarStyle();
  return (
    <Box sx={styles.container}>
      <Render condition={actionButtons.length || advancedActionButtons.length}>
        <div style={styles.buttonList}>
          <Render condition={actionButtons.length}>
            {actionButtons.map(({ action, tooltip, type, name }: any) => (
              <Tooltip key={type} title={tooltip} placement="bottom">
                <Button action={action} name={name} icon={type} />
              </Tooltip>
            ))}
          </Render>
          <Render condition={advancedActionButtons.length}>
            <DropdownMenu
              transformOrigin={{
                vertical: -50,
                horizontal: 'center',
              }}
              trigger={<Button icon="more_vert" name="buttonBarMenuTrigger" />}
            >
              <DropdownMenuList
                items={advancedActionButtons.map(({ action, title }: any) => ({
                  action,
                  label: title,
                  scope: `catalog-header:button-bar:${title}`,
                }))}
              />
            </DropdownMenu>
          </Render>
        </div>
      </Render>
      <div style={styles.buttonList}>
        {permanentButtons.map(({ action, tooltip, type, name }: any) => (
          <Tooltip key={type} title={tooltip} placement="bottom">
            <Button action={action} icon={type} name={name} />
          </Tooltip>
        ))}
      </div>
    </Box>
  );
};

export default ButtonBar;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTheme } from '@mui/material';
import { Theme } from '@mintlab/ui/types/Theme';

export const useButtonBarStyle = () => {
  const {
    mintlab: { greyscale },
  } = useTheme<Theme>();

  return {
    container: {
      display: 'flex',
      '&>div:not(:last-child):after': {
        content: '""',
        width: '1px',
        height: '30px',
        background: greyscale.darker,
        position: 'absolute',
        bottom: '10px',
        right: '0px',
      },
    },
    buttonList: { position: 'relative', display: 'flex' } as const,
  };
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import catalog from './catalog.svg';

const tableMaxWidth = '1600px';
const titleBarHeight = '72px';
const tableMargin = '20px';
const floatingBannerMargin = '114px';
const cardPadding = '8px';
const tableSideSpace = `${tableMargin} + ${cardPadding}`;

/**
 * @return {JSS}
 */
export const catalogStyleSheet = () => ({
  wrapper: {
    height: '100%',
    overflow: 'hidden',
  },
  contentWrapper: {
    position: 'relative',
    height: `calc(100% - ${titleBarHeight})`,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
  },
  sheet: {
    position: 'relative',
    overflowY: 'hidden',
    height: '100%',
    width: '100%',
    border: 'none',
    margin: 'auto',
    'background-image': `url(${catalog})`,
    'background-size': '18px',
  },
  sheetWidth: {
    width: 'calc(100% - 500px)',
  },
  tableWrapper: {
    display: 'block',
    maxWidth: tableMaxWidth,
    width: `calc(100% - (${tableSideSpace}) * 2)`,
    height: `calc(100% - ${tableMargin})`,
    margin: `${tableMargin} auto 0 auto`,
    padding: '0px',
  },
  tableWrapperMovingItems: {
    height: `calc(100% - ${tableMargin} - ${floatingBannerMargin})`,
  },
  typeCell: {
    width: '150px',
  },
  iconCell: {
    width: '50px',
  },
  loader: {
    position: 'absolute',
    top: 0,
    right: '50%',
  },
  card: {
    position: 'relative',
    boxSizing: 'border-box',
    height: 'calc(100% + 24px)',
  },
  addElementWrapper: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    zIndex: 1,
    padding: 24,

    '&.withMoveBanner': {
      bottom: floatingBannerMargin,
    },
  },
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const valuesStyleSheet = ({
  mintlab: { greyscale },
  palette: { primary },
}) => ({
  time: {
    marginLeft: '10px',
    color: greyscale.evenDarker,
  },
  link: {
    color: primary.main,
    display: 'flex',
    '& span': {
      marginLeft: '6px',
    },
  },
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { Button } from '@mintlab/ui/App/Material/Button';
import ActionFeedback from '@mintlab/ui/App/Material/ActionFeedback';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { withStyles } from '@mui/styles';
import { actionStylesheet } from './Action.style';

/**
 * @reactProps {Object} classes
 * @reactProps {Object} value
 * @reactProps {Object} t
 * @return {ReactElement}
 */
const CopyToClipboard = ({ classes, value, t }) => {
  const ref = useRef(null);

  const copy = () => {
    const current = ref.current;
    current.type = 'text';
    current.select();
    document.execCommand('copy');
    current.type = 'hidden';
  };

  return (
    <div>
      <input ref={ref} type="hidden" value={value} />
      <ActionFeedback
        title={t('catalog:detailView.copiedToClipboard')}
        type="default"
      >
        {({ handleOpen }) => (
          <div>
            <Tooltip title={t('catalog:detailView.copyToClipboard')}>
              <Button
                name="fileCopy"
                icon="file_copy"
                iconSize="extraSmall"
                action={() => {
                  copy();
                  handleOpen();
                }}
                classes={{
                  root: classes.valueActionButton,
                }}
              />
            </Tooltip>
          </div>
        )}
      </ActionFeedback>
    </div>
  );
};

export default withStyles(actionStylesheet)(CopyToClipboard);

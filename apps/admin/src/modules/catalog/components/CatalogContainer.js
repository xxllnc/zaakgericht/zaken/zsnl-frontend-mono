// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { showDialog } from '../../../store/ui/ui.actions';
import { toggleCatalogItem, loadMore } from '../store/items/items.actions';
import { fetchCatalogItem } from '../store/details/details.actions';
import { AJAX_STATE_PENDING } from '../../../library/redux/ajax/createAjaxConstants';
import { invoke } from '../../../store/route/route.actions';
import { getPathToItem } from '../library/pathGetters';
import { inSearch } from '../library/inSearch';
import {
  clearMoveItems as clearMoveItemsAction,
  confirmMoveItems as confirmMoveItemsAction,
} from '../store/moveItems/moveItems.actions';
import Catalog from './Catalog';
import { DIALOG_ADD_ELEMENT } from './Dialogs/dialogs.constants';

const navigableItemTypes = [
  'folder',
  'case_type',
  'object_type',
  'custom_object_type',
];

/**
 * @param {*} selected
 * @param {*} beingMoved
 * @returns {Object}
 */
export const addRowData = (selected, beingMoved) => row => ({
  ...row,
  path: getPathToItem(row.id, row.type),
  isNavigable: navigableItemTypes.includes(row.type),
  icon: `entityType.${row.type}`,
  id: row.id,
  selected: selected.includes(row.id),
  beingMoved: beingMoved.some(item => item.id === row.id),
});

export const markSelected = selected => row => ({
  ...row,
  selected: selected.includes(row.id),
});

/**
 * @param {Object} state
 * @param {Object} state.catalog
 * @param {Object} state.catalog.items
 * @param {Object} state.catalog.details
 * @return {Object}
 */
const mapStateToProps = state => {
  if (!state.catalog) {
    return {};
  }

  const {
    catalog: { items, details, moveItems },
    route,
  } = state;

  return {
    load: items.load,
    catalogItem: details.data,
    pageLoaded: items.pageLoaded,
    loadingItems: items.state === AJAX_STATE_PENDING,
    currentFolderUUID: items.currentFolderUUID,
    currentFolderName: items.currentFolderName,
    detailView: items.detailView,
    items: items.items,
    loadingDetailView: details.state === AJAX_STATE_PENDING,
    loadingTable: items.state === AJAX_STATE_PENDING,
    selectedItems: items.selectedItems,
    moveItems: moveItems.items,
    showDetailView: details.showDetailView,
    movingItems: moveItems.state === AJAX_STATE_PENDING,
    inSearch: inSearch(route),
  };
};

/**
 * @param {Function} dispatch
 * @param {Function} props
 * @param {Array} props.segments
 * @return {Object}
 */
const mapDispatchToProps = dispatch => {
  const dispatchInvoke = payload => dispatch(invoke(payload));
  const doNavigate = path => {
    dispatchInvoke({
      path,
    });
  };

  return {
    doNavigate,
    addElement: () =>
      dispatch(
        showDialog({
          type: DIALOG_ADD_ELEMENT,
        })
      ),
    fetchCatalogItem: () => dispatch(fetchCatalogItem()),
    clearMoveItems: () => dispatch(clearMoveItemsAction()),
    confirmMoveItems: payload => dispatch(confirmMoveItemsAction(payload)),
    invoke: dispatchInvoke,
    toggleItem: bindActionCreators(toggleCatalogItem, dispatch),
    loadMore: payload => dispatch(loadMore(payload)),
  };
};

/**
 * @param {Object} stateProps
 * @param {Object} dispatchProps
 * @param {Object} ownProps
 * @return {Object}
 */
const mergeProps = (stateProps, dispatchProps, ownProps) => {
  const { selectedItems, showDetailView, items, currentFolderUUID, moveItems } =
    stateProps;

  const { doNavigate } = dispatchProps;
  const { t } = ownProps;

  const rows = items.map(addRowData(selectedItems, moveItems));
  const columnTranslations = t('catalog:column', { returnObjects: true });
  const addColumnHeaders = column => ({
    ...column,
    label: columnTranslations[column.name],
  });
  const columns = [
    { name: 'selected', width: 50 },
    { name: 'icon', width: 75 },
    { name: 'name', width: 1 },
    { name: 'type', width: 200 },
  ].map(addColumnHeaders);

  const onRowNavigate = ({ isNavigable, path }) => {
    if (isNavigable) {
      doNavigate(path);
    }
  };

  return {
    ...stateProps,
    ...dispatchProps,
    ...ownProps,
    columns,
    rows,
    onRowNavigate,
    showDetailView,
    confirmMoveItems() {
      dispatchProps.confirmMoveItems({
        currentFolderUUID,
        moveItems,
      });
    },
    t,
  };
};

/**
 * Connects {@link Catalog} with {@link i18next}, and
 * the store.
 * @return {ReactElement}
 */
const CatalogContainer = withTranslation()(
  connect(mapStateToProps, mapDispatchToProps, mergeProps)(Catalog)
);

export default CatalogContainer;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { FormikValues } from 'formik';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import {
  updateField,
  setRequired,
  hideFields,
  showFields,
  setOptional,
  Rule,
  valueEquals,
} from '@zaaksysteem/common/src/components/form/rules';

type IntegrationType = ValueType<string, { module: string }>;

const DocumentTemplate: React.FunctionComponent<any> = ({
  saveAction,
  formDefinition,
  id,
  t,
  saving,
  initializing,
  hide,
}) => {
  const handleOnSubmit = (values: FormikValues) => {
    saveAction({
      values: {
        ...values,
        integration_uuid: values.integration_uuid.value,
      },
    });
  };

  const matchesIntegration = (module: string) => (field: any) => {
    const integration: IntegrationType = (field.integrations || []).find(
      (thisIntegration: { value: string }) => {
        return thisIntegration.value === field.value.value;
      }
    );
    return integration && integration?.data?.module === module;
  };

  const rules: any[] = [
    new Rule()
      .when('integration_uuid', valueEquals('default'))
      .then(showFields(['file']))
      .and(hideFields(['integration_reference']))
      .and(setOptional(['integration_reference']))
      .and(setRequired(['file']))
      .else(hideFields(['file']))
      .and(showFields(['integration_reference']))
      .and(setRequired(['integration_reference']))
      .and(setOptional(['file'])),
    new Rule().when('integration_uuid', matchesIntegration('xential')).then(
      updateField('integration_reference', {
        label: t('documentTemplate:fields.templateExternalName.labelXential'),
      })
    ),
    new Rule().when('integration_uuid', matchesIntegration('stuf_dcr')).then(
      updateField('integration_reference', {
        label: t('documentTemplate:fields.templateExternalName.labelStufDCR'),
      })
    ),
  ];

  const title = t('documentTemplate:dialog.title', {
    action: id ? t('common:edit') : t('common:create'),
  });

  return (
    <FormDialog
      formDefinition={formDefinition}
      onSubmit={handleOnSubmit}
      title={title}
      scope="catalog-document-template-dialog"
      rules={rules}
      icon="insert_drive_file"
      isInitialValid={id ? true : false}
      initializing={initializing}
      saving={saving}
      onClose={hide}
    />
  );
};

export default DocumentTemplate;

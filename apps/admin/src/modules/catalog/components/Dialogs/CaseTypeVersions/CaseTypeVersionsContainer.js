// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { AJAX_STATE_PENDING } from '../../../../../library/redux/ajax/createAjaxConstants';
import { initCaseTypeVersionsActivate } from '../../../store/caseTypeVersions/caseTypeVersions.actions';
import CaseTypeVersions from './CaseTypeVersions';

const { keys } = Object;
const valueIsEmpty = value => !value || !keys(value).length;
const getValue = (type, value, t) => {
  if (valueIsEmpty(value)) return '-';

  switch (type) {
    case 'modified_components':
      return keys(value)
        .filter(key => value[key])
        .map(key => t(`caseType:modified_components:${key}`))
        .join(', ');
    default:
      return value;
  }
};
const mapDetails = (details, t) =>
  keys(details).map(detail => ({
    title: t(`caseTypeVersions:details:${detail}`),
    value: getValue(detail, details[detail], t),
  }));

const mapVersions =
  t =>
  ({
    attributes: {
      active,
      created,
      id,
      version,
      display_name,
      modified_components,
      change_note,
    },
    case_type_id,
  }) => ({
    active,
    details: mapDetails({ display_name, modified_components, change_note }, t),
    version_id: id,
    case_type_id,
    created,
    version,
  });

const mapStateToProps = (
  {
    catalog: {
      caseTypeVersions: { id, state, versions },
    },
  },
  { t }
) => ({
  case_type_id: id,
  loading: state === AJAX_STATE_PENDING,
  versions: versions.map(mapVersions(t)).reverse(),
});

const mapDispatchToProps = dispatch => {
  return {
    initCaseTypeVersionsActivate: payload =>
      dispatch(initCaseTypeVersionsActivate(payload)),
  };
};

const connectedDialog = withTranslation()(
  connect(mapStateToProps, mapDispatchToProps)(CaseTypeVersions)
);

export default connectedDialog;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { FormikValues } from 'formik';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';

const CaseTypeVersionsActivate: React.FunctionComponent<any> = ({
  formDefinition,
  t,
  hide,
  caseTypeVersionsActivate,
  case_type_id,
  version_id,
  versionToActivate,
}) => {
  const handleOnSubmit = (values: FormikValues) => {
    caseTypeVersionsActivate({
      case_type_id,
      version_id,
      reason: values.reason,
    });
  };

  const title = t('caseTypeVersions:dialog.titleActivate', {
    version: versionToActivate,
  });

  return (
    <FormDialog
      formDefinition={formDefinition}
      onSubmit={handleOnSubmit}
      title={title}
      scope="catalog-case-type-versions-activate-dialog"
      icon="extension"
      onClose={hide}
    />
  );
};

export default CaseTypeVersionsActivate;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Select, { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import { FormFieldComponentType } from '@zaaksysteem/common/src/components/form/types/Form2.types';
import { useLocationChoicesQuery } from './LocationFinder.library';

export const LocationFinder: FormFieldComponentType<
  ValueType<string>,
  { appointmentIntegrationUuid?: string }
> = props => {
  const [selectProps, ServerErrorDialog] = useLocationChoicesQuery(
    props?.config.appointmentIntegrationUuid
  );

  return (
    <>
      <Select {...props} {...selectProps} />
      {ServerErrorDialog}
    </>
  );
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import FlatValueSelect from '../../../../../../../components/Form/FlatValueSelect';

const Types = props => {
  return (
    <FlatValueSelect
      {...props}
      menuPortalTarget={document.getElementById(props.dialogId)}
    />
  );
};

export default Types;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery } from '@tanstack/react-query';
import { request } from '@zaaksysteem/common/src/library/request/request';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';

export const useLocationChoicesQuery = (
  appointmentUuid: string | undefined
) => {
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  const data = useQuery(
    ['locations', appointmentUuid],
    async ({ queryKey: [__, appointment_id] }) => {
      const body = await request<{
        result: { instance: { data: { id: string; label: string }[] } };
      }>(
        'GET',
        `/api/v1/sysin/interface/${appointment_id}/trigger/get_location_list`
      ).catch(err => void setTimeout(() => openServerErrorDialog(err), 1500));
      return body
        ? body.result.instance.data.map(location => ({
            value: location.id,
            label: location.label,
          }))
        : [];
    }
  );

  const selectProps = {
    loading: data.status === 'loading',
    choices: data.data || [],
    isDisabled: data.data === undefined || data.data.length === 0,
  };

  return [selectProps, ServerErrorDialog] as const;
};

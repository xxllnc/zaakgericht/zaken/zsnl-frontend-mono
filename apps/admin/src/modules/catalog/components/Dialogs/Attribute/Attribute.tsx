// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import { FormFields } from '@zaaksysteem/common/src/components/form/fields';
import isValidMagicString from '@zaaksysteem/common/src/components/form/fields/MagicStringGenerator/isValidMagicstring';
import {
  Rule,
  valueOneOf,
  valueEquals,
  hideFields,
  showFields,
  setFieldValue,
  hasValue,
  transferDataAsConfig,
} from '@zaaksysteem/common/src/components/form/rules';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types';
import {
  PRODUCT_FINDER,
  OBJECT_TYPE_FINDER,
  LOCATION_FINDER,
  TYPES,
  VERSIONS,
} from '../../../../../components/Form/Constants/fieldTypes';
import { saveAttribute } from '../../../store/attribute/attribute.actions';
import Types from './Components/Types/Types';
import Versions from './Components/Versions/Versions';
import { LocationFinder } from './Components/LocationFinder/LocationFinder';
import { ProductFinder } from './Components/ProductFinder/ProductFinder';
import {
  attributeFormSelector,
  fetchAppointmentIntegrationId,
  mangleValuesForSave,
  defaultInitialValues,
  fetchAppointmentV2Interfaces,
} from './Attribute.library';
import { getFormDefinition } from './Attribute.formDefinition';

//Custom type mapping for the Attribute Dialog
const fieldComponents = {
  [TYPES]: Types,
  [VERSIONS]: Versions,
  [LOCATION_FINDER]: LocationFinder,
  [PRODUCT_FINDER]: ProductFinder,
  [OBJECT_TYPE_FINDER]: FormFields[OBJECT_TYPE_FINDER],
};

// Custom Yup validation mapping for the Attribute Dialog
const validationMap = {
  magic_string: isValidMagicString,
};

const Attribute: React.FunctionComponent<any> = ({ hide }) => {
  const [t] = useTranslation();
  const dispatch = useDispatch();
  const [appointmentInterfaceUuid, setAppointmentUuid] =
    React.useState<string>();

  const [formDefinition, setFormDefinition] = React.useState<FormDefinition>(
    []
  );
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  const { editedAttributeUuid, editedAttributeInitialValues, loading, saving } =
    useSelector(attributeFormSelector);

  const fetchingAppointemtIntegration =
    typeof appointmentInterfaceUuid !== 'string' || formDefinition.length === 0;
  const isCreating = !editedAttributeInitialValues;

  const title = t('attribute:dialog.title', {
    action: editedAttributeUuid ? t('common:edit') : t('common:create'),
  });

  React.useEffect(() => {
    const promises = [
      fetchAppointmentIntegrationId(openServerErrorDialog),
      fetchAppointmentV2Interfaces(openServerErrorDialog),
    ];

    Promise.all(promises).then(responses => {
      const [id, appointmentV2interfaces] = responses;
      setAppointmentUuid(id || '');
      !loading &&
        setFormDefinition(
          getFormDefinition(
            editedAttributeInitialValues || defaultInitialValues,
            t,
            id || '',
            isCreating,
            appointmentV2interfaces
          )
        );
    });
  }, [loading]);

  const rules = React.useMemo(
    () => [
      new Rule()
        .when('name', () => isCreating)
        .then(transferDataAsConfig('name', 'magic_string', 'rawName')),
      new Rule()
        .when('attribute_type', valueOneOf(['checkbox', 'select', 'option']))
        .then(showFields(['attribute_values']))
        .else(hideFields(['attribute_values'])),
      new Rule()
        .when('attribute_type', valueEquals('file'))
        .then(
          showFields([
            'document_category',
            'document_trust_level',
            'document_origin',
          ])
        )
        .else(
          hideFields([
            'document_category',
            'document_trust_level',
            'document_origin',
          ])
        ),
      new Rule()
        .when('attribute_type', valueEquals('appointment'))
        .then(showFields(['appointment_location_id', 'appointment_product_id']))
        .else(
          hideFields(['appointment_location_id', 'appointment_product_id'])
        ),
      new Rule()
        .when('appointment_location_id', hasValue)
        .then(showFields(['appointment_product_id']))
        .and(
          transferDataAsConfig(
            'appointment_location_id',
            'appointment_product_id',
            'locationUuid'
          )
        )
        .else(hideFields(['appointment_product_id'])),
      new Rule()
        .when('attribute_type', valueOneOf(['relationship']))
        .then(showFields(['relationship_type']))
        .else(hideFields(['relationship_type']))
        .and(setFieldValue('relationship_type', null)),
      new Rule()
        .when('relationship_type', valueOneOf(['custom_object']))
        .then(showFields(['object_type', 'type_multiple']))
        .else(hideFields(['object_type', 'type_multiple']))
        .and(setFieldValue('object_type', [])),
      new Rule()
        .when(
          'attribute_type',
          valueOneOf(['numeric', 'select', 'text', 'text_uc', 'textarea'])
        )
        .then(showFields(['type_multiple'])),
      new Rule()
        .when('attribute_type', valueEquals('appointment_v2'))
        .then(showFields(['appointment_interface_uuid']))
        .else(hideFields(['appointment_interface_uuid'])),
      new Rule()
        .when(
          'attribute_type',
          valueOneOf([
            'address_v2',
            'geojson',
            'appointment_v2',
            'relationship',
          ])
        )
        .then(hideFields(['value_default']))
        .else(showFields(['value_default'])),
    ],
    [isCreating]
  );

  return (
    <React.Fragment>
      <FormDialog
        formDefinition={formDefinition}
        title={title}
        scope="attribute-form-component"
        validationMap={validationMap as any}
        fieldComponents={fieldComponents}
        rules={rules}
        icon="extension"
        isInitialValid={!isCreating}
        initializing={loading || fetchingAppointemtIntegration}
        saving={saving}
        onClose={hide}
        onSubmit={values =>
          void dispatch(
            saveAttribute(
              mangleValuesForSave({
                values,
                id: editedAttributeUuid,
                appointment_interface_uuid: appointmentInterfaceUuid,
              })
            )
          )
        }
      />
      {ServerErrorDialog}
    </React.Fragment>
  );
};

export default Attribute;

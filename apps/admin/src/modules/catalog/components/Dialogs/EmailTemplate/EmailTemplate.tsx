// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { FormikValues } from 'formik';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import DocumentAttributeSearcher from './Components/DocumentAttributeFinder';

const EmailTemplate: React.FunctionComponent<any> = ({
  saveAction,
  formDefinition,
  id,
  t,
  saving,
  loading,
  hide,
  currentFolderUUID,
}) => {
  const handleOnSubmit = (values: FormikValues) =>
    saveAction({ values, id, currentFolderUUID });

  // //Custom type mapping for the EmailTemplate Dialog
  const fieldComponents = {
    DocumentAttributeSearcher,
  };

  const title = t('emailTemplate:dialog.title', {
    action: id ? t('common:edit') : t('common:create'),
  });

  return (
    <FormDialog
      formDefinition={formDefinition}
      onSubmit={handleOnSubmit}
      title={title}
      scope="catalog-email-template-dialog"
      fieldComponents={fieldComponents}
      icon="email"
      isInitialValid={id ? true : false}
      initializing={loading}
      saving={saving}
      onClose={hide}
    />
  );
};

export default EmailTemplate;

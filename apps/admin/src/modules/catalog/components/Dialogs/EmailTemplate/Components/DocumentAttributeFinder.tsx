// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { useDocumentAttributeChoicesQuery } from './DocumentAttributeFinder.library';

const DocumentAttributeFinder = ({ ...restProps }) => {
  const [selectProps, ServerErrorDialog] = useDocumentAttributeChoicesQuery();
  return (
    <>
      <Select {...restProps} {...selectProps} />
      {ServerErrorDialog}
    </>
  );
};

export default DocumentAttributeFinder;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { withTranslation } from 'react-i18next';
import { v4 } from 'uuid';
import { asArray, get } from '@mintlab/kitchen-sink/source';
import { AJAX_STATE_PENDING } from '../../../../../library/redux/ajax/createAjaxConstants';
import { saveEmailTemplate } from '../../../store/emailTemplate/emailTemplate.actions';
import formDefinition from '../../../fixtures/emailTemplate';
import EmailTemplate from './EmailTemplate';

const { keys } = Object;

const mangleValuesForSave = ({ values, id }) => {
  const isNew = !id;
  const email_template_uuid = isNew ? v4() : id;

  // Modify values where needed, before they get
  // sent to the save action, and to the API endpoint.
  /* eslint complexity: [2, 4] */
  const fields = keys(values).reduce((accumulator, key) => {
    switch (key) {
      case 'attachments':
        if (!values.attachments) return [];
        accumulator[key] = asArray(values.attachments).map(
          ({ label, value }) => ({
            name: label,
            uuid: value,
          })
        );
        break;
      default:
        accumulator[key] = values[key];
    }
    return accumulator;
  }, {});

  fields.category_uuid = window.location.pathname.split('/catalogus/')[1];

  return {
    fields,
    email_template_uuid,
    isNew,
  };
};

const mapStateToProps = (
  {
    catalog: {
      emailTemplate,
      emailTemplate: { id, values },
      items: { currentFolderUUID },
    },
  },
  { t }
) => {
  // Insert translations, requests and other values into FormDefinition
  const defaultValues = {
    commit_message: id ? t('catalog:defaultEdit') : t('catalog:defaultCreate'),
  };
  const valuesWithDefault = {
    ...defaultValues,
    ...values,
  };
  const mapFormDefinition = field => {
    const translations = {
      'form:loading': t('common:loading'),
    };
    const getValue = name => {
      const value = get(valuesWithDefault, name);
      if (!value) return field.value;

      switch (name) {
        case 'attachments':
          return asArray(value).map(attachment => ({
            label: attachment.name,
            value: attachment.uuid,
          }));
        default:
          return value;
      }
    };

    return {
      ...field,
      label: t(field.label),
      placeholder: t(field.placeholder),
      hint: t(field.hint),
      help: t(field.help),
      ...(field.type === 'DocumentAttributeSearcher' && {
        translations,
      }),
      value: getValue(field.name),
    };
  };

  const newProps = {
    ...emailTemplate,
    formDefinition: formDefinition.map(mapFormDefinition),
    currentFolderUUID,
  };

  return {
    loading: emailTemplate.state === AJAX_STATE_PENDING,
    saving: emailTemplate.savingState === AJAX_STATE_PENDING,
    ...newProps,
  };
};

const mapDispatchToProps = dispatch => ({
  saveAction: ({ values, id }) =>
    dispatch(
      saveEmailTemplate(
        mangleValuesForSave({
          values,
          id,
        })
      )
    ),
});

const connectedDialog = withTranslation()(
  connect(mapStateToProps, mapDispatchToProps)(EmailTemplate)
);

export default connectedDialog;

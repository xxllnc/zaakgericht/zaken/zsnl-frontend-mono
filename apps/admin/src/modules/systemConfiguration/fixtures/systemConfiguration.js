// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const systemConfiguration = [
  {
    slug: 'cases',
    fieldSets: [
      {
        title: 'notifications',
        description: 'notifications',
        fields: [
          'allocation_notification_template_id',
          'new_assigned_case_notification_template_id',
          'new_document_notification_template_id',
          'new_attribute_proposal_notification_template_id',
          'new_ext_pip_message_notification_template_id',
          'new_int_pip_message_notification_template_id',
          'case_term_exceeded_notification_template_id',
          'case_suspension_term_exceeded_notification_template_id',
        ],
      },
      {
        title: 'assignCase',
        fields: ['assignment_department_check'],
      },
      {
        title: 'rejectedCases',
        description: 'rejectedCases',
        fields: ['case_distributor_group', 'case_distributor_role'],
      },
      {
        title: 'caseNumber',
        description: 'caseNumber',
        fields: ['case_number_prefix'],
      },
      {
        title: 'showObject',
        description: 'showObject',
        fields: ['show_object_v1', 'show_object_v2'],
      },
    ],
  },
  {
    slug: 'users',
    fieldSets: [
      {
        title: 'newUsers',
        fields: ['first_login_confirmation', 'first_login_intro'],
      },
      {
        title: 'confirmationNewUsers',
        fields: ['new_user_template'],
      },
      {
        title: 'dashboard',
        fields: ['disable_dashboard_customization'],
      },
      {
        title: 'allowedDomains',
        fields: ['dashboard_ui_xss_uri'],
      },
      {
        title: 'signatureUploadRole',
        fields: ['signature_upload_role'],
      },
    ],
  },
  {
    slug: 'pip',
    fieldSets: [
      {
        title: 'pipFeedback',
        fields: ['feedback_email_template_id'],
      },
      {
        title: 'pipIntro',
        fields: ['pip_login_intro'],
      },
    ],
  },
  {
    slug: 'documents',
    fieldSets: [
      {
        title: 'documentIntake',
        fields: ['document_intake_user'],
      },
      {
        title: 'documentRecognition',
        fields: ['file_username_seperator'],
      },
      {
        title: 'editDocumentOnline',
        fields: ['edit_document_online'],
      },
    ],
  },
  {
    slug: 'about',
    fieldSets: [
      {
        title: 'customerInfo',
        fields: [
          'customer_info_naam',
          'customer_info_naam_lang',
          'customer_info_naam_kort',
          'customer_info_website',
        ],
      },
      {
        title: 'customerAddress',
        fields: [
          'customer_info_adres',
          'customer_info_postcode',
          'customer_info_woonplaats',
        ],
      },
      {
        title: 'customerExtra',
        fields: ['customer_info_postbus', 'customer_info_postbus_postcode'],
      },
      {
        title: 'customerPhone',
        fields: ['customer_info_telefoonnummer', 'customer_info_faxnummer'],
      },
      {
        title: 'customerMail',
        fields: ['customer_info_email', 'customer_info_zaak_email'],
      },
      {
        title: 'customerInfoCountryCode',
        fields: ['customer_info_country_code'],
      },
      {
        title: 'customerInfoGemeente',
        fields: [
          'customer_info_gemeente_portal',
          'customer_info_gemeente_id_url',
        ],
      },
    ],
  },
  {
    slug: 'premium',
    fieldSets: [
      {
        title: 'documentWatcher',
        fields: ['files_locally_editable'],
      },
      {
        title: 'editDocumentOnline',
        fields: ['edit_document_msonline'],
      },
    ],
  },
  {
    slug: 'other',
    fieldSets: [
      {
        title: 'bagPriority',
        fields: ['bag_priority_gemeentes'],
      },
      {
        title: 'preferredChannel',
        fields: ['contact_channel_enabled'],
      },
      {
        title: 'publicManpage',
        fields: ['public_manpage'],
      },
      {
        title: 'customRelationRoles',
        fields: ['custom_relation_roles'],
      },
      {
        title: 'location',
        fields: ['customer_info_latitude', 'customer_info_longitude'],
      },
      {
        title: 'pipAuthorization',
        fields: ['subject_pip_authorization_confirmation_template_id'],
      },
      {
        title: 'exportQueue',
        fields: ['export_queue_email_template_id'],
      },
      {
        title: 'cspConfiguration',
        fields: ['custom_csp_hosts'],
      },
    ],
  },
  {
    slug: 'deprecated',
    fieldSets: [
      {
        title: 'externalSearch',
        description: 'externalSearch',
        fields: [
          'requestor_search_extension_active',
          'requestor_search_extension_name',
          'requestor_search_extension_href',
        ],
      },
      {
        title: 'changePassword',
        fields: ['users_can_change_password'],
      },
      {
        title: 'stufZkn',
        fields: ['enable_stufzkn_simulator'],
      },
      {
        title: 'allowedTemplates',
        fields: ['allowed_templates'],
      },
    ],
  },
];

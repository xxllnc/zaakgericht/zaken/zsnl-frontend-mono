// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxConstants } from '../../../library/redux/ajax/createAjaxConstants';

export const SYSTEMCONFIGURATION_FETCH = createAjaxConstants(
  'SYSTEMCONFIGURATION:FETCH'
);
export const SYSTEMCONFIGURATION_UPDATE_CHOICES = createAjaxConstants(
  'SYSTEMCONFIGURATION:UPDATE_CHOICES'
);
export const SYSTEMCONFIGURATION_SAVE = createAjaxConstants(
  'SYSTEMCONFIGURATION:SAVE'
);
export const SYSTEMCONFIGURATION_DISCARD = 'SYSTEMCONFIGURATION:DISCARD';
export const SYSTEMCONFIGURATION_DISCARD_DONE =
  'SYSTEMCONFIGURATION:DISCARD_DONE';

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withStyles } from '@mui/styles';
import VerticalMenu from '@mintlab/ui/App/Zaaksysteem/VerticalMenu';
import { categoriesStyleSheet } from './Categories.style';

const { assign } = Object;

/**
 * @param {Object} props
 * @param {Array} props.categories
 * @param {Function} props.invoke
 * @param {Object} props.classes
 * @return {ReactElement}
 */
const Categories = ({ categories, invoke, classes }) => {
  const routeTo = slug => {
    invoke({
      path: `/admin/configuratie/${slug}`,
    });
  };

  const mapCategories = category =>
    assign({}, category, {
      action: routeTo.bind(this, category.slug),
      active: category.current,
    });

  return (
    <VerticalMenu
      items={categories.map(mapCategories)}
      classes={{
        menu: classes.menu,
      }}
    />
  );
};

export default withStyles(categoriesStyleSheet)(Categories);

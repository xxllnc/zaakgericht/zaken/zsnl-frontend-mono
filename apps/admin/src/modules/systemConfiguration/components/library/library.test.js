// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { get } from '@mintlab/kitchen-sink/source';
import { getFieldByName, getFieldList, doValidation } from './library';

/* eslint-disable quotes, quote-props */
const fieldSets = [
  {
    title: 'Verzenden PIP-feedback',
    fields: [
      {
        type: 'select',
        config: {
          id_map_field_name: 'id',
          object_type_name: 'email_template',
        },
        label: 'E-mailsjabloon',
        name: 'feedback_email_template_id',
        isMulti: false,
        disabled: false,
        reference: '53be14f6-cfe6-4cf5-aec0-7b3719882492',
        required: false,
        hasInitialChoices: false,
        externalLabel: true,
        value: 'foo',
        constraints: ['number'],
        choicesParameters: {
          url: '/api/v1/email_template',
          match: 'label',
          name: 'feedback_email_template_id',
        },
        rows: 10,
        help: 'Deze instelling wijzigt het sjabloon dat gebruikt wordt bij het verzenden van feedback uit de PIP.',
      },
    ],
  },
  {
    title: 'Welkomsttekst PIP',
    fields: [
      {
        type: 'html',
        config: {
          format: 'html',
          style: 'paragraph',
        },
        label: 'Welkomsttekst PIP',
        name: 'pip_login_intro',
        isMulti: false,
        disabled: false,
        reference: '3c03a785-4909-4f1f-88c7-a65042502a95',
        required: false,
        hasInitialChoices: false,
        externalLabel: true,
        value: 'bar',
        constraints: ['required'],
        rows: 10,
        help: 'Deze tekst verschijnt op de inlogpagina van de PIP. Indien er geen waarde is ingevuld wordt er geen tekst weergegeven.',
      },
    ],
  },
];

describe('the `getFieldByName` function', () => {
  test('gets a specific field by namne', () => {
    const actual = getFieldByName('pip_login_intro', fieldSets);
    const expected = get(fieldSets, '[1].fields[0]');
    expect(actual).toEqual(expected);
  });

  test('returns undefined when not found', () => {
    //
    const actual = getFieldByName('foo', fieldSets);
    const expected = undefined;
    expect(actual).toEqual(expected);
  });
});

describe('the `getFieldList` function', () => {
  test('outputs a flat array of field objects', () => {
    const actual = getFieldList(fieldSets);
    const expected = [
      get(fieldSets, '[0].fields[0]'),
      get(fieldSets, '[1].fields[0]'),
    ];
    expect(actual).toEqual(expected);
  });
});

describe('the `doValidation` function', () => {
  test('outputs the error message for each field that is in an error state', () => {
    const actual = doValidation({
      t: () => 'error message',
      fieldSets,
      values: {
        pip_login_intro: '',
        feedback_email_template_id: 'foo',
      },
    });

    const expected = {
      pip_login_intro: 'error message',
      feedback_email_template_id: 'error message',
    };

    expect(actual).toEqual(expected);
  });

  test('does not output the name of a field if it passess validation', () => {
    const actual = doValidation({
      t: () => 'error message',
      fieldSets,
      values: {
        pip_login_intro: 'foo',
        feedback_email_template_id: 123,
      },
    });

    const expected = {};

    expect(actual).toEqual(expected);
  });
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import LazyLoader from '@mintlab/ui/App/Abstract/LazyLoader';

export const LoadableSystemConfiguration = props => (
  <LazyLoader
    promise={() =>
      import(
        // https://webpack.js.org/api/module-methods/#import
        /* webpackChunkName: "admin.system-configuration" */
        './SystemConfigurationContainer'
      )
    }
    {...props}
  />
);

export default LoadableSystemConfiguration;

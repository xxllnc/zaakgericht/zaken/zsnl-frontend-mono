// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withStyles } from '@mui/styles';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import SubAppHeader from '../../../components/Header/SubAppHeader';
import Title from '../../../components/Header/Title';
import Categories from './Categories/Categories';
import FormikWrapper from './FormikWrapper';
import { systemConfigurationStyleSheet } from './SystemConfiguration.style';

/**
 * @param {Object} props
 * @param {Function} props.invoke
 * @param {Array} props.categories
 * @param {Array} props.fieldSets
 * @param {Function} props.t
 * @param {Object} props.classes
 * @param {Array} props.changed
 * @param {Object} props.banners
 * @param {Function} props.showBanner
 * @param {Function} props.hideBanner
 * @param {boolean} props.discard
 * @param {Function} props.onDiscard
 * @return {ReactElement}
 */

export const SystemConfiguration = props => {
  const {
    classes,
    categories,
    invoke,
    t,
    fieldSets,
    banners,
    hideBanner,
    showBanner,
    save,
    discard,
    onDiscard,
    updateChoices,
  } = props;

  if (!fieldSets) {
    return <Loader />;
  }

  return (
    <div className={classes.wrapper}>
      <SubAppHeader>
        <Title>{t('systemConfiguration:title')}</Title>
      </SubAppHeader>

      <section className={classes.grid}>
        <Categories categories={categories} invoke={invoke} />

        <FormikWrapper
          fieldSets={fieldSets}
          banners={banners}
          t={t}
          hideBanner={hideBanner}
          showBanner={showBanner}
          save={save}
          onDiscard={onDiscard}
          discard={discard}
          updateChoices={updateChoices}
        />
      </section>
    </div>
  );
};

export default withStyles(systemConfigurationStyleSheet)(SystemConfiguration);

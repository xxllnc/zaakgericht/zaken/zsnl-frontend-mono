// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Formik } from 'formik';
import Form from './Form/Form';
import { doValidation, getFieldList } from './library/library';

/**
 * Wraps SystemConfiguration with Formik using
 * the fieldSets (supplied by SystemConfigurationContainer)
 * to initialize the Form.
 *
 * @param {Object} props
 * @return {ReactElement}
 **/
const FormikWrapper = props => {
  const { fieldSets, t } = props;

  if (!fieldSets) {
    return null;
  }

  const initialValues = getFieldList(fieldSets).reduce((accumulator, field) => {
    accumulator[field.name] = field.value;
    return accumulator;
  }, {});

  return (
    <Formik
      initialValues={initialValues}
      enableReinitialize={true}
      validate={values =>
        doValidation({
          values,
          t,
          fieldSets,
        })
      }
      render={formikProps => <Form {...props} {...formikProps} />}
    />
  );
};

export default FormikWrapper;

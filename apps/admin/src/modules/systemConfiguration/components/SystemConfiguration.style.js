// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * @return {JSS}
 */
export const systemConfigurationStyleSheet = () => ({
  wrapper: {
    height: '100%',
    overflow: 'hidden',
  },
  grid: {
    display: 'flex',
    height: 'calc(100% - 68px)',
  },
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import deepMerge from 'deepmerge';
import deepEqual from 'fast-deep-equal';
import { withStyles } from '@mui/styles';
import { useWidth } from '@mintlab/ui/App/library/useWidth';
import Card from '@mintlab/ui/App/Material/Card';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { reduceMap, get, cloneWithout } from '@mintlab/kitchen-sink/source';
import { sharedStylesheet } from '../../../../components/Shared.style';
import { getFieldByName } from '../library/library';
import { formStylesheet } from './Form.style';
import * as typeMap from './typemap';

const { keys } = Object;
const identifier = 'systemConfigForm';

let valuesSingleton;

const Form = ({
  classes,
  fieldSets,
  values,
  dirty,
  errors,
  hideBanner,
  banners,
  showBanner,
  t,
  resetForm,
  initialValues,
  discard,
  onDiscard,
  updateChoices,
  setFieldValue,
  loading,
  save,
}) => {
  valuesSingleton = values;
  const width = useWidth();

  React.useEffect(() => {
    return () => {
      hideBanner({ identifier });
    };
  }, []);

  const undo = [
    {
      action: () => resetForm(),
      label: t('form:undo'),
      icon: 'undo',
    },
  ];

  const bannerTypes = {
    changed: {
      identifier,
      variant: 'secondary',
      label: t('form:unsavedChanges'),
      primary: {
        action: () => {
          const items = keys(valuesSingleton).reduce((accumulator, key) => {
            if (!deepEqual(valuesSingleton[key], initialValues[key])) {
              const { reference } = getFieldByName(key, fieldSets);
              accumulator.push({
                name: key,
                value: valuesSingleton[key],
                reference,
              });
            }
            return accumulator;
          }, []);

          if (keys(items).length) {
            save(items);
          }
        },
        label: t('form:save'),
        icon: 'close',
      },
      secondary: undo,
    },
    error: {
      identifier,
      label: t('form:errorsInForm'),
      variant: 'danger',
      secondary: undo,
    },
  };

  const hasErrors = Boolean(keys(errors).length);

  const map = new Map([
    [
      () => hasErrors,
      () => {
        if (
          get(banners, `${identifier}.variant`) !== bannerTypes.error.variant
        ) {
          showBanner(bannerTypes.error);
        }
      },
    ],
    [
      () => dirty,
      () => {
        if (
          get(banners, `${identifier}.variant`) !== bannerTypes.changed.variant
        ) {
          showBanner(bannerTypes.changed);
        }
      },
    ],
    [
      () => Object.prototype.hasOwnProperty.call(banners, identifier),
      () => hideBanner({ identifier }),
    ],
  ]);

  reduceMap({
    map,
  });

  // Reset the form with the initial values, and execute the onDiscard callback.
  React.useEffect(() => {
    if (discard) {
      resetForm(initialValues);
      onDiscard();
    }
  }, [discard]);

  const mangleAndSet = React.useCallback(
    event => {
      const base = get(event, 'target', event);
      const { name, value, checked } = base;

      const { constraints, type } = getFieldByName(name, fieldSets);

      const mangle = mangleValue => {
        const map = new Map([
          [() => type === 'checkbox', () => checked],
          [
            () => constraints && constraints.includes('number'),
            () => {
              const valueToNumber = Number(mangleValue);
              return Number.isNaN(valueToNumber) ? mangleValue : valueToNumber;
            },
          ],
          [
            () => name === 'case_distributor_group',
            () => {
              updateChoices({
                name: 'case_distributor_role',
                input: value.value,
              });
              return value;
            },
          ],
        ]);

        return reduceMap({
          map,
          fallback: mangleValue,
        });
      };

      setFieldValue(name, mangle(value));
    },
    [fieldSets]
  );

  return (
    <form className={classes.sheet}>
      <div className={classes.form}>
        {fieldSets &&
          fieldSets.map(({ title, description, fields }, index) => {
            return (
              <Card
                sx={{ overflow: 'unset' }}
                title={title}
                description={description}
                key={`${identifier}-${index}`}
              >
                {fields.map(
                  ({
                    constraints = [],
                    name,
                    type,
                    help,
                    hint,
                    label,
                    disableFilterOption,
                    ...rest
                  }) => {
                    const error = errors[name];
                    const required = constraints.includes('required');
                    const value = values[name];
                    const filterOption = disableFilterOption
                      ? option => option
                      : undefined;
                    const isCompact = ['xs', 'sm', 'md'].includes(width);

                    const isHidden = () =>
                      value === undefined ||
                      (name === 'case_distributor_role' &&
                        !values['case_distributor_group']?.value);

                    // eslint-disable-next-line no-confusing-arrow
                    const checked = () =>
                      typeof value === 'boolean' ? value : undefined;

                    const InputComponent = typeMap[type];

                    if (isHidden()) return null;

                    return (
                      <div
                        key={`${identifier}-${name}`}
                        className={classes.formRow}
                      >
                        <FormControlWrapper
                          error={error}
                          required={required}
                          label={label}
                          help={help}
                          hint={hint}
                          compact={isCompact || type === 'creatable'}
                          scope="systemConfig"
                          applyBackgroundColor={type !== 'checkbox'}
                        >
                          <InputComponent
                            checked={checked()}
                            error={error}
                            name={name}
                            onChange={mangleAndSet}
                            required={required}
                            value={value}
                            key={`${identifier}-field-${name}`}
                            filterOption={filterOption}
                            loading={loading}
                            {...cloneWithout(rest, 'value')}
                          />
                        </FormControlWrapper>
                      </div>
                    );
                  }
                )}
              </Card>
            );
          })}
      </div>
    </form>
  );
};

const mergedStyles = theme =>
  deepMerge(sharedStylesheet(theme), formStylesheet(theme));

export default withStyles(mergedStyles)(Form);

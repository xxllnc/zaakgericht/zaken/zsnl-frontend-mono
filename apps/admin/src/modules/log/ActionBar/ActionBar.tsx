// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDebouncedCallback } from 'use-debounce';
//@ts-ignore
import Snackbar from '@mintlab/ui/App/Material/Snackbar/Snackbar.cmp';
import Button from '@mintlab/ui/App/Material/Button';
import TextField from '@mintlab/ui/App/Material/TextField';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { FiltersType, SetFiltersType } from './../Log.types';
import { initiateExport, useUsersQuery } from './../Log.library';
import { useActionBarStyles } from './ActionBar.styles';

type ActionBarPropsType = {
  filters: FiltersType;
  setFilters: SetFiltersType;
};

const ActionBar: React.ComponentType<ActionBarPropsType> = ({
  filters,
  setFilters,
}) => {
  const classes = useActionBarStyles();
  const [t] = useTranslation('log');
  const [snack, setSnack] = useState<string | null>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [keyword, setKeyword] = useState<string>('');
  const [caseId, setCaseId] = useState<string>('');
  const setFilter = (filter: any) => {
    setFilters({ ...filters, ...filter, page: 0 });
  };
  const [debouncedCallback] = useDebouncedCallback((type, value) => {
    if (!value.length || value.length >= 3) setFilter({ [type]: value });
  }, 500);

  const [selectProps, ServerErrorDialog, openServerErrorDialog] =
    useUsersQuery();

  return (
    <>
      <div className={classes.wrapper}>
        <div className={classes.actionWrapper}>
          <Button
            disabled={loading}
            action={() => {
              setLoading(true);
              initiateExport(filters)
                .then(() => {
                  setSnack(t('export.snack'));
                })
                .catch(openServerErrorDialog)
                .finally(() => setLoading(false));
            }}
            name="logExport"
          >
            {t('export.button')}
          </Button>
          <div className={classes.textFilterWrapper}>
            <TextField
              value={keyword}
              onChange={(event: any) => {
                const value = event.target.value;
                setKeyword(value);
                debouncedCallback('keyword', event.target.value);
              }}
              placeholder={t('filters.keyword')}
              closeAction={() => {
                setKeyword('');
                setFilter({ keyword: '' });
              }}
            />
          </div>
          <div className={classes.textFilterWrapper}>
            <TextField
              value={caseId}
              onChange={(event: any) => {
                const value = event.target.value;
                setCaseId(value);
                debouncedCallback('caseId', event.target.value);
              }}
              placeholder={t('filters.caseId')}
              closeAction={() => {
                setCaseId('');
                setFilter({ caseId: '' });
              }}
            />
          </div>
          <div className={classes.selectWrapper}>
            <Select
              {...selectProps}
              value={filters.user}
              name="filters-user"
              placeholder={t('filters.user')}
              onChange={(event: any) => {
                setFilter({
                  user: event.target.value,
                });
              }}
              filterOption={() => true}
            />
          </div>
        </div>
      </div>
      {ServerErrorDialog}
      <Snackbar
        autoHideDuration={5000}
        handleClose={(event: React.ChangeEvent, reason: string) => {
          if (reason === 'clickaway') return;
          setSnack(null);
        }}
        message={snack}
        open={Boolean(snack)}
        classes={{
          root: classes.snack,
        }}
      />
    </>
  );
};

export default ActionBar;

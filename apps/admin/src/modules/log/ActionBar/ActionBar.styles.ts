// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useActionBarStyles = makeStyles(
  ({ mintlab: { greyscale, radius }, typography }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      gap: 10,
      marginBottom: 20,
    },
    actionWrapper: {
      display: 'flex',
      flexWrap: 'wrap',
      alignItems: 'center',
      gap: 20,
    },
    selectWrapper: {
      width: 300,
      height: 41,
      borderRadius: radius.textField,
      backgroundColor: greyscale.light,
    },
    textFilterWrapper: {
      width: 300,
      borderRadius: radius.textField,
      backgroundColor: greyscale.light,
    },
    snack: {
      marginBottom: 35,
    },
  })
);

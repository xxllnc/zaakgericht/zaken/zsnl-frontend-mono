// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type FiltersType = {
  page: number;
  rowsPerPage: number;
  caseId: string;
  keyword: string;
  user: { value: string; label: string } | null;
};

export type SetFiltersType = (filters: FiltersType) => void;

export type RowType = {
  name: string;
  uuid: string;
  caseId: number;
  dateCreated: string;
  event: string;
  component: string;
  user: string;
};

export type DataType = {
  count: number;
  rows: RowType[];
};

export type GetDateType = (filters: FiltersType) => Promise<DataType>;

export type FetchUsersType = (keyword: string) => any;

export type InitiateExportType = (filters: FiltersType) => any;

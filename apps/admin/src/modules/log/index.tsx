// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import Log from './Log';
import locale from './Log.locale';
import { defaultOptions } from './Log.config';

type LogRouteType = {};

const LogModule: React.ComponentType<LogRouteType> = () => {
  const queryClient = new QueryClient({ defaultOptions });

  return (
    <QueryClientProvider client={queryClient}>
      <I18nResourceBundle resource={locale} namespace="log">
        <Log />
      </I18nResourceBundle>
    </QueryClientProvider>
  );
};

export default LogModule;

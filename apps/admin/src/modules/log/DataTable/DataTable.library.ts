// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import fecha from 'fecha';
import { cellRenderer } from './DataTableCell';

export const getColumns = (t: i18next.TFunction, classes: any) =>
  [
    {
      name: 'caseId',
      width: 100,
    },
    {
      name: 'dateCreated',
      width: 200,
    },
    {
      name: 'event',
      width: 200,
      minWidth: 200,
      flexGrow: 1,
    },
    {
      name: 'component',
      width: 200,
    },
    {
      name: 'user',
      width: 200,
    },
  ].map(column => ({
    ...column,
    label: t(`table.columns.${column.name}`),
    minWidth: column.width,
    disableSort: true,
    cellRenderer: (rowData: any) => cellRenderer(rowData, t, classes),
  }));

export const formatDate = (date: string) =>
  date ? fecha.format(new Date(date), 'DD-MM-YYYY HH:mm:ss') : '-';

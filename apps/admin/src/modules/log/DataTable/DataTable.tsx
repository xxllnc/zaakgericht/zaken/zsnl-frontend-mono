// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import Snackbar from '@mintlab/ui/App/Material/Snackbar/Snackbar.cmp';
// @ts-ignore
import Card from '@mintlab/ui/App/Material/Card';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
// @ts-ignore
import Pagination from '@mintlab/ui/App/Material/Pagination';
import SortableTable from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable';
import { useSortableTableStyles } from '@mintlab/ui/App/Zaaksysteem/SortableTable/SortableTable.style';
import { DataType, FiltersType, SetFiltersType } from '../Log.types';
import { useDataTableStyles, useTableStyles } from './DataTable.style';
import { getColumns } from './DataTable.library';

type DataTablePropsType = {
  data?: DataType;
  filters: FiltersType;
  setFilters: SetFiltersType;
};

const DataTable: React.ComponentType<DataTablePropsType> = ({
  data,
  filters,
  setFilters,
}) => {
  const classes = useDataTableStyles();
  const tableStyles = useTableStyles();
  const sortableTableStyles = useSortableTableStyles();
  const [t] = useTranslation('log');
  const [snack, setSnack] = useState<string | null>(null);

  if (!data) {
    return <Loader />;
  }

  const columns = getColumns(t, classes);
  const rows = data.rows;

  return (
    <div className={classes.wrapper}>
      <div className={classes.sheet}>
        <Card
          sx={{
            display: 'block',
            width: '100%',
            margin: `20px 20px 0 20px`,
            overflow: 'auto',
          }}
        >
          <div
            style={{
              flex: '1 1 auto',
              minWidth: columns.reduce(
                (acc, { minWidth }) => acc + minWidth + 10,
                60
              ),
              height: `calc(${rows.length} * 42px + 50px)`,
            }}
          >
            <SortableTable
              styles={{ ...sortableTableStyles, ...tableStyles }}
              rows={rows}
              //@ts-ignore
              columns={columns}
              rowHeight={42}
              loading={false}
              noRowsMessage={t('table.noResults')}
              sortDirectionDefault="DESC"
              sortInternal={false}
              sorting="column"
            />
          </div>
        </Card>
      </div>
      <div className={classes.pagination}>
        <Pagination
          component={'div'}
          count={data.count}
          labelRowsPerPage={`${t('table.labelRowsPerPage')}:`}
          rowsPerPageOptions={[5, 10, 20, 50]}
          rowsPerPage={filters.rowsPerPage}
          changeRowsPerPage={(event: any) =>
            setFilters({
              ...filters,
              page: 0,
              rowsPerPage: event.target.value,
            })
          }
          page={filters.page}
          getNewPage={(page: number) => setFilters({ ...filters, page })}
          labelDisplayedRows={({ from, to, count }: any) =>
            `${from}-${to} ${t('common:of')} ${count}`
          }
        />
      </div>
      <Snackbar
        autoHideDuration={5000}
        handleClose={(event: React.ChangeEvent, reason: string) => {
          if (reason === 'clickaway') return;
          setSnack(null);
        }}
        message={snack}
        open={Boolean(snack)}
        classes={{
          root: classes.snack,
        }}
      />
    </div>
  );
};

export default DataTable;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useObjectTypeStepControlsStyles = makeStyles(
  ({ mintlab: { greyscale, radius } }: any) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      width: '100%',
      justifyContent: 'space-between',
    },

    nextStepWrapper: {
      alignSelf: 'flex-end',
    },

    previousStepWrapper: {
      alignSelf: 'flex-start',
    },
  })
);

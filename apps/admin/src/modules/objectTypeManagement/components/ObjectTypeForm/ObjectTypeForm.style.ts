// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useObjectTypeFormStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexDirection: 'column',
      flexWrap: 'nowrap',
      width: '100%',
      height: 'calc(100vh - 64px)',
    },
    innerWrapper: {
      width: '100%',
      maxWidth: 1024,
    },
    formWrapper: {
      display: 'flex',
      flex: 1,
      width: '100%',
      flexDirection: 'column',
      overflow: 'auto',
      alignItems: 'center',
    },
    buttonWrapper: {
      display: 'flex',
      width: '100%',
      justifyContent: 'center',
      backgroundColor: 'white',
      borderTop: `1px solid ${greyscale.dark}`,
      padding: '16px 0',
    },
  })
);

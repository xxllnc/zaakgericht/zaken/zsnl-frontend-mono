// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { useSubForm } from '@zaaksysteem/common/src/components/form/hooks/useSubForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import Button from '@mintlab/ui/App/Material/Button';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import {
  FormFieldPropsType,
  FormRendererFormField,
} from '@zaaksysteem/common/src/components/form/types/fieldComponent.types';
import { useAttributeFormStyle } from './AttributeForm.style';
import { AttributeItemType } from './types';
import { getAttributeFormDefinition } from './attribute.formDefinition';

export const AttributeForm: React.ComponentType<
  FormFieldPropsType<any, any, AttributeItemType>
> = props => {
  const {
    name,
    remove,
    value,
    formik: { setFieldValue },
  } = props;

  const classes = useAttributeFormStyle();
  const [t] = useTranslation('objectTypeManagement');
  const allFields = getAttributeFormDefinition(value, t);
  const requiredFields = allFields.filter(field => field.required);
  const [dialogOpened, setDialogOpened] = React.useState(false);

  const { fields } = useSubForm({
    namespace: name,
    formDefinition: requiredFields,
    ...props,
  });

  const renderFormField = (field: FormRendererFormField) => {
    const {
      FieldComponent,
      name,
      applyBackgroundColor,
      error,
      touched,
      required,
      help,
      hint,
    } = field;

    return (
      <FormControlWrapper
        label={undefined}
        help={help}
        hint={hint}
        error={error}
        touched={touched}
        required={required}
        applyBackgroundColor={applyBackgroundColor}
        key={`attibute-field-component-${name}`}
      >
        <FieldComponent {...field} />
      </FormControlWrapper>
    );
  };

  return (
    <div className={classes.mainContainer}>
      <div className={classes.headerContainer}>
        <h5 className={classes.attributeName}>{value.name}</h5>
        <div className={classes.actionsContainer}>
          <Button
            name="openDetailsDialog"
            onClick={() => setDialogOpened(true)}
          >
            Instellen
          </Button>
          <Button
            name="removeAttribute"
            icon="close"
            iconSize="small"
            action={remove}
          />
        </div>
      </div>
      <FormDialog
        formDefinition={allFields}
        onSubmit={(nestedValue: any) => {
          setFieldValue(name, { ...(value as any), ...nestedValue });
          setDialogOpened(false);
        }}
        title={value.name}
        scope="attribute-form-item-component"
        icon="extension"
        open={dialogOpened}
        onClose={() => setDialogOpened(false)}
      />
      {fields.map(field => renderFormField(field))}
    </div>
  );
};

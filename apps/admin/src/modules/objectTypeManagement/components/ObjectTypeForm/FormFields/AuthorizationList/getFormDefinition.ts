// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import {
  DEPARTMENT_FINDER,
  ROLE_FINDER,
  RADIO_GROUP,
} from '@zaaksysteem/common/src/components/form/constants/fieldTypes';

const getFormDefinition = ({ t }: { t: i18next.TFunction }) => {
  return [
    {
      name: 'department',
      type: DEPARTMENT_FINDER,
      value: null,
      required: true,
      nestedValue: true,
      placeholder: t('form.fields.authorization.selectDepartment'),
      label: t('form.fields.authorization.departmentLabel'),
    },
    {
      name: 'role',
      type: ROLE_FINDER,
      value: null,
      required: true,
      nestedValue: true,
      placeholder: t('form.fields.authorization.selectRole'),
      label: t('form.fields.authorization.roleLabel'),
    },
    {
      name: 'permission',
      type: RADIO_GROUP,
      label: t('form.fields.authorization.authorizationLabel'),
      value: '',
      required: true,
      choices: [
        {
          label: t('form.fields.authorization.permissions.read'),
          value: 'read',
        },
        {
          label: t('form.fields.authorization.permissions.readwrite'),
          value: 'readwrite',
        },
        {
          label: t('form.fields.authorization.permissions.admin'),
          value: 'admin',
        },
      ],
    },
  ];
};

export default getFormDefinition;

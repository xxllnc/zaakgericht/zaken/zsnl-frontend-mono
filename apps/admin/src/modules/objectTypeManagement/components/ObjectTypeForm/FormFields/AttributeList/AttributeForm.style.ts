// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useAttributeFormStyle = makeStyles(
  ({ typography, palette: { primary } }: Theme) => ({
    mainContainer: { padding: '0 5px' },
    headerContainer: {
      display: 'flex',
      justifyContent: 'space-between',
      paddingBottom: 15,
    },
    actionsContainer: {
      display: 'flex',
      '&>.MuiButton-text': {
        padding: '0 5px',
        borderRadius: 4,
        color: primary.main,
      },
      '&>.MuiIconButton-root': {
        padding: 0,
        marginLeft: 5,
        borderRadius: 4,
      },
    },
    attributeName: {
      ...typography.h5,
      margin: 0,
      display: 'flex',
      alignItems: 'center',
    },
  })
);

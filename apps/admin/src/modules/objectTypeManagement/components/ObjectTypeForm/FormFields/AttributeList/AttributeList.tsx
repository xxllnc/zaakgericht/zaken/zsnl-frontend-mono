// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Select, {
  defaultFilterOption,
} from '@mintlab/ui/App/Zaaksysteem/Select';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { SortableList, useListStyle } from '@mintlab/ui/App/Zaaksysteem/List';
import { useMultiValueField } from '@zaaksysteem/common/src/components/form/hooks/useMultiValueField';
import { FormRendererFormField } from '@zaaksysteem/common/src/components/form/types/fieldComponent.types';
import {
  extractSelectedValue,
  useAttributeChoicesQuery,
} from '../../ObjectTypeForm.library';
import { AttributeForm } from './AttributeForm';
import { AttributeItemType } from './types';

export const AttributeList: React.ComponentType<
  FormRendererFormField<any, any, AttributeItemType[]>
> = props => {
  const { setFieldValue, name, placeholder } = props;
  const classes = useListStyle();
  const { fields, add } = useMultiValueField<any, AttributeItemType>(props);
  const [selectProps, ServerErrorDialog] = useAttributeChoicesQuery();
  const value = fields.map(field => field.value);

  return (
    <div className={classes.listContainer}>
      <SortableList
        value={fields.map(field => ({ ...field, id: field.value.id }))}
        onReorder={reorderedFields =>
          setFieldValue(
            name,
            reorderedFields.map(field => field.value)
          )
        }
        renderItem={field => (
          <AttributeForm {...field} name={field.name as any} />
        )}
      />
      <Select
        {...selectProps}
        variant="generic"
        name={name}
        onChange={event => add(extractSelectedValue(event))}
        startAdornment={<Icon size="small">{iconNames.search}</Icon>}
        isMulti={false}
        value={null}
        isClearable={false}
        placeholder={placeholder}
        filterOption={(option, input) => {
          return (
            defaultFilterOption(option, input || '') &&
            value.every(item => item.id !== option.value.id)
          );
        }}
      />
      {ServerErrorDialog}
    </div>
  );
};

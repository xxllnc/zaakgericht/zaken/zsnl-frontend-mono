// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Select, {
  defaultFilterOption,
} from '@mintlab/ui/App/Zaaksysteem/Select';
import Button from '@mintlab/ui/App/Material/Button';
import Icon from '@mintlab/ui/App/Material/Icon';
import { SortableList, useListStyle } from '@mintlab/ui/App/Zaaksysteem/List';
import { useMultiValueField } from '@zaaksysteem/common/src/components/form/hooks/useMultiValueField';
import { FormRendererFormField } from '@zaaksysteem/common/src/components/form/types';
import {
  extractSelectedValue,
  useCaseTypeRelationChoicesQuery,
} from '../../ObjectTypeForm.library';

type CaseType = {
  id: string;
  name: string;
};

export const CaseTypeRelationList: React.ComponentType<
  FormRendererFormField<any, any, CaseType[]>
> = props => {
  const { setFieldValue, name, placeholder } = props;
  const classes = useListStyle();
  const { fields, add } = useMultiValueField<any, CaseType>(props);
  const value = fields.map(field => field.value);
  const [selectProps, ServerErrorDialog] = useCaseTypeRelationChoicesQuery();

  return (
    <div className={classes.listContainer}>
      <SortableList
        value={fields.map(field => ({ ...field, id: field.value.id }))}
        onReorder={reorderedFields =>
          setFieldValue(
            name,
            reorderedFields.map(field => field.value)
          )
        }
        renderItem={field => (
          <div className={classes.itemContainerSimple}>
            {field?.value?.name}
            <Button
              name="removeRelation"
              iconSize="small"
              icon="close"
              action={field.remove}
            />
          </div>
        )}
      />
      <Select
        {...selectProps}
        variant="generic"
        name={name}
        onChange={event => add(extractSelectedValue(event))}
        startAdornment={<Icon size="small">{'search' as const}</Icon>}
        isMulti={false}
        value={null}
        isClearable={false}
        placeholder={placeholder}
        filterOption={(option, input) => {
          return (
            defaultFilterOption(option, input || '') &&
            value.every(item => item.id !== option.value.id)
          );
        }}
      />
      {ServerErrorDialog}
    </div>
  );
};

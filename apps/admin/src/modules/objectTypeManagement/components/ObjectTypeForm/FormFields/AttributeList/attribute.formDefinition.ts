// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import {
  TEXT,
  CHECKBOX,
} from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AttributeItemType } from './types';

const tNamespace = 'objectTypeManagement:form.fields.custom_fields.form.';

export const getAttributeFormDefinition = (
  attributeItem: AttributeItemType,
  t: i18next.TFunction
): FormDefinition<AttributeItemType> =>
  //@ts-ignore
  [
    {
      name: 'label',
      type: TEXT,
      value: attributeItem.label,
      required: true,
      label: t(tNamespace + 'label.label'),
      placeholder: t(tNamespace + 'label.placeholder'),
    },
    {
      name: 'is_required',
      type: CHECKBOX,
      value: attributeItem.is_required,
      label: t(tNamespace + 'is_required.label'),
      suppressLabel: true,
    },
    {
      name: 'is_hidden_field',
      type: CHECKBOX,
      value: attributeItem.is_hidden_field,
      required: false,
      label: t(tNamespace + 'is_hidden_field.label'),
      suppressLabel: true,
    },
    attributeItem.custom_field_type === 'relationship' ||
    attributeItem.custom_field_type === 'address_v2' ||
    attributeItem.custom_field_type === 'geojson'
      ? {
          name: 'use_on_map',
          type: CHECKBOX,
          value: Boolean(attributeItem.use_on_map),
          label: t(tNamespace + 'use_on_map.label'),
          suppressLabel: true,
        }
      : null,
    {
      name: 'description',
      type: TEXT,
      value: attributeItem.description,
      required: false,
      label: t(tNamespace + 'description.label'),
      placeholder: t(tNamespace + 'description.placeholder'),
      isMultiline: true,
    },
    {
      name: 'external_description',
      type: TEXT,
      value: attributeItem.external_description,
      required: false,
      label: t(tNamespace + 'external_description.label'),
      placeholder: t(tNamespace + 'external_description.placeholder'),
      isMultiline: true,
    },
  ].filter(Boolean);

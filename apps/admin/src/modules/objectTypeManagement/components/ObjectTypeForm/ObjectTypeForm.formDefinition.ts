// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import { FormDefinition } from '@zaaksysteem/common/src/components/form/types';
import {
  CHECKBOX_GROUP,
  TEXTAREA,
} from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  ATTRIBUTE_LIST,
  CASETYPE_RELATION_LIST,
  OBJECTTYPE_RELATION_LIST,
  TEXT,
} from '../../../../components/Form/Constants/fieldTypes';
import { ObjectTypeFormShapeType } from './ObjectTypeForm.types';

export const getObjectTypeFormDefinition = ({
  t,
}: {
  t: i18next.TFunction;
}): FormDefinition<ObjectTypeFormShapeType> => [
  {
    name: 'custom_fields',
    type: ATTRIBUTE_LIST,
    format: 'object',
    value: [],
    required: false,
    multiValue: true,
    placeholder: t(
      'objectTypeManagement:form.fields.custom_fields.placeholder'
    ),
  },
  {
    name: 'components_changed',
    type: CHECKBOX_GROUP,
    value: [],
    choices: [
      {
        label: t(
          'objectTypeManagement:form.fields.components_changed.choices.generic'
        ),
        value: 'attributes',
      },
      {
        label: t(
          'objectTypeManagement:form.fields.components_changed.choices.attributes'
        ),
        value: 'custom_fields',
      },
      {
        label: t(
          'objectTypeManagement:form.fields.components_changed.choices.relations'
        ),
        value: 'relationships',
      },
      {
        label: t(
          'objectTypeManagement:form.fields.components_changed.choices.authorizations'
        ),
        value: 'authorizations',
      },
    ],
    required: true,
    label: t('objectTypeManagement:form.fields.components_changed.label'),
  },
  {
    name: 'changes',
    type: TEXTAREA,
    isMultiline: true,
    value: '',
    required: true,
    label: t('objectTypeManagement:form.fields.changes.label'),
    help: t('objectTypeManagement:form.fields.changes.help'),
    placeholder: t('objectTypeManagement:form.fields.changes.placeholder'),
  },

  {
    name: 'name',
    type: TEXT,
    value: '',
    required: true,
    label: t('objectTypeManagement:form.fields.name.label'),
    help: t('objectTypeManagement:form.fields.name.help'),
    placeholder: t('objectTypeManagement:form.fields.name.placeholder'),
  },
  {
    name: 'title',
    type: TEXT,
    value: '',
    required: true,
    label: t('objectTypeManagement:form.fields.title.label'),
    help: t('objectTypeManagement:form.fields.title.help'),
    placeholder: t('objectTypeManagement:form.fields.title.placeholder'),
  },
  {
    name: 'subtitle',
    type: TEXT,
    value: '',
    required: false,
    label: t('objectTypeManagement:form.fields.subtitle.label'),
    help: t('objectTypeManagement:form.fields.subtitle.help'),
    placeholder: t('objectTypeManagement:form.fields.subtitle.placeholder'),
  },
  {
    name: 'external_reference',
    type: TEXT,
    value: '',
    label: t('objectTypeManagement:form.fields.external_reference.label'),
    help: t('objectTypeManagement:form.fields.external_reference.help'),
    placeholder: t(
      'objectTypeManagement:form.fields.external_reference.placeholder'
    ),
  },

  {
    name: 'caseTypeRelations',
    type: CASETYPE_RELATION_LIST,
    format: 'object',
    value: [],
    multiValue: true,
    label: t('objectTypeManagement:form.fields.caseTypeRelations.label'),
    help: t('objectTypeManagement:form.fields.caseTypeRelations.help'),
    placeholder: t(
      'objectTypeManagement:form.fields.custom_fields.placeholder'
    ),
  },
  {
    name: 'objectTypeRelations',
    type: OBJECTTYPE_RELATION_LIST,
    format: 'object',
    value: [],
    multiValue: true,
    label: t('objectTypeManagement:form.fields.objectTypeRelations.label'),
    help: t('objectTypeManagement:form.fields.objectTypeRelations.help'),
    placeholder: t(
      'objectTypeManagement:form.fields.custom_fields.placeholder'
    ),
  },
  {
    format: 'object',
    name: 'authorizations',
    type: 'AuthorizationList',
    value: [],
    required: true,
  },
];

export const getObjectTypeFormSteps = (t: i18next.TFunction) => [
  {
    title: t('objectTypeManagement:form.steps.general.title'),
    description: t('objectTypeManagement:form.steps.general.description'),
    fieldNames: ['name', 'title', 'subtitle', 'external_reference'],
  },
  {
    title: t('objectTypeManagement:form.steps.attributes.title'),
    fieldNames: ['custom_fields'],
  },
  {
    title: t('objectTypeManagement:form.steps.relations.title'),
    fieldNames: ['caseTypeRelations', 'objectTypeRelations'],
  },
  {
    title: t('objectTypeManagement:form.steps.rights.title'),
    fieldNames: ['authorizations'],
  },
  {
    title: t('objectTypeManagement:form.steps.audit.title'),
    fieldNames: ['components_changed', 'changes'],
  },
];

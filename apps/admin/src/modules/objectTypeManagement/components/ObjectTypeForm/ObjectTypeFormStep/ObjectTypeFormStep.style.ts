// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useObjectTypeFormStepStyles = makeStyles(
  ({ mintlab: { greyscale } }: any) => ({
    stepIntro: {
      margin: '64px 0',
    },
    stepIntroSubTitle: {
      paddingTop: 16,
      maxWidth: 768,
    },
    fieldWrapper: {
      padding: '24px 0',
      borderTop: `1px solid ${greyscale.dark}`,

      '&:first-child': {
        border: 0,
        paddingTop: 0,
      },
    },
  })
);

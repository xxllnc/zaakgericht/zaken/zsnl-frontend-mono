// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { FormStepType } from '@zaaksysteem/common/src/components/form/hooks/useSteppedForm';
import { Stepper, StepType } from '@mintlab/ui/App/Material/Stepper/Stepper';

export type ObjectTypeProgressBarPropsType = {
  steps: FormStepType[];
};

export const ObjectTypeProgressBar: React.ComponentType<
  ObjectTypeProgressBarPropsType
> = ({ steps }) => {
  const stepperSteps: StepType[] = steps.map<StepType>(step => ({
    name: step.title,
    active: step.isActive,
    completed: step.isValid,
    disabled: !step.isTouched,
    error: step.isTouched && !step.isValid,
  }));

  return (
    <div>
      <Stepper steps={stepperSteps} />
    </div>
  );
};

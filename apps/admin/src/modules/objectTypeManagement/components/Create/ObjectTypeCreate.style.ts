// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useObjectTypeCreateStyle = makeStyles(() => ({
  wrapper: {
    height: '100%',
    overflow: 'hidden',
  },
}));

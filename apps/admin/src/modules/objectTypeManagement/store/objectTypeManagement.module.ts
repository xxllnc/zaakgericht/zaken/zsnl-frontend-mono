// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { objectTypeManagement } from './objectTypeManagement.reducer';
import { objectTypeManagementMiddleware } from './objectTypeManagement.middleware';

export function getObjectTypeManagementModule() {
  return {
    id: 'objectType',
    reducerMap: {
      objectTypeManagement,
    },
    middlewares: [objectTypeManagementMiddleware],
  };
}

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import Transactions from './Transactions/Transactions';
import Transaction from './Transaction/Transaction';
import locale from './Transactions.locale';

type TransactionsModuleType = {};

const TransactionsModule: React.ComponentType<TransactionsModuleType> = () => {
  return (
    <I18nResourceBundle resource={locale} namespace="transactions">
      <Router>
        <Routes>
          <Route path={'admin/transacties'}>
            <Route path={''} element={<Transactions />} />
            <Route path={':transactionUuid'} element={<Transaction />} />
          </Route>
        </Routes>
      </Router>
    </I18nResourceBundle>
  );
};

export default TransactionsModule;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export default {
  nl: {
    transactions: 'Transacties',
    integration: {
      placeholder: 'Selecteer een koppelprofiel…',
      all: 'Alle koppelprofielen',
    },
    withErrors: 'Alleen met fout',
    keyword: 'Typ om te zoeken…',
    table: {
      noResults: 'Geen resultaten',
      labelRowsPerPage: 'Per pagina',
      unknown: 'Onbekend',
      columns: {
        status: 'Status',
        record: 'Naam (Voorbeeld)',
        direction: 'Richting',
        externalId: 'Extern ID',
        nextAttempt: 'Volgende poging',
        created: 'Aangemaakt',
        records: 'Records',
        errors: 'Fouten',
      },
      values: {
        direction: {
          incoming: 'Inkomend',
          outgoing: 'Uitgaand',
        },
      },
    },
    manual: {
      button: 'Handmatige transactie',
      dialog: {
        title: 'Handmatige transactie',
        fields: {
          integration: 'Koppelprofiel',
          inputData: 'Bericht (tekst)',
        },
      },
    },
  },
};

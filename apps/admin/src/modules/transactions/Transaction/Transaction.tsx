// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { useQuery } from '@tanstack/react-query';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import Typography from '@mui/material/Typography';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import SubAppHeader from '../../../components/Header/SubAppHeader';
import { useTransactionsStyles } from './Transaction.style';
import { RecordFiltersType } from './../Transactions.types';
import { getTransaction, getRecords } from './Transaction.library';
import Form from './Form/Form';
import Records from './Records/Records';

type TransactionParamsType = {
  transactionUuid: string;
};

const Transaction: React.ComponentType = () => {
  const classes = useTransactionsStyles();
  const [t] = useTranslation('transactions');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const { transactionUuid } = useParams<
    keyof TransactionParamsType
  >() as TransactionParamsType;

  const [filters, setFilters] = useState<RecordFiltersType>({
    page: 0,
    numRows: 20,
  });

  const { data: transaction } = useQuery(
    ['transaction', transactionUuid],
    () => getTransaction(transactionUuid),
    {
      onError: (error: V2ServerErrorsType) => openServerErrorDialog(error),
    }
  );

  const { data: recordsData } = useQuery(
    ['records', transactionUuid, filters],
    () => getRecords(transactionUuid, filters),
    {
      onError: (error: V2ServerErrorsType) => openServerErrorDialog(error),
    }
  );

  if (!transaction || !recordsData) {
    return <Loader />;
  }

  return (
    <div className={classes.wrapper}>
      <SubAppHeader>
        <Link key={'case'} to={'..'}>
          <Typography variant="h3">{t('transactions')}</Typography>
        </Link>
        <Icon size="small">{iconNames.navigate_next}</Icon>
        <Typography variant="h3">{transaction.interface}</Typography>
      </SubAppHeader>
      <div className={classes.content}>
        <Form transaction={transaction} />
        <Records
          recordsData={recordsData}
          filters={filters}
          setFilters={setFilters}
        />
        {ServerErrorDialog}
      </div>
    </div>
  );
};

export default Transaction;

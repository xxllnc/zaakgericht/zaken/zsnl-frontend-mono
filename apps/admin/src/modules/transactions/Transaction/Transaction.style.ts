// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useTransactionsStyles = makeStyles(({ typography }: Theme) => ({
  wrapper: {
    fontFamily: typography.fontFamily,
    height: 'calc(100% - 45px)',
    display: 'flex',
    flexDirection: 'column',
  },
  content: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    gap: 20,
  },
}));

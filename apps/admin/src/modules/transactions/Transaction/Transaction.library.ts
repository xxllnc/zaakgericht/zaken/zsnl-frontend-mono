// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { GetRecordsType, GetTransactionType } from '../Transactions.types';
import { formatTransaction } from '../Transactions/Transactions.library';
import { fetchTransaction, fetchRecords } from './Transaction.requests';

export const getTransaction: GetTransactionType = async transactionUuid => {
  const result = await fetchTransaction(transactionUuid);

  return formatTransaction(result);
};

const formatRecords = ({
  id,
  preview_string,
  is_error,
  date_executed,
  input,
  output,
  transaction_id,
}: any) => ({
  uuid: id,
  name: id,
  preview: preview_string,
  result: Boolean(is_error),
  created: date_executed,
  inputData: input,
  transactionId: transaction_id,
  input,
  output,
});

export const getRecords: GetRecordsType = async (transactionUuid, filters) => {
  const params = {
    zapi_num_rows: filters.numRows,
    zapi_page: filters.page,
  };

  const response = await fetchRecords(transactionUuid, params);

  return {
    count: response.num_rows,
    rows: response.result.map(formatRecords),
  };
};

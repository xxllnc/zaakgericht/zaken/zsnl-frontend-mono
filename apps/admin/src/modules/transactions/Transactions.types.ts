// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

// INTEGRATIONS

export type IntegrationTypeType = {
  name: string;
  manualType: string;
};

export type GetIntegrationTypesType = () => Promise<IntegrationTypeType[]>;
export type FetchIntegrationTypesType = () => Promise<any>;

export type IntegrationType = {
  value: string;
  manualType: string;
  label: string;
};

export type GetIntegrationType = (
  integrationTypes: IntegrationTypeType[] | undefined,
  defaultIntegration: IntegrationType
) => Promise<IntegrationType[]>;
export type FetchIntegrationsType = () => Promise<any>;

// TRANSACTIONS

export type TransactionType = {
  uuid: string;
  status: boolean;
  name: string;
  interface: string;
  record: {
    uuid: string;
    preview: string;
  };
  direction: string;
  externalId: string;
  nextAttempt: string;
  created: string;
  records: number;
  errors: number;
  inputData: string;
};

export type FetchTransactionsType = (params: {
  freeform_filter: string | undefined;
  interface_id: number | undefined;
  'records.is_error': number | string;
  zapi_num_rows: number;
  zapi_page: number;
  zapi_order_by?: string;
  zapi_order_by_direction?: string;
}) => Promise<any>;

export type TransactionsDataType = {
  count: number;
  rows: TransactionType[];
};

export type GetTransactionsType = (
  values: any
) => Promise<TransactionsDataType>;

export type FetchTransactionType = (transactionUuid: string) => Promise<any>;

export type GetTransactionType = (
  transactionUuid: string
) => Promise<TransactionType>;

export type RefreshTransactionsType = () => void;

// RECORDS

export type RecordType = {
  uuid: string;
  name: string;
  preview: string;
  result: boolean;
  executed: string;
  transactionId: string;
  created: string;
  input: string;
  output: string;
};

export type FetchRecordsType = (
  transActionUuid: string,
  params: {
    zapi_num_rows: number;
    zapi_page: number;
  }
) => Promise<any>;

export type RecordFiltersType = {
  numRows: number;
  page: number;
};

export type RecordsDataType = {
  count: number;
  rows: RecordType[];
};

export type SetRecordsFiltersType = (filters: RecordFiltersType) => void;

export type GetRecordsType = (
  transactionUuid: string,
  filters: RecordFiltersType
) => Promise<RecordsDataType>;

export type SetRecordType = (record: RecordType) => void;

// OTHER

export type FiltersType = {
  integration: IntegrationType;
  withError: boolean;
  keyword: string;
  page: number;
  numRows: number;
  sortBy?: string;
  sortDirection?: string;
};

export type SetFiltersType = React.Dispatch<React.SetStateAction<FiltersType>>;

export type SelectedRowsType = string[];
export type SetSelectedType = React.Dispatch<
  React.SetStateAction<SelectedRowsType>
>;

export type EverythingSelectedType = boolean;
export type SetAllSelectedType = React.Dispatch<
  React.SetStateAction<EverythingSelectedType>
>;

// ACTION

export type ActionType = 'retry' | 'delete';

export type PerformActionActionType = (
  action: ActionType,
  filters: FiltersType,
  selected: SelectedRowsType,
  allSelected: EverythingSelectedType
) => Promise<boolean>;

export type PerformActionType = (
  action: ActionType,
  data: {
    freeform_filter: string;
    interface_id?: number;
    'records.is_error': number | string;
    selection_id: number[];
    selection_type: string;
  }
) => Promise<any>;

// MANUAL TRANSACTION

export type PerformManualTransactionType = (values: any) => Promise<boolean>;

export type PostManualTransactionType = (data: {
  interface: number;
  input_data?: string;
  input_filestore_uuid?: string;
  input_references?: string;
}) => Promise<any>;

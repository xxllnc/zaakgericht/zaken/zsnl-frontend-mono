// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  GetIntegrationType,
  GetIntegrationTypesType,
  TransactionType,
  GetTransactionsType,
  PerformActionActionType,
  PerformManualTransactionType,
} from '../Transactions.types';
import {
  fetchIntegrations,
  fetchIntegrationTypes,
  fetchTransactions,
  performAction,
  postManualTransaction,
} from './Transactions.requests';

export const getIntegrations: GetIntegrationType = async (
  integrationTypes,
  defaultIntegration
) => {
  if (!integrationTypes) return [];

  const response = await fetchIntegrations();

  const integrations = response.map(({ id, name }: any) => {
    const integrationType = integrationTypes.find(type => type.name === name);

    return {
      value: id.toString(),
      manualType: integrationType?.manualType || 'unsupported',
      label: `${name} (${id})`,
    };
  });

  return [defaultIntegration, ...integrations];
};

export const getIntegrationTypes: GetIntegrationTypesType = async () => {
  const integrationTypes = await fetchIntegrationTypes();

  return integrationTypes.map(({ label, manual_type }: any) => ({
    name: label,
    manualType: manual_type[0],
  }));
};

export const sortSupportedColumnDict = {
  direction: 'me.direction',
  externalId: 'me.external_transaction_id',
  created: 'me.date_created',
};

export const formatTransaction = ({
  id,
  state,
  interface_id,
  result_preview,
  direction,
  external_transaction_id,
  date_next_retry,
  date_created,
  total_count,
  error_count,
  input_data,
}: any): TransactionType => ({
  uuid: id,
  name: id,
  status: state === 'success',
  interface: interface_id.name,
  record: {
    uuid: result_preview[0]?.id,
    preview: result_preview[0]?.preview_string?.trim() || '',
  },
  direction,
  externalId: external_transaction_id,
  nextAttempt: date_next_retry,
  created: date_created,
  records: total_count,
  errors: error_count,
  inputData: input_data,
});

export const getTransactions: GetTransactionsType = async ({
  keyword,
  integration,
  withError,
  numRows,
  page,
  sortBy,
  sortDirection,
}) => {
  const params = {
    freeform_filter: keyword || undefined,
    interface_id: integration.value === '0' ? undefined : integration.value,
    'records.is_error': withError ? 1 : '',
    zapi_num_rows: numRows,
    zapi_page: page + 1,
    // @ts-ignore
    zapi_order_by: sortBy ? sortSupportedColumnDict[sortBy] : undefined,
    zapi_order_by_direction: sortDirection?.toLowerCase(),
  };

  const response = await fetchTransactions(params);

  return {
    count: response.num_rows,
    rows: response.result.map(formatTransaction),
  };
};

export const performActionAction: PerformActionActionType = async (
  action,
  filters,
  selected,
  everythingSelected
) => {
  const data = {
    freeform_filter: filters.keyword,
    interface_id: Number(filters.integration.value) || undefined,
    'records.is_error': filters.withError ? 1 : '',
    selection_id: selected.map(Number),
    selection_type: everythingSelected ? 'all' : 'subset',
  };

  return await performAction(action, data);
};

export const performManualTransaction: PerformManualTransactionType = async (
  values: any
) => {
  const {
    integration: { value, manualType },
    text,
    file: files,
    references,
  } = values;

  let data = {
    interface: Number(value),
    ...(manualType === 'text' ? { input_data: text } : {}),
    ...(manualType === 'file'
      ? {
          input_filestore_uuid: files.map(
            (file: { value: string }) => file.value
          ),
        }
      : {}),
    ...(manualType === 'references'
      ? {
          input_references: references.map((reference: { value: string }) => ({
            reference: reference.value,
          })),
        }
      : {}),
  };

  return await postManualTransaction(data);
};

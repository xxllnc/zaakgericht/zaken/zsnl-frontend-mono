// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useActionBarStyles = makeStyles(
  ({ mintlab: { greyscale, radius }, typography }: Theme) => ({
    wrapper: {
      display: 'flex',
      flexWrap: 'wrap',
      alignItems: 'center',
      gap: 20,
      marginBottom: 20,
    },
    integrationTypeWrapper: {
      width: 400,
      borderRadius: radius.textField,
      backgroundColor: greyscale.light,
    },
    snack: {
      marginBottom: 35,
    },
  })
);

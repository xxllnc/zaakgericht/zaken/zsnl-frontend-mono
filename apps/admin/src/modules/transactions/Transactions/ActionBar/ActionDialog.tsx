// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelectionBehaviourReturnType } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
import {
  Dialog,
  DialogTitle,
  //@ts-ignore
} from '@mintlab/ui/App/Material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import Divider from '@mui/material/Divider';
import DialogContent from '@mui/material/DialogContent';
//@ts-ignore
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import {
  FiltersType,
  ActionType,
  SelectedRowsType,
  RefreshTransactionsType,
  EverythingSelectedType,
} from '../../Transactions.types';
import { performActionAction } from '../Transactions.library';

type FiltersDialogPropsType = {
  filters: FiltersType;
  selectedRows: SelectedRowsType;
  everythingSelected: EverythingSelectedType;
  refreshTransactions: RefreshTransactionsType;
  resetAll: useSelectionBehaviourReturnType['resetAll'];
  setSnack: (text: string) => void;
  onClose: () => void;
  open: boolean;
};

const ActionDialog: React.ComponentType<FiltersDialogPropsType> = ({
  filters,
  selectedRows,
  everythingSelected,
  refreshTransactions,
  resetAll,
  setSnack,
  onClose,
  open,
}) => {
  const [t] = useTranslation('transactions');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const dialogEl = useRef();

  const title = t('action.dialog.title');

  const performAction = (action: ActionType) => {
    performActionAction(action, filters, selectedRows, everythingSelected)
      .then(() => {
        setSnack(t(`action.snack.${action}`));
        refreshTransactions();
        resetAll();
        onClose();
      })
      .catch(openServerErrorDialog);
  };

  return (
    <>
      <Dialog
        disableBackdropClick={true}
        open={open}
        onClose={onClose}
        scope={'transactions-action-dialog'}
        ref={dialogEl}
        //@ts-ignore
        fullWidth={true}
      >
        <DialogTitle
          elevated={true}
          icon="delete"
          title={title}
          onCloseClick={onClose}
        />
        <DialogContent>
          {!everythingSelected && (
            <p>
              {t('action.dialog.warning.selected', {
                count: selectedRows.length,
              })}
            </p>
          )}
          {everythingSelected && (
            <>
              <p>{`${t('action.dialog.warning.all')}:`}</p>
              <ul>
                {filters.integration.value && (
                  <li>{`${t('action.dialog.integration')}: ${
                    filters.integration.label
                  }`}</li>
                )}
                {filters.withError && <li>{t('action.dialog.withError')}</li>}
                {filters.keyword && (
                  <li>{`${t('action.dialog.withKeyword')}: ${
                    filters.keyword
                  }`}</li>
                )}
              </ul>
            </>
          )}
          <p>{t('action.dialog.check')}</p>
        </DialogContent>
        <>
          <Divider />
          <DialogActions>
            {createDialogActions(
              [
                {
                  text: t('action.delete'),
                  onClick() {
                    performAction('delete');
                  },
                },
                {
                  text: t('action.retry'),
                  onClick() {
                    performAction('retry');
                  },
                },
                {
                  text: t('common:dialog.cancel'),
                  onClick: onClose,
                },
              ],
              'datastore-bulk-dialog'
            )}
          </DialogActions>
        </>
      </Dialog>
      {ServerErrorDialog}
    </>
  );
};

export default ActionDialog;

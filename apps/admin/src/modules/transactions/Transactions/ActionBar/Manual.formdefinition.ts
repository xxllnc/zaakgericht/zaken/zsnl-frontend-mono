// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import {
  Rule,
  hideFields,
  showFields,
} from '@zaaksysteem/common/src/components/form/rules';
import { IntegrationType } from '../../Transactions.types';

type GetFormDefinitionType = (
  t: i18next.TFunction,
  integrations: IntegrationType[]
) => AnyFormDefinitionField[];

export const getManualDialogFormDefinition: GetFormDefinitionType = (
  t,
  integrations
) => [
  {
    name: 'integration',
    value: null,
    type: fieldTypes.SELECT,
    required: true,
    label: t('manual.dialog.fields.integration'),
    placeholder: t('integration.placeholder'),
    choices: integrations.slice(1),
  },
  {
    name: 'unsupported',
    value: t('manual.dialog.fields.unsupported'),
    type: fieldTypes.TEXTAREA,
    readOnly: true,
  },
  {
    name: 'text',
    value: '',
    type: fieldTypes.TEXTAREA,
    label: t('manual.dialog.fields.text'),
  },
  {
    name: 'file',
    value: null,
    type: fieldTypes.UPLOAD,
    label: t('manual.dialog.fields.file'),
  },
  {
    name: 'references',
    value: null,
    type: fieldTypes.MULTI_VALUE_TEXT,
    label: t('manual.dialog.fields.references'),
  },
];

export const getManualDialogRules = () =>
  ['unsupported', 'text', 'file', 'references'].map(fieldName =>
    new Rule()
      .when('integration', field => {
        // @ts-ignore
        return field?.value?.manualType === fieldName;
      })
      .then(showFields([fieldName]))
      .else(hideFields([fieldName]))
  );

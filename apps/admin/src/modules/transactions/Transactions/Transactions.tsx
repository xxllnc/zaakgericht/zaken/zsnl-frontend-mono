// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import { useSelectionBehaviour } from '@zaaksysteem/common/src/hooks/useSelectionBehaviour';
import Typography from '@mui/material/Typography';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import SubAppHeader from '../../../components/Header/SubAppHeader';
import { FiltersType } from '../Transactions.types';
import { useTransactionsStyles } from './Transactions.style';
import {
  getIntegrations,
  getIntegrationTypes,
  getTransactions,
} from './Transactions.library';
import ActionBar from './ActionBar/ActionBar';
import DataTable from './DataTable/DataTable';

const useDebounce = (value: FiltersType, delay: number) => {
  const [debouncedValue, setDebouncedValue] = React.useState(value);

  useEffect(() => {
    const handler: NodeJS.Timeout = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    return () => {
      clearTimeout(handler);
    };
  }, [value, delay]);

  return debouncedValue;
};

const Transactions = () => {
  const classes = useTransactionsStyles();
  const [t] = useTranslation('transactions');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const defaultIntegration = {
    value: '0',
    manualType: 'all',
    label: t('integration.all'),
  };
  const [filters, setFilters] = useState<FiltersType>({
    integration: defaultIntegration,
    withError: false,
    keyword: '',
    page: 0,
    numRows: 20,
  });

  const debouncedFilters = useDebounce(filters, 300);
  const { data, isLoading: loading } = useQuery(
    ['transactions', debouncedFilters],
    () => getTransactions(debouncedFilters),
    {
      onSuccess: () => selectionProps.resetAll(),
      onError: (error: V2ServerErrorsType) => openServerErrorDialog(error),
    }
  );

  const { data: integrationTypes } = useQuery(
    ['integrationTypes'],
    getIntegrationTypes,
    {
      onError: (error: V2ServerErrorsType) => openServerErrorDialog(error),
    }
  );

  const { data: integrations } = useQuery(
    ['integrations', integrationTypes],
    () => getIntegrations(integrationTypes, defaultIntegration),
    {
      enabled: Boolean(integrationTypes),
      onError: (error: V2ServerErrorsType) => openServerErrorDialog(error),
    }
  );

  const queryClient = useQueryClient();
  const selectionProps = useSelectionBehaviour({
    rows: data?.rows,
    selectEverythingTranslations: t('table.selectEverything', {
      returnObjects: true,
    }),
    page: filters.page,
    resultsPerPage: filters.numRows,
  });

  const { selectedRows, everythingSelected, resetAll } = selectionProps;

  const refreshTransactions = () =>
    queryClient.invalidateQueries(['transactions']);

  if (!integrationTypes || !integrations) {
    return <Loader />;
  }

  return (
    <div className={classes.wrapper}>
      <SubAppHeader>
        <Typography variant="h3">{t('transactions')}</Typography>
      </SubAppHeader>
      <ActionBar
        integrations={integrations}
        filters={filters}
        setFilters={setFilters}
        selectedRows={selectedRows}
        everythingSelected={everythingSelected}
        refreshTransactions={refreshTransactions}
        resetAll={resetAll}
      />
      <DataTable
        loading={loading}
        data={data}
        filters={filters}
        setFilters={setFilters}
        selectedRows={selectedRows}
        selectionProps={selectionProps}
      />
      {ServerErrorDialog}
    </div>
  );
};

export default Transactions;

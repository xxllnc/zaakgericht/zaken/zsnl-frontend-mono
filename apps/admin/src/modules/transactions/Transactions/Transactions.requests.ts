// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import {
  FetchIntegrationsType,
  FetchIntegrationTypesType,
  FetchTransactionsType,
  PerformActionType,
  PostManualTransactionType,
} from '../Transactions.types';

export const fetchIntegrations: FetchIntegrationsType = async () => {
  const url = buildUrl('/sysin/interface', { zapi_no_pager: 1 });

  const response = await request('GET', url);

  return response.result;
};

export const fetchIntegrationTypes: FetchIntegrationTypesType = async () => {
  const url = buildUrl('/sysin/interface/modules/all', { zapi_no_pager: 1 });

  const response = await request('GET', url);

  return response.result;
};

export const fetchTransactions: FetchTransactionsType = async params => {
  const url = buildUrl('/sysin/transaction', params);

  const response = await request('GET', url);

  return response;
};

export const performAction: PerformActionType = async (action, data) => {
  const url = `/sysin/transaction/${action}`;

  const response = await request('POST', url, data);

  return response;
};

export const postManualTransaction: PostManualTransactionType = async data => {
  const url = `/sysin/interface/${data.interface}/manual_process`;

  const response = await request('POST', url, data);

  return response;
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import { Link } from 'react-router-dom';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { TransactionType } from '../../Transactions.types';
import { formatDate } from '../../Transactions.library';

type CellRendererType = (
  rowData: {
    dataKey: any;
    rowData: TransactionType;
  },
  t: i18next.TFunction,
  classes: any
) => React.ReactElement;

/* eslint complexity: [2, 13] */
export const cellRenderer: CellRendererType = (
  { dataKey, rowData },
  t,
  classes
) => {
  switch (dataKey) {
    case 'status': {
      return rowData.status ? (
        <Icon size="small" color="secondary">
          {iconNames.done}
        </Icon>
      ) : (
        <Icon size="small" color="error">
          {iconNames.close}
        </Icon>
      );
    }
    case 'record': {
      return (
        <div className={classes.record}>
          <Link key={'transaction'} to={rowData.uuid.toString()}>
            {rowData.interface}
          </Link>

          {rowData.record.preview && (
            <span className={classes.recordPreview}>
              {rowData.record.preview}
            </span>
          )}
        </div>
      );
    }
    case 'direction': {
      return (
        <span>
          {t(`table.values.direction.${rowData.direction}`) as string}
        </span>
      );
    }
    case 'externalId': {
      const value =
        rowData.externalId === 'unknown'
          ? (t('table.unknown') as string)
          : rowData.externalId;

      if (!value) {
        return <span>-</span>;
      } else if (value.length > 30) {
        return (
          <Tooltip title={value}>
            <span>{value}</span>
          </Tooltip>
        );
      } else {
        return <span>{value}</span>;
      }
    }
    case 'nextAttempt': {
      return <span>{formatDate(rowData.nextAttempt)}</span>;
    }
    case 'created': {
      return <span>{formatDate(rowData.created)}</span>;
    }
    default: {
      // @ts-ignore
      return <div className={classes.tableCell}>{rowData[dataKey] || '-'}</div>;
    }
  }
};

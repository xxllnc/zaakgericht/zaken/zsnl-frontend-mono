// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

// @ts-ignore
import transactionsTable from './transactionsTable.svg';

const tablePadding = '24px';
const tableMargin = '20px';
const cardPadding = '8px';
const tableSideSpace = `${tableMargin} + ${cardPadding}`;
const paginatorSideSpace = `${tableSideSpace} + ${tablePadding}`;

export const useDataTableStyles = makeStyles(
  ({ mintlab: { greyscale, radius }, palette: { elephant } }: Theme) => ({
    wrapper: {
      flexGrow: 1,
      overflow: 'auto',
    },
    sheet: {
      width: '100%',
      height: '100%',
      'background-image': `url(${transactionsTable})`,
      'background-size': '18px',
      display: 'flex',
      position: 'relative',
      backgroundColor: greyscale.light,
      borderRadius: `${radius.sheet}px ${radius.sheet}px 0 0`,
    },
    loader: {
      position: 'absolute',
      bottom: 1,
      width: '100%',
    },
    pagination: {
      width: `calc(100% - (${paginatorSideSpace}))`,
      position: 'fixed',
      bottom: '0px',
    },
    snack: {
      marginBottom: 35,
    },
    recordPreview: {
      marginLeft: 5,
      textDecoration: 'none',
      color: elephant.main,
    },
  })
);

export const useTableStyles = makeStyles(
  ({ mintlab: { greyscale }, palette: { primary } }: Theme) => ({
    tableRow: {
      borderBottom: `1px solid ${greyscale.dark}`,
      alignItems: 'center',
      '&:hover': {
        backgroundColor: primary.lightest,
      },
    },
  })
);

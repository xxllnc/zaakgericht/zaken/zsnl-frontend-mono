// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useTransactionsStyles = makeStyles(() => ({
  wrapper: {
    height: 'calc(100% - 55px)',
    display: 'flex',
    flexDirection: 'column',
  },
}));

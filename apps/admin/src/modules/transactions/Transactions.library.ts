// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import fecha from 'fecha';

export const formatDate = (date: string) =>
  date ? fecha.format(new Date(date), 'DD-MM-YYYY HH:mm:ss') : '-';

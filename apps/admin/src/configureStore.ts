// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createStore } from 'redux-dynamic-modules-core';
import { getThunkExtension } from 'redux-dynamic-modules-thunk';
import getRouterModule from './store/route/route.module';
import getSessionModule from './store/session/session.module';
import getUIModule from './store/ui/ui.module';
import getAppModule from './store/app/app.module';

export const configureStore = (initialState: any) => {
  const store = createStore(
    { initialState, extensions: [getThunkExtension()] },
    getAppModule(),
    getRouterModule(),
    getSessionModule(),
    getUIModule()
  );

  return store;
};

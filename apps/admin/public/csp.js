// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

let nonce;

if (window.trustedTypes && window.trustedTypes.createPolicy) {
  window.trustedTypes.createPolicy('default', {
    createHTML: function (string) {
      // eslint-disable-next-line no-undef
      return DOMPurify.sanitize(string, { RETURN_TRUSTED_TYPE: true });
    },
    createScriptURL: function (string) {
      return string;
    },
    createScript: function (string) {
      return string;
    },
  });
}

document.createElement = (function (create) {
  return function () {
    var ret = create.apply(this, arguments);
    if (ret.tagName.toLowerCase() === 'style') {
      nonce =
        nonce ||
        document
          .querySelector('[property="csp-nonce"]')
          .getAttribute('content');
      ret.setAttribute('nonce', nonce);
    }
    return ret;
  };
})(document.createElement);

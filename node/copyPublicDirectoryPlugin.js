// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { fileURLToPath } from 'url';
import * as path from 'path';
import * as fs from 'fs-extra';

const thisFileDirectory = path.dirname(fileURLToPath(import.meta.url));

export default appName => {
  const p1 = path.resolve(thisFileDirectory, '..', 'apps', appName, 'public');
  const p2 = path.resolve(thisFileDirectory, '..', 'build', 'apps', appName);
  const p3 = path.resolve(
    thisFileDirectory,
    '..',
    'node_modules',
    'dompurify',
    'dist',
    'purify.min.js'
  );
  const p4 = path.resolve(
    thisFileDirectory,
    '..',
    'build',
    'apps',
    appName,
    'purify.js'
  );

  return {
    apply(compiler) {
      compiler.hooks.done.tap('PublicDirectoryPlugin', () => {
        fs.copySync(p1, p2, {
          recursive: true,
          filter: path => !path.includes('index.html'),
        });
        fs.copySync(p3, p4);
      });
    },
  };
};

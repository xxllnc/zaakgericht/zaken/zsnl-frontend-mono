// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/* eslint-disable no-console */

import { promisify } from 'util';
import { exec } from 'child_process';
import { readdir } from 'fs';
import glob from 'glob';

const asyncGlob = promisify(glob);
const asyncExec = promisify(exec);
const asyncReaddir = promisify(readdir);

async function countLinesInFiles(files) {
  const contents = await Promise.all(
    files.map(filePath => asyncExec(`cat ${filePath} | wc -l`))
  );
  return contents
    .map(({ stdout }) => parseInt(stdout, 10))
    .reduce((acc, num) => acc + num, 0);
}

async function printStats(basePath) {
  const jsFiles = await asyncGlob(
    `${basePath}/{src,source,App,types}/**/!(*.test|*.story).js`
  );
  const tsFiles = await asyncGlob(
    `${basePath}/{src,source,App,types}/**/!(*.test|*.story).ts?(x)`
  );
  const jsLOC = await countLinesInFiles(jsFiles);
  const tsLOC = await countLinesInFiles(tsFiles);
  const totalLOC = jsLOC + tsLOC;
  const progress = Math.round((tsLOC / totalLOC) * 100) / 2;

  const progressBar = Array(50)
    .fill(null)
    .map((value, index) => (index < progress ? `\u2588` : `\u2591`))
    .join('');

  console.log(
    `  ${progressBar} ${
      progress * 2
    }% => ${basePath} (Loc: ${totalLOC}, JS: ${jsLOC}, TS: ${tsLOC})`
  );
}

async function getDirectoryPaths(basePath) {
  const directories = await asyncReaddir(basePath);
  return directories.map(item => `${basePath}/${item}`);
}

async function printStatsForDirectories() {
  const apps = await getDirectoryPaths('apps');
  const packages = await getDirectoryPaths('packages');
  const allDirectories = [...apps, ...packages];

  console.log('TypeScript conversion stats:');
  allDirectories.forEach(dir => printStats(dir));
}

printStatsForDirectories();

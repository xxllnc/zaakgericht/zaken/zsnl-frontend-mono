// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const test = require('tape');
const { reduceMap } = require('./iterable');

test('reduceMap()', assert => {
  const createMap = () =>
    new Map([
      [() => false, () => false],
      [(first, second) => second === 'b', (third, fourth) => fourth],
    ]);

  {
    const actual = reduceMap({
      map: createMap(),
      keyArguments: ['a', 'b'],
      valueArguments: ['c', 'd'],
    });
    const expected = 'd';
    const message =
      'returns the output of the first value function where the key function is truthy';

    assert.equal(actual, expected, message);
  }
  {
    const actual = reduceMap({
      map: createMap(),
      fallback: 'foo',
    });
    const expected = 'foo';
    const message = 'returns the fallback value if there are no matches';

    assert.equal(actual, expected, message);
  }
  {
    const actual = reduceMap({
      map: createMap(),
      valueArguments: ['c', 'd'],
      fallback: (first, second) => second,
    });
    const expected = 'd';
    const message =
      'supports fallback as a function, with the functionArgs supplied';

    assert.equal(actual, expected, message);
  }
  assert.end();
});

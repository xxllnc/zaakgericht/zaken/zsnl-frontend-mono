// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const { assign } = Object;

/**
 * Bind an infinite set of methods to a class instance.
 *
 * @param {Object} instance
 *   Instance of a class
 * @param {...string} methods
 *   Method names of a class
 */
export function bind(instance, ...methods) {
  const reduceBoundMethods = (accumulator, name) =>
    assign(accumulator, {
      [name]: instance[name].bind(instance),
    });
  const boundMethods = methods.reduce(reduceBoundMethods, {});

  assign(instance, boundMethods);
}

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const test = require('tape');
const sinon = require('sinon');
const { preventDefaultAndCall } = require('./preventDefaultAndCall');

const createEventMock = ({
  altKey = false,
  ctrlKey = false,
  shiftKey = false,
  metaKey = false,
} = {}) => ({
  altKey,
  ctrlKey,
  shiftKey,
  metaKey,
  preventDefault: sinon.spy(),
});

test('preventDefaultAndCall()', assert => {
  const callback = sinon.spy();
  const event = createEventMock();
  preventDefaultAndCall(callback)(event);

  assert.equal(
    callback.callCount,
    1,
    'executes the callback when no key is pressed'
  );
  assert.equal(
    event.preventDefault.callCount,
    1,
    'prevents default behavior when no key is pressed'
  );
  assert.end();
});

test(`preventDefaultAndCall()`, assert => {
  ['altKey', 'ctrlKey', 'shiftKey', 'metaKey'].forEach(key => {
    const callback = sinon.spy();
    const event = createEventMock({
      [key]: true,
    });

    preventDefaultAndCall(callback)(event);
    assert.equal(
      callback.callCount,
      0,
      `does not execute the callback when ${key} is pressed`
    );
    assert.equal(
      event.preventDefault.callCount,
      0,
      `does not prevent default behavior when ${key} is pressed`
    );
  });
  assert.end();
});

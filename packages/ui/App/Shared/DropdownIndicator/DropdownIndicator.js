// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withStyles } from '@mui/styles';
import IconButton from '@mui/material/IconButton';
import Icon from '../../Material/Icon/Icon';
import { dropdownIndicatorStylesheet } from './DropdownIndicator.style';

/**
 * @param {Object} props
 * @param {Object} props.classes
 * @param {Function} [props.action]
 * @return {ReactElement}
 */
const DropdownIndicator = ({ classes, action }) => (
  <IconButton onClick={action} color="inherit" classes={classes}>
    <Icon size="small">arrow_drop_down</Icon>
  </IconButton>
);

export default withStyles(dropdownIndicatorStylesheet)(DropdownIndicator);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const moveItem: <T>(
  items: T[],
  sourceIndex: number,
  destinationIndex: number
) => T[] = (items, sourceIndex, destinationIndex) => {
  const nx = [...items];
  const [moved] = nx.splice(sourceIndex, 1);
  nx.splice(destinationIndex, 0, moved);

  return nx;
};

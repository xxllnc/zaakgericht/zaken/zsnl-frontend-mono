// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useTheme } from '@mui/material';
import { Theme } from '../../../types/Theme';

export const useDropdownMenuStyle = () => {
  const {
    mintlab: { radius, shadows },
  } = useTheme<Theme>();

  return {
    borderRadius: radius.dropdownMenu,
    padding: '8px',
    boxShadow: shadows.medium,
  };
};

export const useDropdownMenuButtonStyle = () => {
  const {
    palette: { primary },
    typography,
    mintlab: { greyscale, radius },
  } = useTheme<Theme>();

  return {
    borderRadius: radius.dropdownButton,
    color: greyscale.offBlack,
    '&:hover': {
      backgroundColor: `${greyscale.offBlack}0D`,
      color: primary.main,
    },
    height: '36px',
    padding: '6px 20px 6px 11px',
    justifyContent: 'flex-start',
    '& > span:first-of-type > svg': {
      marginRight: '18px',
    },
    fontWeight: typography.fontWeightRegular,
  };
};

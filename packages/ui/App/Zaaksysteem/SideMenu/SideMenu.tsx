// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import List from '@mui/material/List';
import Counter from '@mintlab/ui/App/Zaaksysteem/Counter/Counter';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { useSideMenuStyles } from './SideMenu.styles';

export type SideMenuItemType = {
  label?: string;
  tooltip?: string;
  ariaLabel?: string;
  component?: any;
  selected?: boolean;
  href?: string;
  count?: number;
  icon?: React.ReactElement;
  [key: string]: any;
};

export type SideMenuPropsType = {
  items: SideMenuItemType[];
};

export const SideMenu: React.ComponentType<SideMenuPropsType> = ({ items }) => {
  const classes = useSideMenuStyles();

  return (
    <List component="nav" role="navigation">
      {items.map(
        /* eslint complexity: [2, 7] */
        (
          {
            component,
            icon,
            label,
            selected,
            href,
            tooltip,
            count,
          }: SideMenuItemType,
          index
        ) => (
          <Tooltip key={index} title={tooltip || ''} placement="right">
            <ListItem
              button
              role="button"
              href={href}
              component={component}
              selected={selected}
              classes={{
                root: classes.root,
                selected: classes.selected,
              }}
            >
              {icon && (
                <ListItemIcon
                  classes={{
                    root: selected ? classes.iconSelected : undefined,
                  }}
                >
                  <Counter
                    classes={{
                      counter: classes.counter,
                    }}
                    count={count || 0}
                  >
                    {icon}
                  </Counter>
                </ListItemIcon>
              )}
              {label && <ListItemText>{label}</ListItemText>}
            </ListItem>
          </Tooltip>
        )
      )}
    </List>
  );
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/* eslint-disable */
import { React, stories } from '../../story';
import Icon, { iconNames } from '../../Material/Icon/Icon';
import { SideMenu, SideMenuItemType } from './SideMenu';

stories(module, __dirname, {
  Default() {
    const CustomComponent = React.forwardRef(({ children, ...rest }, ref) => (
      <div {...rest}>{children}</div>
    ));

    const items: SideMenuItemType[] = [
      {
        label: 'Home',
        icon: <Icon>{iconNames.home}</Icon>,
        selected: true,
      },
      {
        label: 'Files',
        icon: <Icon>{iconNames.folder}</Icon>,
      },
      {
        label: 'Custom',
        icon: <Icon>{iconNames.search}</Icon>,
        component: CustomComponent,
      },
    ];
    return <SideMenu items={items} />;
  },
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { getStyleFromWidth } from './getStyleFromWidth';

const WIDTH = 100;
const HEIGHT = 50;
const viewbox = [WIDTH, HEIGHT];

describe('The `Svg` component’s `getStyleFromWidth` utility function', () => {
  test('returns', () => {
    const actual = getStyleFromWidth(viewbox, '60px');
    const asserted = {
      width: '60px',
      height: '30px',
    };

    expect(actual).toEqual(asserted);
  });

  test('adds a top padding instead of height if the height is a percentage', () => {
    const actual = getStyleFromWidth(viewbox, '60%');
    const asserted = {
      width: '60%',
      paddingTop: '30%',
    };

    expect(actual).toEqual(asserted);
  });
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as Svg } from './Svg';
export * from './Svg';
export default Svg;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/* eslint-disable no-magic-numbers */
import { React, number, stories } from '../../story';
import Svg from '.';

const HEADS_UP = 'Beware percentages in a Flexbox context.';

const LeftCircle = () => <circle fill="#006b98" cx="50" cy="50" r="50" />;

const RightCircle = () => <circle fill="#bedf3a" cx="150" cy="50" r="50" />;

const responsiveStyle = {
  display: 'block',
  width: '400px',
  height: '200px',
  border: '1px solid #ccc',
  textAlign: 'center',
};

const rangeOptions = {
  range: true,
  min: 0,
  max: 100,
  step: 1,
};

stories(module, __dirname, {
  'viewBox size': function ViewBoxSize() {
    return (
      <Svg viewBox={100}>
        <LeftCircle />
      </Svg>
    );
  },
  'Responsive horizontal': function ResponsiveHorizontal() {
    return (
      <div>
        {HEADS_UP}
        <div style={responsiveStyle}>
          <Svg
            width={`${number('Width %', 25, rangeOptions)}%`}
            viewBox={[200, 100]}
          >
            <LeftCircle />
            <RightCircle />
          </Svg>
        </div>
      </div>
    );
  },
  'Responsive vertical': function ResponsiveVertical() {
    return (
      <div>
        {HEADS_UP}
        <div style={responsiveStyle}>
          <Svg
            height={`${number('Height %', 25, rangeOptions)}%`}
            viewBox={[200, 100]}
          >
            <LeftCircle />
            <RightCircle />
          </Svg>
        </div>
      </div>
    );
  },
  'Fixed width': function FixedWidth() {
    return (
      <Svg width={`${number('Width (px)', 100)}px`} viewBox={[200, 100]}>
        <LeftCircle />
        <RightCircle />
      </Svg>
    );
  },
  'Fixed height': function FixedHeight() {
    return (
      <Svg height={`${number('Height (px)', 50)}px`} viewBox={[200, 100]}>
        <LeftCircle />
        <RightCircle />
      </Svg>
    );
  },
});

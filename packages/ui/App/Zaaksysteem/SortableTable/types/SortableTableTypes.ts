// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  ColumnProps,
  SortDirectionType,
  TableCellProps,
  Size,
} from 'react-virtualized';
export type {
  TableRowProps,
  RowMouseEventHandlerParams,
} from 'react-virtualized';
import { OnDragEndResponder, DraggableProvided } from 'react-beautiful-dnd';
import { useSortableTableStyles } from './../SortableTable.style';

export type SortableTablePropsType = {
  rows: RowType[];
  columns: ColumnType[];
  onRowDoubleClick?: any;
  loading?: boolean;
  noRowsMessage: string;
  noRowsProps?: {
    [key: string]: any;
  };
  /** If provided, the rows will be rendered with a fixed height,
   * with content cut off with ellipsis. If not, rows will fit to
   * their content. */
  rowHeight?: number;
  selectable?: boolean;
  onSelectPage?: (
    event: React.MouseEvent<any>,
    isSelected: boolean,
    selected: string[]
  ) => void;
  onSelectEverything?: (event: React.MouseEvent<any>) => any;
  selectEverythingTranslations?: {
    select: string;
    selectButton: string;
    deselect: string;
    deselectButton: string;
    selectEverything: string;
    deselectEverything: string;
  };
  styles?: ReturnType<typeof useSortableTableStyles>;
  sortDirectionDefault?: SortDirectionType;
  sorting?: SortType;
  sortInternal?: boolean;
  sortingText?: string;
  onDragEnd?: OnDragEndResponder;
  onResize?: (size: Size) => void;
  headerHeight?: number;
  selectedRows?: string[];
  [key: string]: any;
};

export type RowType = {
  uuid: string;
  name: string;
  selected?: boolean;
  parent?: string;
  active?: boolean;
};

export type CellRendererPropsType = Pick<
  TableCellProps,
  'dataKey' | 'rowData' | 'parent' | 'columnIndex' | 'rowIndex'
>;

export interface ColumnType
  extends Omit<ColumnProps, 'cellRenderer' | 'dataKey'> {
  label?: string;
  defaultSort?: boolean;
  showFromWidth?: number;
  name: string;
  cellRenderer?: ({
    dataKey,
    rowData,
  }: CellRendererPropsType) => React.ReactChild;
}

export type SortType = 'none' | 'column' | 'dragdrop';

export type GetVisibleColumnsType = Pick<
  SortableTablePropsType,
  'sorting' | 'selectable' | 'columns' | 'classes'
> &
  Pick<Size, 'width'>;

export type RowComponentType = {
  isDragging?: boolean;
  isScrolling?: boolean;
  draggingText?: string;
  provided: DraggableProvided;
  classes?: any;
  style?: any;
} & Partial<SortableTablePropsType>;

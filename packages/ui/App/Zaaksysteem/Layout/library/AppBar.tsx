// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Box from '@mui/material/Box';
import MuiAppBar from '@mui/material/AppBar';
import MuiToolbar from '@mui/material/Toolbar';
import { useTheme } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import Button from '../../../Material/Button';
import { addScopeAttribute } from '../../../library/addScope';

export const AppBar = ({
  children,
  menuLabel,
  onMenuClick,
  scope,
  searchComponent,
}: any) => {
  const theme = useTheme();
  return (
    <MuiAppBar
      color="inherit"
      position="static"
      sx={{ marginTop: '10px', boxShadow: 'none' }}
      {...addScopeAttribute(scope, 'app-bar')}
    >
      <MuiToolbar
        disableGutters={true}
        sx={{
          [theme.breakpoints.up('xs')]: {
            minHeight: '48px',
          },
        }}
      >
        <div style={{ padding: '10px 15px' }}>
          <Button
            action={onMenuClick}
            label={menuLabel}
            name="adminMenuToggle"
            icon="menu"
          />
        </div>
        <Box
          sx={{
            flexGrow: 1,
            padding: '0px 6px',
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            '&:children': {
              flexGrow: 0,
            },
          }}
        >
          <Typography variant="subtitle1">{children}</Typography>
          <div style={{ flexGrow: 1, alignItems: 'center', padding: '0 8px' }}>
            {searchComponent}
          </div>
        </Box>
      </MuiToolbar>
    </MuiAppBar>
  );
};

export default AppBar;

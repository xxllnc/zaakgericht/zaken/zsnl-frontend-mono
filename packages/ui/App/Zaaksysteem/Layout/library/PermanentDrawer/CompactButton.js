// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTheme } from '@mui/material/styles';
import ButtonBase from '@mui/material/ButtonBase';
import { preventDefaultAndCall } from '@mintlab/kitchen-sink/source';
import Typography from '@mui/material/Typography';
import Icon from '../../../../Material/Icon/Icon';
import { addScopeAttribute } from '../../../../library/addScope';

export const CompactButton = ({
  action,
  icon,
  label,
  href,
  target,
  active = false,
  scope,
}) => {
  const theme = useTheme();

  return (
    <ButtonBase
      sx={{
        display: 'flex',
        flexDirection: 'column',
        width: `64px`,
        height: `64px`,
        borderRadius: `32px`,
        color: active
          ? theme.palette.primary.main
          : theme.mintlab.greyscale.evenDarker,

        '&:hover, &:focus': {
          color: theme.palette.primary.main,
        },
        '&:hover': {
          '& :nth-child(2)': {
            visibility: 'visible',
          },
        },
        '& .MuiTypography-caption': {
          display: 'block',
          visibility: 'hidden',
          fontSize: '0.6rem',
          marginTop: '2px',
        },
      }}
      onClick={
        action &&
        (event =>
          preventDefaultAndCall(() => {
            action();
          })(event))
      }
      component="a"
      href={href}
      target={target}
      {...addScopeAttribute(scope, 'button')}
    >
      <Icon size="small">{icon}</Icon>

      <Typography variant="caption">{label}</Typography>
    </ButtonBase>
  );
};

export default CompactButton;

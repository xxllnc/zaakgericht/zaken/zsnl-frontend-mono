// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment } from 'react';
import ButtonBase from '@mui/material/ButtonBase';
import { useTheme } from '@mui/material/styles';
import Drawer from '@mui/material/Drawer';
import Divider from '@mui/material/Divider';
import Typography from '@mui/material/Typography';
import { addScopeProp } from '../../../../library/addScope';
import Render from '../../../../Abstract/Render';
import MenuGroup from './MenuGroup';

const TemporaryDrawer = ({
  navigation,
  open,
  subtitle,
  title,
  toggle,
  scope,
}) => {
  const {
    mintlab: { greyscale, shadows },
    palette: { primary },
    typography,
  } = useTheme();

  return (
    <Drawer
      sx={{
        '& .MuiDrawer-paper': {
          minWidth: '16rem',
          padding: '7px',
          'justify-content': 'space-between',
          boxShadow: shadows.medium,
        },
      }}
      onClose={toggle}
      open={open}
      variant="temporary"
      ModalProps={{
        hideBackdrop: true,
        onClick: toggle,
      }}
    >
      <div style={{ padding: '14px 18px' }}>
        <Typography variant="subtitle2" sx={{ marginBottom: '4px' }}>
          {title}
        </Typography>
        <div
          style={{
            color: greyscale.evenDarker,
            fontFamily: typography.fontFamily,
            fontWeight: typography.fontWeightLight,
            fontSize: '10px',
          }}
        >
          {subtitle}
        </div>
      </div>

      <Render condition={navigation.primary}>
        <MenuGroup
          sx={{ height: '100%' }}
          navigation={navigation.primary}
          {...addScopeProp(scope, 'primary')}
        />
      </Render>

      <Render condition={navigation.secondary}>
        <Fragment>
          <Divider />
          <MenuGroup
            navigation={navigation.secondary}
            {...addScopeProp(scope, 'secondary')}
          />
        </Fragment>
      </Render>

      <Render condition={navigation.about}>
        <ButtonBase
          onClick={navigation.about.action}
          sx={{
            'justify-content': 'flex-start',
            color: greyscale.evenDarker,
            margin: '16px 20px',
            '&:hover': {
              color: primary.main,
            },
          }}
        >
          {navigation.about.label}
        </ButtonBase>
      </Render>
    </Drawer>
  );
};

export default TemporaryDrawer;

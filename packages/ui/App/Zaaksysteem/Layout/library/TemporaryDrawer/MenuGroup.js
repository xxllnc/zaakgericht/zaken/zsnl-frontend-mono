// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTheme } from '@mui/material/styles';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import MenuItem from '@mui/material/MenuItem';
import MenuList from '@mui/material/MenuList';
import Typography from '@mui/material/Typography';
import { preventDefaultAndCall } from '@mintlab/kitchen-sink/source';
import Icon from '../../../../Material/Icon/Icon';
import { addScopeAttribute } from '../../../../library/addScope';

export const MenuGroup = ({ navigation, scope, sx }) => {
  const {
    palette,
    mintlab: { greyscale },
  } = useTheme();
  return (
    <MenuList
      component="nav"
      sx={{
        ...sx,
        flex: 'auto',
        '& > *:not(:last-child)': {
          marginBottom: '10px',
          marginLeft: '3px',
        },
      }}
      {...addScopeAttribute(scope, 'menu')}
    >
      {navigation.map(({ action, href, icon, label, target }, index) => {
        const active = window.location.href.includes(href);
        return (
          <MenuItem
            key={index}
            sx={{
              color: active ? palette.primary.main : greyscale.darkest,
              backgroundColor: active
                ? `${palette.primary.main}0D`
                : 'transparent',
              '&:hover': {
                color: palette.primary.main,
              },
              '$active&:hover': {
                backgroundColor: `${palette.primary.main}0D`,
              },
              ':not($active)&:hover': {
                backgroundColor: `${greyscale.offBlack}0D`,
              },
            }}
            component="a"
            href={href}
            target={target}
            onClick={
              action &&
              (event =>
                preventDefaultAndCall(() => {
                  action();
                })(event))
            }
            {...addScopeAttribute(scope, 'menu', label, 'item')}
          >
            <ListItemIcon sx={{ color: 'inherit' }}>
              <Icon size="small" sx={{ color: 'inherit' }}>
                {icon}
              </Icon>
            </ListItemIcon>
            <ListItemText disableTypography={true}>
              <Typography
                variant="body2"
                sx={{ color: 'inherit', marginLeft: '15px' }}
              >
                {label}
              </Typography>
            </ListItemText>
          </MenuItem>
        );
      })}
    </MenuList>
  );
};

export default MenuGroup;

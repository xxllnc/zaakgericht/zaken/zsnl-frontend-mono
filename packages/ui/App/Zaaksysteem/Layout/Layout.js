// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { withStyles } from '@mui/styles';
import { addScopeProp, addScopeAttribute } from '../../library/addScope';
import AppBar from './library/AppBar';
import PermanentDrawer from './library/PermanentDrawer/PermanentDrawer';
import TemporaryDrawer from './library/TemporaryDrawer/TemporaryDrawer';
import Banners from './library/Banners/Banners';
import { layoutStyleSheet } from './Layout.style';

export const Layout = ({
  children,
  classes,
  customer,
  drawer,
  identity,
  isDrawerOpen,
  menuLabel,
  toggleDrawer,
  banners,
  scope,
  searchComponent,
}) => (
  <div className={classes.root} {...addScopeAttribute(scope, 'layout')}>
    <AppBar
      menuLabel={menuLabel}
      onMenuClick={toggleDrawer}
      searchComponent={searchComponent}
      {...addScopeProp(scope, 'layout')}
    >
      {identity}
    </AppBar>

    <Banners banners={banners} {...addScopeProp(scope, 'layout')} />

    <TemporaryDrawer
      navigation={drawer}
      open={isDrawerOpen}
      subtitle={customer}
      title={identity}
      toggle={toggleDrawer}
      {...addScopeProp(scope, 'layout', 'drawer')}
    />

    <div className={classes.drawerAndContentContainer}>
      <PermanentDrawer
        navigation={drawer}
        {...addScopeProp(scope, 'layout', 'navigation')}
      />

      <div className={classes.content}>{children}</div>
    </div>
  </div>
);

export default withStyles(layoutStyleSheet)(Layout);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * Style Sheet for the {@link ChoiceChips} component
 *
 * @param {Object} theme
 * @return {JSS}
 */
export const choiceChipsStylesheet = () => ({
  list: {
    display: 'flex',
    padding: 0,
    margin: 0,
    flexWrap: 'wrap',
    marginTop: -20,
  },
  listItem: {
    padding: 0,
    listStyleType: 'none',
    margin: '20px 20px 0px 0px',
  },
});

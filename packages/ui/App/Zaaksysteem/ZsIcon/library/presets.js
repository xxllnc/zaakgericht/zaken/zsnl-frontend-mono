// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

// TO-DO get backgroundColors from theme, once the grid palette is implemented
const getPresets = ({
  palette: {
    primary,
    secondary,
    error,
    vivid,
    bluebird,
    coral,
    basalt,
    elephant,
    grass,
    lizard,
    pansy,
    sahara,
  },
}) => {
  const defaultFile = {
    name: 'insert_drive_file',
    color: primary.main,
  };

  return {
    channel: {
      inverted: {
        frontdesk: {
          name: 'room_service',
          backgroundColor: '#EFC16A',
        },
        assignee: {
          name: 'person',
          backgroundColor: '#FF0000',
        },
        email: {
          name: 'alternate_email',
          backgroundColor: '#B2749F',
        },
        mail: {
          name: 'drafts',
          backgroundColor: '#6635C1',
        },
        phone: {
          name: 'phone',
          backgroundColor: '#23BA86',
        },
        social_media: {
          name: 'thumb_up',
          backgroundColor: '#3B5998',
        },
        webform: {
          name: 'format_list_bulleted',
          backgroundColor: '#337FCB',
        },
        postex: {
          name: 'drafts',
          backgroundColor: '#337FCB',
        },
      },
    },
    threadType: {
      inverted: {
        contactMoment: {
          name: 'chat_bubble',
          backgroundColor: 'green',
        },
        email: {
          name: 'email',
          backgroundColor: primary.main,
        },
        emailThread: {
          name: 'email_conversation',
          backgroundColor: primary.main,
        },
        note: {
          name: 'notes',
          backgroundColor: '#EBC32F',
          color: '#000000',
        },
        pipMessage: {
          name: 'email',
          backgroundColor: primary.main,
        },
        pipMessageThread: {
          name: 'email_conversation',
          backgroundColor: primary.main,
        },
        postex: {
          name: 'email',
          backgroundColor: primary.main,
        },
        failed: {
          name: 'warning',
          backgroundColor: 'transparent',
          color: error.dark,
        },
      },
    },
    entityType: {
      file: {
        name: 'file_copy',
        color: pansy.main,
      },
      person: {
        name: 'person',
        color: secondary.main,
      },
      organization: {
        name: 'domain',
        color: primary.dark,
      },
      employee: {
        name: 'badge',
        color: pansy.main,
      },
      case: {
        name: 'folder',
        color: sahara.main,
      },
      case_type: {
        name: 'ballot',
        color: vivid['4'],
      },
      folder: {
        name: 'folder',
        color: vivid['2'],
      },
      object_type: {
        name: 'layers',
        color: vivid['3'],
      },
      custom_object_type: {
        name: 'layers',
        color: vivid['3'],
      },
      attribute: {
        name: 'extension',
        color: vivid['1'],
      },
      email_template: {
        name: 'mail_outline',
        color: vivid['5'],
      },
      document_template: {
        name: 'insert_drive_file',
        color: vivid['6'],
      },
      other: {
        name: 'settings',
        color: basalt.main,
      },
      contact_moment: {
        name: 'import_contacts',
        color: vivid['1'],
      },
      contact: {
        name: 'person',
        color: secondary.main,
      },
      inverted: {
        file: {
          name: 'file_copy',
          backgroundColor: pansy.main,
        },
        person: {
          name: 'person',
          backgroundColor: secondary.main,
        },
        organization: {
          name: 'domain',
          backgroundColor: primary.dark,
        },
        employee: {
          name: 'badge',
          backgroundColor: pansy.main,
        },
        case: {
          name: 'folder',
          backgroundColor: sahara.main,
        },
        case_type: {
          name: 'ballot',
          backgroundColor: vivid['4'],
        },
        folder: {
          name: 'folder',
          backgroundColor: vivid['2'],
        },
        object_type: {
          name: 'layers',
          backgroundColor: vivid['3'],
        },
        custom_object_type: {
          name: 'layers',
          backgroundColor: vivid['3'],
        },
        attribute: {
          name: 'extension',
          backgroundColor: vivid['1'],
        },
        email_template: {
          name: 'mail_outline',
          backgroundColor: vivid['5'],
        },
        document_template: {
          name: 'insert_drive_file',
          backgroundColor: vivid['6'],
        },
        other: {
          name: 'settings',
          backgroundColor: basalt.main,
        },
        contact_moment: {
          name: 'import_contacts',
          backgroundColor: vivid['1'],
        },
        contact: {
          name: 'import_contacts',
          backgroundColor: vivid['1'],
        },
      },
    },
    actions: {
      selectAll: {
        name: 'select_all',
        color: vivid['5'],
      },
      import: {
        name: 'publish',
        color: 'black',
      },
    },
    file: {
      error: {
        name: 'insert_drive_file',
        color: error.dark,
      },
      pdf: {
        name: 'insert_drive_file',
        color: coral.dark,
      },
      png: {
        name: 'image',
        color: primary.light,
      },
      jpg: {
        name: 'image',
        color: primary.light,
      },
      doc: {
        name: 'insert_drive_file',
        color: bluebird.main,
      },
      docx: {
        name: 'insert_drive_file',
        color: bluebird.main,
      },
      ppt: {
        name: 'insert_drive_file',
        color: coral.main,
      },
      pptx: {
        name: 'insert_drive_file',
        color: coral.main,
      },
      xls: {
        name: 'insert_drive_file',
        color: lizard.main,
      },
      xlsx: {
        name: 'insert_drive_file',
        color: lizard.main,
      },
      default: defaultFile,
    },
    itemType: {
      folder: {
        name: 'folder',
        color: vivid[2],
      },
      file: defaultFile,
    },
    caseStatus: {
      new: {
        name: 'stars',
        color: sahara.main,
      },
      open: {
        name: 'play_circle_filled',
        color: bluebird.main,
      },
      stalled: {
        name: 'pause_circle_filled',
        color: elephant.main,
      },
      resolved: {
        name: 'check_circle',
        color: grass.main,
      },
      transfer: {
        name: 'cloud_upload',
        color: bluebird.light,
      },
      destroy: {
        name: 'cancel',
        color: error.dark,
      },
    },
    search: {
      inverted: {
        savedSearch: {
          name: 'search',
          backgroundColor: grass.main,
        },
      },
    },
  };
};

export default getPresets;

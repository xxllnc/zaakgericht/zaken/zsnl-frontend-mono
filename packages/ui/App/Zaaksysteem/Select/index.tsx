// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Suspense } from 'react';
import { Loader } from '../Loader';

export type { ValueType } from './types/ValueType';

const Form = React.lazy(
  () =>
    import(
      // https://webpack.js.org/api/module-methods/#import
      /* webpackChunkName: "ui.select-form" */
      './FormSelect'
    )
);

const Creatable = React.lazy(
  () =>
    import(
      // https://webpack.js.org/api/module-methods/#import
      /* webpackChunkName: "ui.select-creatable" */
      './CreatableSelect'
    )
);

export const FormSelect: typeof import('./FormSelect').FormSelect = props => (
  <Suspense fallback={<Loader />}>
    <Form {...props} />
  </Suspense>
);

export const CreatableSelect: typeof import('./CreatableSelect').CreatableSelect =
  props => (
    <Suspense fallback={<Loader />}>
      <Creatable {...props} />
    </Suspense>
  );

export { renderOptionWithIcon } from './library/renderOptionWithIcon';
export { renderTagsWithIcon } from './library/renderTagsWithIcon';
export { renderMultilineOption } from './library/renderMultilineOption';
export { defaultFilterOption } from './library/filterOption';

export default FormSelect;

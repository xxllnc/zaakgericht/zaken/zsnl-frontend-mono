// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Autocomplete from '@mui/material/Autocomplete';
import Chip from '@mui/material/Chip';
import TextField from '../../Material/TextField';
import { BaseSelectPropsType } from './types/BaseSelectPropsType';
import defaultFilterOptions from './library/filterOption';

/* eslint complexity: [2, 8] */
export const CreatableSelect: React.ComponentType<
  BaseSelectPropsType<any> & { createType?: string }
> = ({
  disabled,
  error,
  value,
  name,
  onBlur,
  onChange,
  filterOption,
  choices,
  placeholder,
  isClearable,
  createType,
  renderTags,
  sx = {},
  inputProps = {},
}) => {
  return (
    <Autocomplete
      freeSolo
      multiple
      selectOnFocus
      clearOnBlur
      handleHomeEndKeys
      id={name}
      disabled={disabled}
      options={choices || []}
      disableClearable={isClearable === false}
      sx={{
        '& .MuiAutocomplete-inputRoot': { padding: '7px 39px 7px 8px' },
        ...sx,
      }}
      filterOptions={(options, params) => {
        const filtered = options.filter(opt =>
          //@ts-ignore
          (filterOption || defaultFilterOptions)(opt, params.inputValue)
        );

        const { inputValue } = params;
        if (
          inputValue !== '' &&
          //@ts-ignore
          !options.some(option => inputValue === option)
        ) {
          filtered.push({
            //@ts-ignore
            inputValue,
            label: `Aanmaken: "${inputValue}"`,
          });
        }

        return filtered;
      }}
      onChange={(ev, value) => {
        const resValue = value.map((val: any) =>
          typeof val === 'string'
            ? { label: val, value: val, type: createType }
            : {
                label: val.inputValue || val.value,
                value: val.inputValue || val.value,
                type: createType,
              }
        );
        // setCreatableValue(resValue);
        onChange &&
          onChange({
            ...ev,
            target: {
              ...ev.target,
              name,
              value: resValue,
            },
          });
      }}
      //@ts-ignore
      renderTags={
        renderTags ||
        ((value, getTagProps) =>
          //@ts-ignore
          value.map((option: any, index: number) => {
            return (
              // eslint-disable-next-line react/jsx-key
              <Chip
                sx={{
                  margin: '0 3px',
                  '& .MuiSvgIcon-root': { fontSize: '20px' },
                }}
                variant="outlined"
                label={option.label}
                {...getTagProps({ index })}
              />
            );
          }))
      }
      //@ts-ignore
      value={value || []}
      renderInput={props => (
        <TextField
          onBlur={onBlur}
          name={name}
          error={error}
          placeholder={placeholder}
          {...props}
          {...inputProps}
        />
      )}
    />
  );
};

export default CreatableSelect;

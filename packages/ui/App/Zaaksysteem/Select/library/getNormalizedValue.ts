// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { asArray } from '@mintlab/kitchen-sink/source';

export const DELAY = 400;
export const MIN_CHARACTERS = 3;

// eslint-disable-next-line complexity
export const getNormalizedValue = ({
  value,
  choices,
  isMulti,
}: {
  value: any;
  choices: any;
  isMulti: boolean;
}) => {
  if (!value || (Array.isArray(value) && !value.length))
    return value || (isMulti ? [] : '');

  const parsedValues = asArray(value).map((val: any) => {
    if (typeof val === 'object') return val;
    if (!choices || !asArray(choices).length) return val;

    //try to find choice by value
    let foundChoice;
    foundChoice = choices.find((choice: any) => choice.value === val);

    //try to find choice by label
    if (!foundChoice) {
      foundChoice = choices.find((choice: any) => choice.label === val);
    }

    return foundChoice ? foundChoice : val;
  });

  return isMulti ? parsedValues : parsedValues[0];
};

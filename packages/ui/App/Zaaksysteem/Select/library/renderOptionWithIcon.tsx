// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import MultilineOption from '../Option/MultilineOption';

const optionIconStylePresets = {
  person: {
    fontSize: '14px',
    height: '21.875px',
    display: 'inline-flex',
    color: 'rgb(255, 255, 255)',
    borderRadius: '50%',
    width: '21.875px',
    backgroundColor: 'rgb(48, 170, 77)',
    justifyContent: 'center',
    alignItems: 'center',
  },
};

export const renderOptionWithIcon =
  (
    iconName: keyof typeof iconNames | ((option: any) => keyof typeof iconNames)
  ) =>
  (props: any, option: any) => {
    const name = typeof iconName === 'string' ? iconName : iconName(option);
    return (
      <MultilineOption
        {...props}
        {...option}
        icon={
          <Icon
            size={21}
            //@ts-ignore
            style={optionIconStylePresets[name]}
          >
            {name}
          </Icon>
        }
      />
    );
  };

export default renderOptionWithIcon;

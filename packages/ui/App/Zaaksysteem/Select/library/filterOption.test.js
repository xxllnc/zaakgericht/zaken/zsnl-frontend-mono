// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import filterOption from './filterOption';

const option = {
  data: {
    label: 'Súdwest-Fryslân',
    alternativeLabels: ['érg koud', 'KNÄCKEBRÖD RÅG'],
  },
};

describe('the `filterOption` function', () => {
  test('return true if the label partially matches', () => {
    const actual = filterOption(option, 'sud');
    expect(actual).toBe(true);
  });

  test('returns true if one of the alternative labels partially matches', () => {
    const actual = filterOption(option, 'knackebrod');
    expect(actual).toBe(true);
  });

  test('returns true if input is an empty string', () => {
    const actual = filterOption(option, '');
    expect(actual).toBe(true);
  });

  test('returns true if there is no input', () => {
    const actual = filterOption(option, undefined);
    expect(actual).toBe(true);
  });

  test('returns false if there are no matches', () => {
    const actual = filterOption(option, 'foo');
    expect(actual).toBe(false);
  });
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, UseTheKnobs, stories, boolean, text } from '../../story';
import { Render } from '.';

stories(module, __dirname, {
  Default() {
    return (
      <div>
        <UseTheKnobs />
        <Render condition={boolean('Active', false)}>
          <h1>{text('Greeting', 'Hello, world!')}</h1>
        </Render>
      </div>
    );
  },
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const Render: React.ComponentType<{
  children: any;
  condition: boolean;
}> = ({ children, condition }) => {
  if (condition) {
    return children;
  }

  return null;
};

export default Render;

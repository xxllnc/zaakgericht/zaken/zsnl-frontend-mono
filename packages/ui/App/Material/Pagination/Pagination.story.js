// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import MuiTable from '@mui/material/Table';
import MuiTableBody from '@mui/material/TableBody';
import MuiTableRow from '@mui/material/TableRow';
import { React, stories } from '../../story';
import Pagination from '.';

/* eslint-disable no-magic-numbers */
const stores = {
  Default: {
    count: 101,
    page: 0,
    rowsPerPage: 10,
  },
};

stories(
  module,
  __dirname,
  {
    Default({ store, count, page, rowsPerPage }) {
      function changeRowsPerPage(event) {
        store.set({
          page: 0,
          rowsPerPage: event.target.value,
        });
      }

      function getNewPage(newPage) {
        store.set({
          page: newPage,
        });
      }

      return (
        <MuiTable>
          <MuiTableBody>
            <MuiTableRow>
              <Pagination
                page={page}
                count={count}
                labelRowsPerPage={'Per pagina:'}
                rowsPerPageOptions={[5, 10, 25, 50, 100]}
                rowsPerPage={rowsPerPage}
                changeRowsPerPage={changeRowsPerPage}
                labelDisplayedRows={({ count: newCount, from, to }) =>
                  `${from}–${to} van ${newCount}`
                }
                getNewPage={getNewPage}
                scope="story"
              />
            </MuiTableRow>
          </MuiTableBody>
        </MuiTable>
      );
    },
  },
  stores
);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import { useTheme } from '@mui/material';
import { addScopeAttribute } from '../../../library/addScope';
import Icon, { IconNameType } from '../../Icon/Icon';
import Button from '../../Button/Button';
import { Theme } from '../../../../types/Theme';

export type DialogTitlePropsType = {
  icon?: React.ElementType | IconNameType;
  id?: string;
  onCloseClick?: () => void;
  elevated?: boolean;
  title: string;
  scope?: string;
  buttonProps?: {
    [key: string]: any;
  };
};

/* eslint complexity: [2, 7] */
export const DialogTitle: React.ComponentType<DialogTitlePropsType> = ({
  icon,
  title,
  id,
  scope,
  elevated,
  onCloseClick,
  buttonProps,
}) => {
  const {
    mintlab: { greyscale },
  } = useTheme<Theme>();
  const [t] = useTranslation('common');

  const defaultRoot = {
    width: 'auto',
    display: 'flex',
    alignItems: 'center',
    height: '70px',
    flex: '0 0 auto',
    padding: '0px 24px',
    zIndex: 1,
    margin: 0,
  };

  return (
    <Box
      sx={
        elevated
          ? {
              ...defaultRoot,
              boxShadow: '0px 0px 5px 0px rgba(0,0,0,0.3)',
              '&>*:nth-child(1)': {
                color: greyscale.darkest,
              },
            }
          : defaultRoot
      }
      {...addScopeAttribute(scope, 'title')}
    >
      {icon && typeof icon === 'string' && (
        <span style={{ margin: '0px 12px 2px 0px' }}>
          <Icon>{icon as IconNameType}</Icon>
        </span>
      )}

      {icon !== undefined && typeof icon !== 'string' && (
        //@ts-ignore
        <span style={{ paddingRight: '12px' }}>{icon}</span>
      )}

      <Typography
        variant="h6"
        sx={{
          flex: 1,
          whiteSpace: 'nowrap',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
        }}
      >
        {title}
      </Typography>

      {onCloseClick && (
        <div style={{ paddingLeft: 8 }}>
          <Button
            name="modalClose"
            aria-label={t('accessibility.close')}
            action={onCloseClick}
            icon="close"
            {...buttonProps}
          />
        </div>
      )}
    </Box>
  );
};

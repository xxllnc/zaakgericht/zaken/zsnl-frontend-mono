# 🔌 `TextField` component

> _Material Design_ **Text field**.

A multi-purpose textfield component, for use in forms and as generic UI components. Features include:

- Three different out-of-the-box styles: form, generic1, generic2
- Start- and end adornments
- Input formatting, suitable for currency fields and other fixed formatting

Use the default export from `index.tsx` or the named `TextField` export from this file.

## See also

- [`TextField` stories](/npm-mintlab-ui/storybook/?selectedKind=Material/TextField)
- [`TextField` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#material-textfield)

## External resources

- [_Material Guidelines_: Text fields](https://material.io/design/components/text-fields.html)
- [_Material-UI_ `TextField` API](https://material-ui.com/api/text-field/)

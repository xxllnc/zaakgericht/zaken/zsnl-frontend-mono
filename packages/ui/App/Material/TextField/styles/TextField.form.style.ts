// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useFormStyles = makeStyles(
  ({ palette: { error, elephant }, mintlab: { radius } }: Theme) => ({
    formControl: {
      width: '100%',
      backgroundColor: 'transparent',
      borderRadius: radius.textField,
    },
    formControlReadonly: {
      width: '100%',
      backgroundColor: 'transparent',
      borderRadius: radius.textField,
      boxShadow: '0 0 5px gray',
      border: '1px solid black',
    },
    inputRoot: {
      '&&': {
        marginTop: 0,
        '&&:before': {
          display: 'none', //removes the black default underline
        },
      },
    },
    inputRootErrorFocus: {
      background: error.light,
      borderRadius: radius.textField,
      '&&:after': {
        display: 'none',
      },
    },
    endAdornment: {
      marginRight: '4px',
    },
  })
);

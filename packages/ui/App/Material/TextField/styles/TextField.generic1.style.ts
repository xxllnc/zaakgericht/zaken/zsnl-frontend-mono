// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useGeneric1Styles = makeStyles(
  ({
    palette: { primary, elephant },
    mintlab: { greyscale, radius },
    typography,
  }: Theme) => ({
    formControl: {
      width: '100%',
      padding: '0px 6px 0px 12px',
      'background-color': greyscale.light,
      'border-radius': radius.genericInput,
      'justify-content': 'center',
      height: 40,
      fontWeight: typography.fontWeightRegular,
      color: greyscale.offBlack,
    },
    // Input
    input: {
      marginRight: '6px',
      '&::placeholder': {
        color: elephant.dark,
        opacity: 1,
      },
    },
    focus: {
      color: primary.dark,
      'background-color': primary.lightest,
      '& > *': {
        color: primary.dark,
      },
    },
    // end adornment
    endAdornment: {
      marginRight: '4px',
    },
  })
);

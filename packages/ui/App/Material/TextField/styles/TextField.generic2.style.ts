// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useGeneric2Styles = makeStyles(
  ({
    palette: { common, elephant },
    mintlab: { greyscale, shadows },
    typography,
  }: Theme) => ({
    // Form Control
    formControl: {
      width: '100%',
      padding: '0px 6px 0px 12px',
      'background-color': greyscale.light,
      'border-radius': '23px',
      'justify-content': 'center',
      height: '46px',
      fontWeight: typography.fontWeightLight,
    },
    // Input
    input: {
      marginRight: '6px',
      '&::placeholder': {
        color: elephant.dark,
        opacity: 1,
      },
    },
    // all focus
    focus: {
      color: greyscale.offBlack,
      backgroundColor: common.white,
    },
    // root focus
    rootFocus: {
      boxShadow: shadows.flat,
    },
    // end adornment
    endAdornment: {
      marginRight: '4px',
    },
  })
);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import MuiRadioGroup from '@mui/material/RadioGroup';
import FormControl from '@mui/material/FormControl';
import { addScopeAttribute, addScopeProp } from '../../library/addScope';
import { Choice } from './library/Choice';

/**
 * *Material Design* **Radio button** selection control.
 * - facade for *Material-UI* `RadioGroup`, `Radio,`
 *   `FormLabel`, `FormControl`, `FormControlLabel`
 *   and `FormHelperText`
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/RadioGroup
 * @see /npm-mintlab-ui/documentation/consumer/manual/RadioGroup.html
 *
 * @reactProps {Array} choices
 * @reactProps {boolean} [disabled=false]
 * @reactProps {string} [scope]
 * @reactProps {string} name
 * @reactProps {boolean} [required=false]
 * @reactProps {string} label
 * @reactProps {Function} onChange
 * @reactProps {string} value
 */
const RadioGroup = ({
  choices,
  disabled = false,
  scope,
  name,
  required = false,
  onChange,
  value,
  classes,
}) => {
  return (
    <FormControl component="fieldset" required={required}>
      <MuiRadioGroup
        name={name}
        value={value}
        onChange={onChange}
        disabled={disabled}
        classes={classes}
        {...addScopeAttribute(scope, 'radio-group')}
      >
        {choices.map((choice, index) => (
          <Choice
            key={index}
            {...choice}
            disabled={disabled || choice.disabled}
            {...addScopeProp(scope, 'radio-group')}
          />
        ))}
      </MuiRadioGroup>
    </FormControl>
  );
};

export default RadioGroup;

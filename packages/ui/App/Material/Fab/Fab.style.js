// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * Style Sheet for the {@link Fab} Fab component
 * @param {Object} theme
 * @return {JSS}
 */
export const fabStylesheet = () => ({});

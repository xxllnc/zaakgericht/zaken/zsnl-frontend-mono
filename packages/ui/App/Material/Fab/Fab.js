// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import MuiFab from '@mui/material/Fab';
import { withStyles } from '@mui/styles';
import { addScopeAttribute } from '../../library/addScope';
import Icon from '../Icon';
import { fabStylesheet } from './Fab.style';

/**
 * *Material Design* **Fab**.
 * - facade for *Material-UI* `Fab`
 *   additional props are passed through
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Fab
 * @see /npm-mintlab-ui/documentation/consumer/manual/Fab.html
 *
 * @param {Object} props
 * @param {Object} props.classes
 * @param {string} props.size
 * @param {ReactElement} props.children
 * @param {string} [props.scope]
 * @return {ReactElement}
 */
export const Fab = props => {
  const { classes, action, children, color, label, scope, size, sx } = props;

  return (
    <MuiFab
      classes={classes}
      color={color}
      aria-label={label}
      onClick={action}
      size={size}
      sx={sx}
      {...addScopeAttribute(scope, 'button')}
    >
      <Icon style={{ display: 'flex' }}>{children}</Icon>
    </MuiFab>
  );
};

export default withStyles(fabStylesheet)(Fab);

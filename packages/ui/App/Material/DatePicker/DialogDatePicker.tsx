// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import fecha from 'fecha';
import {
  usePickerState,
  WrapperVariantContext,
} from '@mui/x-date-pickers/internals';
import { PickersModalDialog } from '@mui/x-date-pickers/internals/components/PickersModalDialog';
import { KeyboardDateInput } from '@mui/x-date-pickers/internals/components/KeyboardDateInput';
import {
  datePickerValueManager,
  useDatePickerDefaultizedProps,
} from '@mui/x-date-pickers/DatePicker/shared';
import { useDateValidation } from '@mui/x-date-pickers/internals/hooks/validation/useDateValidation';
import { CalendarOrClockPicker } from '@mui/x-date-pickers/internals/components/CalendarOrClockPicker';
import { DatePickerToolbar } from '@mui/x-date-pickers/DatePicker/DatePickerToolbar';
import useForkRef from '@mui/material/utils/useForkRef';
import TextField from '../TextField';
import LocalizationProvider from './LocalizationProvider';
import { DatePickerPropsType } from './DatePicker.types';

function DatePickerWrapper(props: any) {
  const {
    children,
    onClear,
    onDismiss,
    onCancel,
    onAccept,
    onSetToday,
    open,
    DialogProps,
    components,
    componentsProps,
    DateInputProps,
  } = props;

  const ownInputRef = React.useRef<HTMLInputElement>(null);
  const inputRef = useForkRef(DateInputProps.inputRef, ownInputRef);

  return (
    <WrapperVariantContext.Provider value="desktop">
      <KeyboardDateInput {...DateInputProps} inputRef={inputRef} />
      <PickersModalDialog
        DialogProps={DialogProps}
        onAccept={onAccept}
        onClear={onClear}
        onDismiss={onDismiss}
        onCancel={onCancel}
        onSetToday={onSetToday}
        open={open}
        components={components}
        componentsProps={componentsProps}
      >
        {children}
      </PickersModalDialog>
    </WrapperVariantContext.Provider>
  );
}

//@ts-ignore
export const InternalDatePicker = inProps => {
  const props = useDatePickerDefaultizedProps(
    //@ts-ignore
    inProps,
    'MuiDesktopDatePicker'
  );

  //@ts-ignore
  const validationError = useDateValidation(props) !== null;
  const { pickerProps, inputProps, wrapperProps } = usePickerState(
    //@ts-ignore
    props,
    datePickerValueManager
  );

  const {
    onChange,
    ToolbarComponent = DatePickerToolbar,
    value,
    //@ts-ignore
    components,
    //@ts-ignore
    componentsProps,
    //@ts-ignore
    localeText,
    ...other
  } = props;

  const AllDateInputProps = {
    ...inputProps,
    ...other,
    components,
    componentsProps,
    validationError,
  };

  return (
    <DatePickerWrapper
      {...wrapperProps}
      DateInputProps={AllDateInputProps}
      components={components}
      componentsProps={componentsProps}
    >
      <CalendarOrClockPicker
        {...pickerProps}
        // eslint-disable-next-line jsx-a11y/no-autofocus
        autoFocus
        toolbarTitle={props.label || props.toolbarTitle}
        ToolbarComponent={ToolbarComponent}
        //@ts-ignore
        DateInputProps={AllDateInputProps}
        components={components}
        componentsProps={componentsProps}
        {...other}
      />
    </DatePickerWrapper>
  );
};

export const DialogDatePicker = ({
  value,
  onChange,
  name,
  clearable = true,
  disabled = false,
  placeholder = 'dd-mm-jjjj',
  // These are different, because fecha and date-fns expect
  // different format of date template strings
  // https://github.com/date-fns/date-fns/blob/main/docs/unicodeTokens.md
  displayFormat = 'dd-MM-yyyy',
  outputFormat = 'YYYY-MM-DD',
  ...rest
}: DatePickerPropsType) => {
  const parsedValue = value ? fecha.parse(value, outputFormat) : value;
  const [currentValue, setCurrentValue] = React.useState(parsedValue);

  const handleChange = (newValue: any) => {
    onChange &&
      //@ts-ignore
      !isNaN(new Date(newValue)) &&
      onChange({
        target: {
          name: name || '',
          value: fecha.format(newValue, outputFormat),
        },
      });
  };

  return (
    <LocalizationProvider>
      <InternalDatePicker
        disabled={disabled}
        value={currentValue}
        onAccept={handleChange}
        onChange={(val: any) => setCurrentValue(val)}
        inputFormat={displayFormat}
        clearable={clearable}
        renderInput={(params: any) => (
          <TextField
            {...params}
            onBlur={() => handleChange(currentValue)}
            inputProps={{ ...params.inputProps, placeholder, name }}
          />
        )}
        {...rest}
      />
    </LocalizationProvider>
  );
};

export default DialogDatePicker;

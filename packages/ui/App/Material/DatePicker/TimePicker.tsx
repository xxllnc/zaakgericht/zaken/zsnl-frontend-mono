// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { MobileTimePicker } from '@mui/x-date-pickers/MobileTimePicker';
import InputAdornment from '@mui/material/InputAdornment';
import TextField from '../TextField';
import Icon, { iconNames } from '../Icon/Icon';
import LocalizationProvider from './LocalizationProvider';

const TimePicker = (props: any) => (
  <LocalizationProvider>
    <MobileTimePicker
      {...props}
      renderInput={props => (
        //@ts-ignore
        <TextField
          {...props}
          endAdornment={
            <InputAdornment position="end">
              <Icon size="small">{iconNames.access_time}</Icon>
            </InputAdornment>
          }
        />
      )}
    />
  </LocalizationProvider>
);

export default TimePicker;

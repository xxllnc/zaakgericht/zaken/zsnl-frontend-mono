// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import nl from 'date-fns/locale/nl/index.js';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';

export default ({ children }: { children: any }) => (
  <LocalizationProvider
    dateAdapter={AdapterDateFns}
    adapterLocale={nl}
    localeText={{
      cancelButtonLabel: 'Annuleren',
      datePickerDefaultToolbarTitle: 'Selecteer een datum',
      dateTimePickerDefaultToolbarTitle: 'Selecteer een datum en tijd',
      timePickerDefaultToolbarTitle: 'Selecteer een tijd',
    }}
  >
    {children}
  </LocalizationProvider>
);

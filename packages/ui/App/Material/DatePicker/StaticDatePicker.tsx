// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { StaticDatePicker as DatePicker } from '@mui/x-date-pickers/StaticDatePicker';
//@ts-ignore
import { callOrNothingAtAll } from '@mintlab/kitchen-sink/source';
import TextField from '../TextField';
import { DatePickerPropsType } from './DatePicker.types';
import LocalizationProvider from './LocalizationProvider';

// outputFormat = 'yyyy-MM-dd',
export const StaticDatePicker = ({
  value,
  onChange,
  name,
  clearable = true,
  disabled = false,
  displayFormat = 'dd/mm/yyyy',
  placeholder = 'dd/mm/jjjj',
  sx,
  ...rest
}: DatePickerPropsType) => {
  const handleChange = (newValue: any) => {
    callOrNothingAtAll(onChange, () => [
      {
        target: {
          name,
          // value: utils.format(newValue, outputFormat),
          value: newValue,
        },
      },
    ]);
  };

  return (
    <LocalizationProvider>
      <DatePicker
        disabled={disabled}
        value={value}
        onChange={handleChange}
        onAccept={handleChange}
        inputFormat={displayFormat}
        components={{ ActionBar: () => null }}
        renderInput={params => (
          //@ts-ignore
          <TextField
            {...params}
            inputProps={{ ...params.inputProps, placeholder, name }}
          />
        )}
        {...rest}
      />
    </LocalizationProvider>
  );
};

export default StaticDatePicker;

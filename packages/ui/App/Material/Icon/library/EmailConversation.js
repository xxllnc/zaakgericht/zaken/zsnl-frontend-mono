// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import createSvgIcon from '@mui/icons-material/utils/createSvgIcon';

export default createSvgIcon(
  <React.Fragment>
    <path
      d="M4.3,15.3V5.9H3.7C2.8,5.9,2,6.6,2,7.6v8c0,2.7,2.2,4.9,4.9,4.9h11.8c0.9,0,1.7-0.7,1.6-1.7v-0.4H7.4
	C5.7,18.4,4.3,17,4.3,15.3z M20.4,4h-13C6.5,4,5.8,4.7,5.8,5.6v9.7c0,0.9,0.7,1.6,1.6,1.6h13c0.9,0,1.6-0.7,1.6-1.6V5.6
	C22,4.7,21.3,4,20.4,4z M20.4,7.2l-6.5,4.1L7.4,7.2V5.6l6.5,4.1l6.5-4.1V7.2z"
    />
  </React.Fragment>,
  'EmailConversation'
);

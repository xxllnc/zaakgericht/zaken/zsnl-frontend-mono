// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import MuiCard from '@mui/material/Card';
import MuiCardContent from '@mui/material/CardContent';
import MuiCardHeader from '@mui/material/CardHeader';
import { withStyles } from '@mui/styles';
import Render from '../../Abstract/Render';
import { addScopeAttribute } from '../../library/addScope';
import { cardStylesheet } from './Card.style';

/**
 * *Material Design* **Card**.
 * - facade for *Material-UI* `Card`, `CardHeader` and `CardContent`
 * - children are rendered in the `CardContent`,
 *   additional props are passed through
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Card
 * @see /npm-mintlab-ui/documentation/consumer/manual/Card.html
 *
 * @param {Object} props
 * @param {Object} props.classes
 * @param {ReactElement} props.children
 * @param {string} props.description
 * @param {string} props.title
 * @param {string} [props.scope]
 * @return {ReactElement}
 */
export const Card = props => {
  const { classes, children, title, description, scope, ...rest } = props;

  return (
    <MuiCard
      classes={{
        root: classes.card,
      }}
      {...addScopeAttribute(scope, 'card')}
      {...rest}
    >
      <Render condition={title || description}>
        <MuiCardHeader
          classes={{
            root: classes.header,
            title: classes.title,
            subheader: classes.subheader,
          }}
          title={title}
          subheader={description}
          {...addScopeAttribute(scope, 'card-header')}
        />
      </Render>

      <MuiCardContent
        classes={{
          root: classes.content,
        }}
        {...addScopeAttribute(scope, 'card-content')}
      >
        {children}
      </MuiCardContent>
    </MuiCard>
  );
};

export default withStyles(cardStylesheet)(Card);

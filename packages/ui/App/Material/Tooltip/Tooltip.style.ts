// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useTooltipStyles = makeStyles(
  ({ palette: { primary, common, error }, mintlab: { radius } }: Theme) => ({
    wrapper: {
      width: '100%',
    },
    all: {
      padding: '8px',
      maxWidth: '300px',
      borderRadius: radius.tooltip,
      '&&': {
        fontSize: 14,
      },
    },
    popper: {
      opacity: 1,
    },
    default: {
      backgroundColor: common.black,
    },
    error: {
      backgroundColor: error.dark,
    },
    info: {
      backgroundColor: primary.lightest,
      color: primary.main,
    },
  })
);

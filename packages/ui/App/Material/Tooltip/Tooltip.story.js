// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { React, stories, text, select, boolean } from '../../story';
import Tooltip from '.';

stories(module, __dirname, {
  Default() {
    return (
      <div
        style={{
          width: '100px',
        }}
      >
        <Tooltip
          title={text('Title', 'Hello, world!')}
          placement={select('Placement', [
            'bottom-end',
            'bottom-start',
            'bottom',
            'left-end',
            'left-start',
            'left',
            'right-end',
            'right-start',
            'right',
            'top-end',
            'top-start',
            'top',
          ])}
          type={select('Type', ['default', 'error', 'info'])}
          disabled={boolean('Disabled', false)}
        >
          <div
            style={{
              border: '2px solid black',
              padding: '5px',
            }}
          >
            Tooltip
          </div>
        </Tooltip>
      </div>
    );
  },
});

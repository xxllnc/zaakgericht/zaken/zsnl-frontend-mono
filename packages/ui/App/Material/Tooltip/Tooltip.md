# 🔌 `Tooltip` component

> *Material Design* **Tooltip**.

## See also

- [`Tooltip` stories](/npm-mintlab-ui/storybook/?selectedKind=Material/Tooltip)
- [`Tooltip` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#material-tooltip)

## External resources

- [*Material Guidelines*: Tooltips](https://material.io/design/components/tooltips.html)
- [*Material-UI* Tooltip API](https://material-ui.com/api/tooltip/)

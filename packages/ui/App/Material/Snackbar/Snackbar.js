// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Component } from 'react';
import equals from 'fast-deep-equal';
import { bind, callOrNothingAtAll } from '@mintlab/kitchen-sink/source';
import { withStyles } from '@mui/styles';
import SnackbarCmp from './Snackbar.cmp';
import { snackbarStyleSheet } from './Snackbar.style';

/**
 * *Material Design* **Snackbar**.
 * - facade for *Material-UI* `Snackbar`
 * - all props but `onQueueEmpty`, `onClose`, `onExited`, `open`, `action` and `classes`
 *   are spread to that component
 *
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Snackbar
 * @see /npm-mintlab-ui/documentation/consumer/manual/Snackbar.html
 * @see https://material-ui.com/api/snackbar/
 *
 * @reactProps {string} message
 * @reactProps {Function} onQueueEmpty
 * @reactProps {number} autoHideDuration
 * @reactProps {Object} anchorOrigin
 * @reactProps {Object} classes
 */
export class Snackbar extends Component {
  /**
   * @ignore
   */
  static get defaultProps() {
    return {
      open: false,
      autoHideDuration: 6000,
      key: new Date().getTime(),
      anchorOrigin: {
        vertical: 'bottom',
        horizontal: 'left',
      },
    };
  }

  /**
   * @param {Object} props
   */
  constructor(props) {
    super(props);
    this.queue = [props];
    this.state = {
      open: false,
      currentMessage: {},
    };
    bind(this, 'handleClose', 'handleExited');
  }

  // Lifecycle methods:

  /**
   * @see https://reactjs.org/docs/react-component.html#componentdidmount
   */
  componentDidMount() {
    this.processQueue();
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#componentdidupdate
   *
   * @param {Object} prevProps
   */
  componentDidUpdate(prevProps) {
    const { props, state, queue } = this;

    if (!equals(props, prevProps)) {
      queue.push(props);

      if (state.open) {
        this.setState({
          open: false,
        });
      } else {
        this.processQueue();
      }
    }
  }

  /**
   * @see https://reactjs.org/docs/react-component.html#render
   *
   * @return {ReactElement}
   */
  render() {
    const {
      state: { open, currentMessage },
      handleClose,
      handleExited,
      props: { scope, closeLabel = 'Close' },
    } = this;

    return (
      <SnackbarCmp
        handleClose={handleClose}
        handleExited={handleExited}
        open={open}
        scope={scope}
        closeLabel={closeLabel}
        {...currentMessage}
      />
    );
  }

  // Custom methods:

  /**
   * @param {Object} event
   * @param {string} reason
   */
  handleClose(event, reason) {
    if (reason === 'clickaway') {
      return;
    }

    this.setState({
      open: false,
    });
  }

  handleExited() {
    this.processQueue();
  }

  processQueue() {
    const {
      props: { onQueueEmpty },
      queue,
    } = this;

    if (queue.length) {
      this.setState({
        currentMessage: queue.shift(),
        open: true,
      });
    } else {
      callOrNothingAtAll(onQueueEmpty, () => []);
    }
  }
}

export default withStyles(snackbarStyleSheet)(Snackbar);

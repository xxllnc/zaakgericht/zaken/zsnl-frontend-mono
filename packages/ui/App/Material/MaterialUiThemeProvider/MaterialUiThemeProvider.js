// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createElement } from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { theme } from './library/theme';
import './library/roboto/roboto.css';

/**
 * Theme provider for wrapping the consumer's component tree root.
 *
 * @see /npm-mintlab-ui/documentation/consumer/manual/MaterialUiThemeProvider.html
 * @see https://material-ui.com/customization/themes/
 *
 * @param {Object} props
 * @param {*} props.children
 * @return {ReactElement}
 */
const MaterialUiThemeProvider = ({ children, ...rest }) =>
  createElement(
    ThemeProvider,
    {
      theme,
      ...rest,
    },
    children
  );

export default MaterialUiThemeProvider;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
// eslint-disable-next-line import/no-unresolved
import { shallow } from 'enzyme';
import { withTheme } from '@mui/styles';
import { theme as themeModule } from './library/theme';
import MaterialUiThemeProvider from '.';

/**
 * @test {MaterialUiThemeProvider}
 */
describe('The `MaterialUiThemeProvider` component', () => {
  test('makes the theme available to a `withTheme` HoC descendent', () => {
    function executor(resolve) {
      const App = ({ children }) => <div>{children}</div>;

      const ThemeConsumer = ({ theme }) => {
        resolve(theme);

        return null;
      };

      const ThemeConsumeHoc = withTheme(ThemeConsumer);

      shallow(
        <MaterialUiThemeProvider>
          <App>
            <ThemeConsumeHoc />
          </App>
        </MaterialUiThemeProvider>
      ).render();
    }

    return new Promise(executor).then(theme => {
      expect(theme).toEqual(themeModule);
    });
  });
});

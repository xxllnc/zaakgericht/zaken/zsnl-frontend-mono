# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [10.38.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.37.6...@mintlab/ui@10.38.0) (2022-01-26)


### Bug Fixes

* **ContactView:** Implement case status icons for archival_statusses transfer and destroy ([dcc17f5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/dcc17f55db9d20665eaf5711a45a2489c884bc8e))
* **DocumentExplorer:** MINTY-6618 - Give close search button a name ([8532327](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8532327ddddf82c0349afa249bc336035d8add98))
* **Geo:** MINTY-6540 - adjust map styling in object form ([3cbdfef](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3cbdfef00a9ab28c6e9b37b16cb287d4cb478815))
* **Select:** MINTY-8079 - fix input change not working properly ([b200a85](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b200a85c29dda1734fe3e74b0924d45e7e5fd242))
* **SortableTable:** MINTY-7005 - change logic for sorting values & dates ([069628c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/069628c79b892cd3ea051be220b013a7a4f4cca0))


### Features

* **Case:** Add relation tab with the related cases tables ([c4b3925](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c4b39253759c0c3831c61b9d287c70fc3afd061b))
* **Case:** MINTY-7111 - Add table planned cases to relation tab ([82080b6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/82080b60e390704d28564ec174083ed733342041))
* **Case:** MINTY-7963 - Implement contact importer in relation tab ([cad4334](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/cad433479c7608a2f6178acae2a27a1514f59bf4))
* **CaseRelations:** MINTY-7259 - implement sorting in case relations table ([ab6bcb3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ab6bcb3eb574d1420ac18a53b4fc46fa430831f2))
* **ContactView:** MINTY-5796 - Location tab in contact view ([34718f2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/34718f2258032d43726e9258e29e9d1d2c98fa33))
* **ContactView:** MINTY-6052 - add contact view setup ([f5ad9b4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f5ad9b4472f52cc0936ba7781c4943ab3e4b3a5e))
* **ContactView:** MINTY-6980, MINTY-6982: add Timeline for ContactView, add export feature ([795682b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/795682b957d92dba94879966ece1f0bd780dbe6f))
* **Contactview:** MINTY-7096 - Add overview and decorate table ([3c36e5e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3c36e5e0c4a18ec874776d6268f74f0d1327c3cc))
* **ContactView:** MINTY-7376, MINTY-7377, MINTY-7378 - rework Cases Table with autoloading, filtering, sorting from backend ([f83b0f9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f83b0f9cc8340efbdef7e92ed3984079ae4a0fdd))
* **DatePicker:** MINTY-5649 - change Datepicker icon color to darker ([af937e2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/af937e270a35e68239e3a1e08401e6c60b8a1969))
* **FileSelect:** MINTY-4836 - change FileSelect texts to account for multivalue option ([6d28c31](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6d28c3107adb361c869cab33b9e1a61c84ebb10a))
* **Intake:** MINTY-6443 - add option to reject document in intake ([88f50eb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/88f50eb53bf3d7a7b79eac8c450c0f7480fac966))
* **Intake:** MINTY-6733 - add document preview functionality ([9adb63b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9adb63b775f42df0bd6930974ab3df1b537a86a1))
* **Intake:** MINTY-8234 - change add to case dialog to show documentnames in case of multiple ([c7ec5e5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c7ec5e500fddeee1afea25685479fbd9d46aafa4))
* **Objects:** MINTY-7929 - add support for multivalue object attribute ([9d35477](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9d354772c8ae64e0dd9818752ea67e6fccf1be8b))
* **PIP:** MINTY-6621 - add misc. labeling features to PIP forms ([307a14f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/307a14fdfce396e1e3acd62ee76a3dd79f1a6fed))
* **Search:** MINTY-7847, MINTY-7853 - add filters to main search, open in new window ([9cb3fbd](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9cb3fbdf17bc533aaffa4fc03ebab9e778d054d9))
* **SortableTable:** MINTY-7238 - add re-ordering feature to Sortable Table ([5dceb99](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5dceb998a432ab03425f2da33ce363b1dd74308d))
* **Tasks:** MINTY-7248 - change filtering to server side for Tasks Dashboard Widget ([5cadb28](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5cadb28d58e1f755fcf217a5ad10475c5122afce))
* **Timeline:** MINTY-5337 - add Timeline component ([5ad8858](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ad885846dfa18a48da2245ee482cd49984fac15))
* **Timeline:** MINTY-8235 - add types filter to case timeline ([87aff18](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/87aff18ca80da0fb2572fccf1b41f1d56a736a6d))
* **Upload:** MINTY-7169 - add tooltip hover on uploaded files in case of backend error ([ef29380](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ef293807740d96d9fd96d641fc2c2994727cc8ce))





## [10.37.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.37.5...@mintlab/ui@10.37.6) (2020-12-21)

**Note:** Version bump only for package @mintlab/ui





## [10.37.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.37.4...@mintlab/ui@10.37.5) (2020-12-17)

**Note:** Version bump only for package @mintlab/ui





## [10.37.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.37.3...@mintlab/ui@10.37.4) (2020-11-19)

**Note:** Version bump only for package @mintlab/ui





## [10.37.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.37.2...@mintlab/ui@10.37.3) (2020-11-11)

**Note:** Version bump only for package @mintlab/ui





## [10.37.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.37.1...@mintlab/ui@10.37.2) (2020-11-11)

**Note:** Version bump only for package @mintlab/ui





## [10.37.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.37.0...@mintlab/ui@10.37.1) (2020-10-30)

**Note:** Version bump only for package @mintlab/ui





# [10.37.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.36.3...@mintlab/ui@10.37.0) (2020-10-08)


### Features

* **TasksWidget:** MINTY-4976 - add Tasks widget features with overlay form ([ba9c2bf](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ba9c2bf))
* **TasksWidget:** MINTY-4976 - change outputFormat to config ([069fd79](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/069fd79))
* **TasksWidget:** MINTY-4976 - fix crash bug with department finder labels ([b372f6e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b372f6e))





## [10.36.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.36.2...@mintlab/ui@10.36.3) (2020-10-02)

**Note:** Version bump only for package @mintlab/ui





## [10.36.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.36.1...@mintlab/ui@10.36.2) (2020-09-30)

**Note:** Version bump only for package @mintlab/ui





## [10.36.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.36.0...@mintlab/ui@10.36.1) (2020-09-17)

**Note:** Version bump only for package @mintlab/ui





# [10.36.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.35.2...@mintlab/ui@10.36.0) (2020-09-16)


### Features

* **Main:** MINTY-4759 - add menu items to main menu bar ([8ed4876](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8ed4876))





## [10.35.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.35.1...@mintlab/ui@10.35.2) (2020-09-16)

**Note:** Version bump only for package @mintlab/ui





## [10.35.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.35.0...@mintlab/ui@10.35.1) (2020-09-15)


### Bug Fixes

* **SortableTable:** MINTY-4869 - fix table sorting by clicking on header ([22b88c6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/22b88c6))





# [10.35.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.34.0...@mintlab/ui@10.35.0) (2020-09-04)


### Features

* **ObjectView:** Implement readOnly mode for objectView ([3ae03a7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3ae03a7))





# [10.34.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.33.0...@mintlab/ui@10.34.0) (2020-09-03)


### Features

* **ObjectView:** MINTY-4867, MINTY-4877 - add relationships overview to Object View ([57ef51d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/57ef51d))





# [10.33.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.32.4...@mintlab/ui@10.33.0) (2020-09-03)


### Features

* **Main:** MINTY-4758 - start new action from Object View ([1867c5b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1867c5b))





## [10.32.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.32.3...@mintlab/ui@10.32.4) (2020-09-03)

**Note:** Version bump only for package @mintlab/ui





## [10.32.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.32.2...@mintlab/ui@10.32.3) (2020-09-01)

**Note:** Version bump only for package @mintlab/ui





## [10.32.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.32.1...@mintlab/ui@10.32.2) (2020-09-01)

**Note:** Version bump only for package @mintlab/ui





## [10.32.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.32.0...@mintlab/ui@10.32.1) (2020-09-01)

**Note:** Version bump only for package @mintlab/ui





# [10.32.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.31.6...@mintlab/ui@10.32.0) (2020-08-20)


### Features

* **Main:** MINTY-4752 - add Search from Object View ([ab753fb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ab753fb))





## [10.31.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.31.5...@mintlab/ui@10.31.6) (2020-08-19)

**Note:** Version bump only for package @mintlab/ui





## [10.31.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.31.4...@mintlab/ui@10.31.5) (2020-08-11)

**Note:** Version bump only for package @mintlab/ui





## [10.31.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.31.3...@mintlab/ui@10.31.4) (2020-08-07)

**Note:** Version bump only for package @mintlab/ui





## [10.31.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.31.2...@mintlab/ui@10.31.3) (2020-08-07)

**Note:** Version bump only for package @mintlab/ui





## [10.31.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.31.1...@mintlab/ui@10.31.2) (2020-08-06)

**Note:** Version bump only for package @mintlab/ui





## [10.31.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.31.0...@mintlab/ui@10.31.1) (2020-08-06)

**Note:** Version bump only for package @mintlab/ui





# [10.31.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.30.3...@mintlab/ui@10.31.0) (2020-07-23)


### Features

* **Intake:** Add support in External Contact for importing companies ([c43bdeb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c43bdeb))





## [10.30.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.30.2...@mintlab/ui@10.30.3) (2020-07-23)

**Note:** Version bump only for package @mintlab/ui





## [10.30.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.30.1...@mintlab/ui@10.30.2) (2020-07-17)

**Note:** Version bump only for package @mintlab/ui





## [10.30.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.30.0...@mintlab/ui@10.30.1) (2020-07-15)

**Note:** Version bump only for package @mintlab/ui





# [10.30.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.29.18...@mintlab/ui@10.30.0) (2020-07-09)


### Features

* **External Contacts:** MINTY-4347: add External Contact search/import for persons ([ef299fe](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ef299fe))





## [10.29.18](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.29.13...@mintlab/ui@10.29.18) (2020-07-09)

**Note:** Version bump only for package @mintlab/ui





## [10.29.13](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.29.12...@mintlab/ui@10.29.13) (2020-07-02)

**Note:** Version bump only for package @mintlab/ui





## [10.29.12](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.29.11...@mintlab/ui@10.29.12) (2020-06-30)

**Note:** Version bump only for package @mintlab/ui





## [10.29.11](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.29.10...@mintlab/ui@10.29.11) (2020-06-26)

**Note:** Version bump only for package @mintlab/ui





## [10.29.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.29.9...@mintlab/ui@10.29.10) (2020-06-25)

**Note:** Version bump only for package @mintlab/ui





## [10.29.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.29.8...@mintlab/ui@10.29.9) (2020-06-23)

**Note:** Version bump only for package @mintlab/ui





## [10.29.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.29.7...@mintlab/ui@10.29.8) (2020-06-18)

**Note:** Version bump only for package @mintlab/ui





## [10.29.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.29.6...@mintlab/ui@10.29.7) (2020-06-12)

**Note:** Version bump only for package @mintlab/ui





## [10.29.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.29.5...@mintlab/ui@10.29.6) (2020-06-11)


### Bug Fixes

* **Forms:** MINTY-4191 - fix form validation for file uploads ([eb4291b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/eb4291b))





## [10.29.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.29.4...@mintlab/ui@10.29.5) (2020-06-09)


### Bug Fixes

* **Intake:** MINTY-4139 - fix error on sorting by date ([bcae3c4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/bcae3c4))





## [10.29.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.29.3...@mintlab/ui@10.29.4) (2020-06-05)

**Note:** Version bump only for package @mintlab/ui





## [10.29.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.29.2...@mintlab/ui@10.29.3) (2020-06-03)

**Note:** Version bump only for package @mintlab/ui





## [10.29.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.29.1...@mintlab/ui@10.29.2) (2020-05-29)

**Note:** Version bump only for package @mintlab/ui





## [10.29.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.29.0...@mintlab/ui@10.29.1) (2020-05-14)

**Note:** Version bump only for package @mintlab/ui





# [10.29.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.28.0...@mintlab/ui@10.29.0) (2020-05-14)


### Features

* **Intake:** MINTY-3868 - add description column to Intake ([c1c6df2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c1c6df2))





# [10.28.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.27.0...@mintlab/ui@10.28.0) (2020-05-13)


### Features

* **Catalog:** MINTY-3303 - add more details to Object View for Custom Object Type ([8959f71](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8959f71))





# [10.27.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.26.2...@mintlab/ui@10.27.0) (2020-05-12)


### Features

* **CustomObject:** MINTY-3937 & MINTY-3926 - change authorization to radiobuttons, refactor saving and getting, enable server error dialog ([a2fa2eb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a2fa2eb))





## [10.26.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.26.1...@mintlab/ui@10.26.2) (2020-05-12)

**Note:** Version bump only for package @mintlab/ui





## [10.26.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.26.0...@mintlab/ui@10.26.1) (2020-05-11)

**Note:** Version bump only for package @mintlab/ui





# [10.26.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.25.0...@mintlab/ui@10.26.0) (2020-05-01)


### Features

* **CreateObject:** MINTY-3472 - add Rights step to Object Create with associated components ([8699e78](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8699e78))





# [10.25.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.24.4...@mintlab/ui@10.25.0) (2020-04-30)


### Features

* **ObjectView:** MINTY-3779 Implement side menu for object attributes view ([ef41a85](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ef41a85))
* **SideMenu:** MINTY-3779 Add SideMenu component ([ec155a4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ec155a4))





## [10.24.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.24.3...@mintlab/ui@10.24.4) (2020-04-28)

**Note:** Version bump only for package @mintlab/ui





## [10.24.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.24.2...@mintlab/ui@10.24.3) (2020-04-16)

**Note:** Version bump only for package @mintlab/ui





## [10.24.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.24.1...@mintlab/ui@10.24.2) (2020-04-14)

**Note:** Version bump only for package @mintlab/ui





## [10.24.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.24.0...@mintlab/ui@10.24.1) (2020-04-10)

**Note:** Version bump only for package @mintlab/ui





# [10.24.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.23.3...@mintlab/ui@10.24.0) (2020-04-09)


### Features

* **BreadcrumbBar:** MINTY-3628 Implement header containing breadcrumb component which can be used in modules ([4a85e72](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4a85e72))





## [10.23.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.23.2...@mintlab/ui@10.23.3) (2020-04-06)

**Note:** Version bump only for package @mintlab/ui





## [10.23.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.23.1...@mintlab/ui@10.23.2) (2020-04-03)


### Bug Fixes

* **PipDocuments:** MINTY-3619 - fix PipDocuments not showing all rows ([67cf987](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/67cf987))





## [10.23.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.23.0...@mintlab/ui@10.23.1) (2020-03-30)

**Note:** Version bump only for package @mintlab/ui





# [10.23.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.22.0...@mintlab/ui@10.23.0) (2020-03-30)


### Features

* **Stepper:** MINTY-3485 Add Stepper UI component ([d0f77b6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d0f77b6))





# [10.22.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.21.1...@mintlab/ui@10.22.0) (2020-03-19)


### Features

* **DocumentExplorer:** MINTY-3300 - add initial PDF preview features, misc. ([a131814](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a131814))
* **DocumentExplorer:** MINTY-3300 - change Loader, exports ([b25d770](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b25d770))
* **DocumentExplorer:** MINTY-3300 - split up DocumentExplorer functionality, create new Intake entrypoint, add Selectable state to table ([5bae0d3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5bae0d3))





## [10.21.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.21.0...@mintlab/ui@10.21.1) (2020-03-17)

**Note:** Version bump only for package @mintlab/ui





# [10.21.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.20.3...@mintlab/ui@10.21.0) (2020-03-05)


### Bug Fixes

* **CaseDocuments:** apply MR comments ([778fd18](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/778fd18))


### Features

* **CaseDocuments:** add misc features ([8845760](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8845760))
* **CaseDocuments:** MINTY-2884: add file icons to FileBrowser based on mime type ([4223ab5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4223ab5))
* **CaseDocuments:** MINTY-2995 - enable creating of documents from file uploads ([2792d26](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2792d26))
* **CaseDocuments:** MINTY-3043, MINTY-3045: get documents from server, implement breadcrumb, error handling ([9519313](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9519313))
* **CaseDocuments:** MINTY-3191 - add misc. UX tweaks, code layout ([11658c1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/11658c1))
* **CaseDocuments:** MINTY-3191 - implement misc. UX/Design changes ([a2c44c3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a2c44c3))
* **DocumentPreview:** MINTY-2960 Add DocumentPreview component ([ef352ac](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ef352ac))
* **Documents:** MINTY-2861 - add initial CaseDocuments ([237e411](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/237e411))
* **SortableTable:** MINTY-3266 - add storybook story for Sortable Table ([42be482](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/42be482))





## [10.20.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.20.2...@mintlab/ui@10.20.3) (2020-03-03)

**Note:** Version bump only for package @mintlab/ui





## [10.20.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.20.1...@mintlab/ui@10.20.2) (2020-02-24)

**Note:** Version bump only for package @mintlab/ui





## [10.20.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.20.0...@mintlab/ui@10.20.1) (2020-02-21)

**Note:** Version bump only for package @mintlab/ui





# [10.20.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.19.4...@mintlab/ui@10.20.0) (2020-02-17)


### Features

* **MultiValueText:** MINTY-3076 Add entered value on blur ([5c098f8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5c098f8))





## [10.19.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.19.3...@mintlab/ui@10.19.4) (2020-02-10)

**Note:** Version bump only for package @mintlab/ui





## [10.19.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.19.2...@mintlab/ui@10.19.3) (2020-02-07)

**Note:** Version bump only for package @mintlab/ui

## [10.19.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.19.1...@mintlab/ui@10.19.2) (2020-02-06)

**Note:** Version bump only for package @mintlab/ui

## [10.19.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.19.0...@mintlab/ui@10.19.1) (2020-02-06)

**Note:** Version bump only for package @mintlab/ui

# [10.19.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.18.0...@mintlab/ui@10.19.0) (2020-01-10)

### Features

- **Communication:** MINTY-2692 Add role selection to email recipient form ([2122179](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2122179))
- **Communication:** MINTY-2693 Add EmailRecipient selection component ([c67d8e9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c67d8e9))

# [10.18.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.17.0...@mintlab/ui@10.18.0) (2020-01-09)

### Bug Fixes

- **Tasks:** fix npm error, date-fns ([6f2a1a3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6f2a1a3))
- **UI:** fix component imports ([797f9a9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/797f9a9))

### Features

- **CaseManagement:** add task creator ([939fbbb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/939fbbb))
- **DatePicker:** MINTY-2601 - add onClose ability to DatePicker ([732401e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/732401e))
- **Tasks:** add misc. style/UX improvements ([b619af4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b619af4))
- **Tasks:** MINTY-1575 - add basic form fields, DatePicker ([07ed5e5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/07ed5e5))
- **Tasks:** MINTY-1575 - add edit, delete, unlock functionality ([8dca70b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8dca70b))
- **Tasks:** MINTY-1575 - fix build/import problems ([23c0700](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/23c0700))
- **Tasks:** MINTY-2695 - rewrite internal logic to immediate responding to user actions ([c0f74c2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c0f74c2))

# [10.17.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.16.3...@mintlab/ui@10.17.0) (2020-01-09)

### Bug Fixes

- **Communication:** MINTY-2262 - allow e-mail addresses over multiple lines ([db44093](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/db44093))
- **UI:** Fix aria-labels on snackbar and dialogs ([775a287](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/775a287))

### Features

- **CaseAttributes:** add Numeric field ([61d9b6e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/61d9b6e))
- **Communication:** Allow reply form to scale up with content ([0d34d84](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0d34d84))
- **Communication:** MINTY-2664 Implement icon which indicates mulitple messages in the thread ([4a50cdb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4a50cdb))
- **Forms:** MINTY-2323 - add generic FormDialog component and implement throughout ([7f46465](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7f46465))

## [10.16.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.16.2...@mintlab/ui@10.16.3) (2019-12-19)

**Note:** Version bump only for package @mintlab/ui

## [10.16.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.16.1...@mintlab/ui@10.16.2) (2019-11-21)

**Note:** Version bump only for package @mintlab/ui

## [10.16.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.16.0...@mintlab/ui@10.16.1) (2019-10-25)

**Note:** Version bump only for package @mintlab/ui

# [10.16.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.15.2...@mintlab/ui@10.16.0) (2019-10-17)

### Features

- **Catalog:** MINTY-1551, MINTY-1548: add clickable foldername and url to internal registrationform to details, refactor large parts of the view code ([031bfff](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/031bfff))
- **Communication:** Add actionMenus to threadList, thread and message ([665d4b7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/665d4b7))
- **Theme:** adjust some theming for better contrast ([33d10c6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/33d10c6))
- **Thread:** Add attachments view with empty click ([2b613ed](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2b613ed))

## [10.15.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.15.1...@mintlab/ui@10.15.2) (2019-09-30)

**Note:** Version bump only for package @mintlab/ui

## [10.15.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.15.0...@mintlab/ui@10.15.1) (2019-09-26)

### Bug Fixes

- **FileUpload:** Handle filename overflow in file uploader ([90feeb5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/90feeb5))

# [10.15.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.14.0...@mintlab/ui@10.15.0) (2019-09-09)

### Bug Fixes

- **Typescript:** fix misc. errors ([a5eb790](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a5eb790))

### Features

- **TypeScript:** Add tsconfig.json, which is needed to enable typescript ([da89a6b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/da89a6b))

# [10.14.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.13.0...@mintlab/ui@10.14.0) (2019-09-06)

### Features

- **Communication:** MINTY-1294 Implement `CaseFinder` component in add `ContactMoment` form ([27af541](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/27af541))

# [10.13.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.12.0...@mintlab/ui@10.13.0) (2019-08-29)

### Bug Fixes

- **Communication:** Make icons with background white ([72610f1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/72610f1))

### Features

- **Communication:** add person/company icons to Contactfinder, some styling ([a2f2da2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a2f2da2))
- **Communication:** Add thread views for contactmoment and note ([e60867e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e60867e))
- **Communication:** MINTY-1115 Add `SearchField` to `Theads` ([5ec462a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ec462a))
- **Communication:** MINTY-1115 Add `Threads` filter component ([409738f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/409738f))
- **Communication:** MINTY-1115 Add direction icon to indicate incoming or outgoing messages ([6b8e4fa](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6b8e4fa))
- **Communication:** MINTY-1399 - add (temporary) colors for entitytypes ([4a0d023](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4a0d023))
- **Communication:** Process MR feedback ([8b17f80](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8b17f80))
- **Communication:** Tweak some styling ([bb1158d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/bb1158d))
- **Layout:** Let menu buttons be anchors to allow opening in new tab ([67f6910](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/67f6910))
- **UI:** Add IconRounded and ZsIcon ([c042b11](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c042b11))
- **UI:** Upgrade to MUI4 ([ac921f6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ac921f6))

# [10.12.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.11.0...@mintlab/ui@10.12.0) (2019-08-27)

### Features

- **UI:** MINTY-1472 - add stories for viewing / generating elements of the theme ([0c9db5f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0c9db5f))

# [10.11.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.10.0...@mintlab/ui@10.11.0) (2019-08-12)

### Features

- **ProximaNova:** MINTY-1306 Change font family to Proxima Nova ([2e3009f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2e3009f))

# [10.10.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.9.1...@mintlab/ui@10.10.0) (2019-08-06)

### Features

- **Admin:** MINTY-1281 Move admin app to mono repo ([f6a1d35](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f6a1d35))

## [10.9.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.9.0...@mintlab/ui@10.9.1) (2019-07-31)

**Note:** Version bump only for package @mintlab/ui

# [10.9.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.8.3...@mintlab/ui@10.9.0) (2019-07-31)

### Features

- **Communication:** add general Communication setup and Thread list ([7cb92e6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7cb92e6))

## [10.8.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@mintlab/ui@10.8.2...@mintlab/ui@10.8.3) (2019-07-25)

**Note:** Version bump only for package @mintlab/ui

## 10.8.2 (2019-07-23)

**Note:** Version bump only for package @mintlab/ui

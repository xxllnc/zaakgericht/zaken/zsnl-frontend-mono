// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { SizeInfo } from 'react-virtualized';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { useTableLoaderStyles } from './InfiniteTableLoader.styles';

const InfiniteTableLoader = ({
  loading,
  tableDimensions,
  offset = 0,
}: {
  loading: boolean;
  tableDimensions: SizeInfo | null;
  offset?: number;
}) => {
  const classes = useTableLoaderStyles();
  if (!loading || !tableDimensions || tableDimensions?.height < 10) return null;
  return (
    <div
      className={classes.loader}
      style={{
        top: tableDimensions.height - offset,
      }}
    >
      <Loader />
    </div>
  );
};

export default InfiniteTableLoader;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useSubHeaderStyles = makeStyles(
  ({ mintlab: { greyscale } }: Theme) => ({
    wrapper: {},
    titleWrapper: {
      display: 'flex',
    },
    title: {
      padding: '5px 0',
    },
    description: {
      padding: '5px 0',
      color: greyscale.evenDarker,
    },
  })
);

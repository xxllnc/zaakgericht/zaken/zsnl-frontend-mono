// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Modal from '@mui/material/Modal/Modal';
import Slide from '@mui/material/Slide';
import { DocumentPreview } from './DocumentPreview';
import { DocumentPreviewPropsType } from './DocumentPreview.types';

interface DocumentPreviewModalPropsType extends DocumentPreviewPropsType {
  open: boolean;
}

export const DocumentPreviewModal: React.ComponentType<
  DocumentPreviewModalPropsType
> = ({ open, ...props }) => {
  return (
    <Modal open={open} closeAfterTransition>
      <Slide direction="up" in={open}>
        <DocumentPreview {...props} />
      </Slide>
    </Modal>
  );
};

export default DocumentPreviewModal;

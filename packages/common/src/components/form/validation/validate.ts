// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as yup from 'yup';
import { FormValue } from '../types/formDefinition.types';

export function validate(
  schema: yup.ObjectSchema<any>,
  values: {
    [key: string]: FormValue | FormValue[];
  }
) {
  return schema.validate(values, { abortEarly: false });
}

export default validate;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as yup from 'yup';
import * as i18next from 'i18next';
import {
  FormDefinition,
  AnyFormDefinitionField,
} from '../types/formDefinition.types';
import { ValidationRule, createValidationRule } from './createValidationRule';

/**
 * Creates a Yup schema based on the provided formDefinition,
 * validationMap and translation function. If a field is hidden
 * or disabled, a given rule will not be added.
 *
 * If a custom rule for a field type is provided in the validationMap, that rule will be used.
 *
 * This schema is then validated against the provided form values.
 *
 * Returns a promise that resolves with the yupErrors object.
 **/

export type ValidationMap = {
  [key: string]: ValidationRule<any>;
};

interface CreateValidationOptions {
  formDefinition: FormDefinition;
  validationMap: ValidationMap;
  t: i18next.TFunction;
}

let localeSet = false;

const getRuleForField = (
  field: AnyFormDefinitionField,
  validationMap: ValidationMap,
  t: i18next.TFunction
) => {
  if (field.hidden || field.disabled || typeof field.type === 'undefined') {
    return;
  }

  return validationMap[field.name]
    ? validationMap[field.name]({ t, field })
    : createValidationRule(field, t);
};

const getRulesForFields = (
  fields: AnyFormDefinitionField[],
  validationMap: ValidationMap,
  t: i18next.TFunction
) => {
  return fields.reduce((accumulator, field) => {
    const resolvedRule = getRuleForField(field, validationMap, t);

    if (resolvedRule) {
      accumulator[field.name] = resolvedRule;
    }

    return accumulator;
  }, {} as { [k: string]: any });
};

export const createValidation = ({
  formDefinition,
  validationMap,
  t,
}: CreateValidationOptions) => {
  if (!localeSet) {
    yup.setLocale(t('common:validations.yup', { returnObjects: true }));
    localeSet = true;
  }

  const schema = getRulesForFields(formDefinition, validationMap, t);

  return yup.object().shape(schema);
};

export default createValidation;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { FormikProps } from 'formik';
import { ComponentType } from 'react';
import {
  GenericFormDefinitionFieldAttributes,
  RegisterValidationForNamespaceType,
} from './generic.types';
import { FormDefinitionField } from './fieldDefinition.types';

export type FormFieldPropsType<
  FormShape = any,
  Config = any,
  ValueType = unknown
> = {
  value: ValueType;
  checked: boolean;
  error?: string;
  touched: boolean;
  onChange: (event: React.ChangeEvent<any>) => void;
  onBlur: (event: React.SyntheticEvent) => void;
  submit?: () => void;
  validateForm: () => void;
  registerValidation: RegisterValidationForNamespaceType;
  formik: FormikProps<FormShape>;
} & GenericFormDefinitionFieldAttributes<FormShape, Config, ValueType> &
  Pick<
    FormikProps<FormShape>,
    'setFieldValue' | 'setFieldTouched' | 'registerField'
  >;

export interface FormRendererFormField<
  FormShape = any,
  Config = any,
  ValueType = unknown
> extends FormFieldPropsType<FormShape, Config, ValueType> {
  definition: FormDefinitionField<FormShape>;
  FieldComponent: ComponentType<FormFieldPropsType<FormShape>>;
}

export interface FormRendererFieldComponentPropsType<FormShape = any> {
  name: keyof FormShape;
  error?: string;
}

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { get } from '@mintlab/kitchen-sink/source';
import {
  AnyFormDefinitionField,
  AnyFormValueType,
} from '../../types/formDefinition.types';

export const updateFields =
  (fieldNames: string[], update: any) =>
  (field: AnyFormDefinitionField): AnyFormDefinitionField => ({
    ...field,
    ...(fieldNames.includes(field.name) ? update : {}),
  });

export type RuleEngineActionExecution = (
  fields: AnyFormDefinitionField[]
) => AnyFormDefinitionField[];
export type RuleEngineMultiFieldAction = (
  fieldNames: string[],
  value?: any
) => RuleEngineActionExecution;
export type RuleEngineSingleFieldAction = (
  fieldName: string,
  value:
    | AnyFormValueType
    | {
        [key: string]: AnyFormValueType;
      }
) => RuleEngineActionExecution;

export type RuleEngineAction =
  | RuleEngineSingleFieldAction
  | RuleEngineMultiFieldAction;

export const hideFields: RuleEngineMultiFieldAction = fieldNames => fields =>
  fields.map(updateFields(fieldNames, { hidden: true }));

export const showFields: RuleEngineMultiFieldAction = fieldNames => fields =>
  fields.map(updateFields(fieldNames, { hidden: false }));

export const setFieldValue: RuleEngineSingleFieldAction =
  (fieldName, value) => fields =>
    fields.map(updateFields([fieldName], { value }));

export const setDisabled: RuleEngineMultiFieldAction = fieldNames => fields =>
  fields.map(updateFields(fieldNames, { disabled: true }));

export const setEnabled: RuleEngineMultiFieldAction = fieldNames => fields =>
  fields.map(updateFields(fieldNames, { disabled: false }));

export const setRequired: RuleEngineMultiFieldAction = fieldNames => fields =>
  fields.map(updateFields(fieldNames, { required: true }));

export const setOptional: RuleEngineMultiFieldAction = fieldNames => fields =>
  fields.map(updateFields(fieldNames, { required: false }));

export const updateField: RuleEngineSingleFieldAction =
  (fieldName, update) => fields =>
    fields.map(updateFields([fieldName], update));

export const transferDataAsConfig =
  (
    sourceFieldName: string,
    targetFieldName: string,
    configKey: string,
    dataKey: string = 'value'
  ): RuleEngineActionExecution =>
  fields => {
    const getValue = () => {
      const field = fields.find(field => field.name === sourceFieldName) as any;
      if (dataKey === 'value') {
        return field?.value && field?.value.value
          ? field?.value.value
          : field?.value;
      } else {
        return get(field, dataKey);
      }
    };

    const config =
      fields.find(field => field.name === targetFieldName)?.config || {};

    return fields.map(
      updateFields([targetFieldName], {
        config: {
          ...config,
          //@ts-ignore
          [configKey]: getValue(),
        },
      })
    );
  };

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

type CustomFieldsType = {
  [key: string]: any;
};

type FormatCaseCustomFields = (customFields: CustomFieldsType) => any;

/* eslint complexity: [2, 12] */
const formatCaseCustomFields: FormatCaseCustomFields = customFields =>
  Object.keys(customFields).reduce((acc, key) => {
    const { type, value } = customFields[key];

    switch (type) {
      case 'file': {
        return {
          ...acc,
          [key]: {
            type,
            value: value.map((file: any) => ({
              label: file.filename,
              value: file.uuid,
              key: file.uuid,
            })),
          },
        };
      }
      case 'relationship': {
        const multiValue = !value.specifics;

        return {
          ...acc,
          [key]: {
            type,
            value: multiValue ? value.value : [value],
          },
        };
      }
      default: {
        return {
          ...acc,
          [key]: {
            type,
            value,
          },
        };
      }
    }
  }, {});

export default formatCaseCustomFields;

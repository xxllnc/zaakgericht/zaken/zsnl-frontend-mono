// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICaseManagement } from '@zaaksysteem/generated';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import {
  ReadonlyLinkContainerStyleSheet,
  iconStyleSheet,
} from './ReadonlyValuesContainer.styles';

interface ValueWithHrefType extends ValueType<string, string> {
  href?: string;
}

export const ReadonlyValuesContainer: React.ComponentType<{
  value: any;
  config?: any;
}> = ({ value, config }) => {
  const props = {
    style: config?.style || {
      whiteSpace: 'pre',
      padding: '8px 12px',
    },
    tabIndex: 0,
  } as const;

  if ((!value && value !== 0) || (Array.isArray(value) && value.length === 0)) {
    return <div {...props}>-</div>;
  } else {
    const normalizedValue = Array.isArray(value)
      ? value.map(val => val.toString()).join('\n')
      : value;
    return <div {...props}>{normalizedValue}</div>;
  }
};

/* eslint complexity: [2, 11] */
export const ReadonlyLinkContainer: React.ComponentType<{
  value: any;
  type?: string;
}> = ({ value, type }): any => {
  const props = {
    style: {
      whiteSpace: 'pre',
    },
    tabIndex: 0,
  } as const;

  const classes = ReadonlyLinkContainerStyleSheet();
  const iconClasses = iconStyleSheet();

  const getUrl = (value: ValueWithHrefType, type: string) => {
    switch (type) {
      case 'subject':
        return buildUrl<APICaseManagement.GetCustomObjectTypeRequestParams>(
          '/redirect/contact_page',
          { uuid: value.value }
        );
      case 'custom_object':
        return `/main/object/${value.value}`;
      default:
        throw new Error('Unknown type: ' + type);
    }
  };

  if (type === 'document' && !Array.isArray(value) && value) {
    return <ReadonlyValuesContainer value={value?.label} />;
  } else if (type && value) {
    const getLinkElement = (valueObj: ValueWithHrefType) => {
      return (
        <a
          {...props}
          href={getUrl(valueObj, type)}
          target="_blank"
          rel="noopener noreferrer"
          className={classes.container}
        >
          {valueObj.label}
          <Icon classes={iconClasses} size={18}>
            {iconNames.open_in_new}
          </Icon>
        </a>
      );
    };

    return Array.isArray(value)
      ? value.map(getLinkElement)
      : getLinkElement(value);
  } else if (value && !Array.isArray(value)) {
    const base = value.href || value;
    return (
      <a {...props} href={/^http/.test(base) ? base : '//' + base}>
        {value}
      </a>
    );
  } else {
    return <div {...props}>-</div>;
  }
};

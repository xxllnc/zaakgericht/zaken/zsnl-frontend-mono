// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import {
  AnyFormDefinitionField,
  FormValuesType,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { RelationshipTypeType } from '@zaaksysteem/common/src/components/form/fields/Relationship/Relationship';

type ZsValueRelationDefault = {
  value: string;
  specifics: {
    relationship_type: string;
    metadata: {
      summary: string;
      description?: string;
    };
  };
};

type ZsMultiValueRelationDefault = {
  value: ZsValueRelationDefault[];
};

type ZsValueRelationDocument = {
  value: string;
  specifics: {
    relationship_type: string;
    metadata: {
      filename: string;
    };
  };
};

type ZsValueRelation =
  | ZsValueRelationDefault
  | ZsValueRelationDocument
  | ZsMultiValueRelationDefault
  | null;

type TransformersType = {
  [key: string]: (
    value: any | any[],
    field: AnyFormDefinitionField
  ) => ZsValueRelation;
};

/* eslint complexity: [2, 12] */
const transformers: TransformersType = {
  relationship: (value, field) => {
    const relationshipType: RelationshipTypeType =
      field.config.relationshipType;

    const getValueObject = (currentValue: any) => {
      return {
        value: currentValue?.value,
        specifics: {
          relationship_type: relationshipType,
          metadata: {
            summary: currentValue.label,
            ...(currentValue.subLabel && {
              description: currentValue.subLabel,
            }),
          },
        },
      };
    };

    switch (relationshipType) {
      case 'custom_object':
      case 'subject': {
        const arrayValue = Array.isArray(value) ? value : [value];

        return value ? { value: arrayValue.map(getValueObject) } : null;
      }
      case 'document':
        return value?.value
          ? {
              value: value.value,
              specifics: {
                relationship_type: relationshipType,
                metadata: {
                  filename: value.label,
                },
              },
            }
          : null;
    }
  },
};

const generateCustomFieldValues = (
  values: FormValuesType,
  fields: AnyFormDefinitionField[]
) =>
  Object.entries(values).reduce((acc, [key, value]) => {
    const field = fields.find(field => field.name === key);

    if (!field || value === undefined) {
      return acc;
    }

    const transformedValue = Object.prototype.hasOwnProperty.call(
      transformers,
      field.type
    )
      ? transformers[field?.type](value, field)
      : { value };

    return {
      ...acc,
      [key]: transformedValue,
    };
  }, {});

export default generateCustomFieldValues;

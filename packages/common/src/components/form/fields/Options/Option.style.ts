// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Theme } from '@mintlab/ui/types/Theme';
import { makeStyles } from '@mui/styles';

export const useOptionStylesheet = makeStyles(
  ({ palette, mintlab: { greyscale, shadows } }: Theme) => ({
    wrapper: {
      background: palette.common.white,
      padding: '6px',
      display: 'flex',
      alignItems: 'center',
      borderRadius: '4px',
      marginBottom: '16px',
      boxShadow: shadows.flat,
    },
    dragging: {
      backgroundColor: greyscale.dark,
    },
    handle: {
      color: greyscale.darker,
    },
    delete: {},
    label: {
      flex: 1,
      marginLeft: '12px',
      overflow: 'hidden',
      fontFamily: '"Roboto", "Helvetica", "Arial", sans-serif',
    },
  })
);

export type OptionClassesType = ReturnType<typeof useOptionStylesheet>;

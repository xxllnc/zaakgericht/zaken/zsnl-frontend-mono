// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as yup from 'yup';
import deepEqual from 'fast-deep-equal';
import { generateMagicString } from './MagicStringGenerator.library';

let timeoutId: null | number = null;
const DELAY = 200;

const isValidMagicstring = ({ t, field }: any) => {
  const { required } = field;
  function validateFunc(value = '' as string | null) {
    if (!value) {
      return false;
    }

    return new Promise(resolve => {
      if (timeoutId) {
        window.clearTimeout(timeoutId);
      }
      timeoutId = window.setTimeout(() => {
        resolve(undefined);
      }, DELAY);
    })
      .then(() => generateMagicString(value, () => {}))
      .then(magicstring => {
        if (!deepEqual(magicstring, value)) {
          //@ts-ignore
          return this.createError({
            message: t('validations:string.invalidMagicString', {
              suggestion: magicstring,
            }),
            path: 'magic_string',
          });
        }
        return true;
      });
  }

  const base = yup.string();

  return required
    ? base.required().test('is-valid-magicstring', '', validateFunc)
    : base.test('is-valid-magicstring', '', validateFunc);
};

export default isValidMagicstring;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useQuery } from '@tanstack/react-query';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICommunication } from '@zaaksysteem/generated';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';

export type CaseStatusType = 'new' | 'open' | 'stalled' | 'resolved';

export const useCaseChoicesQuery = (t: any, statusFilter: CaseStatusType[]) => {
  const [input, setInput] = React.useState<string | undefined>(undefined);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const enabled = Boolean(input);

  const data = useQuery(
    ['cases', input, statusFilter.join('')],
    async ({ queryKey: [__, keyword, filter] }) => {
      const body = await request<APICommunication.SearchCaseResponseBody>(
        'GET',
        buildUrl<APICommunication.SearchCaseRequestParams>(
          `/api/v2/communication/search_case`,
          {
            ...(filter ? { filter: { status: statusFilter } } : {}),
            search_term: keyword || '',
            minimum_permission: 'read',
            limit: 25,
          }
        )
      ).catch(openServerErrorDialog);

      return body
        ? (body.data || []).map(
            ({
              id,
              attributes: { display_id, case_type_name, status, description },
            }) => {
              const label = `${display_id}: ${case_type_name}`;
              const subLabel = [t(`case.status.${status}`), description]
                .filter(item => item)
                .join(' - ');

              return {
                value: id,
                label,
                subLabel,
              };
            }
          )
        : [];
    },
    { enabled }
  );

  const selectProps = {
    onInputChange: (ev: any, val: any, reason: any) =>
      ['input', 'clear'].includes(reason) && setInput(val),
    loading: data.status === 'loading' && enabled,
    choices: data.data || [],
  };

  return [selectProps, ServerErrorDialog] as const;
};

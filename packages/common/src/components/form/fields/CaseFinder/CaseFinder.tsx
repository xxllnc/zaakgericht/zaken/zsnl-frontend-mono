// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import Select, {
  renderMultilineOption,
} from '@mintlab/ui/App/Zaaksysteem/Select';
import { FormFieldComponentType } from '../../types/Form2.types';
import { CaseStatusType, useCaseChoicesQuery } from './CaseFinder.library';

const CaseFinder: FormFieldComponentType<
  ValueType<string>,
  { filter: { status: CaseStatusType[] } }
> = props => {
  const [t] = useTranslation('common');
  const [selectProps, ServerErrorDialog] = useCaseChoicesQuery(
    t,
    props?.config?.filter?.status || []
  );

  return (
    <>
      <Select
        {...props}
        {...selectProps}
        renderOption={renderMultilineOption}
        filterOption={() => true}
      />
      {ServerErrorDialog}
    </>
  );
};

export default CaseFinder;

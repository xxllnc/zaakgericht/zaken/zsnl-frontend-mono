// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useCheckboxGroupStyles = makeStyles({
  list: {
    margin: 0,
    padding: 0,
    listStyle: 'none',
  },
});

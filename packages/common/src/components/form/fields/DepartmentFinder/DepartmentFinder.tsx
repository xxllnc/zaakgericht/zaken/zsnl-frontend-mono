// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import Select, {
  renderMultilineOption,
  renderTagsWithIcon,
} from '@mintlab/ui/App/Zaaksysteem/Select';
import { useDepartmentChoicesQuery } from './DepartmentFinder.library';
import { DepartmentFinderOptionType } from './DepartmentFinder.types';

type DepartmentFinderPropsType = {
  value: DepartmentFinderOptionType;
  multiValue?: any;
  styles?: any;
  config?: any;
  [key: string]: any;
};

export const DepartmentFinder: FunctionComponent<DepartmentFinderPropsType> = ({
  value,
  multiValue = false,
  styles,
  config = {},
  ...restProps
}: any) => {
  const [selectProps, ServerErrorDialog, emptyChoicesResult] =
    useDepartmentChoicesQuery();

  return (
    <>
      <Select
        {...restProps}
        {...selectProps}
        value={value}
        disabled={Boolean(restProps?.disabled || emptyChoicesResult)}
        isMulti={multiValue}
        renderOption={renderMultilineOption}
        renderTags={renderTagsWithIcon('people')}
      />
      {ServerErrorDialog}
    </>
  );
};

export default DepartmentFinder;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery } from '@tanstack/react-query';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICaseManagement } from '@zaaksysteem/generated';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { DepartmentFinderOptionType } from './DepartmentFinder.types';

export const useDepartmentChoicesQuery = () => {
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  const data = useQuery(
    ['departments'],
    async ({ queryKey: [__, keyword] }) => {
      const body = await request<APICaseManagement.GetDepartmentsResponseBody>(
        'GET',
        '/api/v2/cm/authorization/get_departments'
      ).catch(openServerErrorDialog);
      const data = body
        ? (body.data || []).map(department => ({
            id: department.id,
            name: department.attributes?.name || '',
            parent: department?.relationships?.parent?.data?.id || null,
          }))
        : [];

      const optionsReducer =
        (depth: number) =>
        (
          acc: DepartmentFinderOptionType[],
          current: { name: string; id: string }
        ) => {
          acc = [
            ...acc,
            {
              label: current.name,
              value: current.id,
              key: current.id,
              depth,
              type: 'department',
            },
          ];

          const children = data.filter(
            thisItem => thisItem.parent === current.id
          );
          if (children.length) {
            acc = children.reduce(optionsReducer(depth + 1), acc);
          }
          return acc;
        };

      return data
        .filter(thisItem => thisItem.parent === null)
        .reduce(optionsReducer(0), []);
    }
  );

  const selectProps = {
    loading: data.status === 'loading',
    choices: data.data || [],
  };

  return [
    selectProps,
    ServerErrorDialog,
    data.status === 'success' && selectProps.choices.length === 0,
  ] as const;
};

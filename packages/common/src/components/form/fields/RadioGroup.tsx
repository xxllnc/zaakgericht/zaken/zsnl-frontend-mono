// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import MaterialRadioGroup from '@mintlab/ui/App/Material/RadioGroup';
import { FormFieldPropsType } from '../types/formDefinition.types';
import { ReadonlyValuesContainer } from '../library/ReadonlyValuesContainer';

const RadioGroup: React.FunctionComponent<FormFieldPropsType> = props => {
  const { readOnly, value } = props;

  return readOnly ? (
    <ReadonlyValuesContainer value={value} />
  ) : (
    <MaterialRadioGroup {...props} />
  );
};

export default RadioGroup;

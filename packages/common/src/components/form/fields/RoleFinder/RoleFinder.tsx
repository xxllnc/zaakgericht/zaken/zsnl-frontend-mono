// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { FunctionComponent } from 'react';
import Select, { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import { useRoleChoicesQuery } from './RoleFinder.library';

type RoleFinderPropsType = {
  value: ValueType<string>;
  name: string;
  config?: any;
  [key: string]: any;
};

const RoleFinder: FunctionComponent<RoleFinderPropsType> = ({
  value,
  name,
  config,
  ...restProps
}) => {
  const parentRoleUuid = config?.parentRoleUuid;
  const [selectProps, ServerErrorDialog, emptyChoicesResult] =
    useRoleChoicesQuery(parentRoleUuid);

  return (
    <>
      <Select
        {...restProps}
        {...selectProps}
        name={name}
        value={value}
        disabled={Boolean(restProps?.disabled) || emptyChoicesResult}
      />
      {ServerErrorDialog}
    </>
  );
};

export default RoleFinder;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useQuery } from '@tanstack/react-query';
import { request } from '@zaaksysteem/common/src/library/request/request';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';

type ChoiceType = {
  type: string;
  id: any;
  attributes?: {
    name: string;
    code: number;
  };
};

export const useCountryChoicesQuery = () => {
  const [input, setInput] = React.useState<string | undefined>(undefined);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const enabled = Boolean(input);

  const data = useQuery(
    ['countries', input],
    async ({ queryKey: [__, keyword] }) => {
      const body = await request<any>(
        'GET',
        '/api/v2/cm/contact/get_countries'
      ).catch(openServerErrorDialog);

      let unknown: any;

      const countries = body
        ? ((body.data as Array<ChoiceType>) || [])
            .map(choice => {
              return {
                label: choice?.attributes?.name || '',
                value: choice?.attributes?.code,
              };
            })
            .filter(choice => {
              if (choice?.value === 0) unknown = choice;
              return choice?.value !== 0;
            })
            .sort((aChoice, bChoice) =>
              aChoice.label > bChoice.label ? 1 : -1
            )
        : [];

      return !countries || !countries?.length
        ? []
        : [...[unknown], ...countries];
    },
    { enabled }
  );

  const selectProps = {
    onInputChange: (ev: any, val: any, reason: any) =>
      ['input', 'clear'].includes(reason) && setInput(val),
    loading: data.status === 'loading' && enabled,
    choices: data.data || [],
  };

  return [
    selectProps,
    ServerErrorDialog,
    data.status === 'success' && selectProps.choices.length === 0,
  ] as const;
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { FormValue } from '../../types/formDefinition.types';
import { RoleAndSubjectType } from '../CaseRoleFinder/CaseRoleFinder.types';

export type CaseRequestorConfigType = {
  valueResolver?: (role: RoleAndSubjectType) => CaseRequestorType;
  itemFilter?: (role: RoleAndSubjectType) => boolean;
  errorMessage?: string;
  caseUuid: string;
};

export type CaseRequestorType = {
  value: FormValue;
  label: string | React.ReactElement;
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useStyles = makeStyles(() => {
  return {
    wrapper: {
      display: 'flex',
      alignContent: 'flex-start',
      padding: '0px 20px 4px 20px',
    },
  };
});

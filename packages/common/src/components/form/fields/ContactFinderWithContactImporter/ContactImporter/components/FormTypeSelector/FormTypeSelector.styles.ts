// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useFormTypeSelectorStyles = makeStyles(
  ({ mintlab: { greyscale, radius } }: Theme) => {
    return {
      formTypeSelector: {
        marginBottom: 28,
        justifyContent: 'flex-end',
        display: 'flex',
        flexDirection: 'column',
      },
      radioSelector: {
        flexDirection: 'row',
        marginBottom: 6,
      },
      selectWrapper: {
        display: 'flex',
        '&>*:nth-child(1)': {
          alignSelf: 'center',
          marginRight: 16,
        },
      },
      select: {
        backgroundColor: greyscale.light,
        borderRadius: radius.defaultFormElement,
        flex: 1,
      },
    };
  }
);

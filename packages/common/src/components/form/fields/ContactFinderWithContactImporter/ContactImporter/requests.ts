// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { request } from '@zaaksysteem/common/src/library/request/request';
import { StoreShapeType, AnyTypeFields, AnyRecordType } from './types';

export const fetchSession = async () =>
  request<any>('GET', '/api/v1/session/current');

export const fetchModulesConfig = async () =>
  request<any>(
    'GET',
    '/api/v1/sysin/interface/get_by_module_name/stufconfig?page=1&rows_per_page=20'
  );

export const fetchRemote = async ({
  fields,
  selectedFormType,
  selectedInterface,
}: {
  fields: AnyTypeFields;
} & Pick<StoreShapeType, 'selectedFormType' | 'selectedInterface'>) => {
  const getUrl = () => {
    if (selectedInterface === 'local') {
      return '/api/v1/subject?page=1&rows_per_page=100';
    } else {
      return selectedFormType === 'person'
        ? `/api/v1/subject/remote_search/${selectedInterface}`
        : `/api/v1/subject/remote_search/`;
    }
  };

  const body = {
    query: {
      match: {
        subject_type: selectedFormType,
        ...fields,
      },
    },
  };

  return request<any>('POST', getUrl(), body).then(response => {
    return response.result.instance.rows || [];
  });
};

export const remoteImport = ({
  selectedInterface,
  body,
  selectedFormType,
}: {
  body: AnyRecordType;
} & Pick<StoreShapeType, 'selectedInterface' | 'selectedFormType'>) => {
  const getUrl = () => {
    if (selectedFormType === 'person' && selectedInterface !== 'local') {
      return `/api/v1/subject/remote_import/${selectedInterface}`;
    } else {
      return '/api/v1/subject/remote_import/';
    }
  };

  return request<any>('POST', getUrl(), body).then(response => {
    return response.result.instance || null;
  });
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const locale = {
  nl: {
    types: {
      person: 'Persoon',
      company: 'Organisatie',
    },
    fields: {
      rsin: 'RSIN',
      bsn: 'BSN',
      dateOfBirth: 'Geboortedatum',
      familyName: 'Achternaam',
      prefix: 'Voorvoegsel',
      zipCode: 'Postcode',
      streetNumber: 'Huisnummer',
      streetNumberLetter: 'Huisletter',
      streetNumberSuffix: 'Huisnummertoevoeging',
      cocNumber: 'KVK-nummer',
      cocLocationNumber: 'Vestigingsnummer',
      street: 'Straatnaam',
    },
    warnings: {
      isDeceased: 'Contact is overleden.',
      isSecret: 'Contact heeft geheimhouding.',
      hasCorrespondenceAddress: 'Contact heeft correspondentie-adres.',
      isInResearch: 'Contact is in onderzoek.',
    },
    columns: {
      name: 'Naam',
      dateOfBirth: 'Geboortedatum',
      address: 'Adres',
      cocNumber: 'KVK-nr.',
      cocLocationNumber: 'Vestigingsnr.',
      companyName: 'Handelsnaam',
    },
    importContact: 'Importeer een contact',
    import: 'Importeren',
    confirm: 'Weet u zeker dat u {{preview}} wilt importeren?',
    noResults:
      'Geen resultaten gevonden. Verfijn de zoekopdracht en probeer opnieuw.',
    maxRows:
      'Er worden maximaal {{rows}} resultaten weergegeven. Verfijn eventueel de zoekopdracht.',
    searchIn: 'Zoeken in',
    local: 'Zaaksysteem (intern)',
  },
};

export default locale;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useResultsTableStyles = makeStyles(
  ({ palette: { coral } }: Theme) => {
    return {
      maxRowsMessage: {
        marginBottom: 12,
      },
      male: {
        color: '#42a3fe',
      },
      female: {
        color: '#e647a2',
      },
      nameCell: {
        display: 'flex',
        alignItems: 'center',
        '&>* :last-child': {
          color: coral.darker,
          marginLeft: 6,
          maxWidth: 'min-content',
        },
      },
    };
  }
);

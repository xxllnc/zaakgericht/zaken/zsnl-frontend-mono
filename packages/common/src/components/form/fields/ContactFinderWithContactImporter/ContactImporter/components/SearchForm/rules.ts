// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  Rule,
  setRequired,
  setOptional,
  hasValue,
} from '@zaaksysteem/common/src/components/form/rules';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { PersonFields, CompanyFields } from '../../types';

const allEmpty = (fields: AnyFormDefinitionField[]) => {
  const withValues = Object.values(fields).filter(field => hasValue(field));
  return !withValues.length;
};

const findField = (fields: AnyFormDefinitionField[], fieldName: string) =>
  fields.filter((field: any) => field.name === fieldName)[0];

/* eslint complexity: [2, 20] */
const personFormValidations =
  (fieldNames: any) => (fields: AnyFormDefinitionField[]) => {
    const familyName = findField(fields, 'familyName');
    const dateOfBirth = findField(fields, 'dateOfBirth');
    const bsn = findField(fields, 'bsn');
    const zipCode = findField(fields, 'zipCode');
    const streetNumber = findField(fields, 'streetNumber');

    fields = setOptional([
      'bsn',
      'familyName',
      'zipCode',
      'streetNumber',
      'dateOfBirth',
    ])(fields);

    const validities = {
      bsn: Boolean(bsn.value),
      familyDOB: Boolean(familyName.value && dateOfBirth.value),
      address: Boolean(zipCode.value && streetNumber.value),
    };

    if (!allEmpty(fields)) {
      if (bsn.value && !validities.familyDOB && !validities.address) {
        fields = setRequired(['bsn'])(fields);
      }

      if (
        (familyName.value || dateOfBirth.value) &&
        !validities.bsn &&
        !validities.address
      ) {
        fields = setRequired(['familyName', 'dateOfBirth'])(fields);
      }

      if (
        (zipCode.value || streetNumber.value) &&
        !validities.bsn &&
        !validities.familyDOB
      ) {
        fields = setRequired(['zipCode', 'streetNumber'])(fields);
      }
    } else {
      fields = setRequired(['bsn'])(fields);
    }

    return fields;
  };

export const personRules = [
  new Rule<PersonFields>().when(fields => true).then(personFormValidations('')),
];

export const companyRules = [
  new Rule<CompanyFields>()
    .when(fields => allEmpty(fields as any))
    .then(
      setRequired([
        'rsin',
        'cocNumber',
        'cocLocationNumber',
        'name',
        'zipCode',
        'number',
        'company',
      ])
    )
    .else(
      setOptional([
        'rsin',
        'cocNumber',
        'cocLocationNumber',
        'name',
        'zipCode',
        'number',
        'company',
      ])
    ),
  new Rule<CompanyFields>()
    .when('rsin', field => hasValue(field))
    .then(setRequired(['rsin'])),
  new Rule<CompanyFields>()
    .when('company', field => hasValue(field))
    .then(setRequired(['company'])),
  new Rule<CompanyFields>()
    .when('cocNumber', field => hasValue(field))
    .then(setRequired(['cocNumber'])),
  new Rule<CompanyFields>()
    .when('cocLocationNumber', field => hasValue(field))
    .then(setRequired(['cocLocationNumber'])),
  new Rule<CompanyFields>()
    .when('street', field => hasValue(field))
    .then(setRequired(['street', 'zipCode', 'number'])),
  new Rule<CompanyFields>()
    .when('zipCode', field => hasValue(field))
    .then(setRequired(['zipCode', 'number'])),
];

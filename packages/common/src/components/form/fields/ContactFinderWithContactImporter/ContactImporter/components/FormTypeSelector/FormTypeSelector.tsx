// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
//@ts-ignore
import RadioGroup from '@mintlab/ui/App/Material/RadioGroup';
import FlatValueSelect from '@zaaksysteem/common/src/components/form/fields/FlatValueSelect';
import Typography from '@mui/material/Typography';
import { StoreShapeType } from '../../types';
import {
  getInterfacesChoicesPerson,
  getInterfacesChoicesCompany,
} from '../../library';
import { useFormTypeSelectorStyles } from './FormTypeSelector.styles';

type FormTypeSelectorPropsType = {
  onChangeFormType: (event: React.ChangeEvent) => void;
  onChangeInterface: (event: React.ChangeEvent) => void;
  t: i18next.TFunction;
} & Pick<
  StoreShapeType,
  | 'allowedFormTypes'
  | 'interfaces'
  | 'selectedFormType'
  | 'selectedInterface'
  | 'externalSearchAllowed'
>;

/* eslint complexity: [2, 12] */
const FormTypeSelector: React.ComponentType<FormTypeSelectorPropsType> = ({
  selectedFormType,
  onChangeFormType,
  onChangeInterface,
  interfaces,
  allowedFormTypes,
  selectedInterface,
  externalSearchAllowed,
  t,
}) => {
  const classes = useFormTypeSelectorStyles();
  const choices = [
    ...(allowedFormTypes.includes('person')
      ? [
          {
            label: t('ContactImporter:types.person'),
            value: 'person',
          },
        ]
      : []),
    ...(allowedFormTypes.includes('company')
      ? [
          {
            label: t('ContactImporter:types.company'),
            value: 'company',
          },
        ]
      : []),
  ];

  return (
    <div className={classes.formTypeSelector}>
      <RadioGroup
        choices={choices}
        name="formType"
        value={selectedFormType}
        scope="form-type-selector"
        onChange={onChangeFormType}
        classes={{
          root: classes.radioSelector,
        }}
      />
      {externalSearchAllowed && (
        <div className={classes.selectWrapper}>
          <Typography variant="body1">
            {t('ContactImporter:searchIn') + ':'}
          </Typography>
          <div className={classes.select}>
            <FlatValueSelect
              value={selectedInterface}
              choices={
                selectedFormType === 'person'
                  ? getInterfacesChoicesPerson(interfaces, t)
                  : getInterfacesChoicesCompany(externalSearchAllowed, t)
              }
              onChange={onChangeInterface}
            />
          </div>
        </div>
      )}
    </div>
  );
};

export default FormTypeSelector;

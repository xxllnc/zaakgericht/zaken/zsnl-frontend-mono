// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import Button from '@mintlab/ui/App/Material/Button';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { FormRendererFormField } from '@zaaksysteem/common/src/components/form/types/fieldComponent.types';
import ResultsTable from '../ResultsTable/ResultsTable';
import { StoreShapeType, SelectRowType, AnyTypeFields } from '../../types';
import FormTypeSelector from '../FormTypeSelector/FormTypeSelector';
import {
  setFormTypeAction,
  setSelectedInterfaceAction,
  clearSearchResultsAction,
} from '../../store/actions';
import { getFormDefinition } from '../../formDefinition';
import { useSearchFormStyles } from './SearchForm.styles';
import { getFormattedRows } from './library';
import { personRules, companyRules } from './rules';

type SearchFormPropsType = {
  onSearch: (fields: AnyTypeFields) => void;
  onSelectRow: SelectRowType;
  t: i18next.TFunction;
  state: StoreShapeType;
  dispatch: React.Dispatch<any>;
};

const isFormEmpty = (values: any) =>
  Object.values(values).filter((fieldValue: any) => Boolean(fieldValue))
    .length === 0;

const SearchForm: React.ComponentType<SearchFormPropsType> = ({
  onSearch,
  onSelectRow,
  state,
  dispatch,
  t,
}) => {
  const classes = useSearchFormStyles();
  const {
    selectedFormType,
    allowedFormTypes,
    selectedInterface,
    interfaces,
    loading,
    rows,
    externalSearchAllowed,
  } = state;
  const formDefinition = getFormDefinition({ type: selectedFormType, t });

  const { fields, formik } = useForm<any>({
    formDefinition: formDefinition,
    isInitialValid: false,
    rules: selectedFormType === 'person' ? personRules : companyRules,
    onSubmit: () => {},
    onChange: () => {},
  });

  const { values, isValid } = formik;
  const emptyForm = isFormEmpty(values);

  const renderField = (field: FormRendererFormField) => {
    const {
      FieldComponent,
      name,
      label,
      applyBackgroundColor,
      error,
      touched,
      ...rest
    } = field;
    const { required, help, hint } = rest;

    return (
      <FormControlWrapper
        label={label}
        help={help}
        hint={hint}
        error={error}
        touched={touched}
        required={required}
        applyBackgroundColor={applyBackgroundColor}
        key={`contact-importer-search-form-${name}`}
        compact={false}
        className={classes.formControlWrapper}
      >
        <FieldComponent {...field} />
      </FormControlWrapper>
    );
  };

  return (
    <div className={classes.searchFormWrapper}>
      <div className={classes.form}>
        <FormTypeSelector
          selectedFormType={selectedFormType}
          selectedInterface={selectedInterface}
          allowedFormTypes={allowedFormTypes}
          interfaces={interfaces}
          onChangeFormType={({ target }: React.ChangeEvent<any>) => {
            dispatch(clearSearchResultsAction());
            dispatch(setFormTypeAction(target.value));
          }}
          onChangeInterface={({ target }: React.ChangeEvent<any>) =>
            dispatch(setSelectedInterfaceAction(target.value))
          }
          t={t}
          externalSearchAllowed={externalSearchAllowed}
        />

        <div>{fields.map(renderField)}</div>
        <Button
          component="button"
          onClick={() => {
            onSearch(values);
          }}
          name="executeImporterSearch"
          disabled={!isValid || emptyForm || loading}
          sx={{
            alignSelf: 'flex-end',
            width: 'min-content',
            marginTop: 8,
          }}
        >
          {t('common:dialog.search')}
        </Button>
      </div>
      <div className={classes.results}>
        {loading ? (
          <Loader />
        ) : rows ? (
          <ResultsTable
            rows={getFormattedRows({
              rows,
              formType: selectedFormType,
            })}
            searchType={selectedFormType}
            onSelectRow={onSelectRow}
            t={t}
          />
        ) : null}
      </div>
    </div>
  );
};

export default SearchForm;

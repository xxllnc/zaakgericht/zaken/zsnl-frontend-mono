// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { OpenServerErrorDialogType } from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import * as i18next from 'i18next';
import { fetchRemote } from './requests';
import {
  setSearchResultsAction,
  setLoadingAction,
  clearSearchResultsAction,
} from './store/actions';
import {
  InterfaceType,
  CapabilitiesType,
  AnyRecordType,
  StoreShapeType,
  FormType,
  AnyTypeFields,
} from './types';

export const getRow = ({
  uuid,
  state,
}: {
  uuid: string | null;
  state: StoreShapeType;
}): AnyRecordType | null => {
  if (!uuid || !state) return null;
  const rows = state?.rows?.filter(thisRow => thisRow.uuid === uuid);
  return rows && rows.length ? rows[0] : null;
};

/* eslint complexity: [2, 22] */
export const isSearchableInterface = (
  interf: InterfaceType,
  capabilities: CapabilitiesType
) => {
  const { config } = interf;

  if (!capabilities || !capabilities.length) return false;

  if (
    Boolean(config?.gbav_search) === false &&
    Boolean(config?.local_search) === false
  ) {
    return false;
  }

  if (Boolean(config?.search_extern_webform_only) === true) {
    return false;
  }

  if (
    Boolean(config?.gbav_search_role_restriction) === true &&
    Boolean(capabilities.includes('contact_search_extern') === false)
  ) {
    return false;
  }

  return true;
};

type FieldsMapType = {
  [key in FormType]: {
    [key: string]: {
      name: string;
      parse?: (value: any) => any;
    };
  };
};
const fieldsMap: FieldsMapType = {
  person: {
    bsn: {
      name: 'subject.personal_number',
    },
    familyName: { name: 'subject.family_name' },
    prefix: { name: 'subject.prefix' },
    zipCode: { name: 'subject.address_residence.zipcode' },
    streetNumber: { name: 'subject.address_residence.street_number' },
    streetNumberLetter: {
      name: 'subject.address_residence.street_number_letter',
    },
    suffix: { name: 'subject.address_residence.street_number_suffix' },
    dateOfBirth: {
      name: 'subject.date_of_birth',
      parse: (value: Date) =>
        new Date(new Date(value).setHours(12)).toISOString().replace(/T.*/, ''),
    },
  },
  company: {
    rsin: { name: 'subject.rsin' },
    cocNumber: { name: 'subject.coc_number' },
    cocLocationNumber: { name: 'subject.coc_location_number' },
    company: { name: 'subject.company' },
    street: { name: 'subject.address_residence.street' },
    zipCode: { name: 'subject.address_residence.zipcode' },
    number: { name: 'subject.address_residence.street_number' },
    letter: { name: 'subject.address_residence.street_number_letter' },
    suffix: { name: 'subject.address_residence.street_number_suffix' },
  },
};

const hasValue = (value: any) =>
  value === false || value === null || value === '' ? false : true;

export const doRemoteSearch = async ({
  fields,
  state,
  dispatch,
  openServerErrorDialog,
}: {
  fields: AnyTypeFields;
  state: StoreShapeType;
  dispatch: React.Dispatch<any>;
  openServerErrorDialog: OpenServerErrorDialogType;
}) => {
  const { selectedFormType, selectedInterface } = state;

  const parsedFields = Object.entries(fields).reduce((acc, currentValue) => {
    const [key, value] = currentValue;
    const newKey = fieldsMap[selectedFormType][key].name;

    if (hasValue(value)) {
      const field = fieldsMap[selectedFormType][key];
      acc[newKey] = field.parse !== undefined ? field.parse(value) : value;
    }

    return acc;
  }, {} as any);

  dispatch(setLoadingAction(true));
  dispatch(clearSearchResultsAction());

  try {
    const result = await fetchRemote({
      fields: parsedFields,
      selectedFormType,
      selectedInterface,
    });
    dispatch(setSearchResultsAction(result, selectedFormType));
  } catch (error: any) {
    const errorType = error?.result?.type || '';
    if (errorType.indexOf('validation') > -1) return;

    openServerErrorDialog(error);
  } finally {
    dispatch(setLoadingAction(false));
  }
};

export const getInterfacesChoicesPerson = (
  interfaces: InterfaceType[],
  t: i18next.TFunction
) => {
  let localChoice = [{ label: t('ContactImporter:local'), value: 'local' }];

  return [
    ...localChoice,
    ...interfaces.map(interf => {
      return {
        label: interf.name,
        value: interf.id,
      };
    }),
  ];
};

export const getInterfacesChoicesCompany = (
  externalSearchAllowed: boolean,
  t: i18next.TFunction
) => {
  return [
    { label: t('ContactImporter:local'), value: 'local' },
    ...[externalSearchAllowed && { label: 'Extern', value: 'external' }],
  ];
};

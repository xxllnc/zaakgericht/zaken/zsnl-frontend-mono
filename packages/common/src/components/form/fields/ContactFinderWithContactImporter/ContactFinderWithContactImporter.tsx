// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
//@ts-ignore
import IconButton from '@mui/material/IconButton';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import ContactFinder from '@zaaksysteem/common/src/components/form/fields/ContactFinder/ContactFinder';
import { FormFieldComponentType } from '../../types/Form2.types';
import ContactImporter from './ContactImporter/ContactImporter';
import { useStyles } from './ContactFinderWithContactImporter.styles';
import locale from './ContactFinderWithContactImporter.locale';

const ContactFinderWithContactImporter: FormFieldComponentType<
  any,
  {
    subjectTypes?: SubjectTypeType[];
  }
> = props => {
  const [t] = useTranslation('');
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  const { onChange, name, disabled, config } = props;
  const subjectTypes = config?.subjectTypes || [
    'person',
    'organization',
    'employee',
  ];

  const externalAllowed = () => {
    if (disabled) return false;
    if (
      !subjectTypes.includes('person') &&
      !subjectTypes.includes('organization')
    )
      return false;

    return true;
  };

  const handleSelectRow = ({ label, value, type }: ValueType<String>) => {
    onChange({
      target: {
        name,
        value: {
          label,
          value,
          type,
        },
      },
    } as React.ChangeEvent<any>);
    setOpen(false);
  };

  return (
    <div className={classes.wrapper}>
      <div className={classes.component}>
        <ContactFinder {...props} />
      </div>

      <div className={classes.button}>
        <Tooltip title={t('ContactFinderWithContactImporter:import')}>
          <IconButton
            onClick={() => setOpen(true)}
            color="inherit"
            disabled={!externalAllowed()}
          >
            <Icon size="small">{iconNames.import}</Icon>
          </IconButton>
        </Tooltip>
      </div>

      {open && (
        <ContactImporter
          onSelectRow={handleSelectRow}
          onClose={() => setOpen(false)}
          subjectTypes={subjectTypes}
        />
      )}
    </div>
  );
};

/* eslint-disable-next-line */
export default (props: any) => (
  <I18nResourceBundle
    resource={locale}
    namespace="ContactFinderWithContactImporter"
  >
    <ContactFinderWithContactImporter {...props} />
  </I18nResourceBundle>
);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import fecha from 'fecha';
import { DialogDatePicker } from '@mintlab/ui/App/Material/DatePicker';
import { ReadonlyValuesContainer } from '../library/ReadonlyValuesContainer';
import { FormFieldComponentType } from '../types/Form2.types';

const DatePicker: FormFieldComponentType<string> = props => {
  const { readOnly, value, dateFormat } = props;
  const date =
    //@ts-ignore
    value === null || isNaN(new Date(value))
      ? value
      : fecha.format(new Date(value), dateFormat || 'YYYY-MM-DD');

  return readOnly ? (
    <ReadonlyValuesContainer value={date} />
  ) : (
    <DialogDatePicker {...props} />
  );
};

export default DatePicker;

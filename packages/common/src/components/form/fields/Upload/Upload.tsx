// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment, useState } from 'react';
//@ts-ignore
import { FileList, File } from '@mintlab/ui/App/Zaaksysteem/FileSelect';
import Button from '@mintlab/ui/App/Material/Button';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { cloneWithout, asArray } from '@mintlab/kitchen-sink/source';
import { useTheme } from '@mui/material';
import { Theme } from '@mintlab/ui/types/Theme';
import { FormFieldPropsType } from '../../types/formDefinition.types';
import { FormFieldComponentType } from '../../types/Form2.types';
import { STATUS_PENDING } from '../../constants/fileStatus';
import { useUploadStyle } from './Upload.style';
import { FileObject } from './Upload.types';
import UploadDialog from './Upload.Dialog';
import Upload from './Upload.Comp';
import UploadReadonly from './Upload.Readonly';

export interface FileSelectProps extends FormFieldPropsType {
  value: any;
  readOnly?: boolean;
}

/**
 * Renders a FileSelect from the mintlab/ui library, based
 * on the provided value(s).
 *
 * Handles adding and removing files by calling onChange when needed.
 *
 * If the uploadDialog prop is false, the FileSelect will
 * render the upload interface, with the current files,
 * directly.
 * If the uploadDialog prop is true, a button will be rendered,
 * which will open a dialog containing the upload interface.
 *
 * Internally, the list of files will be always be handled as an array.
 * If multiValue is true, an array of file objects will be set as value.
 * If multivalue is false, a single file object will be set as value.
 */
/* eslint complexity: [2, 9] */
const FileSelect: FormFieldComponentType<
  any[],
  { withPreview: boolean; fileType: 'file' | 'document' }
> = ({
  accept,
  value = [],
  uploadDialog = false,
  multiValue = true,
  name,
  readOnly,
  onChange,
  setFieldTouched,
  config,
}) => {
  const theme = useTheme<Theme>();
  const normalizedValue = value ? asArray(value) : [];
  const classes = useUploadStyle();
  const [t] = useTranslation('');
  const [dialogOpen, setDialogOpen] = useState(false);

  const generateList = () => {
    if (!normalizedValue || !normalizedValue.length) return [];

    const mapFiles = (file: FileObject) => {
      const status = () =>
        file.status
          ? {
              status: file.status,
            }
          : {};

      return (
        <File
          key={file.key}
          name={file.label}
          {...status()}
          onDeleteClick={() => removeFile(file.key)}
          {...(file.errorCode && {
            iconHoverMessage: t([
              `common:serverErrors.keys.${file.errorCode}`,
              `common:serverErrors.${file.errorCode}`,
              'common:serverErrors.invalid/file',
            ]),
          })}
        />
      );
    };

    return <FileList>{normalizedValue.map(mapFiles)}</FileList>;
  };

  const list = generateList();

  const uploadDialogContent = () => {
    return (
      <Fragment>
        <UploadDialog
          open={dialogOpen}
          onClose={() => toggleDialog()}
          onConfirm={(files: FileObject[]) => addFiles(files)}
          classes={{
            paper: classes.dialogRoot,
          }}
          accept={accept}
          multiValue={multiValue}
        />
        <div>
          {normalizedValue && normalizedValue.length > 0 && (
            <div className={classes.list}>{list}</div>
          )}

          <Button
            name="toggleUploadDialog"
            action={() => toggleDialog()}
            icon="attachment"
            iconSize="small"
            sx={{
              '&>*:first-of-type >*:first-of-type': {
                color: theme.mintlab.greyscale.darker,
              },
            }}
          >
            {t('fileSelect.addFile')}
          </Button>
        </div>
      </Fragment>
    );
  };

  const toggleDialog = () => setDialogOpen(!dialogOpen);

  const addFiles = (files: FileObject[]) =>
    handleOnChange([...normalizedValue, ...asArray(files)]);

  const removeFile = (key: string) =>
    handleOnChange(normalizedValue.filter(thisValue => thisValue.key !== key));

  const setFileStatus = (key: string, props: Partial<FileObject>) => {
    const mapValue = (thisFile: FileObject) => {
      if (thisFile.key === key) {
        return {
          ...cloneWithout(thisFile, 'status'),
          ...props,
        };
      } else {
        return thisFile;
      }
    };

    handleOnChange(normalizedValue.map(mapValue));
  };

  const handleOnChange = (changeValue: FileObject[]) => {
    const filesPending = changeValue.some(
      file => file.status === STATUS_PENDING
    );
    const getValue = () => {
      if (!changeValue.length) return [];
      return multiValue ? changeValue : changeValue[0];
    };

    onChange({
      target: {
        name,
        value: getValue(),
      },
    } as React.ChangeEvent<any>);

    /*
     * Set the 'touched' property of this field according to
     * whether there are any pending files, so the form will not
     * flash any error messages.
     */
    setFieldTouched(name, !filesPending);
  };

  if (uploadDialog) {
    return uploadDialogContent();
  } else if (readOnly) {
    return (
      <UploadReadonly
        value={normalizedValue}
        multiValue={multiValue}
        withPreview={config?.withPreview || false}
        config={config}
      />
    );
  } else {
    return (
      <Upload
        accept={accept}
        selectInstructions={
          multiValue
            ? t('fileSelect.selectInstructionsPlural')
            : t('fileSelect.selectInstructionsSingular')
        }
        dragInstructions={
          multiValue
            ? t('fileSelect.dragInstructionsPlural')
            : t('fileSelect.dragInstructionsSingular')
        }
        dropInstructions={
          multiValue
            ? t('fileSelect.dropInstructions')
            : t('fileSelect.dropInstructionsSingular')
        }
        orLabel={t('fileSelect.orLabel')}
        value={normalizedValue}
        multiValue={multiValue}
        list={list}
        addFiles={addFiles}
        setFileStatus={setFileStatus}
        fileType={config?.fileType || 'file'}
      />
    );
  }
};

export default FileSelect;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { buildUrl } from '@mintlab/kitchen-sink/source';
//@ts-ignore
import { FileSelect } from '@mintlab/ui/App/Zaaksysteem/FileSelect';
//@ts-ignore
import { FileList, File } from '@mintlab/ui/App/Zaaksysteem/FileSelect';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { DocumentPreviewModal } from '../../../DocumentPreview';
import { ReadonlyValuesContainer } from '../../library/ReadonlyValuesContainer';
import { FileObject } from './Upload.types';

type UploadReadonlyProps = {
  value: any[];
  multiValue: boolean;
  withPreview: boolean;
  config: any;
};

type PreviewPropsType = {
  title: string;
  downloadUrl: string;
  contentType: string;
  url: string;
};

/* eslint complexity: [2, 7] */
const UploadReadonly: React.ComponentType<UploadReadonlyProps> = ({
  value,
  multiValue,
  withPreview,
  config,
}) => {
  const [previewItem, setPreviewItem] = React.useState(
    null as PreviewPropsType | null
  );
  const openPreviewModal = (uuid: string) => {
    const getDocumentUrl = buildUrl('/api/v2/document/get_document', {
      document_uuid: uuid,
    });
    const previewDocumentUrl = buildUrl('/api/v2/document/preview_document', {
      id: uuid,
    });
    const downloadUrl = buildUrl('/api/v2/document/download_document', {
      id: uuid,
    });

    request('GET', getDocumentUrl).then(response => {
      setPreviewItem({
        title: response.data.attributes.name,
        contentType: response.data.attributes.mimetype,
        url: previewDocumentUrl,
        downloadUrl,
      });
    });
  };

  const mapFiles = (file: FileObject) => {
    const status = () =>
      file.status
        ? {
            status: file.status,
          }
        : {};

    return (
      <File
        key={file.key}
        name={file.label}
        {...status()}
        onLinkClick={
          withPreview
            ? () => openPreviewModal(file.value || file.uuid || '')
            : false
        }
        onDownloadClick={
          config.getDownloadUrl
            ? () => window.open(config.getDownloadUrl(file.value))
            : undefined
        }
      />
    );
  };

  return (
    <React.Fragment>
      {value.length ? (
        <FileSelect
          hasFiles={value && value.length}
          multiValue={multiValue}
          fileList={<FileList>{(value || []).map(mapFiles)}</FileList>}
          applyBackground={false}
        />
      ) : (
        <ReadonlyValuesContainer value={null} />
      )}
      {withPreview && (
        <DocumentPreviewModal
          open={previewItem !== null}
          onClose={() => setPreviewItem(null)}
          {...(previewItem || {})}
        />
      )}
    </React.Fragment>
  );
};

export default UploadReadonly;

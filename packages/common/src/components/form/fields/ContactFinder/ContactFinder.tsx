// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useEffect } from 'react';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { iconNames } from '@mintlab/ui/App/Material/Icon';
import Select, {
  renderOptionWithIcon,
  renderTagsWithIcon,
} from '@mintlab/ui/App/Zaaksysteem/Select';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { usePrevious } from '@zaaksysteem/common/src/hooks/usePrevious';
import { FormFieldComponentType } from '../../types/Form2.types';
import { fetchContactChoices } from './ContactFinder.library';

const ContactFinder: FormFieldComponentType<ValueType<string>, any> = ({
  multiValue,
  config,
  value,
  ...restProps
}) => {
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const { choices } = restProps;
  const [internalChoices, setInternalChoices] = useState([] as any);
  const [loading, setLoading] = useState(false);
  const prev = usePrevious(choices);

  const fetchContactChoicesFunction = fetchContactChoices(
    config,
    openServerErrorDialog
  );

  useEffect(() => {
    if (choices && prev !== choices) {
      setInternalChoices(choices);
    }
  }, [choices]);

  const determineIcon = (option: any) =>
    option.type === 'organization' ? iconNames.domain : iconNames.person;

  return (
    <React.Fragment>
      <Select
        {...restProps}
        value={value}
        choices={internalChoices}
        loading={loading}
        isMulti={multiValue}
        onInputChange={async (ev, input: string) => {
          if (input) {
            setLoading(true);
            const result = (await fetchContactChoicesFunction(input)) || [];
            setInternalChoices(result);
            setLoading(false);
            return result;
          }
        }}
        renderTags={renderTagsWithIcon(determineIcon)}
        renderOption={renderOptionWithIcon(determineIcon)}
        filterOption={() => true}
      />
      {ServerErrorDialog}
    </React.Fragment>
  );
};

export default ContactFinder;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Select, { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import { FormFieldComponentType } from '../../types/Form2.types';
import { useObjectChoicesQuery } from './ObjectFinder.library';

export const ObjectFinder: FormFieldComponentType<
  ValueType<string[]>,
  { relationshipUuid: string }
> = props => {
  const objectTypeUuid = props.config?.relationshipUuid;
  const [selectProps, ServerErrorDialog] =
    useObjectChoicesQuery(objectTypeUuid);

  return (
    <>
      <Select {...props} {...selectProps} filterOption={() => true} />
      {ServerErrorDialog}
    </>
  );
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import classnames from 'classnames';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { FormFieldComponentType } from '../../types/Form2.types';
import { useCaseDocumentFinderStyle } from './CaseDocumentFinder.style';
import { useCaseDocumentChoicesQuery } from './CaseDocumentFinder.library';

const CaseDocumentFinder: FormFieldComponentType<
  ValueType<string>,
  { caseUuid: string }
> = ({ multiValue = true, config: { caseUuid }, ...restProps }) => {
  const classes = useCaseDocumentFinderStyle();
  const [selectProps, ServerErrorDialog] =
    useCaseDocumentChoicesQuery(caseUuid);

  return (
    <React.Fragment>
      <div
        className={classnames(
          restProps.applyBackgroundColor && classes.withBackground
        )}
      >
        <Select {...restProps} {...selectProps} isMulti={multiValue} />
      </div>
      {ServerErrorDialog}
    </React.Fragment>
  );
};

export default CaseDocumentFinder;

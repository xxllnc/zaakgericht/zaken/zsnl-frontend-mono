// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useQuery } from '@tanstack/react-query';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APIDocument } from '@zaaksysteem/generated';

export const useCaseDocumentChoicesQuery = (caseUuid: string) => {
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  const data = useQuery(
    ['caseDocuments', caseUuid],
    async ({ queryKey: [__, case_uuid] }) => {
      const body = await request<APIDocument.SearchDocumentResponseBody>(
        'GET',
        buildUrl<APIDocument.SearchDocumentRequestParams>(
          `/api/v2/document/search_document`,
          { case_uuid: case_uuid }
        )
      ).catch(openServerErrorDialog);

      return body
        ? (body.data || [])
            .map(document => ({
              value: document.relationships.file.data.id,
              label: document.attributes.name,
            }))
            .sort((docA, docB) => docA.label.localeCompare(docB.label))
        : [];
    }
  );

  const selectProps = {
    loading: data.status === 'loading',
    choices: data.data || [],
  };

  return [selectProps, ServerErrorDialog] as const;
};

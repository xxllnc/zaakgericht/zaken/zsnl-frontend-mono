// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { IntegrationContextType } from '@mintlab/ui/types/MapIntegration';
import { FormFieldComponentType } from '../../types/Form2.types';
import { ReadonlyValuesContainer } from '../../library/ReadonlyValuesContainer';
import { AddressValueType } from './Address.types';
import { useAddressChoicesQuery, fetchAddressLookup } from './Address.library';

export const AddressBoxField: FormFieldComponentType<
  AddressValueType,
  { context: IntegrationContextType }
> = props => {
  const { value, name, onChange, readOnly } = props;
  const [selectProps, ServerErrorDialog] = useAddressChoicesQuery();

  return (
    <>
      {readOnly ? (
        <div style={{ paddingBottom: 10 }}>
          <ReadonlyValuesContainer value={value?.address.full} />
        </div>
      ) : (
        <Select
          {...props}
          {...selectProps}
          value={value ? { label: value.address.full, value } : null}
          onChange={event => {
            const selectedPdokSuggestionId = event.target.value?.value;
            if (selectedPdokSuggestionId) {
              fetchAddressLookup(selectedPdokSuggestionId).then(value => {
                onChange({
                  target: {
                    name,
                    value,
                  },
                });
              });
            } else {
              onChange({
                target: { name, value: null },
              });
            }
          }}
        />
      )}
      {ServerErrorDialog}
    </>
  );
};

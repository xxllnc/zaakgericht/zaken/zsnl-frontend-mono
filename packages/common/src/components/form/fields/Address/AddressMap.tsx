// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { GeoMap } from '@mintlab/ui/App/External/GeoMap';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { IntegrationContextType } from '@mintlab/ui/types/MapIntegration';
import { FormFieldComponentType } from '../../types/Form2.types';
import { ReadonlyValuesContainer } from '../../library/ReadonlyValuesContainer';
import { AddressValueType } from './Address.types';
import {
  useAddressChoicesQuery,
  fetchAddressLookup,
  fetchClosestAddress,
} from './Address.library';

export const AddressMapField: FormFieldComponentType<
  AddressValueType,
  { context: IntegrationContextType }
> = props => {
  const {
    value,
    name,
    onChange,
    readOnly,
    config: { context },
  } = props;
  const markerPosition = props.value?.geojson.features[0].geometry;
  const [selectProps, ServerErrorDialog, openServerErrorDialog] =
    useAddressChoicesQuery();

  return (
    <>
      {readOnly ? (
        <div style={{ paddingBottom: 10 }}>
          <ReadonlyValuesContainer value={value?.address.full} />
        </div>
      ) : (
        <Select
          {...props}
          {...selectProps}
          value={value ? { label: value.address.full, value } : null}
          onChange={event => {
            const selectedPdokSuggestionId = event.target.value?.value;
            if (selectedPdokSuggestionId) {
              fetchAddressLookup(selectedPdokSuggestionId).then(value => {
                onChange({
                  target: {
                    name,
                    value,
                  },
                });
              });
            } else {
              onChange({
                target: { name, value: null },
              });
            }
          }}
        />
      )}
      <GeoMap
        canDrawFeatures={false}
        name={name}
        context={context}
        markerPosition={markerPosition}
        onClick={point =>
          !readOnly &&
          fetchClosestAddress(point, openServerErrorDialog).then(
            val => val && onChange({ target: { name, value: val } })
          )
        }
        geoFeature={null}
        minHeight={370}
      />
      {ServerErrorDialog}
    </>
  );
};

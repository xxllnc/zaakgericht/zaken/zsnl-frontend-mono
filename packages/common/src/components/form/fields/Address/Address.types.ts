// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type PdokAddressSuggestion = {
  type: string;
  weergavenaam: string;
  id: string;
  score: number;
};

export type PdokAddressFull = {
  bron: string;
  woonplaatscode: string;
  type: 'adres';
  woonplaatsnaam: string;
  wijkcode: string;
  huis_nlt: string;
  openbareruimtetype: string;
  buurtnaam: string;
  gemeentecode: string;
  rdf_seealso: string;
  weergavenaam: string;
  huisnummertoevoeging: string;
  straatnaam_verkort: string;
  id: string;
  gekoppeld_perceel: string[];
  gemeentenaam: string;
  buurtcode: string;
  wijknaam: string;
  identificatie: string;
  openbareruimte_id: string;
  waterschapsnaam: string;
  provinciecode: string;
  postcode: string;
  provincienaam: string;
  centroide_ll: string;
  nummeraanduiding_id: string;
  waterschapscode: string;
  adresseerbaarobject_id: string;
  huisnummer: number;
  provincieafkorting: string;
  centroide_rd: string;
  straatnaam: string;
  gekoppeld_appartement: string[];
};

export type PdokAddressFreeSearchResponseType = {
  response: {
    numFound: number;
    start: number;
    maxScore: number;
    docs: PdokAddressFull[];
  };
};

export type PdokAddressSuggestionResponseType = {
  response: {
    numFound: number;
    start: number;
    maxScore: number;
    docs: PdokAddressSuggestion[];
  };
};

export type AddressValueType = {
  bag: {
    type: 'nummeraanduiding';
    id: string;
  };
  address: { full: string };
  geojson: {
    type: 'FeatureCollection';
    features: [
      {
        type: 'Feature';
        properties: {};
        geometry: {
          type: 'Point';
          coordinates: [number, number];
        };
      }
    ];
  };
};

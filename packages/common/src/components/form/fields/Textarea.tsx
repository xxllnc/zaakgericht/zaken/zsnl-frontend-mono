// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { TextField } from '@mintlab/ui/App/Material/TextField';
import { ReadonlyValuesContainer } from '../library/ReadonlyValuesContainer';
import { FormFieldComponentType } from '../types/Form2.types';

export const Textarea: FormFieldComponentType<string> = ({
  readOnly,
  ...restProps
}) => {
  return readOnly ? (
    <ReadonlyValuesContainer value={restProps.value} />
  ) : (
    <TextField
      {...restProps}
      isMultiline={true}
      sx={{
        '& .MuiInputBase-input': {
          padding: '5px 0px 0px',
          borderRadius: '8px',
        },
        '& .MuiInputBase-root': {
          padding: '10px 5px 0px 15px',
          backgroundColor: 'transparent',
          '&:hover': {
            backgroundColor: 'transparent',
          },
        },
      }}
    />
  );
};

export default Textarea;

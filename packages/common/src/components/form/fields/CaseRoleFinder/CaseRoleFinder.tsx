// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import Select, {
  renderOptionWithIcon,
  renderTagsWithIcon,
} from '@mintlab/ui/App/Zaaksysteem/Select';
import { FormFieldComponentType } from '../../types/Form2.types';
import { CaseRoleFinderConfigType } from './CaseRoleFinder.types';
import { useCaseRoleChoicesQuery } from './CaseRoleFinder.library';

const getIcon = (caseRole: any) =>
  caseRole?.subject.type === 'organization' ? 'domain' : 'person';

const CaseRoleFinder: FormFieldComponentType<
  ValueType<string>,
  CaseRoleFinderConfigType
> = ({ multiValue, config, value, ...restProps }) => {
  const [selectProps, ServerErrorDialog] = useCaseRoleChoicesQuery(config);

  return (
    <>
      <Select
        {...restProps}
        {...selectProps}
        value={value}
        isMulti={multiValue}
        renderOption={renderOptionWithIcon(getIcon)}
        renderTags={renderTagsWithIcon(getIcon)}
        filterOption={() => true}
      />
      {ServerErrorDialog}
    </>
  );
};

export default CaseRoleFinder;

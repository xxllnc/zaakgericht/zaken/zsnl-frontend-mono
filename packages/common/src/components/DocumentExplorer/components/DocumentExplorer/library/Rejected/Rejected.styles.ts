// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useRejectedStyles = makeStyles(({ typography }: Theme) => {
  return {
    table: {
      width: 400,
    },
    row: {
      display: 'flex',
      alignItems: 'flex-start',
      '& :nth-child(1)': {
        width: 120,
        fontWeight: typography.fontWeightBold,
        flexShrink: 0,
      },
      '& :nth-child(2)': {},
      marginBottom: 12,
    },
  };
});

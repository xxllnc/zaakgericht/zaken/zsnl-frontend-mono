// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';

export const useDocumentExplorerStyles = makeStyles(
  ({
    typography,
    breakpoints,
    palette: { elephant, coral },
    mintlab: { greyscale },
  }: Theme) => {
    const section = {
      borderBottom: `1px solid ${greyscale.dark}`,
      alignItems: 'center',
      paddingTop: 10,
      paddingBottom: 10,
    };

    const defaultFont = {
      fontWeight: typography.fontWeightRegular,
      fontSize: 13,
    };

    return {
      wrapper: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
      },
      topBar: {
        ...section,
        display: 'flex',
        justifyContent: 'space-between',

        [breakpoints.down('xs')]: {
          flexDirection: 'column',
          padding: 10,
        },
      },
      searchAndFiltersContainer: {
        display: 'flex',
        flexGrow: 1,
        justifyContent: 'space-between',
        '&>*': {
          marginLeft: 10,
        },
        '&>div:nth-child(1)': {
          maxWidth: 300,
        },
        '&>div:nth-child(2)': {
          width: '190px',
          marginRight: 12,
        },
        [breakpoints.down('xs')]: {
          flexDirection: 'column',
          '&>div:nth-child(1)': {
            maxWidth: '100%',
          },
          '&>div:nth-child(2)': {
            maxWidth: '100%',
          },
          '&>*': {
            maxWidth: '100%',
            margin: '0 0 5px 0',
          },
        },
      },
      table: {
        flex: 1,
        color: elephant.dark,
        ...defaultFont,
      },
      toolbar: {
        ...section,
        display: 'flex',
        minHeight: 80,
        paddingLeft: 20,
        justifyContent: 'flex-end',
      },
      actionButtons: {
        display: 'flex',
        flexGrow: 1,
        justifyContent: 'flex-end',
      },
      iconWrapper: {
        marginRight: 10,
      },
      nameCell: {
        display: 'flex',
        overflow: 'visible',
        alignItems: 'center',
        justifyContent: 'space-between',
        '&>*': {
          width: 'auto',
        },
        '&>:first-child': {
          flexGrow: 0,
          flexShrink: 0,
        },
        '&>:nth-child(2)': {
          flexShrink: 1,
          flexGrow: 1,
        },
      },
      statusIndicator: {
        color: coral.dark,
        marginLeft: 10,
        fontSize: 11,
        fontWeight: typography.fontWeightMedium,
        flexShrink: 0,
        flexGrow: 0,
      },
      extension: {
        textTransform: 'capitalize',
      },
      name: {
        fontWeight: typography.fontWeightMedium,
        color: elephant.darkest,
      },
      extensionName: {
        fontWeight: typography.fontWeightLight,
      },
      breadCrumbs: {
        flex: 1,
        display: 'flex',
        alignItems: 'center',
      },
    };
  }
);

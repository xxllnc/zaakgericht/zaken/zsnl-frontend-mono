// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const locale = {
  nl: {
    columns: {
      modified: {
        label: 'Gewijzigd',
      },
      download: {
        label: '',
        tooltip: 'Download bestand',
      },
      open: {
        label: '',
        tooltip: 'Openen in nieuw venster',
      },
      name: {
        label: 'Naam',
        notAccepted: 'In behandeling',
        rejected: 'Afgewezen',
        rejectedTitle: 'Document afgewezen',
        rejectedBy: 'Afgewezen door',
        rejectedReason: 'Reden',
      },
      description: {
        label: 'Omschrijving',
      },
      type: {
        label: 'Type',
      },
    },
    search: {
      placeholder: 'Zoek een bestand of map...',
      name: 'Zoeken',
      close: 'Sluiten',
    },
    filters: {
      assignment: {
        assigned: 'Toegewezen',
        unassigned: 'Niet toegewezen',
        all: 'Alles',
      },
    },
    addFiles: {
      button: 'Toevoegen',
    },
    breadCrumb: {
      home: 'Bestanden',
      search: 'Zoekresultaten',
    },
    table: {
      noRows: 'Er zijn geen bestanden of mappen om weer te geven.',
    },
    fileTypes: {
      default: 'Bestand',
      folder: 'Map',
      image: 'Afbeelding',
      spreadsheet: 'Spreadsheet',
      document: 'Document',
      presentation: 'Presentatie',
      pdf: 'PDF-Document',
    },
  },
};

export default locale;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ActionWithPayload } from '@zaaksysteem/common/src/types/ActionWithPayload';
import { BreadCrumbItemType } from '../components/DocumentExplorer/types/types';
import {
  ItemType,
  DocumentItemType,
} from '../components/FileExplorer/types/FileExplorerTypes';

export type NavigateActionType = ActionWithPayload<{ id: string }, 'navigate'>;
export const navigateAction = (id: string): NavigateActionType => {
  return {
    type: 'navigate',
    payload: {
      id,
    },
  };
};

export type SetLoadingActionType = ActionWithPayload<
  { loading: boolean },
  'setLoading'
>;
export const setLoadingAction = (loading: boolean): SetLoadingActionType => {
  return {
    type: 'setLoading',
    payload: {
      loading,
    },
  };
};

export type DoSearchActionType = ActionWithPayload<
  { search: string },
  'doSearch'
>;
export const doSearchAction = (search: string): DoSearchActionType => {
  return {
    type: 'doSearch',
    payload: {
      search,
    },
  };
};

export type SetItemsActionType = ActionWithPayload<
  {
    items: ItemType[];
    search: string;
    path: BreadCrumbItemType[];
  },
  'setItems'
>;
export const setItemsAction = (
  items: ItemType[],
  search: string,
  path: BreadCrumbItemType[]
): SetItemsActionType => {
  return {
    type: 'setItems',
    payload: {
      items,
      path,
      search,
    },
  };
};

export type SetSearchItemsAction = ActionWithPayload<
  {
    rows: DocumentItemType[];
  },
  'setSearchItems'
>;
export const setSearchItemsAction = (
  result: DocumentItemType[]
): SetSearchItemsAction => {
  return {
    type: 'setSearchItems',
    payload: {
      rows: result,
    },
  };
};

export type ToggleSelectActionType = ActionWithPayload<
  {
    uuid: string;
  },
  'toggleSelected'
>;
export const toggleSelectedAction = (uuid: string): ToggleSelectActionType => {
  return {
    type: 'toggleSelected',
    payload: {
      uuid,
    },
  };
};

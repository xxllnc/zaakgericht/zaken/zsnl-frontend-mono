// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Button from '@mintlab/ui/App/Material/Button';

type DialogActionButtonProps = {
  text: string;
  onClick?: () => void;
  color?: string;
  disabled?: boolean;
};

export const createDialogActions = (
  buttons: DialogActionButtonProps[],
  scope: string
) => {
  return buttons
    .filter(button => button && button.text)
    .map(({ onClick, text, ...rest }, index) => (
      <Button
        color={index === 0 ? 'primary' : undefined}
        name={`${scope}#${text}`}
        variant="text"
        key={index}
        action={onClick}
        {...rest}
      >
        {text}
      </Button>
    ))
    .reverse();
};

export default createDialogActions;

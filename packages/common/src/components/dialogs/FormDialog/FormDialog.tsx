// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import React, { useRef } from 'react';
import { useTranslation } from 'react-i18next';
import { DialogActions, DialogContent, Divider, useTheme } from '@mui/material';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
//@ts-ignore
import { unique } from '@mintlab/kitchen-sink/source/unique';
//@ts-ignore
import {
  Dialog,
  DialogTitle,
  //@ts-ignore
} from '@mintlab/ui/App/Material/Dialog';
import createDialogActions from '@zaaksysteem/common/src/components/dialogs/library/createDialogActions';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import {
  FormDefinition,
  FormValuesType,
  UseFormType,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { DialogTitlePropsType } from '@mintlab/ui/App/Material/Dialog/DialogTitle/DialogTitle';
import { useForm } from '../../form/hooks/useForm';
import { useStyles } from './FormDialog.style';

export type FormDialogPropsType<Values> = Omit<
  UseFormType<Values>,
  'onSubmit'
> & {
  formDefinition: FormDefinition<Values>;
  onClose: () => void;
  title: string;
  onSubmit: (values: FormValuesType<Values>) => void;
  scope?: string;
  saveLabel?: string;
  icon?: DialogTitlePropsType['icon'];
  initializing?: boolean;
  saving?: boolean;
  open?: boolean;
  container?: React.ReactInstance | null;
  formDefinitionT?: i18next.TFunction;
  minHeight?: number;
};

function FormDialog<Values>({
  onSubmit,
  formDefinition,
  title,
  scope,
  saveLabel,
  icon,
  initializing = false,
  saving = false,
  validationMap,
  fieldComponents,
  rules,
  isInitialValid = false,
  formDefinitionT,
  onClose,
  open = true,
  container,
  minHeight = 400,
}: FormDialogPropsType<Values>) {
  const dialogEl = useRef(null);
  const [t] = useTranslation('common');
  const theme = useTheme<any>();
  const classes = useStyles(minHeight)(theme);

  const FormDialogContent = () => {
    const {
      fields,
      formik: { isValid, values },
    } = useForm({
      t: formDefinitionT,
      //@ts-ignore
      formDefinition,
      isInitialValid: isInitialValid,
      validationMap: validationMap,
      rules: rules,
      fieldComponents: fieldComponents,
    });

    return (
      <React.Fragment>
        <DialogContent>
          {fields.map(
            ({ FieldComponent, key, type, suppressLabel, ...rest }) => {
              const props = cloneWithout(rest, 'definition', 'mode');

              return (
                <FormControlWrapper
                  {...props}
                  label={suppressLabel ? false : props.label}
                  compact={true}
                  key={`${props.name}-formcontrol-wrapper`}
                >
                  <FieldComponent
                    {...props}
                    t={t}
                    containerRef={dialogEl.current}
                    submit={() => {
                      if (!isValid) return;
                      //@ts-ignore
                      onSubmit(values);
                    }}
                  />
                </FormControlWrapper>
              );
            }
          )}
        </DialogContent>
        <Divider />
        <DialogActions>
          {createDialogActions(
            [
              {
                disabled: saving || !isValid,
                text: saveLabel ? saveLabel : t('dialog.save'),
                onClick() {
                  //@ts-ignore
                  onSubmit(values);
                },
              },
              {
                text: t('forms.cancel'),
                onClick: onClose,
              },
            ],
            scope || ''
          )}
        </DialogActions>
      </React.Fragment>
    );
  };

  return (
    <Dialog
      classes={{ paper: classes.dialogRoot }}
      aria-label={title}
      open={open}
      onClose={onClose}
      ref={dialogEl}
      disableBackdropClick={true}
      container={container}
    >
      <DialogTitle
        elevated={true}
        id={unique()}
        title={title}
        onCloseClick={onClose}
        scope={scope}
        {...(icon && { icon })}
        buttonProps={{ name: t('dialog.close') }}
      />
      {initializing && (
        <Loader style={{ paddingTop: 0, margin: '60px auto' }} />
      )}
      {!initializing && <FormDialogContent />}
    </Dialog>
  );
}

export default FormDialog;

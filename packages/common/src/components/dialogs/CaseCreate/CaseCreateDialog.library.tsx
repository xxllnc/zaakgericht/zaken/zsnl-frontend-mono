// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { CaseTypeOptionType } from '@zaaksysteem/common/src/components/form/fields/CaseTypeFinder/CaseTypeFinder.library';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { ContactType } from '@zaaksysteem/common/src/components/form/fields/ContactFinder/ContactFinder.types';
import { APICaseManagement, APIDocument } from '@zaaksysteem/generated';
import {
  CaseCreateDialogPropsType,
  GetShouldPrefillCaseTypeType,
  TranslateRequestorTypesType,
} from './CaseCreateDialog.types';
import { useStyles } from './CaseCreateDialog.styles';

export const getSavedCasetypeUuid = async () => {
  const userSettings = await request('GET', '/api/user/settings');
  const savedCaseTypeSettings = userSettings.result[0].remember_casetype;

  // new users who have never saved a casetype do not have any settings yet
  if (!savedCaseTypeSettings) return null;

  const { casetype_id: savedCaseTypeUuid, remember } = savedCaseTypeSettings;

  // remembering a casetype can be set to true, even if there is no casetype set
  // and also, a casetype can be set while remembering a casetype is false
  if (!remember || !savedCaseTypeUuid) return null;

  return savedCaseTypeUuid;
};

export const getCasetype = async (uuid: string) => {
  const body =
    await request<APICaseManagement.GetCaseTypeActiveVersionResponseBody>(
      'GET',
      buildUrl(`/api/v2/cm/case_type/get_active_version`, {
        case_type_uuid: uuid,
      })
    );

  if (!body) return null;

  const savedCaseType = {
    value: uuid,
    label: body.data?.attributes.name,
    data: {
      id: uuid,
      name: body.data?.attributes.name,
      type_of_requestors: body.data?.attributes.requestor.type_of_requestors,
      initiator_source: body.data?.attributes.initiator_source,
    },
  };

  return savedCaseType;
};

const translateRequestorTypes: TranslateRequestorTypesType = requestorTypes => [
  ...(requestorTypes.includes('medewerker') ? ['employee'] : []),
  ...(requestorTypes.includes('niet_natuurlijk_persoon')
    ? ['organization']
    : []),
  ...(requestorTypes.includes('natuurlijk_persoon_na') ||
  requestorTypes.includes('natuurlijk_persoon')
    ? ['person']
    : []),
];

// if there is also a contact being prefilled, then only prefill casetype if it supports the contact type
export const getShouldPrefillCaseType: GetShouldPrefillCaseTypeType = (
  contactTypeOption,
  savedCaseTypeOption
) => {
  const requestorTypesV0 = savedCaseTypeOption?.data.type_of_requestors;

  if (!requestorTypesV0) return false;
  if (!contactTypeOption) return true;

  const contactType = contactTypeOption?.type;
  const requestorTypes = translateRequestorTypes(requestorTypesV0);

  return requestorTypes.includes(contactType);
};

export const getLegacyDocumentId = (uuid: string): Promise<number> => {
  return request('POST', '/api/file/get_ids', { file_uuids: [uuid] }).then(
    (data: { result: { [key: string]: number }[] }) => {
      return data.result[0][uuid] || 0;
    }
  );
};

export const getDocumentName = (uuid: string): Promise<string> => {
  const url = buildUrl<APIDocument.GetDocumentRequestParams>(
    '/api/v2/document/get_document',
    {
      document_uuid: uuid,
    }
  );

  return request('GET', url).then(results => {
    return results?.data?.attributes?.name || '';
  });
};

// Get full details of the contact and return a Select Option object
export const getContactSelectOption = async (
  contact: CaseCreateDialogPropsType['contact']
): Promise<ContactType | null> => {
  if (!contact) return null;

  const url = buildUrl<APICaseManagement.GetContactRequestParams>(
    '/api/v2/cm/contact/get_contact',
    contact
  );
  const result = await request('GET', url);

  return result
    ? {
        value: contact.uuid,
        type: contact.type,
        label: result.data.attributes.name,
      }
    : null;
};

export const getRequestorFilter = ({
  data: { type_of_requestors },
}: CaseTypeOptionType) => {
  const requestorTypes = translateRequestorTypes(type_of_requestors);

  return { config: { subjectTypes: requestorTypes } };
};

export const RequestorTitle = ({
  contactOption,
  t,
}: {
  contactOption: ContactType;
  t: i18next.TFunction;
}) => {
  const classes = useStyles();

  return (
    <div className={classes.wrapper}>
      {t('caseCreate:requestorTitle') as string}
      <>&nbsp;</>
      <span>{contactOption.label}</span>
    </div>
  );
};

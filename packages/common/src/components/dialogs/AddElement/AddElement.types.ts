// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import { IconNameType } from '@mintlab/ui/App/Material/Icon';
import * as i18next from 'i18next';
import { addElementStylesheet } from './AddElement.style';

export type ClassesType = ReturnType<typeof addElementStylesheet>;

export type AddElementPropsType = {
  t: i18next.TFunction;
  classes: ClassesType;
  sections: SectionType[][];
  title?: string;
  titleIcon?: IconNameType;
  hide: () => void;
};

export type SectionType = {
  type: string;
  title?: string;
  icon?: string | React.ReactElement;
  disabled?: boolean;
  action: () => void;
};

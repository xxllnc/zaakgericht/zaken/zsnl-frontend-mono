// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/**
 * @param {Object} theme
 * @return {JSS}
 */
export const addElementStylesheet = ({ mintlab: { greyscale } }) => ({
  elementWrapper: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    padding: '16px 16px',
    justifyContent: 'center',
    minWidth: 400,
    '&:first-child': {
      paddingTop: 0,
    },

    '&:last-child': {
      paddingBottom: 16,
    },
  },
  element: {
    border: 'none',
    flex: '0 0 auto',
    width: 'calc(33.3% - 8px)',
    borderRadius: 15,
    margin: 4,
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    height: 130,
    padding: 12,
    transition: 'background 0.2s',
    cursor: 'pointer',
    background: 'none',

    '&:hover': {
      backgroundColor: greyscale.dark,
    },
  },
  elementIcon: {
    flex: '1 0 auto',
    display: 'flex',
    alignItems: 'center',
  },
  elementTitle: {
    flex: '0 1 32px',
  },
  dividerWrapper: {
    padding: '0 24px',
  },
});

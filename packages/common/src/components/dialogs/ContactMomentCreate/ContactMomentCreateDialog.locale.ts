// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
const locale = {
  nl: {
    title: 'Contactmoment aanmaken',
    contactTitle: 'Contactmoment aanmaken voor',
    contact: {
      label: 'Contact',
      placeholder: 'Selecteer contact',
      choices: {
        person: 'Burger',
        organization: 'Organisatie',
        employee: 'Medewerker',
      },
    },
    contactChannel: {
      label: 'Contactkanaal',
      placeholder: 'Selecteer contactkanaal',
      choices: {
        assignee: 'Behandelaar',
        frontdesk: 'Balie',
        phone: 'Telefoon',
        mail: 'Post',
        email: 'E-mail',
        webform: 'Webformulier',
        social_media: 'Sociale media',
      },
    },
    direction: {
      label: 'Richting',
      choices: {
        incoming: 'Inkomend',
        outgoing: 'Uitgaand',
      },
    },
    case: {
      label: 'Zaak',
    },
    content: {
      label: 'Samenvatting',
    },
  },
};

export default locale;

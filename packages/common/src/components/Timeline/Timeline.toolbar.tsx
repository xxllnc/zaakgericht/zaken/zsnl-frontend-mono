// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
//@ts-ignore
import { DialogDatePicker } from '@mintlab/ui/App/Material/DatePicker';
import Popover from '@mui/material/Popover';
import { Button } from '@mintlab/ui/App/Material/Button';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { useStyles } from './Timeline.styles';
import { ToolbarPropsType } from './Timeline.types';
import { Checkboxes } from './Timeline.library';

const Toolbar: React.FunctionComponent<ToolbarPropsType> = ({
  t,
  handleKeywordChange,
  keywordValue,
  keywordCloseAction,
  startDateCloseAction,
  handleStartDateChange,
  handleEndDateChange,
  startDateValue,
  endDateValue,
  endDateCloseAction,
  hasExport = false,
  onExportButtonClick,
  filters,
  handleFilterChange,
}) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);
  const open = Boolean(anchorEl);
  const arrowIcon = (
    <Icon size="small" color="inherit">
      {open ? iconNames.arrow_drop_up : iconNames.arrow_drop_down}
    </Icon>
  );

  return (
    <div className={classes.toolbarWrapper}>
      {/* <TextField
        variant="generic1"
        onChange={handleKeywordChange}
        value={keywordValue}
        closeAction={keywordCloseAction}
        placeholder={t('search')}
      /> */}

      {filters && filters.length ? (
        <>
          <Button
            name="timelineSetFilters"
            sx={{ marginRight: '10px' }}
            aria-describedby={'filters-popover'}
            action={(event: any) => {
              setAnchorEl(event.currentTarget);
            }}
            endIcon={arrowIcon}
          >
            {t('filtersLabel')}
          </Button>
          <Popover
            id={'filters-popover'}
            open={open}
            anchorEl={anchorEl}
            onClose={() => {
              setAnchorEl(null);
            }}
            anchorOrigin={{
              vertical: 'bottom',
              horizontal: 'left',
            }}
            PaperProps={{
              sx: {
                '& ul': {
                  padding: '0 20px',
                },
                '& li': {
                  listStyle: 'none',
                },
              },
            }}
          >
            <div className={classes.popover}>
              <Checkboxes
                filters={filters}
                handleFilterChange={handleFilterChange}
              />
            </div>
          </Popover>
        </>
      ) : (
        <div>&nbsp;</div>
      )}

      <DialogDatePicker
        name="startDate"
        value={startDateValue}
        onChange={handleStartDateChange}
        placeholder={t('start')}
        maxDate={endDateValue}
      />
      <div>{t('till') as string}</div>
      <DialogDatePicker
        name="endDate"
        value={endDateValue}
        onChange={handleEndDateChange}
        placeholder={t('now')}
        minDate={startDateValue}
      />
      {hasExport && (
        <Button name="timelineExport" action={onExportButtonClick}>
          {t('exportLabel')}
        </Button>
      )}
    </div>
  );
};

export default Toolbar;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import * as i18next from 'i18next';
import { getDataFuncType } from '@zaaksysteem/common/src/hooks/useInfiniteScroll';

export type TimelineItemType = {
  id: number | string;
  type: string;
  exception?: string;
  author: string;
  date: Date;
  description: string;
  caseNumber?: string;
};

export type ExportFunctionType = ({
  startDate,
  endDate,
}: {
  startDate?: string | null;
  endDate?: string | null;
}) => Promise<any>;

export type TimelinePropsType = {
  getData: getDataFuncType<TimelineItemType, any>;
  exportFunction?: ExportFunctionType;
  filtersOptions?: FiltersOptionType[];
};

export type ToolbarPropsType = {
  t: i18next.TFunction;
  handleKeywordChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  keywordValue?: string;
  keywordCloseAction: () => void;
  startDateCloseAction: () => void;
  handleStartDateChange: (ev: {
    target: { name: string; value: string | null };
  }) => void;
  handleEndDateChange: (ev: {
    target: { name: string; value: string | null };
  }) => void;
  startDateValue: string | null;
  endDateValue: string | null;
  endDateCloseAction: () => void;
  hasExport?: Boolean;
  onExportButtonClick?: () => Promise<any>;
  filters?: FiltersOptionType[];
  handleFilterChange?: (event: any) => void;
};

export type FiltersOptionType = {
  label: string;
  value: 'document' | 'case_update';
  checked: boolean;
};

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState, useRef } from 'react';
import {
  InfiniteLoader,
  List,
  AutoSizer,
  CellMeasurerCache,
  CellMeasurer,
} from 'react-virtualized';
import { useDebouncedCallback } from 'use-debounce';
import fecha from 'fecha';
import { useTranslation } from 'react-i18next';
import 'react-virtualized/styles.css';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { V2ServerErrorsType } from '@zaaksysteem/common/src/types/ServerError';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import useInfiniteScroll from '@zaaksysteem/common/src/hooks/useInfiniteScroll';
//@ts-ignore
import Snackbar from '@mintlab/ui/App/Material/Snackbar/Snackbar.cmp';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
//@ts-ignore
import Typography from '@mui/material/Typography';
import Tooltip from '@mintlab/ui/App/Material/Tooltip';
import { useStyles } from './Timeline.styles';
import Toolbar from './Timeline.toolbar';
import { getFormattedDate, getItemProperties } from './Timeline.library';
import {
  TimelineItemType,
  TimelinePropsType,
  FiltersOptionType,
} from './Timeline.types';
import locale from './Timeline.locale';

const REMOTE_ROW_COUNT = 999;
const DELAY = 300;
const MINLENGTH = 3;
const THRESHOLD = 5;
export const PAGE_LENGTH = 10;

const Timeline: React.FunctionComponent<TimelinePropsType> = ({
  getData,
  exportFunction,
  filtersOptions = [],
}) => {
  const classes = useStyles();
  const [t] = useTranslation('Timeline');
  const infiniteLoaderRef = useRef<InfiniteLoader>(null);
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const [keyword, setKeyword] = useState('');
  const [startDate, setStartDate] = useState<string | null>(null);
  const [endDate, setEndDate] = useState<string | null>(null);
  const [exportSnackOpen, setExportSnackOpen] = useState<boolean>(false);
  const [filters, setFilters] = useState<FiltersOptionType[]>(filtersOptions);

  const { list, isRowLoaded, loadMoreRows, resetList, loading } =
    useInfiniteScroll<TimelineItemType, any>({
      ref: infiniteLoaderRef,
      pageLength: PAGE_LENGTH,
      getData,
      getDataParams: {
        keyword,
        startDate,
        endDate,
        filters,
      },
    });

  const [debouncedCallback] = useDebouncedCallback(value => {
    if (!value.length || value.length >= MINLENGTH) {
      resetList();
    }
  }, DELAY);

  const handleKeywordChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const val = event.target.value;
    setKeyword(val);
    debouncedCallback(val.trim());
  };

  const handleExportButtonClick = async () => {
    if (!exportFunction) return null;

    const result = await exportFunction({
      startDate,
      endDate,
    }).catch((error: V2ServerErrorsType) => {
      openServerErrorDialog(error);
    });

    if (result) setExportSnackOpen(true);
  };

  const handleFilterChange = (event: any) => {
    const target = event.target;
    const newFilters = filters.map(item => {
      if (item.value !== target.name) return item;
      return {
        ...item,
        checked: target.checked,
      };
    });

    setFilters(newFilters);
    resetList();
  };

  const cache = React.useMemo(
    () =>
      new CellMeasurerCache({
        fixedWidth: true,
      }),
    []
  );

  React.useEffect(() => {
    const resizeListener = () => cache.clearAll();
    window.addEventListener('resize', resizeListener);

    return () => window.removeEventListener('resize', resizeListener);
  }, []);

  function rowRenderer({
    key,
    index,
    parent,
    style,
  }: {
    key: string;
    index: number;
    parent: any;
    style: any;
  }) {
    const listItem = list[index];

    if (!listItem) return null;

    const dateLabel = getFormattedDate(listItem.date, t);
    const prevListItem = list[index - 1];
    const prevDateLabel = prevListItem
      ? getFormattedDate(prevListItem.date, t)
      : '';
    const itemProperties = getItemProperties(listItem, t);

    return (
      <CellMeasurer
        parent={parent}
        cache={cache}
        columnIndex={0}
        key={key}
        rowIndex={index}
      >
        {({ measure }) => (
          <div key={key} className={classes.item} style={style}>
            <div className={classes.itemTime}>
              {prevDateLabel === dateLabel ? '' : dateLabel}
            </div>
            <div className={classes.itemType}>
              <div>{itemProperties.icon}</div>
              <div>&nbsp;</div>
            </div>
            <div className={classes.itemContent}>
              <Typography variant="h6">{itemProperties.title}</Typography>
              <p>
                {listItem.author} {t('on')}{' '}
                {fecha.format(new Date(listItem.date), 'D MMMM YYYY, H:mm')}
              </p>
              <Tooltip title={itemProperties.description}>
                <div className={classes.itemDescription}>
                  {itemProperties.description}
                </div>
              </Tooltip>
              {itemProperties.exception && (
                <div className={classes.itemException}>
                  {itemProperties.exception}
                </div>
              )}
            </div>
          </div>
        )}
      </CellMeasurer>
    );
  }

  return (
    <div className={classes.wrapper}>
      {ServerErrorDialog}
      <Snackbar
        autoHideDuration={6000}
        handleClose={(event: React.ChangeEvent, reason: string) => {
          if (reason === 'clickaway') return;
          setExportSnackOpen(false);
        }}
        message={t('exported')}
        open={exportSnackOpen}
      />
      <Toolbar
        handleKeywordChange={handleKeywordChange}
        keywordValue={keyword}
        keywordCloseAction={() => {
          setKeyword('');
          resetList();
        }}
        startDateValue={startDate}
        handleStartDateChange={event => {
          setStartDate(event.target.value);
          resetList();
        }}
        startDateCloseAction={() => {
          setStartDate(null);
          resetList();
        }}
        endDateValue={endDate}
        handleEndDateChange={event => {
          setEndDate(event.target.value);
          resetList();
        }}
        endDateCloseAction={() => {
          setEndDate(null);
          resetList();
        }}
        t={t}
        hasExport={Boolean(exportFunction)}
        onExportButtonClick={handleExportButtonClick}
        filters={filters}
        handleFilterChange={handleFilterChange}
      />
      <div className={classes.content}>
        <InfiniteLoader
          isRowLoaded={isRowLoaded}
          loadMoreRows={loadMoreRows}
          rowCount={REMOTE_ROW_COUNT}
          threshold={THRESHOLD}
          ref={infiniteLoaderRef}
        >
          {({ onRowsRendered, registerChild }) => (
            <AutoSizer>
              {({ height, width }) => (
                <List
                  deferredMeasurementCache={cache}
                  height={height}
                  onRowsRendered={onRowsRendered}
                  ref={registerChild}
                  rowCount={list.length || 1}
                  rowHeight={cache.rowHeight}
                  rowRenderer={rowRenderer}
                  width={width}
                />
              )}
            </AutoSizer>
          )}
        </InfiniteLoader>
        {loading ? (
          <div className={classes.loader}>
            <Loader />
          </div>
        ) : null}
      </div>
    </div>
  );
};

/* eslint-disable-next-line */
export default (props: TimelinePropsType) => (
  <I18nResourceBundle resource={locale} namespace="Timeline">
    <Timeline {...props} />
  </I18nResourceBundle>
);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ComponentType, ReactElement } from 'react';

type DialogCreatorType = (...args: any[]) => ReactElement;
interface Dialogs {
  [key: string]: ComponentType<any> | DialogCreatorType;
}

type AddDialogs = (items: Dialogs) => void;
type RemoveDialogs = (items: Dialogs) => void;

let dialogs: Dialogs = {};

const addDialogs: AddDialogs = items => {
  dialogs = {
    ...dialogs,
    ...items,
  };
};

const removeDialogs: RemoveDialogs = items => {
  const removeKeys = Object.keys(items);

  dialogs = Object.entries(dialogs)
    .filter(([key]) => removeKeys.includes(key))
    .reduce(
      (acc, [key, value]) => ({
        ...acc,
        [key]: value,
      }),
      {}
    );
};

export default function useDialogs(): [Dialogs, AddDialogs, RemoveDialogs] {
  return [dialogs, addDialogs, removeDialogs];
}

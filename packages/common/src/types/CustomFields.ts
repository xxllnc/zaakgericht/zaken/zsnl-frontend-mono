// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export type CustomFieldTypeType =
  | 'text'
  | 'richtext'
  | 'option'
  | 'select'
  | 'checkbox'
  | 'textarea'
  | 'email'
  | 'url'
  | 'bankaccount'
  | 'valuta'
  | 'geojson'
  | 'relationship'
  | 'date'
  | 'address_v2'
  | 'numeric'
  | 'file';

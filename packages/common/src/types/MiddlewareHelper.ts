// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Dispatch, Action, MiddlewareAPI } from 'redux';

export type MiddlewareHelper<S, A = Action<any>> = (
  store: MiddlewareAPI<Dispatch<any>, S>,
  next: Dispatch<any>,
  action: A
) => any;

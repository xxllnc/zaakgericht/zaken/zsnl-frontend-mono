// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxAction } from '../../library/redux/ajax/createAjaxAction';
import { ActionWithPayload } from '../../types/ActionWithPayload';
import {
  SESSION_FETCH,
  SESSION_AUTH_LOGIN,
  SESSION_AUTH_LOGOUT,
} from './session.constants';

const fetchAjaxAction = createAjaxAction(SESSION_FETCH);

/**
 * @return {Function}
 */
export const fetchSession = () =>
  fetchAjaxAction({
    url: '/api/v1/session/current',
    method: 'GET',
  });

export type LoginActionPayloadType = string;

export const login = (
  payload: LoginActionPayloadType
): ActionWithPayload<any> => ({
  type: SESSION_AUTH_LOGIN,
  payload,
});

/**
 * @param {*} payload
 * @return {Function}
 */
export const logout = (payload: any): ActionWithPayload<any> => ({
  type: SESSION_AUTH_LOGOUT,
  payload,
});

# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.56.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.55.1...@zaaksysteem/common@0.56.0) (2022-01-26)


### Bug Fixes

* **CaseCreatE:** MINTY-8414 - Fix spelling of the contact channel value E-mail ([bdc00e2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/bdc00e210135d23f29b5f50a5a775a9567cc3ca1))
* **CaseCreate:** MINTY-8416 - don't show document field when no document is present ([e9d6cb6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e9d6cb679d14ba622dce23014a971da651110da6))
* **Case:** fix merge issues and small review findings ([9a0619b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9a0619b59c122ddfb4ea519d0040186fdf6118ed))
* **CaseTypeFinder:** MINTY-6972 - fix unable to remove value ([fadab94](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/fadab9445d2cba1951cb65f713c5b2d2b9da87b8))
* **Communication:** MINTY-5760 - Filter non-requestors from requestor fetch results ([7289799](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/72897991e7f3685f310486c34c317fbcff7e20ac))
* **Communication:** MINTY-6618 - Give message search field a name ([7a01669](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7a016699ce0a7227d40aa4f5f7e88770d3bcdd86))
* **ContactView:** MINTY-7843 - Allow PersonalNumber to only be requested by 'Persoonsverwerker' ([95c2f6d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/95c2f6d86250c1a56b1f1eb6fa3e85eda8dd8f57))
* **DocumentExplorer:** MINTY-6618 - Give close search button a name ([8532327](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8532327ddddf82c0349afa249bc336035d8add98))
* **Documentintake:** Fix spelling social media contact channel ([ec72d71](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ec72d714ab380f1929358e48c79bd18c194713d1))
* **DocumentIntake:** MINTY-6971 - fix not being able to open import contact dialog ([3702e49](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3702e4949d34b73f5b06a3e8952a6588adb94c5a))
* **DocumentIntake:** MINTY-7727 - Remove flashing scrollbar ([d6e2877](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d6e28779489fc7ec9213f3a7a6a98480e347b938))
* **Intake:** MINTY-6740 - fix bug with starting new case form ([b1223fa](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b1223fac3f4943de68dd5ab7368fe62c5ce860f7))
* **ObjectView:** MINTY-8073 - add missing logging translations ([fae01c7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/fae01c7ab7ed7859bc6052c4427c30d95609fb2e))


### Features

* add row count functionality to infinite scroll / data provider ([b1b4e51](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b1b4e5138b1019fff82914baf3e802202995b373))
* **BreadcrumbBar:** MINTY-7787 - Disable options that have not been implemented yet ([f8b4e9f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f8b4e9f4844a296b4bc32c3b66510912b1efe8a3))
* **CaseCreate:** MINTY-6048 - add ability to prefill requestor ([e4ac430](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e4ac430d863e0ec37fe0dc51aaedfa81501c32ac))
* **CaseCreate:** MINTY-8136 - start case create with different contact channels ([e413c95](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e413c959d82873e70c9315b8050978fd0d65b9ff))
* **CaseCreate:** MINTY-8233 - Allow case type to be remembered ([888baaf](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/888baafee364a11e2c616bdd167c0a3f56d7409a))
* **Case:** MINTY-7112 - Add contacts to relations tab ([d8c8148](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d8c8148dbf3cb387e5866f335ddbd977585561de))
* **Case:** MINTY-7115 - Add planned emails to relations tab ([7732403](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/77324031c7f0de2250fd8573989d0f673a828d05))
* **Case:** MINTY-7963 - Implement contact importer in relation tab ([cad4334](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/cad433479c7608a2f6178acae2a27a1514f59bf4))
* **CaseTypeFinder:** MINTY-6888 - add support for on/offline finding of case types ([5aae3b1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5aae3b1a6deff6d44c6159026b6920ddf0e7c6b6))
* **ContactView:** MINTY-6980, MINTY-6982: add Timeline for ContactView, add export feature ([31b9738](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/31b97383504c928b9ce6d21d9d01bb657c94bf49))
* **ContactView:** MINTY-6980, MINTY-6982: add Timeline for ContactView, add export feature ([795682b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/795682b957d92dba94879966ece1f0bd780dbe6f))
* **Contactview:** MINTY-7096 - Add overview and decorate table ([3c36e5e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3c36e5e0c4a18ec874776d6268f74f0d1327c3cc))
* **ContactView:** MINTY-7547 - Show inactive warning for employees without roles ([0f9e4b4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0f9e4b41772ade3aece8d904c8e457a4349b532e))
* **ContactView:** MINTY-8222 - Implement secret and internaleNote notifications ([1555457](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1555457e5bd052bb4da00c3dfb2643eb3ff13259))
* **DocumentExplorer:** MINTY-6746 - only show document explorer labels in case of document type ([9e92eda](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9e92edac7c25b54a1b44c1b5947e379fbb8b9702))
* **Documentintake:** MINTY-7791 - Add button to navigate to v1 intake ([6a9ba4a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6a9ba4a84f4acdaf1e3bad62aaf379aaeb86932f))
* **FileSelect:** MINTY-4836 - change FileSelect texts to account for multivalue option ([6d28c31](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6d28c3107adb361c869cab33b9e1a61c84ebb10a))
* **Importer:** MINTY-7384 - implement grouped validation in person form ([93e355f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/93e355fe6a1e8a403491c66562133fff86f11756))
* **Importer:** MINTY-7480 - rework logic for importing external persons ([970d5e0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/970d5e0f9608fd4385097da29c6b188fc9842a95))
* **Importer:** MINTY-7480 - rework logic for importing external persons ([a4921f7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a4921f7784282057890f746970c486546376650c))
* **Importer:** MINTY-7480 - rework logic for importing external persons ([8f24ec1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8f24ec1c682eec95b66ab458f18a1f7f356483ec))
* **Intake:** MINTY-6443 - add option to reject document in intake ([88f50eb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/88f50eb53bf3d7a7b79eac8c450c0f7480fac966))
* **Intake:** MINTY-6733 - add document preview functionality ([9adb63b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9adb63b775f42df0bd6930974ab3df1b537a86a1))
* **Intake:** MINTY-7571 - Use backend for sorting, filtering, paging in document intake ([785033f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/785033f078101c9a0939f2a612490ac591621bf5))
* **Intake:** MINTY-7571 - Use backend for sorting, filtering, paging in document intake - change loading mechanism on page 0 ([f2779a9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f2779a97478932a7121ec1958afbdfda339a9fc3))
* **Intake:** MINTY-7790 - add ability to search/import from KVK in Intake import ([7c98747](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7c987472c2a9f0e488cf0ccae0d9b833eb9b9472))
* **Intake:** MINTY-7790 - add ability to search/import from KVK in Intake import - fix gender issue ([818d8b2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/818d8b20d4872a64b513b2f791dbab3eccb80094))
* **Intake:** MINTY-8234 - change add to case dialog to show documentnames in case of multiple ([c7ec5e5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c7ec5e500fddeee1afea25685479fbd9d46aafa4))
* **Intake:** MINTY-8234 - change add to case dialog to show documentnames in case of multiple - fix broken config ([b46336a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b46336a7b027c6c6227d63f248c9795c3cecee84))
* **ObjectForm:** MINTY-7460 - Prefill values based on casetype attribute mapping ([6852990](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/685299021f2e83d7f41a6e5527385518a1181ee1))
* **ObjectForm:** MINTY-7466 - Hide system attributes ([821bfdf](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/821bfdf78b9d6241778212d11fdedb547b152519))
* **Objects:** MINTY-7929 - add support for multivalue object attribute ([0357f23](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0357f23aebaa34c7f6d7a93231ef92c170974790))
* **Objects:** MINTY-7929 - add support for multivalue object attribute ([9d35477](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9d354772c8ae64e0dd9818752ea67e6fccf1be8b))
* **ObjectView:** MINTY-5177 - Allow editing ([2702760](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2702760f4eb375b1d4acde2db041b6c3395a066a))
* **ObjectView:** MINTY-7467 - Support system attributes ([4c0a745](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4c0a745d54faeff9f2c3c92d78756ba8817e4f78))
* **RolePicker:** MINTY-8232 - Categorize roles by system and department and sort A-Z ([86b03f3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/86b03f30a477d24066c9cc4229631b1cf9adf014))
* **Search:** MINTY-7847, MINTY-7853 - add filters to main search, open in new window ([9cb3fbd](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9cb3fbdf17bc533aaffa4fc03ebab9e778d054d9))
* **Tasks:** MINTY-7248 - change filtering to server side for Tasks Dashboard Widget ([5cadb28](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5cadb28d58e1f755fcf217a5ad10475c5122afce))
* **Timeline:** add timeline page for cases ([ca7e98c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ca7e98c5bb3dff9376cb19a397d7dc3d683ddb9a))
* **Timeline:** MINTY-5337 - add Timeline component ([5ad8858](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ad885846dfa18a48da2245ee482cd49984fac15))
* **Timeline:** MINTY-7610 - add timeline page for cases - better handling of icons ([88bf7d3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/88bf7d3a7013e9bbe8cc5c0bb9ccea480d8d31bc))
* **Timeline:** MINTY-7845 - Fix timeline icons and titles, and prefix case descriptions ([9d342a6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9d342a6fe789d128c6b3e2ad1e0fb8046ee047cc))
* **Timeline:** MINTY-8235 - add types filter to case timeline ([87aff18](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/87aff18ca80da0fb2572fccf1b41f1d56a736a6d))
* **Upload:** MINTY-7169 - add tooltip hover on uploaded files in case of backend error ([ef29380](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ef293807740d96d9fd96d641fc2c2994727cc8ce))
* **Upload:** MINTY-7448 - Prevent deadlocks by uploading files in series ([254a4a9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/254a4a9544375e4c02240f64d9f8eb3e2c33955a))





## [0.55.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.55.0...@zaaksysteem/common@0.55.1) (2020-12-21)

**Note:** Version bump only for package @zaaksysteem/common





# [0.55.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.54.4...@zaaksysteem/common@0.55.0) (2020-12-18)


### Features

* **ObjectForm:** MINTY-5537 - Prefill case data when creating a new object ([996ee0f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/996ee0f7422a81e84b04c77140b3f148af1c8908))





## [0.54.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.54.3...@zaaksysteem/common@0.54.4) (2020-12-18)

**Note:** Version bump only for package @zaaksysteem/common





## [0.54.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.54.2...@zaaksysteem/common@0.54.3) (2020-12-17)

**Note:** Version bump only for package @zaaksysteem/common





## [0.54.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.54.1...@zaaksysteem/common@0.54.2) (2020-12-10)

**Note:** Version bump only for package @zaaksysteem/common





## [0.54.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.54.0...@zaaksysteem/common@0.54.1) (2020-12-04)

**Note:** Version bump only for package @zaaksysteem/common





# [0.54.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.53.1...@zaaksysteem/common@0.54.0) (2020-11-25)


### Features

* **Fields:** MINTY-5651 - add numeric field type ([85ff0b1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/85ff0b1234f03cc7460be6c773596292c9e20635))





## [0.53.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.53.0...@zaaksysteem/common@0.53.1) (2020-11-19)

**Note:** Version bump only for package @zaaksysteem/common





# [0.53.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.52.6...@zaaksysteem/common@0.53.0) (2020-11-18)


### Features

* **Objectform:** MINTY-5650 - Support checkbox field ([c0aeb79](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c0aeb79))





## [0.52.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.52.5...@zaaksysteem/common@0.52.6) (2020-11-11)

**Note:** Version bump only for package @zaaksysteem/common





## [0.52.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.52.4...@zaaksysteem/common@0.52.5) (2020-11-11)

**Note:** Version bump only for package @zaaksysteem/common





## [0.52.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.52.3...@zaaksysteem/common@0.52.4) (2020-11-11)

**Note:** Version bump only for package @zaaksysteem/common





## [0.52.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.52.2...@zaaksysteem/common@0.52.3) (2020-10-30)

**Note:** Version bump only for package @zaaksysteem/common





## [0.52.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.52.1...@zaaksysteem/common@0.52.2) (2020-10-26)


### Bug Fixes

* **Intake:** MINTY-5507 - remove ??? from intake ([e7e89ca](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e7e89ca))





## [0.52.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.52.0...@zaaksysteem/common@0.52.1) (2020-10-26)


### Bug Fixes

* **ContactFinder:** MINTY-5313 - fix broken contact finder configs ([73f6c23](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/73f6c23))





# [0.52.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.51.1...@zaaksysteem/common@0.52.0) (2020-10-15)


### Features

* **ObjectView:** MINTY-5261 - Custom field link in object view ([04b26b1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/04b26b1))





## [0.51.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.51.0...@zaaksysteem/common@0.51.1) (2020-10-15)

**Note:** Version bump only for package @zaaksysteem/common





# [0.51.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.50.0...@zaaksysteem/common@0.51.0) (2020-10-15)


### Features

* **Intake:** MINTY-5324 - add extension hints and label to Document Intake ([e6d0049](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e6d0049))





# [0.50.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.49.1...@zaaksysteem/common@0.50.0) (2020-10-08)


### Features

* **TasksWidget:** MINTY-4976 - add Tasks widget features with overlay form ([ba9c2bf](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ba9c2bf))
* **TasksWidget:** MINTY-4976 - change outputFormat to config ([069fd79](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/069fd79))
* **TasksWidget:** MINTY-4976 - fix crash bug with department finder labels ([b372f6e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b372f6e))
* **TasksWidget:** MINTY-4976 - misc MR related code improvements ([8a27f33](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8a27f33))





## [0.49.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.49.0...@zaaksysteem/common@0.49.1) (2020-10-02)

**Note:** Version bump only for package @zaaksysteem/common





# [0.49.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.48.6...@zaaksysteem/common@0.49.0) (2020-09-30)


### Features

* **Intake:** MINTY-5066 & MINTY-5069 - change intake behaviour in Pip/Intake ([aaa8bdf](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/aaa8bdf))





## [0.48.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.48.5...@zaaksysteem/common@0.48.6) (2020-09-30)

**Note:** Version bump only for package @zaaksysteem/common





## [0.48.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.48.4...@zaaksysteem/common@0.48.5) (2020-09-29)


### Bug Fixes

* **Intake:** MINTY-5067 - fix casetype finder not showing options ([77bb7ad](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/77bb7ad))





## [0.48.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.48.3...@zaaksysteem/common@0.48.4) (2020-09-25)

**Note:** Version bump only for package @zaaksysteem/common





## [0.48.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.48.2...@zaaksysteem/common@0.48.3) (2020-09-23)

**Note:** Version bump only for package @zaaksysteem/common





## [0.48.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.48.1...@zaaksysteem/common@0.48.2) (2020-09-18)

**Note:** Version bump only for package @zaaksysteem/common





## [0.48.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.48.0...@zaaksysteem/common@0.48.1) (2020-09-17)

**Note:** Version bump only for package @zaaksysteem/common





# [0.48.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.47.3...@zaaksysteem/common@0.48.0) (2020-09-17)


### Features

* **PIP:** MINTY-3636 - Make prefilled case uuids visible as read-only ([f2eba03](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f2eba03))





## [0.47.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.47.2...@zaaksysteem/common@0.47.3) (2020-09-16)

**Note:** Version bump only for package @zaaksysteem/common





## [0.47.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.47.1...@zaaksysteem/common@0.47.2) (2020-09-16)

**Note:** Version bump only for package @zaaksysteem/common





## [0.47.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.47.0...@zaaksysteem/common@0.47.1) (2020-09-15)

**Note:** Version bump only for package @zaaksysteem/common





# [0.47.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.46.0...@zaaksysteem/common@0.47.0) (2020-09-15)


### Features

* **Main:** MINTY-4808 - create case from + button ([2f1fbe9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2f1fbe9))





# [0.46.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.45.1...@zaaksysteem/common@0.46.0) (2020-09-04)


### Features

* **ObjectView:** Implement readOnly mode for objectView ([3ae03a7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3ae03a7))





## [0.45.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.45.0...@zaaksysteem/common@0.45.1) (2020-09-03)

**Note:** Version bump only for package @zaaksysteem/common





# [0.45.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.44.0...@zaaksysteem/common@0.45.0) (2020-09-03)


### Features

* **ObjectView:** MINTY-4867, MINTY-4877 - add relationships overview to Object View ([57ef51d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/57ef51d))





# [0.44.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.43.4...@zaaksysteem/common@0.44.0) (2020-09-03)


### Features

* **Main:** MINTY-4758 - start new action from Object View ([1867c5b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1867c5b))





## [0.43.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.43.3...@zaaksysteem/common@0.43.4) (2020-09-03)

**Note:** Version bump only for package @zaaksysteem/common





## [0.43.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.43.2...@zaaksysteem/common@0.43.3) (2020-09-01)

**Note:** Version bump only for package @zaaksysteem/common





## [0.43.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.43.1...@zaaksysteem/common@0.43.2) (2020-09-01)

**Note:** Version bump only for package @zaaksysteem/common





## [0.43.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.43.0...@zaaksysteem/common@0.43.1) (2020-09-01)

**Note:** Version bump only for package @zaaksysteem/common





# [0.43.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.42.0...@zaaksysteem/common@0.43.0) (2020-08-27)


### Features

* **Dashboard:** MINTY-4800 - add Tasks Dashboard widget merged ([df0dffc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/df0dffc))





# [0.42.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.41.0...@zaaksysteem/common@0.42.0) (2020-08-21)


### Features

* **objectForm:** MINTY-4749 - Add support for currency, email, webaddress, iban, date ([f18d1ea](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f18d1ea))





# [0.41.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.40.1...@zaaksysteem/common@0.41.0) (2020-08-20)


### Features

* **Main:** MINTY-4752 - add Search from Object View ([ab753fb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ab753fb))





## [0.40.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.40.0...@zaaksysteem/common@0.40.1) (2020-08-19)

**Note:** Version bump only for package @zaaksysteem/common





# [0.40.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.36.1...@zaaksysteem/common@0.40.0) (2020-08-11)


### Features

* **Intake:** MINTY-4507 - add thumbnail feature to Intake ([7695409](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7695409))
* **Object:** Add support for relationship custom_object and subject ([6e83436](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6e83436))





# [0.39.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.36.1...@zaaksysteem/common@0.39.0) (2020-08-07)


### Features

* **Intake:** MINTY-4507 - add thumbnail feature to Intake ([7695409](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7695409))
* **Object:** Add support for relationship custom_object and subject ([6e83436](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6e83436))





# [0.38.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.36.1...@zaaksysteem/common@0.38.0) (2020-08-07)


### Features

* **Intake:** MINTY-4507 - add thumbnail feature to Intake ([7695409](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7695409))
* **Object:** Add support for relationship custom_object and subject ([6e83436](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6e83436))





# [0.37.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.36.1...@zaaksysteem/common@0.37.0) (2020-08-06)


### Features

* **Intake:** MINTY-4507 - add thumbnail feature to Intake ([7695409](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7695409))
* **Object:** Add support for relationship custom_object and subject ([6e83436](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6e83436))





## [0.36.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.36.1...@zaaksysteem/common@0.36.2) (2020-08-06)

**Note:** Version bump only for package @zaaksysteem/common





## [0.36.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.36.0...@zaaksysteem/common@0.36.1) (2020-08-06)

**Note:** Version bump only for package @zaaksysteem/common





# [0.36.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.35.2...@zaaksysteem/common@0.36.0) (2020-07-23)


### Features

* **Intake:** Add support in External Contact for importing companies ([c43bdeb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c43bdeb))





## [0.35.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.35.1...@zaaksysteem/common@0.35.2) (2020-07-23)


### Bug Fixes

* **Catalog:** Fix ObjectTypeSelect setting form value ([87bcc09](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/87bcc09))





## [0.35.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.35.0...@zaaksysteem/common@0.35.1) (2020-07-23)

**Note:** Version bump only for package @zaaksysteem/common





# [0.35.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.34.5...@zaaksysteem/common@0.35.0) (2020-07-23)


### Features

* **Catalog:** Add support for attribute type Relation ([546907a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/546907a))





## [0.34.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.34.4...@zaaksysteem/common@0.34.5) (2020-07-17)

**Note:** Version bump only for package @zaaksysteem/common





## [0.34.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.34.3...@zaaksysteem/common@0.34.4) (2020-07-15)


### Bug Fixes

* **PipCaseDocuments:** MINTY-4542 - Prevent document view from breaking when mimetype is null ([f372c2f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f372c2f))





## [0.34.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.34.2...@zaaksysteem/common@0.34.3) (2020-07-15)

**Note:** Version bump only for package @zaaksysteem/common





## [0.34.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.34.1...@zaaksysteem/common@0.34.2) (2020-07-15)

**Note:** Version bump only for package @zaaksysteem/common





## [0.34.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.34.0...@zaaksysteem/common@0.34.1) (2020-07-14)

**Note:** Version bump only for package @zaaksysteem/common





# [0.34.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.33.10...@zaaksysteem/common@0.34.0) (2020-07-09)


### Features

* **External Contacts:** MINTY-4347: add External Contact search/import for persons ([ef299fe](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ef299fe))





## [0.33.10](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.33.9...@zaaksysteem/common@0.33.10) (2020-07-09)

**Note:** Version bump only for package @zaaksysteem/common





## [0.33.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.33.8...@zaaksysteem/common@0.33.9) (2020-07-02)

**Note:** Version bump only for package @zaaksysteem/common





## [0.33.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.33.7...@zaaksysteem/common@0.33.8) (2020-06-30)

**Note:** Version bump only for package @zaaksysteem/common





## [0.33.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.33.6...@zaaksysteem/common@0.33.7) (2020-06-26)

**Note:** Version bump only for package @zaaksysteem/common





## [0.33.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.33.5...@zaaksysteem/common@0.33.6) (2020-06-25)

**Note:** Version bump only for package @zaaksysteem/common





## [0.33.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.33.4...@zaaksysteem/common@0.33.5) (2020-06-23)

**Note:** Version bump only for package @zaaksysteem/common





## [0.33.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.33.3...@zaaksysteem/common@0.33.4) (2020-06-18)

**Note:** Version bump only for package @zaaksysteem/common





## [0.33.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.33.2...@zaaksysteem/common@0.33.3) (2020-06-17)

**Note:** Version bump only for package @zaaksysteem/common





## [0.33.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.33.1...@zaaksysteem/common@0.33.2) (2020-06-12)

**Note:** Version bump only for package @zaaksysteem/common





## [0.33.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.33.0...@zaaksysteem/common@0.33.1) (2020-06-11)


### Bug Fixes

* **Forms:** MINTY-4191 - fix form validation for file uploads ([eb4291b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/eb4291b))





# [0.33.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.32.4...@zaaksysteem/common@0.33.0) (2020-06-11)


### Features

* **Intake:** MINTY-4179 - add document number tooltip ([9cf6c1f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9cf6c1f))





## [0.32.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.32.3...@zaaksysteem/common@0.32.4) (2020-06-09)

**Note:** Version bump only for package @zaaksysteem/common





## [0.32.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.32.2...@zaaksysteem/common@0.32.3) (2020-06-05)

**Note:** Version bump only for package @zaaksysteem/common





## [0.32.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.32.1...@zaaksysteem/common@0.32.2) (2020-06-03)

**Note:** Version bump only for package @zaaksysteem/common





## [0.32.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.32.0...@zaaksysteem/common@0.32.1) (2020-05-29)

**Note:** Version bump only for package @zaaksysteem/common





# [0.32.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.31.3...@zaaksysteem/common@0.32.0) (2020-05-28)


### Features

* **case:** MINTY-4034 allow objects to be created from the case view ([ee6dd74](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ee6dd74))





## [0.31.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.31.2...@zaaksysteem/common@0.31.3) (2020-05-27)

**Note:** Version bump only for package @zaaksysteem/common





## [0.31.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.31.1...@zaaksysteem/common@0.31.2) (2020-05-19)

**Note:** Version bump only for package @zaaksysteem/common





## [0.31.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.31.0...@zaaksysteem/common@0.31.1) (2020-05-14)

**Note:** Version bump only for package @zaaksysteem/common





# [0.31.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.30.1...@zaaksysteem/common@0.31.0) (2020-05-14)


### Features

* **Intake:** MINTY-3868 - add description column to Intake ([c1c6df2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c1c6df2))





## [0.30.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.30.0...@zaaksysteem/common@0.30.1) (2020-05-13)

**Note:** Version bump only for package @zaaksysteem/common





# [0.30.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.29.3...@zaaksysteem/common@0.30.0) (2020-05-12)


### Features

* **CustomObject:** MINTY-3937 & MINTY-3926 - change authorization to radiobuttons, refactor saving and getting, enable server error dialog ([a2fa2eb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a2fa2eb))





## [0.29.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.29.2...@zaaksysteem/common@0.29.3) (2020-05-12)

**Note:** Version bump only for package @zaaksysteem/common





## [0.29.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.29.1...@zaaksysteem/common@0.29.2) (2020-05-11)

**Note:** Version bump only for package @zaaksysteem/common





## [0.29.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.29.0...@zaaksysteem/common@0.29.1) (2020-05-01)

**Note:** Version bump only for package @zaaksysteem/common





# [0.29.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.28.1...@zaaksysteem/common@0.29.0) (2020-05-01)


### Features

* **CreateObject:** MINTY-3472 - add Rights step to Object Create with associated components ([8699e78](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8699e78))





## [0.28.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.28.0...@zaaksysteem/common@0.28.1) (2020-04-30)

**Note:** Version bump only for package @zaaksysteem/common





# [0.28.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.27.0...@zaaksysteem/common@0.28.0) (2020-04-28)


### Features

* **SubForms:** MINTY-3806 Add `useSubForm` hook that enables the use of namespaced subforms ([ffcde68](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ffcde68))





# [0.27.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.26.9...@zaaksysteem/common@0.27.0) (2020-04-20)


### Features

* **CommunicationModule:** MINTY-3790 Send filename when creating external message ([b72104d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b72104d))





## [0.26.9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.26.8...@zaaksysteem/common@0.26.9) (2020-04-16)

**Note:** Version bump only for package @zaaksysteem/common





## [0.26.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.26.7...@zaaksysteem/common@0.26.8) (2020-04-16)

**Note:** Version bump only for package @zaaksysteem/common





## [0.26.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.26.6...@zaaksysteem/common@0.26.7) (2020-04-14)

**Note:** Version bump only for package @zaaksysteem/common





## [0.26.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.26.5...@zaaksysteem/common@0.26.6) (2020-04-10)

**Note:** Version bump only for package @zaaksysteem/common





## [0.26.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.26.4...@zaaksysteem/common@0.26.5) (2020-04-09)

**Note:** Version bump only for package @zaaksysteem/common





## [0.26.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.26.3...@zaaksysteem/common@0.26.4) (2020-04-07)


### Bug Fixes

* **PipDocuments:** MINTY-3619 - fix PipDocuments not showing all rows ([a175c33](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a175c33))





## [0.26.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.26.2...@zaaksysteem/common@0.26.3) (2020-04-06)

**Note:** Version bump only for package @zaaksysteem/common





## [0.26.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.26.1...@zaaksysteem/common@0.26.2) (2020-04-03)


### Bug Fixes

* **PipDocuments:** MINTY-3619 - fix PipDocuments not showing all rows ([67cf987](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/67cf987))





## [0.26.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.26.0...@zaaksysteem/common@0.26.1) (2020-04-01)

**Note:** Version bump only for package @zaaksysteem/common





# [0.26.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.25.0...@zaaksysteem/common@0.26.0) (2020-04-01)


### Features

* **ObjectTypeManagement:** Implement Dialog with warning of pending changes that will be lost when leaving the form ([d6ef897](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d6ef897))





# [0.25.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.24.0...@zaaksysteem/common@0.25.0) (2020-03-31)


### Features

* **CheckboxGroup:** MINTY-3473 Add CheckboxGroup component ([61a250a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/61a250a))





# [0.24.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.23.1...@zaaksysteem/common@0.24.0) (2020-03-30)


### Features

* **ObjectTypeManagement:** MINTY-3469 Create first page of ObjectTypeManagement wizard ([d1b8822](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d1b8822))





## [0.23.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.23.0...@zaaksysteem/common@0.23.1) (2020-03-30)

**Note:** Version bump only for package @zaaksysteem/common





# [0.23.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.22.0...@zaaksysteem/common@0.23.0) (2020-03-30)


### Features

* **FormRenderer:** Improve performance and readability of `FormRenderer` component ([f310425](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f310425))
* **FormRenderer:** MINTY-3486 Clean up form definition types ([3f88403](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3f88403))
* **FormRenderer:** Refactor formRenderer to hook and update Formik to latest version 2.1.4 ([c5d1e71](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c5d1e71))
* **SteppedForm:** MINTY-3486 Add type for stepped form and account for it in all helper utilities ([d3ff7ac](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d3ff7ac))
* **SteppedForm:** MINTY-3486 Implement stepped form ([6c5c6e3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6c5c6e3))





# [0.22.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.21.0...@zaaksysteem/common@0.22.0) (2020-03-19)


### Features

* **DocumentExplorer:** MINTY-3300 - add initial PDF preview features, misc. ([a131814](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a131814))
* **DocumentExplorer:** MINTY-3300 - change Loader, exports ([b25d770](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b25d770))
* **DocumentExplorer:** MINTY-3300 - split up DocumentExplorer functionality, create new Intake entrypoint, add Selectable state to table ([5bae0d3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5bae0d3))
* **Intake:** MINTY-3437 - add search component ([2bb171b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2bb171b))





# [0.21.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.20.1...@zaaksysteem/common@0.21.0) (2020-03-17)


### Features

* **CaseRequestor:** MINTY-3297 Use nested form value instead of flat so you have access to the label ([3eccfa3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3eccfa3))
* **FormDefinition:** MINTY-3297 Allow defining type of form value for a specific field ([8bf9a81](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8bf9a81))
* **MessagePreview:** MINTY-3297 Add GenericFormMessagePreview component to display messages that contain magic strings ([13dce0f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/13dce0f))





## [0.20.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.20.0...@zaaksysteem/common@0.20.1) (2020-03-17)

**Note:** Version bump only for package @zaaksysteem/common





# [0.20.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.19.3...@zaaksysteem/common@0.20.0) (2020-03-05)


### Bug Fixes

* **CaseDocuments:** apply MR comments ([778fd18](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/778fd18))
* **CaseDocuments:** fix error dialog not showing, search to autocomplete, fix breadcrumbs ([8d76563](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8d76563))


### Features

* **CaseDocuments:** add dynamic or fixed rowheights, description field ([3170792](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/3170792))
* **CaseDocuments:** add misc features ([8845760](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8845760))
* **CaseDocuments:** MINTY-2880 - add default file/folder sorting ([9719e3a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9719e3a))
* **CaseDocuments:** MINTY-2880 - improve typing, some misc fixes ([da1ef46](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/da1ef46))
* **CaseDocuments:** MINTY-2882 - enable download link ([09f2ce5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/09f2ce5))
* **CaseDocuments:** MINTY-2884 - add search functionality ([f91aea4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f91aea4))
* **CaseDocuments:** MINTY-2884: add file icons to FileBrowser based on mime type ([4223ab5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4223ab5))
* **CaseDocuments:** MINTY-2995 - enable creating of documents from file uploads ([2792d26](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2792d26))
* **CaseDocuments:** MINTY-3043, MINTY-3045: get documents from server, implement breadcrumb, error handling ([9519313](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/9519313))
* **CaseDocuments:** MINTY-3150 - show file status if not accepted ([7425526](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7425526))
* **CaseDocuments:** MINTY-3191 - add misc. UX tweaks, code layout ([11658c1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/11658c1))
* **CaseDocuments:** MINTY-3191 - implement misc. UX/Design changes ([a2c44c3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a2c44c3))
* **DocumentPreview:** MINTY-2960 Add case document preview to dev dashboard ([0f6fb22](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/0f6fb22))
* **DocumentPreview:** MINTY-2960 Add DocumentPreview component ([ef352ac](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ef352ac))
* **DocumentPreview:** MINTY-2960 Add DocumentPreviewModal component which wraps the DocumentPreview component in a modal ([352ae9a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/352ae9a))
* **DocumentPreview:** MINTY-2960 Exclude iOS devices from native PDF view since it's spotty ([963d5b5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/963d5b5))





## [0.19.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.19.2...@zaaksysteem/common@0.19.3) (2020-03-03)

**Note:** Version bump only for package @zaaksysteem/common





## [0.19.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.19.1...@zaaksysteem/common@0.19.2) (2020-02-24)

**Note:** Version bump only for package @zaaksysteem/common





## [0.19.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.19.0...@zaaksysteem/common@0.19.1) (2020-02-21)

**Note:** Version bump only for package @zaaksysteem/common





# [0.19.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.18.8...@zaaksysteem/common@0.19.0) (2020-02-17)


### Features

* **MultiValueText:** MINTY-3076 Add entered value on blur ([5c098f8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5c098f8))





## [0.18.8](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.18.7...@zaaksysteem/common@0.18.8) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/common





## [0.18.7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.18.6...@zaaksysteem/common@0.18.7) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/common





## [0.18.6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.18.5...@zaaksysteem/common@0.18.6) (2020-02-12)

**Note:** Version bump only for package @zaaksysteem/common





## [0.18.5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.18.4...@zaaksysteem/common@0.18.5) (2020-02-10)

**Note:** Version bump only for package @zaaksysteem/common





## [0.18.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.18.3...@zaaksysteem/common@0.18.4) (2020-02-07)

**Note:** Version bump only for package @zaaksysteem/common

## [0.18.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.18.2...@zaaksysteem/common@0.18.3) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/common

## [0.18.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.18.1...@zaaksysteem/common@0.18.2) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/common

## [0.18.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.18.0...@zaaksysteem/common@0.18.1) (2020-02-06)

**Note:** Version bump only for package @zaaksysteem/common

# [0.18.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.17.1...@zaaksysteem/common@0.18.0) (2020-01-30)

### Features

- **communication:** Add email to label of contact search field ([64d48fb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/64d48fb))

## [0.17.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.17.0...@zaaksysteem/common@0.17.1) (2020-01-23)

**Note:** Version bump only for package @zaaksysteem/common

# [0.17.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.16.0...@zaaksysteem/common@0.17.0) (2020-01-23)

### Features

- **Communication:** MINTY-2658 Add CaseRequestor form field which fetches te current case requestor ([8af717c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8af717c))

# [0.16.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.15.0...@zaaksysteem/common@0.16.0) (2020-01-22)

### Bug Fixes

- **CaseDocumentSelect:** Add ConfigType of CaseDocumentSelect ([1c48eb2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1c48eb2))

### Features

- **CaseDocumentSelect:** Autoprovide case documents instead of searching by keyword ([1cab1eb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1cab1eb))
- **MessageForm:** Replace upload ability with ability to select case documents in case context ([e1d1f92](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/e1d1f92))

# [0.15.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.14.0...@zaaksysteem/common@0.15.0) (2020-01-14)

### Features

- **Communication:** MINTY-2704 Add `gemachtigde` option to email recipient selector ([b2a204a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b2a204a))

# [0.14.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.13.0...@zaaksysteem/common@0.14.0) (2020-01-10)

### Features

- **Communication:** MINTY-2658 Remove 'aanvrager' when it is a preset case requestor ([8b72fcd](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8b72fcd))
- **Communication:** MINTY-2665 Use `ContactFinder` component to search for email recipients ([c0a4ad6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c0a4ad6))
- **Communication:** MINTY-2692 Add role selection to email recipient form ([2122179](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/2122179))
- **Communication:** MINTY-2693 Add EmailRecipient selection component ([c67d8e9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c67d8e9))

# [0.13.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.12.0...@zaaksysteem/common@0.13.0) (2020-01-09)

### Bug Fixes

- (CaseManagement) truncate task title ([c5d397b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c5d397b))
- **Forms:** add trim to string validation so spaces are not valid values ([5733b0d](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5733b0d))
- **Tasks:** MINTY-2769 - fix redirect logic in middleware ([1d09b68](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/1d09b68))
- **Tasks:** MINTY-2816 - change get_task_list endpoint to filter ([f127e36](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f127e36))
- **UI:** fix component imports ([797f9a9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/797f9a9))

### Features

- **CaseManagement:** add task creator ([939fbbb](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/939fbbb))
- **ContactFinder:** MINTY-2618 - add type filter support for ContactFinder ([d837b4b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/d837b4b))
- **Tasks:** MINTY-1575 - add basic form fields, DatePicker ([07ed5e5](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/07ed5e5))
- **Tasks:** MINTY-1575 - add edit, delete, unlock functionality ([8dca70b](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8dca70b))
- **Tasks:** MINTY-1575 - fix build/import problems ([23c0700](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/23c0700))
- **Tasks:** MINTY-2695 - process some MR feedback ([8bd89cc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8bd89cc))
- **Tasks:** MINTY-2695 - rewrite internal logic to immediate responding to user actions ([c0f74c2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c0f74c2))

# [0.12.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.11.4...@zaaksysteem/common@0.12.0) (2020-01-09)

### Bug Fixes

- **Communication:** MINTY-2262 - allow e-mail addresses over multiple lines ([db44093](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/db44093))
- **Thread:** Fix search case API to new version as filter-status ([85aff79](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/85aff79))
- **UI:** Fix aria-labels on snackbar and dialogs ([775a287](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/775a287))

### Features

- **AddThread:** Filter out resolved cases ([7f31864](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7f31864))
- **CaseAttributes:** add Numeric field ([61d9b6e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/61d9b6e))
- **CaseAttributes:** add TextField component ([8da8bb9](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/8da8bb9))
- **CaseFinder:** Display the case status of the case results ([5cbfeb0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5cbfeb0))
- **Communication:** MINTY-1798 Add dialog to import message ([b4543aa](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b4543aa))
- **Communication:** MINTY-1925 Implement delete message action and trigger refresh using redux middleware ([4d15e36](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4d15e36))
- **Communication:** Replace omitStatus with a filter for multiple statuses ([f2f8e95](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f2f8e95))
- **ConfirmDialog:** MINTY-1925 Add generic `ConfirmDialog` component and hook ([814f2e0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/814f2e0))
- **Forms:** MINTY-2323 - add generic FormDialog component and implement throughout ([7f46465](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7f46465))
- **i18n:** Move common locale namespace to `@zaaksysteem/common` package ([4a36e92](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/4a36e92))
- **Thread:** Allow threads to be linked to cases from main view ([44a8a51](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/44a8a51))

## [0.11.4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.11.3...@zaaksysteem/common@0.11.4) (2019-12-19)

**Note:** Version bump only for package @zaaksysteem/common

## [0.11.3](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.11.2...@zaaksysteem/common@0.11.3) (2019-11-21)

**Note:** Version bump only for package @zaaksysteem/common

## [0.11.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.11.1...@zaaksysteem/common@0.11.2) (2019-10-29)

**Note:** Version bump only for package @zaaksysteem/common

## [0.11.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.11.0...@zaaksysteem/common@0.11.1) (2019-10-25)

**Note:** Version bump only for package @zaaksysteem/common

# [0.11.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.10.2...@zaaksysteem/common@0.11.0) (2019-10-17)

### Features

- **Snackbar:** MINTY-1788 Implement snackbar in pip and main app ([dcbe4da](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/dcbe4da))

## [0.10.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.10.1...@zaaksysteem/common@0.10.2) (2019-09-30)

**Note:** Version bump only for package @zaaksysteem/common

## [0.10.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.10.0...@zaaksysteem/common@0.10.1) (2019-09-26)

**Note:** Version bump only for package @zaaksysteem/common

# [0.10.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.9.1...@zaaksysteem/common@0.10.0) (2019-09-09)

### Features

- **Thread:** Implement typescript ([896ada2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/896ada2))

## [0.9.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.9.0...@zaaksysteem/common@0.9.1) (2019-09-06)

**Note:** Version bump only for package @zaaksysteem/common

# [0.9.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.8.0...@zaaksysteem/common@0.9.0) (2019-09-05)

### Features

- MINTY-1620: adjust presets for buttons in dialogs/forms ([81adac4](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/81adac4))

# [0.8.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.7.2...@zaaksysteem/common@0.8.0) (2019-08-29)

### Features

- **Alert:** MINTY-1126 Centralize `Alert` component so it can be used in multiple applications ([c44ec56](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/c44ec56))
- **Communication:** MINTY-1115 Add `SearchField` to `Theads` ([5ec462a](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/5ec462a))
- **Communication:** MINTY-1115 Add direction icon to indicate incoming or outgoing messages ([6b8e4fa](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/6b8e4fa))
- **UI:** Upgrade to MUI4 ([ac921f6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/ac921f6))

## [0.7.2](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.7.1...@zaaksysteem/common@0.7.2) (2019-08-27)

**Note:** Version bump only for package @zaaksysteem/common

## [0.7.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.7.0...@zaaksysteem/common@0.7.1) (2019-08-12)

**Note:** Version bump only for package @zaaksysteem/common

# [0.7.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.6.1...@zaaksysteem/common@0.7.0) (2019-08-06)

### Features

- **Admin:** MINTY-1281 Move admin app to mono repo ([f6a1d35](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/f6a1d35))

## [0.6.1](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.6.0...@zaaksysteem/common@0.6.1) (2019-07-31)

**Note:** Version bump only for package @zaaksysteem/common

# [0.6.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.5.0...@zaaksysteem/common@0.6.0) (2019-07-31)

### Features

- **Communication:** add general Communication setup and Thread list ([7cb92e6](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/7cb92e6))

# [0.5.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.4.0...@zaaksysteem/common@0.5.0) (2019-07-30)

### Features

- **FormRenderer:** MINTY-1126 Move `FormRenderer` component with validation and translations from apps project to `@zaaksysteem/common` ([b9e0245](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/b9e0245))

# [0.4.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.3.0...@zaaksysteem/common@0.4.0) (2019-07-29)

### Features

- **i18n:** MINTY-1120 Add i18next to setup and define APP_CONTEXT_ROOT variable on build time. ([a3e6a4c](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a3e6a4c))

# [0.3.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.2.0...@zaaksysteem/common@0.3.0) (2019-07-25)

### Features

- **main:** MINTY-1237 Add session state to @zaaksysteem/common and implement in app ([575898f](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/575898f))

# [0.2.0](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/compare/@zaaksysteem/common@0.1.0...@zaaksysteem/common@0.2.0) (2019-07-25)

### Features

- **main:** MINTY-1232 Add `connected-react-router` package to link router to redux store ([a673309](https://gitlab.com/zaaksysteem/zaaksysteem-frontend-mono/commit/a673309))

# 0.1.0 (2019-07-18)

### Features

- **Publishing:** MINTY-1120 Implement lerna publish command voor npm packages ([35934cc](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/35934cc))
- **Setup:** MINTY-1120 Add storybook ([1dc9406](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/1dc9406))
- **Setup:** MINTY-1120 Centralize create react app overrides in common package ([3cdc9d7](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/3cdc9d7))
- **Setup:** MINTY-1120 Setup monorepo for all apps and packges using lerna and yarn workspaces ([6bd626e](https://gitlab.com/zaaksysteem/zaaksysteem-frontend/commit/6bd626e))

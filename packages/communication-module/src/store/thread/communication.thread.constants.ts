// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxConstants } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';

export const THREAD_FETCH = createAjaxConstants('THREAD_FETCH');
export const ADD_ATTACHMENT_TO_CASE = createAjaxConstants(
  'ADD_ATTACHMENT_TO_CASE'
);
export const ADD_THREAD_TO_CASE = createAjaxConstants('ADD_THREAD_TO_CASE');
export const ADD_SOURCE_FILE_TO_CASE = createAjaxConstants(
  'ADD_SOURCE_FILE_TO_CASE'
);
export const DELETE_MESSAGE = createAjaxConstants('DELETE_MESSAGE');
export const MARK_MESSAGE_AS_READ = createAjaxConstants('MARK_MESSAGE_AS_READ');
export const MARK_MESSAGE_AS_UNREAD = createAjaxConstants(
  'MARK_MESSAGE_AS_UNREAD'
);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Reducer } from 'redux';
import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { handleAjaxStateChange } from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import { APICommunication } from '@zaaksysteem/generated';
import { THREAD_FETCH, DELETE_MESSAGE } from './communication.thread.constants';
import { AnyMessageType } from './../../types/Message.types';
import {
  mapContactMomentMessage,
  mapNoteMessage,
  mapExternalMessage,
} from './library/messageMapFunctions';
import {
  FetchThreadActionPayloadType,
  DeleteMessagePayloadType,
} from './communication.thread.actions';

export interface CommunicationThreadState {
  state: AjaxState;
  caseRelation: null | {
    id: string;
    caseNumber: string;
    status: string;
  };
  id: string;
  messages?: AnyMessageType[];
}

const initialState: CommunicationThreadState = {
  state: AJAX_STATE_INIT,
  caseRelation: null,
  id: '',
  messages: undefined,
};

const getCaseRelationship = (
  response: APICommunication.GetMessageListResponseBody
): any => {
  return response.relationships && response.relationships.case
    ? {
        id: response.relationships.case.data.id,
        caseNumber: response.relationships.case.data.attributes.display_id,
        status: response.relationships.case.data.attributes.status,
      }
    : null;
};

const handleFetchSuccess = (
  state: CommunicationThreadState,
  action: AjaxAction<
    APICommunication.GetMessageListResponseBody,
    FetchThreadActionPayloadType
  >
): CommunicationThreadState => {
  const messages = action.payload.response.data;

  if (!messages) {
    return state;
  }

  return {
    ...state,
    caseRelation: getCaseRelationship(action.payload.response),
    id: action.payload.uuid,
    messages: messages.map<AnyMessageType>(message => {
      switch (message.type) {
        case 'contact_moment':
          return mapContactMomentMessage(message);

        case 'external_message':
          return mapExternalMessage(message);

        case 'note':
          return mapNoteMessage(message);
      }
    }),
  };
};

const handleDeleteMessageSuccess = (
  state: CommunicationThreadState,
  action: AjaxAction<
    APICommunication.DeleteMessageResponseBody,
    DeleteMessagePayloadType
  >
): CommunicationThreadState => {
  if (!state.messages) {
    return state;
  }

  const { id } = action.payload;

  return {
    ...state,
    messages: state.messages.filter(item => item.id !== id),
  };
};

export const threadList: Reducer<
  CommunicationThreadState,
  AjaxAction<unknown>
> = (state = initialState, action) => {
  const { type } = action;

  const handleFetchThreadListStateChange = handleAjaxStateChange(THREAD_FETCH);

  switch (type) {
    case THREAD_FETCH.PENDING:
    case THREAD_FETCH.ERROR:
      return handleFetchThreadListStateChange(state, action);
    case THREAD_FETCH.SUCCESS:
      return handleFetchThreadListStateChange(
        handleFetchSuccess(
          state,
          action as AjaxAction<
            APICommunication.GetMessageListResponseBody,
            FetchThreadActionPayloadType
          >
        ),
        action
      );

    case DELETE_MESSAGE.SUCCESS:
      return handleDeleteMessageSuccess(
        state,
        action as AjaxAction<
          APICommunication.DeleteMessageResponseBody,
          DeleteMessagePayloadType
        >
      );
    default:
      return state;
  }
};

export default threadList;

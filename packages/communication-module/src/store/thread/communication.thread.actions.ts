// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { buildUrl } from '@mintlab/kitchen-sink/source';
import { APICommunication, APIDocument } from '@zaaksysteem/generated';
import { ThunkActionWithPayload } from '@zaaksysteem/common/src/types/ActionWithPayload';
import { CommunicationRootStateType } from '../communication.reducer';
import { TYPE_EMAIL } from '../../library/communicationTypes.constants';
import { SubTypeOfMessage } from '../../types/Message.types';
import {
  THREAD_FETCH,
  ADD_ATTACHMENT_TO_CASE,
  ADD_THREAD_TO_CASE,
  ADD_SOURCE_FILE_TO_CASE,
  DELETE_MESSAGE,
  MARK_MESSAGE_AS_READ,
  MARK_MESSAGE_AS_UNREAD,
} from './communication.thread.constants';

const fetchThreadAjaxAction = createAjaxAction(THREAD_FETCH);
const addThreadToCaseAjaxAction = createAjaxAction(ADD_THREAD_TO_CASE);

type OutputFormatType =
  APIDocument.CreateDocumentFromMessageRequestBody['output_format'];

export type FetchThreadActionPayloadType = {
  uuid: string;
};

export const fetchThreadAction = (uuid: string) => {
  const url = buildUrl<APICommunication.GetMessageListRequestParams>(
    '/api/v2/communication/get_message_list',
    {
      thread_uuid: uuid,
    }
  );

  return fetchThreadAjaxAction<FetchThreadActionPayloadType>({
    url,
    method: 'GET',
    payload: { uuid },
  });
};

const addAttachmentToCaseAjaxAction = createAjaxAction(ADD_ATTACHMENT_TO_CASE);

/**
 * @param {string} id
 * @return {Function}
 */
export const addAttachmentToCaseAction =
  (id: string): ThunkActionWithPayload<CommunicationRootStateType, {}> =>
  dispatch => {
    const data: APIDocument.CreateDocumentFromAttachmentRequestBody = {
      attachment_uuid: id,
    };

    return addAttachmentToCaseAjaxAction({
      url: '/api/v2/document/create_document_from_attachment',
      method: 'POST',
      data,
      payload: data,
    })(dispatch);
  };

export const addThreadToCaseAction = (caseUuid: string, threadUuid: string) => {
  const url = '/api/v2/communication/link_thread_to_case';
  const data: APICommunication.LinkThreadToCaseRequestBody = {
    case_uuid: caseUuid,
    thread_uuid: threadUuid,
    type: TYPE_EMAIL,
  };

  return addThreadToCaseAjaxAction({
    url,
    data,
    method: 'POST',
  });
};

type AddSourceFileToCasePayloadType = {
  id: string;
};

const addSourceFileToCaseAjaxAction = createAjaxAction(ADD_SOURCE_FILE_TO_CASE);

const saveInDocuments = {
  pip: (messageId: string) => {
    const url = '/api/v2/document/create_document_from_message';

    return addSourceFileToCaseAjaxAction<
      AddSourceFileToCasePayloadType,
      APIDocument.CreateDocumentFromMessageRequestBody
    >({
      url,
      method: 'POST',
      data: {
        message_uuid: messageId,
      },
      payload: { id: messageId },
    });
  },
  email: (messageId: string, outputFormat: OutputFormatType) => {
    const url = '/api/v2/document/create_document_from_message';

    return addSourceFileToCaseAjaxAction<
      AddSourceFileToCasePayloadType,
      APIDocument.CreateDocumentFromMessageRequestBody
    >({
      url,
      method: 'POST',
      data: {
        message_uuid: messageId,
        output_format: outputFormat,
      },
      payload: { id: messageId },
    });
  },
  postex: () => {
    /* For now don't save Postex messages, the message isn't used at all
     * by Postex
     */
    return;
  },
};

export const addSourceFileToCase = (
  messageId: string,
  messageSubtype: SubTypeOfMessage | undefined,
  outputFormat: OutputFormatType
) => {
  if (!messageSubtype) {
    throw 'Message subtype must be defined to trigger add source file to case action';
  }

  return saveInDocuments[messageSubtype](messageId, outputFormat);
};

export type DeleteMessagePayloadType = {
  id: string;
};

const deleteAjaxAction = createAjaxAction(DELETE_MESSAGE);

export const deleteMessage = (messageId: string) => {
  const url = '/api/v2/communication/delete_message';

  return deleteAjaxAction<
    DeleteMessagePayloadType,
    APICommunication.DeleteMessageRequestBody
  >({
    url,
    method: 'POST',
    data: {
      message_uuid: messageId,
    },
    payload: { id: messageId },
  });
};

export type MarkMessagePayloadType = {
  messageUuids: string[];
};

const markMessageAsReadAjaxAction = createAjaxAction(MARK_MESSAGE_AS_READ);

export const markMessageAsRead =
  (
    payload: MarkMessagePayloadType
  ): ThunkActionWithPayload<
    CommunicationRootStateType,
    MarkMessagePayloadType
  > =>
  (dispatch, getState) => {
    const url = '/api/v2/communication/mark_messages_read';
    const {
      communication: {
        context: { context },
      },
    } = getState();

    return markMessageAsReadAjaxAction<
      MarkMessagePayloadType,
      APICommunication.MarkMessagesReadRequestBody
    >({
      url,
      method: 'POST',
      data: {
        context: context === 'pip' ? 'pip' : 'employee',
        message_uuids: payload.messageUuids,
      },
      payload,
    })(dispatch);
  };

const markMessageAsUnreadAjaxAction = createAjaxAction(MARK_MESSAGE_AS_UNREAD);

export const markMessageAsUnread =
  ({
    messageUuids,
  }: MarkMessagePayloadType): ThunkActionWithPayload<
    CommunicationRootStateType,
    MarkMessagePayloadType
  > =>
  (dispatch, getState) => {
    const url = '/api/v2/communication/mark_messages_unread';
    const {
      communication: {
        context: { context },
      },
    } = getState();

    const payload = {
      context: (context === 'pip' ? 'pip' : 'employee') as 'pip' | 'employee',
      message_uuids: messageUuids,
    };

    return markMessageAsUnreadAjaxAction<APICommunication.MarkMessagesUnreadRequestBody>(
      {
        url,
        method: 'POST',
        data: payload,
        payload,
      }
    )(dispatch);
  };

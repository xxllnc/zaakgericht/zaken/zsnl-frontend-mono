// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { AnyMessageType } from '../../../types/Message.types';
import { CommunicationContextContextType } from '../../../types/Context.types';
import { ThreadType } from '../../../types/Thread.types';

export const isThreadUnread = (
  context: CommunicationContextContextType,
  thread?: ThreadType
) => {
  if (thread && typeof thread.unreadPip !== 'undefined') {
    return context === 'pip'
      ? Boolean(thread.unreadPip) === true
      : Boolean(thread.unreadEmployee) === true;
  }

  return false;
};

export const isMessageUnread = (
  context: CommunicationContextContextType,
  message?: AnyMessageType
) => {
  if (message && typeof message.readPip !== 'undefined') {
    return context === 'pip'
      ? Boolean(message.readPip) === false
      : Boolean(message.readEmployee) === false;
  }

  return false;
};

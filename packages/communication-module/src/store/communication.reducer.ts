// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { combineReducers } from 'redux';
import threadList, {
  CommunicationThreadListState,
} from './threadList/communication.threadList.reducer';
import thread, {
  CommunicationThreadState,
} from './thread/communication.thread.reducer';
import add, { CommunicationAddState } from './add/communication.add.reducer';
import context, {
  CommunicationContextState,
} from './context/communication.context.reducer';
import importMessage, {
  CommunicationImportMessageState,
} from './importMessage/communication.importMessage.reducer';
import navigate, {
  CommunicationNavigateState,
} from './navigate/communication.navigate.reducer';

interface CommunicationState {
  context: CommunicationContextState;
  add: CommunicationAddState;
  thread: CommunicationThreadState;
  threadList: CommunicationThreadListState;
  importMessage: CommunicationImportMessageState;
  navigate: CommunicationNavigateState;
}

export interface CommunicationRootStateType {
  communication: CommunicationState;
  // v1
  case: any;
}

export const communication = combineReducers<CommunicationState>({
  threadList,
  thread,
  add,
  context,
  navigate,
  importMessage,
});

export default communication;

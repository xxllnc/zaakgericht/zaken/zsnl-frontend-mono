// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const COMMUNICATION_SET_CONTEXT = 'COMMUNICATION_SET_CONTEXT';

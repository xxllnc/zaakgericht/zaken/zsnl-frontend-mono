// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ActionCreator } from 'redux';
import { v4 } from 'uuid';
import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { asArray } from '@mintlab/kitchen-sink/source';
import { ThunkActionWithPayload } from '@zaaksysteem/common/src/types/ActionWithPayload';
import { APICommunication } from '@zaaksysteem/generated';
import { NestedFormValue } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import {
  TYPE_CONTACT_MOMENT,
  TYPE_NOTE,
  TYPE_PIP_MESSAGE,
} from '../../library/communicationTypes.constants';
import { CommunicationRootStateType } from '../communication.reducer';
import {
  SaveContactMomentFormValuesType,
  ExternalMessageParticipantsType,
} from '../../types/Message.types';
import { toParticipantsArray } from '../library/participants';
import {
  SAVE_COMMUNICATION,
  SET_SAVE_COMMUNICATION_PENDING,
} from './communication.add.constants';

export type SaveContactMomentPayloadType = {
  values: SaveContactMomentFormValuesType;
};

export type SaveNotePayloadType = {
  values: Pick<APICommunication.CreateNoteRequestBody, 'content'>;
};

export type SaveMessagePayloadType = Pick<
  APICommunication.CreateExternalMessageRequestBody,
  'case_uuid' | 'content' | 'subject' | 'message_type'
> &
  Partial<
    Pick<
      APICommunication.CreateExternalMessageRequestBody,
      'thread_uuid' | 'participants'
    >
  > & {
    attachments: NestedFormValue | NestedFormValue[] | null;
  };

export type SaveEmailMessagePayloadType = SaveMessagePayloadType &
  ExternalMessageParticipantsType;

const saveAjaxAction = createAjaxAction(SAVE_COMMUNICATION);

/**
 * @param {Object} payload
 * @return {Function}
 */
export const saveAction: ActionCreator<
  ThunkActionWithPayload<CommunicationRootStateType, any> | null
> = payload => {
  const { type } = payload;

  switch (type) {
    case TYPE_CONTACT_MOMENT:
      return saveContactMoment(payload);
    case TYPE_NOTE:
      return saveNote(payload);
    case TYPE_PIP_MESSAGE:
      return saveMessage(payload);
    default:
      return null;
  }
};

export const saveContactMoment =
  (
    payload: SaveContactMomentPayloadType
  ): ThunkActionWithPayload<
    CommunicationRootStateType,
    APICommunication.CreateContactMomentRequestBody
  > =>
  dispatch => {
    const url = '/api/v2/communication/create_contact_moment';
    const { content, channel, direction, contact_uuid, case_uuid } =
      payload.values;

    const data: APICommunication.CreateContactMomentRequestBody = {
      thread_uuid: v4(),
      contact_moment_uuid: v4(),
      //@ts-ignore
      case_uuid: case_uuid?.value || case_uuid || null,
      contact_uuid: contact_uuid.value,
      channel,
      content,
      direction,
    };

    return saveAjaxAction({
      url,
      method: 'POST',
      data,
      payload: data,
    })(dispatch);
  };

/**
 * @param {Object} payload
 * @return {Function}
 */
export const saveNote =
  (
    payload: SaveNotePayloadType
  ): ThunkActionWithPayload<
    CommunicationRootStateType,
    APICommunication.CreateNoteRequestBody
  > =>
  (dispatch, getState) => {
    const {
      communication: { context },
    } = getState();

    const url = '/api/v2/communication/create_note';
    const { caseUuid, contactUuid } = context;
    const { content } = payload.values;

    const data: APICommunication.CreateNoteRequestBody = {
      thread_uuid: v4(),
      note_uuid: v4(),
      content,
      ...(caseUuid
        ? { case_uuid: caseUuid }
        : { contact_uuid: contactUuid || '' }),
    };

    return saveAjaxAction({
      url,
      method: 'POST',
      data,
      payload: data,
    })(dispatch);
  };

type AttachmentType = {
  id: string;
  filename: string;
};

/**
 * @param {Object} payload
 * @return {Function}
 */
export const saveMessage =
  (
    payload: SaveMessagePayloadType
  ): ThunkActionWithPayload<
    CommunicationRootStateType,
    APICommunication.CreateExternalMessageRequestBody
  > =>
  dispatch => {
    const url = '/api/v2/communication/create_external_message';
    const {
      thread_uuid,
      attachments,
      message_type,
      case_uuid,
      content,
      subject,
      participants,
    } = payload;

    const data: APICommunication.CreateExternalMessageRequestBody = {
      case_uuid,
      content,
      subject,
      participants,
      message_type,
      thread_uuid: thread_uuid || v4(),
      message_uuid: v4(),
      attachments: attachments
        ? asArray(attachments).map<AttachmentType>(attachment => ({
            id: attachment.value.toString(),
            filename: attachment.label,
          }))
        : [],
      ...(message_type === 'email' ? { direction: 'outgoing' } : {}),
    };

    return saveAjaxAction<APICommunication.CreateExternalMessageRequestBody>({
      url,
      method: 'POST',
      data,
      payload: data,
    })(dispatch);
  };

export const saveEmailMessage = (payload: SaveEmailMessagePayloadType) => {
  const { from, cc, to, bcc, ...rest } = payload;
  return saveMessage({
    ...rest,
    participants: toParticipantsArray({
      from,
      cc,
      to,
      bcc,
    }),
  });
};

export const setAddCommunicationPending = () => ({
  type: SET_SAVE_COMMUNICATION_PENDING,
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const COMMUNICATION_SET_NAVIGATE = 'COMMUNICATION_SET_NAVIGATE';

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ActionCreator } from 'redux';
import { ActionWithPayload } from '@zaaksysteem/common/src/types/ActionWithPayload';
import { CommunicationNavigateType } from '../../types/Navigate.types';
import { COMMUNICATION_SET_NAVIGATE } from './communincation.navigate.constants';

export type CommunicationSetNavigateActionPayload = CommunicationNavigateType;

export const setCommunicationNavigate: ActionCreator<
  ActionWithPayload<CommunicationSetNavigateActionPayload>
> = (navigate: CommunicationSetNavigateActionPayload) => ({
  type: COMMUNICATION_SET_NAVIGATE,
  payload: navigate,
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Reducer } from 'redux';
import { ActionWithPayload } from '@zaaksysteem/common/src/types/ActionWithPayload';
import { CommunicationNavigateType } from '../../types/Navigate.types';
import { COMMUNICATION_SET_NAVIGATE } from './communincation.navigate.constants';
import { CommunicationSetNavigateActionPayload } from './communication.navigate.actions';

export type CommunicationNavigateState = {
  navigate: CommunicationNavigateType;
};

const initialState: CommunicationNavigateState = {
  navigate: () => {},
};

const navigate: Reducer<
  CommunicationNavigateState,
  ActionWithPayload<CommunicationSetNavigateActionPayload>
> = (state = initialState, action) => {
  if (action.type === COMMUNICATION_SET_NAVIGATE) {
    return {
      ...state,
      navigate: action.payload,
    };
  }

  return state;
};

export default navigate;

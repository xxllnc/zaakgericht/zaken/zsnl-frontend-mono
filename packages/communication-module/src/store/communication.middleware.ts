// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { RouterRootState } from 'connected-react-router';
import { Middleware } from 'redux';
import { batch } from 'react-redux';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { MiddlewareHelper } from '@zaaksysteem/common/src/types/MiddlewareHelper';
import { APICommunication } from '@zaaksysteem/generated';
import { showSnackbar } from '@zaaksysteem/common/src/store/ui/snackbar/snackbar.actions';
import { hideDialog } from '@zaaksysteem/common/src/store/ui/dialog/dialog.actions';
import {
  ADD_THREAD_TO_CASE_DIALOG,
  IMPORT_MESSAGE_DIALOG,
} from '../components/Dialogs/dialog.constants';
import { SAVE_COMMUNICATION } from './add/communication.add.constants';
import { fetchThreadListAction } from './threadList/communication.threadList.actions';
import {
  fetchThreadAction,
  markMessageAsRead,
} from './thread/communication.thread.actions';
import { CommunicationRootStateType } from './communication.reducer';
import {
  ADD_ATTACHMENT_TO_CASE,
  ADD_THREAD_TO_CASE,
  ADD_SOURCE_FILE_TO_CASE,
  THREAD_FETCH,
  MARK_MESSAGE_AS_READ,
  MARK_MESSAGE_AS_UNREAD,
} from './thread/communication.thread.constants';
import { IMPORT_MESSAGE } from './importMessage/communication.importMessage.constants';
import { DELETE_MESSAGE } from './thread/communication.thread.constants';
import { THREAD_LIST_FETCH } from './threadList/communication.threadList.constants';
import { isThreadUnread, isMessageUnread } from './thread/library/isUnread';

const handleSaveCommunicationSuccess: MiddlewareHelper<
  CommunicationRootStateType & RouterRootState,
  AjaxAction<{}, APICommunication.CreateContactMomentRequestBody>
> = (store, next, action) => {
  next(action);

  const state = store.getState();
  const {
    communication: {
      navigate: { navigate },
    },
  } = state;
  const { dispatch } = store;
  const { thread_uuid } = action.payload;
  const url = `view/${thread_uuid}`;

  batch(() => {
    dispatch(fetchThreadListAction());
    dispatch(fetchThreadAction(thread_uuid));

    navigate(url);
  });
};

const handleAddAttachmentToCaseSuccess: MiddlewareHelper<
  CommunicationRootStateType & RouterRootState,
  AjaxAction<{}>
> = (store, next, action) => {
  next(action);

  const { dispatch } = store;

  dispatch(showSnackbar('attachmentAddedToCase'));
};

const handleAddThreadToCaseSuccess: MiddlewareHelper<
  CommunicationRootStateType & RouterRootState,
  AjaxAction<{}>
> = (store, next, action) => {
  next(action);

  const state = store.getState();
  const {
    communication: {
      navigate: { navigate },
    },
  } = state;
  const { dispatch } = store;

  batch(() => {
    dispatch(showSnackbar('threadAddedToCase'));
    dispatch(fetchThreadListAction());
    dispatch(hideDialog(ADD_THREAD_TO_CASE_DIALOG));
    navigate('');
  });
};

const handleAddSourceFileToCaseSuccess: MiddlewareHelper<
  CommunicationRootStateType & RouterRootState,
  AjaxAction<{}>
> = (store, next, action) => {
  next(action);

  const { dispatch } = store;

  dispatch(showSnackbar('sourceFileAddedToCase'));
};

const handleDeleteMessageSuccess: MiddlewareHelper<
  CommunicationRootStateType & RouterRootState,
  AjaxAction<{}>
> = (store, next, action) => {
  next(action);

  const { dispatch, getState } = store;
  const {
    communication: {
      thread: { messages },
      navigate: { navigate },
    },
  } = getState();

  batch(() => {
    dispatch(fetchThreadListAction());

    if (messages && messages.length === 0) {
      navigate('');
    }
  });
};

const handleImportMessageSuccess: MiddlewareHelper<
  CommunicationRootStateType & RouterRootState,
  AjaxAction<{}>
> = (store, next, action) => {
  next(action);

  const { dispatch } = store;

  batch(() => {
    dispatch(showSnackbar('messageImported'));
    dispatch(fetchThreadListAction());
    dispatch(hideDialog(IMPORT_MESSAGE_DIALOG));
  });
};

const handleThreadFetchSuccess: MiddlewareHelper<
  CommunicationRootStateType,
  AjaxAction<{}>
> = (store, next, action) => {
  const {
    communication: {
      thread: { messages: oldMessages = [] },
      context: { context },
    },
  } = store.getState();

  const oldReadMessages = oldMessages
    .filter(message => !isMessageUnread(context, message))
    .map(({ id }) => id);

  next(action);

  const {
    communication: {
      thread: { messages: newMessages = [] },
    },
  } = store.getState();
  const { dispatch } = store;
  const newUnreadMessages = newMessages
    .filter(message => isMessageUnread(context, message))
    .map(({ id }) => id);
  /*
   * Let's assume user is marking message as unread. When this action is successful
   * this will result in the current thread being refetched. Refetching the thread will
   * mark all unread messages in that thread as read. Including the one the user just
   * marked as unread. In order to prevent this we'll have to exclude the message that
   * user marked as unread.
   */

  const unreadMessagesWithoutMessagesPreviouslyRead = newUnreadMessages.filter(
    id => !oldReadMessages.includes(id)
  );

  if (unreadMessagesWithoutMessagesPreviouslyRead.length > 0) {
    dispatch(
      markMessageAsRead({
        messageUuids: unreadMessagesWithoutMessagesPreviouslyRead,
      })
    );
  }
};

const postCommunicationReadCountChange = () => {
  top?.window.postMessage(
    {
      type: 'communicationReadCountChange',
    },
    '*'
  );
};

const handleThreadListFetchSuccess: MiddlewareHelper<
  CommunicationRootStateType,
  AjaxAction<{}>
> = (store, next, action) => {
  const {
    communication: {
      threadList: { threadList: oldThreadList },
      context: { context },
    },
  } = store.getState();

  next(action);

  const {
    communication: {
      thread: { id },
      threadList: { threadList: newThreadList },
    },
  } = store.getState();

  const numberOfUnreadThreadsInOldState = oldThreadList.filter(thread =>
    isThreadUnread(context, thread)
  ).length;
  const numberOfUnreadThreadsInNewState = newThreadList.filter(thread =>
    isThreadUnread(context, thread)
  ).length;

  if (numberOfUnreadThreadsInOldState !== numberOfUnreadThreadsInNewState) {
    postCommunicationReadCountChange();
    id && store.dispatch(fetchThreadAction(id));
  }
};

const refetchThreadList: MiddlewareHelper<
  CommunicationRootStateType,
  AjaxAction<{}>
> = (store, next, action) => {
  const { dispatch } = store;
  next(action);
  dispatch(fetchThreadListAction());
};

/* eslint complexity: [2, 12] */
export const communicationMiddleware: Middleware<
  {},
  CommunicationRootStateType & RouterRootState
> = store => next => action => {
  switch (action.type) {
    case SAVE_COMMUNICATION.SUCCESS:
      return handleSaveCommunicationSuccess(store, next, action);
    case ADD_ATTACHMENT_TO_CASE.SUCCESS:
      return handleAddAttachmentToCaseSuccess(store, next, action);
    case ADD_THREAD_TO_CASE.SUCCESS:
      return handleAddThreadToCaseSuccess(store, next, action);
    case ADD_SOURCE_FILE_TO_CASE.SUCCESS:
      return handleAddSourceFileToCaseSuccess(store, next, action);
    case DELETE_MESSAGE.SUCCESS:
      return handleDeleteMessageSuccess(store, next, action);
    case IMPORT_MESSAGE.SUCCESS:
      return handleImportMessageSuccess(store, next, action);
    case THREAD_FETCH.SUCCESS:
      return handleThreadFetchSuccess(store, next, action);
    case THREAD_LIST_FETCH.SUCCESS:
      return handleThreadListFetchSuccess(store, next, action);
    case MARK_MESSAGE_AS_READ.SUCCESS:
      return refetchThreadList(store, next, action);
    case MARK_MESSAGE_AS_UNREAD.SUCCESS:
      return refetchThreadList(store, next, action);
    default:
      return next(action);
  }
};

export default communicationMiddleware;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { CommunicationRootStateType } from '../communication.reducer';

export const threadNotRelatedOrRelatedCaseNotResolvedSelector = (
  state: CommunicationRootStateType
) => {
  const {
    communication: {
      thread: { caseRelation },
    },
  } = state;

  return caseRelation?.status !== 'resolved';
};

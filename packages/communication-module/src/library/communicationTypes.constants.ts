// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const TYPE_CONTACT_MOMENT = 'contact_moment';
export const TYPE_NOTE = 'note';
export const TYPE_EXTERNAL_MESSAGE = 'external_message';
export const TYPE_EMAIL = 'email';
export const TYPE_PIP_MESSAGE = 'pip';
export const TYPE_MIJN_OVERHEID = 'mijn_overheid';
export const TYPE_POSTEX = 'postex';

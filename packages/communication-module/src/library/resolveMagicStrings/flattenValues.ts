// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  FormValue,
  NestedFormValue,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { FlatFormValues } from './resolveMagicStrings.types';

const joinValueWithLabel = (value: NestedFormValue): string =>
  value.label
    ? `${value.label}|${value.value.toString()}`
    : value.value.toString();

const flattenValue = (value: FormValue) =>
  (value as NestedFormValue).value
    ? joinValueWithLabel(value as NestedFormValue)
    : value.toString();

/**
 * Flatten values in a way that we can later reconstruct the original shape
 *
 * For example:
 * [{ label: 'Foo', value: 'bar' }] => ['Foo|bar']
 */
const getPlainTextValue = (value: FormValue | FormValue[] | null) => {
  if (!value) {
    return '-';
  }

  return Array.isArray(value)
    ? `[${value.map(flattenValue).join(', ')}]`
    : flattenValue(value);
};

export function flattenValues<FormShape>(
  values: FormShape
): Partial<FlatFormValues<FormShape>> {
  //@ts-ignore
  return Object.entries(values).reduce<Partial<FlatFormValues<FormShape>>>(
    (acc, [name, value]) => ({
      ...acc,
      //@ts-ignore
      [name]: getPlainTextValue(value),
    }),
    {}
  );
}

export default flattenValues;

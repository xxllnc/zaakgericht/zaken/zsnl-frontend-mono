// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { DynamicModuleLoader } from 'redux-dynamic-modules-react';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import useMessages from '@zaaksysteem/common/src/library/useMessages';
import useDialogs from '@zaaksysteem/common/src/library/useDialogs';
import { getCommunicationModule } from './store/communication.module';
import CommunicationContainer from './components/CommunicationContainer';
import locale from './locale/communication.locale';
import { CommunicationContextType } from './types/Context.types';
import AddThreadToCaseDialogContainer from './components/Dialogs/AddThreadToCaseDialog/AddThreadToCaseDialogContainer';
import ImportMessageDialogContainer from './components/Dialogs/ImportMessageDialog/ImportMessageDialogContainer';
import {
  ADD_THREAD_TO_CASE_DIALOG,
  IMPORT_MESSAGE_DIALOG,
} from './components/Dialogs/dialog.constants';

export type CommunicationModulePropsType = Omit<
  CommunicationContextType,
  'navigate'
>;

type CommunicationParamsType = {
  contactUuid?: string;
};

const CommunicationModule: React.ComponentType<
  CommunicationModulePropsType
> = options => {
  const [t] = useTranslation('communication');
  const [, addMessages, removeMessages] = useMessages();
  const [, addDialogs] = useDialogs();
  const navigate = useNavigate();
  const { contactUuid } = useParams<
    keyof CommunicationParamsType
  >() as CommunicationParamsType;

  useEffect(() => {
    const messages: { [key: string]: any } = {
      ...t('serverErrors', {
        returnObjects: true,
      }),
      ...t('snackMessages', {
        returnObjects: true,
      }),
    };

    addMessages(messages);

    addDialogs({
      [ADD_THREAD_TO_CASE_DIALOG]: AddThreadToCaseDialogContainer,
      [IMPORT_MESSAGE_DIALOG]: ImportMessageDialogContainer,
    });

    return () => removeMessages(messages);
  }, []);

  const optionsWithParams = contactUuid ? { ...options, contactUuid } : options;

  return (
    //@ts-ignore
    <DynamicModuleLoader
      modules={[getCommunicationModule(optionsWithParams, navigate)]}
    >
      <I18nResourceBundle resource={locale} namespace="communication">
        {/* @ts-ignore */}
        <CommunicationContainer />
      </I18nResourceBundle>
    </DynamicModuleLoader>
  );
};

export default CommunicationModule;

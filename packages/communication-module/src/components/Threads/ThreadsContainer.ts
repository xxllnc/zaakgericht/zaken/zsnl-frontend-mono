// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { AJAX_STATE_PENDING } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { fetchThreadListAction } from '../../store/threadList/communication.threadList.actions';
import { CommunicationRootStateType } from '../../store/communication.reducer';
import { ThreadType } from '../../types/Thread.types';
import Threads, { ThreadsPropsType } from './Threads';

const SortThreadList = (threadA: ThreadType, threadB: ThreadType) =>
  new Date(threadB.date).getTime() - new Date(threadA.date).getTime();

type PropsFromStateType = Pick<
  ThreadsPropsType,
  'threadList' | 'busy' | 'showLinkToCase' | 'context'
>;

const mapStateToProps = ({
  communication: {
    threadList: { threadList, state },
    context: {
      context,
      capabilities: { canSelectCase },
    },
  },
}: CommunicationRootStateType): PropsFromStateType => {
  return {
    context,
    threadList: threadList.sort(SortThreadList),
    busy: state === AJAX_STATE_PENDING,
    showLinkToCase: canSelectCase,
  };
};

type PropsFromDispatchType = Pick<ThreadsPropsType, 'fetchThreadList'>;

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatchType => ({
  fetchThreadList: () => dispatch(fetchThreadListAction() as any),
});

const ThreadsContainer = connect<
  PropsFromStateType,
  PropsFromDispatchType,
  {},
  CommunicationRootStateType
>(
  mapStateToProps,
  mapDispatchToProps
)(Threads);

export default ThreadsContainer;

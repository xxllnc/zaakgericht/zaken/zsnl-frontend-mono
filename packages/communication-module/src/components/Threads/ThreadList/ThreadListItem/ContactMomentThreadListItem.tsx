// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { addScopeProp } from '@mintlab/ui/App/library/addScope';
import ThreadListItem, { ThreadListItemPropsType } from './ThreadListItem';
import ThreadTypeIcon from './ThreadTypeIcon/ThreadTypeIcon';

type ContactMomentThreadListItemPropsType = {
  createdByName: string;
  withName: string;
  channel: string;
  type: string;
} & Omit<ThreadListItemPropsType, 'title' | 'icon'>;

const ContactMomentThreadListItem: React.FunctionComponent<
  ContactMomentThreadListItemPropsType
> = ({ createdByName, withName, channel, ...rest }) => {
  const [t] = useTranslation('communication');
  const title = t('thread.contactMoment.title', {
    channel: t(`channels.${channel}`),
  });
  const subTitle = t('thread.contactMoment.subTitle', {
    createdByName,
    withName,
  });

  return (
    <ThreadListItem
      {...rest}
      title={title}
      subTitle={subTitle}
      {...addScopeProp('thread', 'contactMoment')}
      icon={<ThreadTypeIcon type={`channel.inverted.${channel}`} />}
    />
  );
};

export default ContactMomentThreadListItem;

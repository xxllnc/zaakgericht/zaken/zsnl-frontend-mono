// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
//@ts-ignore
import { useWidth } from '@mintlab/ui/App/library/useWidth';

const ThreadTypeIcon: React.ComponentType<{ type: string }> = ({ type }) => {
  const width = useWidth();
  const iconSize = width === 'xs' ? 'extraSmall' : 'small';
  return <ZsIcon size={iconSize}>{type}</ZsIcon>;
};

export default ThreadTypeIcon;

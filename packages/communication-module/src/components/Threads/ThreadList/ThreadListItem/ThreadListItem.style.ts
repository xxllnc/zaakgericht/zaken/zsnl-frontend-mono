// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';
import { Theme } from '@mintlab/ui/types/Theme';
import { ThreadListItemPropsType } from './ThreadListItem';

export const useThreadListItemStyle = makeStyles(
  ({
    typography,
    palette: { primary, error },
    mintlab: { greyscale },
    breakpoints,
  }: Theme) => ({
    wrapper: {
      display: 'flex',
      borderBottom: `1px solid ${greyscale.dark}`,
      padding: '20px 12px',
      textDecoration: 'none',
      height: '100%',
      position: 'relative',
      transition: `0.2s ease-out`,
      [breakpoints.up('sm')]: {
        padding: 20,
      },
      '&:hover': {
        backgroundColor: greyscale.light,
      },
    },
    selected: {
      backgroundColor: primary.lighter,
      zIndex: 1,
      '&:hover': {
        backgroundColor: primary.lighter,
      },
    },

    unread: {
      '&:after': {
        content: '""',
        display: 'block',
        position: 'absolute',
        left: 0,
        top: 0,
        bottom: 0,
        width: 5,
        backgroundColor: primary.main,
      },
    },

    failed: {
      '&:after': {
        content: '""',
        display: 'block',
        position: 'absolute',
        left: 0,
        top: 0,
        bottom: 0,
        width: 5,
        backgroundColor: error.main,
      },
    },

    /** Icon column */
    type: {
      display: 'flex',
      justifyContent: 'left',
      color: 'white',
      paddingTop: 7,
      paddingRight: 12,
      [breakpoints.up('sm')]: {
        paddingRight: 20,
      },
    },
    /** Content column */
    content: {
      flex: 1,
      display: 'flex',
      flexDirection: 'column',
      overflow: 'hidden',
    },

    titleWrapper: {
      display: 'flex',
      flexDirection: 'row',
    },

    count: {
      ...typography.subtitle1,
      color: greyscale.evenDarker,
      padding: 0,
      margin: 0,
      paddingLeft: 5,
      //@ts-ignore
      fontWeight: typography.fontWeightNormal,
    },

    contentTitle: {
      ...typography.subtitle1,
      color: greyscale.darkest,
      padding: 0,
      margin: 0,
    },
    contentTitleUnread: {
      fontWeight: typography.fontWeightBold,
    },
    ellipsis: {
      overflow: 'hidden',
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis',
    },
    contentSubTitle: {
      ...typography.subtitle2,
      fontWeight: 400,
      padding: 0,
      margin: 0,
      color: (props: ThreadListItemPropsType) =>
        props.isUnread ? primary.main : greyscale.darkest,
      marginBottom: '6px',
    },
    contentSummary: {
      ...typography.body2,
      padding: 0,
      margin: 0,
      color: greyscale.evenDarker,
      lineHeight: 'normal',
    },

    /** Info column */
    info: {
      marginLeft: '10px',
      display: 'flex',
      flexDirection: 'column',
    },
    infoTop: {
      ...typography.caption,
      display: 'flex',
      color: (props: ThreadListItemPropsType) =>
        props.isUnread ? primary.main : greyscale.evenDarker,
      justifyContent: 'flex-end',
      alignItems: 'center',
      '&>span>svg': {
        paddingBottom: 3,
      },
    },
    tags: {
      flex: 1,
      display: 'flex',
      justifyContent: 'flex-end',
      alignItems: 'flex-end',
      '&>*:not(:first-child)': {
        marginLeft: '10px',
      },
    },
  })
);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { addScopeProp } from '@mintlab/ui/App/library/addScope';
import ThreadListItem, { ThreadListItemPropsType } from './ThreadListItem';
import ThreadTypeIcon from './ThreadTypeIcon/ThreadTypeIcon';

type PipMessageThreadListItemPropsType = {
  createdByName: string;
  type: string;
  subject: string;
  messageCount: number;
} & Omit<ThreadListItemPropsType, 'title' | 'tag' | 'icon'>;

const PipMessageThreadListItem: React.FunctionComponent<
  PipMessageThreadListItemPropsType
> = ({ createdByName, type, subject, messageCount, ...rest }) => {
  const [t] = useTranslation('communication');
  const title = t('thread.pipMessage.title', {
    createdByName,
  });
  const tag = t(`thread.tags.${type}`);
  const iconType =
    messageCount > 1
      ? 'threadType.inverted.pipMessageThread'
      : 'threadType.inverted.pipMessage';

  return (
    <ThreadListItem
      {...rest}
      title={title}
      subTitle={subject}
      typeTag={tag}
      {...addScopeProp('thread', 'pipMessage')}
      icon={<ThreadTypeIcon type={iconType} />}
    />
  );
};

export default PipMessageThreadListItem;

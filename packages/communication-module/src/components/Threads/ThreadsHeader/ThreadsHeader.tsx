// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Button from '@mintlab/ui/App/Material/Button';
import TextField from '@mintlab/ui/App/Material/TextField';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
//@ts-ignore
import { unique } from '@mintlab/kitchen-sink/source';
import FlatValueSelect from '@zaaksysteem/common/src/components/form/fields/FlatValueSelect';
import {
  addScopeProp,
  addScopeAttribute,
  // @ts-ignore
} from '@mintlab/ui/App/library/addScope';
import ActionMenu from '../../shared/ActionMenu/ActionMenu';
import { useThreadsHeaderStyle } from './ThreadsHeader.style';

export interface ThreadsHeaderPropsType {
  onFilterChange: (filter: string) => void;
  onSearchTermChange: (searchTerm: string) => void;
  onImportMessage: () => void;
  searchTerm: string;
  filter: string;
  canCreate: boolean;
  canFilter: boolean;
  filterOptions: string[];
  canImportMessage: boolean;
  defaultCreateType: string;
}

/* eslint complexity: [2, 9] */
const ThreadsHeader: React.ComponentType<ThreadsHeaderPropsType> = ({
  canCreate,
  canFilter,
  filter,
  searchTerm,
  onFilterChange,
  onSearchTermChange,
  filterOptions,
  canImportMessage,
  defaultCreateType,
  onImportMessage,
}) => {
  const classes = useThreadsHeaderStyle();
  const [t] = useTranslation(['communication']);
  const filterChoices = filterOptions.sort().map(type => ({
    label: t(`threadTypes.${type}`),
    value: type,
  }));
  const addThreadPath = `new/${defaultCreateType}`;
  const LinkComponent = React.forwardRef<HTMLAnchorElement>(
    //@ts-ignore
    ({ children, ...restProps }, ref) => (
      <Link {...restProps} to={addThreadPath}>
        {children}
      </Link>
    )
  );
  LinkComponent.displayName = 'AddButtonLink';

  const actionButtons = [
    [
      {
        action: onImportMessage,
        label: t('threads.actions.import'),
        condition: canImportMessage,
      },
    ],
  ];

  const clearTextField = () => onSearchTermChange('');

  const handleSearchTermChange = (event: React.ChangeEvent<HTMLInputElement>) =>
    onSearchTermChange(event.currentTarget.value);

  const handleFilterChange = (event: React.ChangeEvent<HTMLSelectElement>) =>
    onFilterChange(event.target.value);

  const displayFilter = canFilter && filterChoices.length > 1;
  const displayActionWrapper =
    canCreate || displayFilter || actionButtons.length > 0;
  const searchId = unique();

  return (
    <div className={classes.wrapper}>
      {displayActionWrapper && (
        <div className={classes.actionsWrapper}>
          {canCreate && (
            <div className={classes.buttonActionWrapper}>
              <Button
                name="addThread"
                component={LinkComponent}
                icon="add"
                {...addScopeProp('add-thread')}
              >
                {t('forms.new')}
              </Button>
            </div>
          )}
          <div
            className={classes.filterActionWrapper}
            {...addScopeAttribute('filter-thread')}
          >
            {displayFilter && (
              <FlatValueSelect
                value={filter}
                isClearable={true}
                isSearchable={false}
                placeholder="Filter"
                choices={filterChoices}
                onChange={handleFilterChange}
              />
            )}
          </div>
          {actionButtons.length > 0 && (
            <ActionMenu actions={actionButtons} scope={'threadList:header'} />
          )}
        </div>
      )}
      <div>
        <label htmlFor={searchId} className={classes.label}>
          {t('thread.searchLabel')}
        </label>
        <TextField
          id={searchId}
          variant="generic2"
          name={t('thread.searchPlaceholder')}
          startAdornment={
            <div className={classes.searchIcon}>
              <Icon size="small" color="inherit">
                {iconNames.search}
              </Icon>
            </div>
          }
          placeholder={t('thread.searchPlaceholder')}
          closeName={t('thread.searchClose')}
          onChange={handleSearchTermChange}
          value={searchTerm}
          {...(searchTerm && { closeAction: clearTextField })}
          {...addScopeProp('search-thread')}
        />
      </div>
    </div>
  );
};

export default ThreadsHeader;

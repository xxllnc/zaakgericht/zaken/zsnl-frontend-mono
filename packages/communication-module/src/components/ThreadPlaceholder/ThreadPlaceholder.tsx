// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useThreadPlaceholderStyle } from './ThreadPlaceholder.style';

const ThreadPlaceholder = ({ children }: { children: any }) => {
  const classes = useThreadPlaceholderStyle();
  return <div className={classes.placeholder}>{children}</div>;
};

export default ThreadPlaceholder;

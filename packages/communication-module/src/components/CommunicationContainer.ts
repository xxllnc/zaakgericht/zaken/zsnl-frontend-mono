// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { CommunicationRootStateType } from '../store/communication.reducer';
import Communication, { CommunicationPropsType } from './Communication';

type PropsFromStateType = Pick<
  CommunicationPropsType,
  'requestor' | 'allowSplitScreen'
>;

const mapStateToProps = ({
  communication: {
    context: {
      contactUuid: requestor,
      capabilities: { allowSplitScreen },
    },
  },
}: CommunicationRootStateType): PropsFromStateType => ({
  requestor,
  allowSplitScreen,
});

const CommunicationContainer = connect<
  PropsFromStateType,
  {},
  {},
  CommunicationRootStateType
>(mapStateToProps)(Communication);

export default CommunicationContainer;

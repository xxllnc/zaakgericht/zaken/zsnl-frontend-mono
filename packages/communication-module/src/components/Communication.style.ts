// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

//@ts-ignore
export const useCommunicationStyle = makeStyles(({ breakpoints }) => ({
  wrapper: {
    height: '100%',
    display: 'flex',
  },
  wrapperResponsive: {
    [breakpoints.up('md')]: {
      '&>:first-child': {
        maxWidth: 420,
      },
    },
    [breakpoints.up('lg')]: {
      '&>:first-child': {
        maxWidth: 580,
      },
    },
  },
  threadListWrapper: {
    flex: '1 0 auto',
    height: '100%',
    width: '100%',
  },
  contentOuterWrapper: {
    padding: 20,
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    height: '100%',
    minWidth: 0,
  },
}));

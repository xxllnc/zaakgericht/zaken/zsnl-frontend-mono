// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import FormDialog from '@zaaksysteem/common/src/components/dialogs/FormDialog/FormDialog';
import {
  FormDefinition,
  FormValuesType,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { ImportMessageFormFields } from '../../../types/Message.types';

export type ImportMessageDialogPropsType = {
  caseUuid: string;
  onSubmit: (
    values: FormValuesType<ImportMessageFormFields>,
    caseUuid: string
  ) => void;
  busy: boolean;
  onClose: () => void;
  [key: string]: any;
};

const formDefinition: FormDefinition<ImportMessageFormFields> = [
  {
    name: 'file_uuid',
    type: fieldTypes.UPLOAD,
    value: null,
    required: true,
    format: 'file',
    accept: ['.msg', '.eml', '.mail'],
    multiValue: false,
    hint: 'dialogs.importMessage.select.hint',
  },
];

const ImportMessageDialog: React.FunctionComponent<
  ImportMessageDialogPropsType
> = ({ onSubmit, onClose, caseUuid, busy }) => {
  const [t] = useTranslation('communication');

  const handleOnSubmit = (values: FormValuesType<ImportMessageFormFields>) =>
    onSubmit(values, caseUuid);

  return (
    <FormDialog<ImportMessageFormFields>
      formDefinition={formDefinition}
      formDefinitionT={t}
      onSubmit={handleOnSubmit}
      onClose={onClose}
      saving={busy}
      title={t('dialogs.importMessage.title')}
      scope="import-message-dialog"
    />
  );
};

export default ImportMessageDialog;

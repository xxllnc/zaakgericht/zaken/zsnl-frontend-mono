// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useAttachmentListStyle = makeStyles(
  ({ mintlab: { shadows, greyscale } }: any) => ({
    wrapper: {
      marginTop: 30,
      '&>*:not(:first-child)': { marginTop: 15 },
    },
  })
);

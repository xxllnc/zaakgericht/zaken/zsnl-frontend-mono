// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { Fragment } from 'react';
import { useTranslation } from 'react-i18next';
import { useTheme } from '@mui/material';
import Button from '@mintlab/ui/App/Material/Button';
import Icon, { iconNames } from '@mintlab/ui/App/Material/Icon';
import { AttachmentType } from '../../../../types/Message.types';
import ActionMenu from '../../../shared/ActionMenu/ActionMenu';
import { useAttachmentStyle } from './Attachment.style';

export type AttachmentPropsType = AttachmentType & {
  canBeAddedToCase: boolean;
  onAddToCase: () => void;
  handlePreviewAction?: () => void;
};

const Attachment: React.FunctionComponent<AttachmentPropsType> = ({
  name,
  size,
  download,
  canBeAddedToCase,
  onAddToCase,
  handlePreviewAction,
}) => {
  const theme = useTheme();
  const [t] = useTranslation('communication');
  const classes = useAttachmentStyle();
  const addAttachmentAction = canBeAddedToCase
    ? [
        {
          action: onAddToCase,
          label: t('attachments.actions.addToCaseDocuments'),
        },
      ]
    : [];
  const actions = [
    [
      {
        action: () => download?.url && window.open(download.url, '_blank'),
        label: t('attachments.actions.download'),
      },
      ...addAttachmentAction,
    ],
  ];
  const KB = Math.max(Math.round(size / 1024), 1);
  const MB = Math.max(Math.round(size / 1024 / 1024), 1);
  const fileSize = KB >= 1024 ? `${MB}MB` : `${KB}KB`;

  return (
    <div className={classes.wrapper}>
      <div className={classes.metaWrapper}>
        <Icon sx={{ color: theme.palette.primary.dark }}>
          {iconNames.attachment}
        </Icon>
        <div className={classes.name}>
          {handlePreviewAction ? (
            <Button
              name="handleAttachementPreview"
              variant="text"
              action={handlePreviewAction}
              sx={{
                color: theme.palette.primary.main,
                textDecoration: 'underline',
                '&:hover': {
                  textDecoration: 'underline',
                },
              }}
            >
              {name}
            </Button>
          ) : (
            <Fragment>{name}</Fragment>
          )}
        </div>
        <div>{fileSize}</div>
      </div>
      <ActionMenu actions={actions} scope="attachment" />
    </div>
  );
};

export default Attachment;

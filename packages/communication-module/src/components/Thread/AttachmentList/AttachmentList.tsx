// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { DocumentPreviewModal } from '@zaaksysteem/common/src/components/DocumentPreview';
import { AttachmentType } from '../../../types/Message.types';
import { useAttachmentListStyle } from './AttachmentList.style';
import Attachment, { AttachmentPropsType } from './Attachment/Attachment';

export type AttachmentListPropsType = {
  attachments: AttachmentType[];
} & Pick<AttachmentPropsType, 'canBeAddedToCase'> & {
    addToCase: (id: string) => void;
    canOpenPDFPreview: boolean;
  };

const showHideContextualMenu = (show: boolean) => {
  top?.window.postMessage(
    {
      type: 'showHideContextualMenu',
      data: {
        show,
      },
    },
    '*'
  );
};

const AttachmentList: React.FunctionComponent<AttachmentListPropsType> = ({
  attachments,
  canBeAddedToCase,
  canOpenPDFPreview,
  addToCase,
}) => {
  const classes = useAttachmentListStyle();
  const [previewOpen, setPreviewOpen] = useState(false);
  const [previewItem, setPreviewItem] = useState<null | AttachmentType>(null);

  return (
    <div className={classes.wrapper}>
      <DocumentPreviewModal
        open={previewOpen}
        title={previewItem?.name}
        url={previewItem?.preview?.url || ''}
        contentType={previewItem?.preview?.contentType || ''}
        downloadUrl={previewItem?.download?.url}
        onClose={() => {
          setPreviewOpen(false);
          showHideContextualMenu(true);
        }}
      />
      {attachments.map((attachment: AttachmentType) => (
        <Attachment
          key={attachment.id}
          {...attachment}
          canBeAddedToCase={canBeAddedToCase}
          onAddToCase={() => addToCase(attachment.id)}
          {...(canOpenPDFPreview && attachment.preview
            ? {
                handlePreviewAction: () => {
                  if (attachment.preview) {
                    setPreviewItem(attachment);
                    setPreviewOpen(true);
                    showHideContextualMenu(false);
                  }
                },
              }
            : {})}
        />
      ))}
    </div>
  );
};

export default AttachmentList;

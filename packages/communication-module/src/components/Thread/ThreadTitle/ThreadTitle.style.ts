// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useThreadTitleStyle = makeStyles(
  ({ palette: { primary } }: any) => ({
    wrapper: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
    },
    title: {
      flexGrow: 1,
    },
    link: {
      textDecoration: 'none',
      color: primary.main,
    },
    tag: {
      backgroundColor: primary.lighter,
      color: primary.main,
    },
  })
);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import ZsIcon from '@mintlab/ui/App/Zaaksysteem/ZsIcon';
import { NoteMessageType } from '../../../types/Message.types';
import MessageHeader from './MessageHeader';
import MessageContent from './MessageContent';

type NoteMessagePropsType = {
  message: NoteMessageType;
  caseNumber?: string;
};

const NoteMessage: React.FunctionComponent<NoteMessagePropsType> = ({
  message: {
    content,
    createdDate,
    sender: { name },
    type,
    id,
  },
}) => {
  const [t] = useTranslation('communication');
  const title = t('thread.note.messageTitle', {
    user: name,
  });
  const icon = <ZsIcon size="small">{'threadType.inverted.note'}</ZsIcon>;

  return (
    <div>
      <MessageHeader date={createdDate} title={title} icon={icon} id={id} />
      <MessageContent content={content} type={type} />
    </div>
  );
};

export default NoteMessage;

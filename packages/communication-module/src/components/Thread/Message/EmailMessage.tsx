// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Accordion, AccordionSummary, AccordionDetails } from '@mui/material';
import {
  ExternalMessageType,
  ParticipantType,
} from '../../../types/Message.types';
import MessageHeader from './MessageHeader';
import MessageContent from './MessageContent';
import MessageSenderIcon from './MessageSenderIcon/MessageSenderIcon';
import CreateExternalMessageTitle from './library/CreateExternalMessageTitle';

type EmailMessagePropsType = {
  message: ExternalMessageType;
  caseNumber?: string;
  expanded?: boolean;
  isUnread: boolean;
};

const formatParticipants = (participants: ParticipantType[]) => {
  const formatParticipant = ({ email, name }: ParticipantType) => {
    return !name || name === email ? email : `${name} (${email})`;
  };

  return participants.map(formatParticipant).join(', ');
};

/* eslint complexity: [2, 7] */
const EmailMessage: React.FunctionComponent<EmailMessagePropsType> = ({
  message,
  isUnread,
  expanded = false,
}) => {
  const {
    content,
    createdDate,
    sender,
    type,
    attachments,
    participants,
    id,
    hasSourceFile,
    failureReason,
  } = message;
  //@ts-ignore
  const icon = <MessageSenderIcon type={sender ? sender.type : 'person'} />;
  const title = CreateExternalMessageTitle(message);
  const [t] = useTranslation('communication');
  const [isExpanded, setExpanded] = useState(isUnread || expanded);
  const participantTextLines = participants
    ? [
        participants.to
          ? `${t('thread.emailMessageHeader.recipient')}: ${formatParticipants(
              participants.to
            )}`
          : null,
        participants.cc.length
          ? `${t('thread.emailMessageHeader.cc')}: ${formatParticipants(
              participants.cc
            )}`
          : null,
        participants.bcc.length
          ? `${t('thread.emailMessageHeader.bcc')}: ${formatParticipants(
              participants.bcc
            )}`
          : null,
      ]
    : [];

  const info = participantTextLines.reduce<string>(
    (acc, line) =>
      line
        ? `${acc}
    ${line}`
        : acc,
    ''
  );

  return (
    <Accordion
      sx={{
        padding: 0,
        boxShadow: 'none',
        '&:before': {
          height: 0,
        },
      }}
      defaultExpanded={isExpanded}
    >
      <AccordionSummary
        onClick={() => setExpanded(!isExpanded)}
        sx={{
          height: '100px',
          padding: 0,
          boxShadow: 'none',
          '& .MuiExpansionPanelSummary-content': {
            margin: 0,
            display: 'inline',
          },
        }}
      >
        <MessageHeader
          date={createdDate}
          title={title}
          icon={icon}
          info={info}
          isUnread={isUnread}
          id={id}
          hasSourceFile={hasSourceFile}
          hasAttachment={Boolean(attachments.length)}
          failureReason={failureReason}
        />
      </AccordionSummary>
      <AccordionDetails sx={{ padding: 0, display: 'inline' }}>
        <MessageContent
          content={content}
          attachments={attachments}
          type={type}
        />
      </AccordionDetails>
    </Accordion>
  );
};

export default EmailMessage;

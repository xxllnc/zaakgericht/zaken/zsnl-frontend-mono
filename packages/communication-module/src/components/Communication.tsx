// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import classnames from 'classnames';
import { Route, Routes } from 'react-router-dom';
//@ts-ignore
import { useWidth } from '@mintlab/ui/App/library/useWidth';
import { useCommunicationStyle } from './Communication.style';
import ThreadsContainer from './Threads/ThreadsContainer';
import ThreadContainer from './Thread/ThreadContainer';
import MessageFormContainer from './MessageForm/NewMessageForm/MessageFormContainer';
import ThreadPlaceholder from './ThreadPlaceholder/ThreadPlaceholder';

export interface CommunicationPropsType {
  width: string;
  allowSplitScreen: boolean;
  requestor?: string;
}

const Communication: React.ComponentType<CommunicationPropsType> = ({
  allowSplitScreen,
}) => {
  const [t] = useTranslation('communication');
  const width = useWidth();
  const classes = useCommunicationStyle();
  const threadListBreakpoint = allowSplitScreen
    ? ['md', 'lg', 'xl'].includes(width)
    : false;

  if (threadListBreakpoint) {
    return (
      <div
        className={classnames(
          classes.wrapper,
          allowSplitScreen && classes.wrapperResponsive
        )}
      >
        <div className={classes.threadListWrapper}>
          <ThreadsContainer />
        </div>

        <div className={classes.contentOuterWrapper}>
          <Routes>
            <Route
              path={'new/:type/*'}
              element={
                <MessageFormContainer
                  classes={classes}
                  alwaysShowBackButton={allowSplitScreen === false}
                  width={width}
                />
              }
            />
            <Route
              path={'view/:threadId'}
              element={<ThreadContainer width={width} />}
            />
            <Route
              path=""
              element={
                <ThreadPlaceholder>{t('placeholder')}</ThreadPlaceholder>
              }
            />
          </Routes>
        </div>
      </div>
    );
  } else {
    return (
      <div
        className={classnames(
          classes.wrapper,
          allowSplitScreen && classes.wrapperResponsive
        )}
      >
        <Routes>
          <Route
            path={'new/:type/*'}
            element={
              <div className={classes.contentOuterWrapper}>
                <MessageFormContainer
                  classes={classes}
                  alwaysShowBackButton={allowSplitScreen === false}
                  width={width}
                />
              </div>
            }
          />
          <Route
            path={'view/:threadId'}
            element={
              <div className={classes.contentOuterWrapper}>
                <ThreadContainer width={width} />
              </div>
            }
          />
          <Route
            path=""
            element={
              <div className={classes.threadListWrapper}>
                <ThreadsContainer />
              </div>
            }
          />
        </Routes>
      </div>
    );
  }
};

export default Communication;

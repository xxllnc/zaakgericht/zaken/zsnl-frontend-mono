// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { FormFieldPropsType } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { useCaseChoicesForContactQuery } from '../../../../../library/requests';
import locale from './locale';

export type CaseSelectorConfigType = {
  contactUuid: string;
};

export interface CaseSelectorPropsType
  extends FormFieldPropsType<unknown, CaseSelectorConfigType, string> {}

const CaseSelector: React.ComponentType<CaseSelectorPropsType> = ({
  config,
  ...restProps
}) => {
  const [t] = useTranslation('CaseSelector');
  const [selectProps, ServerErrorDialog] = useCaseChoicesForContactQuery(
    config?.contactUuid
  );

  if (!config || !config.contactUuid) {
    console.error(
      `The CaseSelector component cannot be used without config.contactUuid.`
    );
    return null;
  }

  return (
    <>
      <Select
        {...restProps}
        {...selectProps}
        filterOption={() => true}
        placeholder={t('choose')}
      />
      {ServerErrorDialog}
    </>
  );
};

export default (props: CaseSelectorPropsType) => (
  <I18nResourceBundle resource={locale} namespace="CaseSelector">
    <CaseSelector {...props} />
  </I18nResourceBundle>
);

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { ComponentType, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import classnames from 'classnames';
import {
  FormFieldPropsType,
  FormValuesType,
  NestedFormValue,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import mapValuesToFormDefinition from '@zaaksysteem/common/src/components/form/library/mapValuesToFormDefinition';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import { useStyles } from './EmailRecipient.style';
import { EmailRecipientFormValuesType } from './EmailRecipient.types';
import rules from './EmailRecipient.rules';
import getFormDefinition from './emailRecipient.formDefinition';

export type EmailRecipientConfigType = {
  caseUuid?: string;
  selectedRecipientType?: string;
};

export interface EmailRecipientPropsType
  extends FormFieldPropsType<
    EmailRecipientFormValuesType,
    EmailRecipientConfigType,
    NestedFormValue | NestedFormValue[]
  > {}

const getRecipientTypeFromValue = (
  value: EmailRecipientPropsType['value'],
  multiValue?: boolean
) =>
  multiValue
    ? (value as NestedFormValue[])[0]?.recipientType
    : (value as NestedFormValue)?.recipientType;

export const EmailRecipient: ComponentType<EmailRecipientPropsType> = ({
  name,
  setFieldValue,
  setFieldTouched,
  value,
  multiValue,
  error,
  config,
}) => {
  const [t] = useTranslation('communication');
  const classes = useStyles();
  const selectedRecipientType =
    getRecipientTypeFromValue(value, multiValue) ||
    config?.selectedRecipientType;

  const formDefinition = useMemo(() => {
    const caseUuid = config && config.caseUuid ? config.caseUuid : '';
    return getFormDefinition({
      selectedRecipientType,
      caseUuid: caseUuid ? caseUuid.toString() : undefined,
    });
  }, [config]);

  const formDefinitionWithValues = useMemo(() => {
    return selectedRecipientType
      ? mapValuesToFormDefinition<EmailRecipientFormValuesType>(
          {
            [selectedRecipientType]: value,
          },
          formDefinition
        )
      : formDefinition;
  }, [formDefinition, config]);

  const getActiveField = (
    values: FormValuesType<EmailRecipientFormValuesType>
  ): keyof EmailRecipientFormValuesType => {
    switch (values.recipient_type) {
      case 'colleague':
      case 'role':
      case 'authorized':
      case 'requestor':
        return values.recipient_type;

      default:
        return 'other';
    }
  };

  const { fields } = useForm({
    t,
    onChange: values => {
      const value = values[getActiveField(values)] || [];
      const valueAsArray = Array.isArray(value)
        ? (value as NestedFormValue[])
        : ([value] as NestedFormValue[]);

      setFieldValue(
        name,
        valueAsArray.map(item => ({
          ...item,
          recipientType: values.recipient_type,
        }))
      );
    },
    onTouched: (touched, values) => {
      setFieldTouched(name, Boolean(touched[getActiveField(values)]));
    },
    formDefinition: formDefinitionWithValues,
    rules,
  });

  return (
    <div>
      {fields.map(
        ({
          name: fieldName,
          FieldComponent,
          applyBackgroundColor,
          label,
          ...restFieldProps
        }) => (
          <div key={fieldName} className={classes.row}>
            {label && <div className={classes.label}>{label}</div>}
            <div
              className={classnames(
                applyBackgroundColor && classes.withBackground,
                classes.field
              )}
            >
              <FieldComponent
                name={fieldName}
                error={error}
                {...restFieldProps}
              />
            </div>
          </div>
        )
      )}
    </div>
  );
};

export default EmailRecipient;

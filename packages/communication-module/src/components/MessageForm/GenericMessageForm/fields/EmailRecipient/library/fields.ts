// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { FormDefinitionField } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { ContactFinderConfigType } from '@zaaksysteem/common/src/components/form/fields/ContactFinder/ContactFinder.types';
import { CaseRequestorConfigType } from '@zaaksysteem/common/src/components/form/fields/CaseRequestor/CaseRequestor.types';
import { mergeErrors } from '@zaaksysteem/common/src/components/form/library/mergeErrors';
import {
  isNotPresetRequestor,
  createDisplayName,
} from '@zaaksysteem/common/src/components/form/fields/CaseRoleFinder/CaseRoleFinder.library';
import { EmailRecipientFormValuesType } from '../EmailRecipient.types';
import { caseRoleValueResolve } from './utils';

export const getRecipientTypeField = (
  selectedRecipientType?: string
): FormDefinitionField<EmailRecipientFormValuesType> => ({
  name: 'recipient_type',
  type: fieldTypes.FLATVALUE_SELECT,
  value: selectedRecipientType || 'requestor',
  label: 'communication:addFields.recipientType',
  choices: [
    {
      value: 'requestor',
      label: 'communication:addFields.recipientTypes.requestor',
    },
    {
      value: 'colleague',
      label: 'communication:addFields.recipientTypes.colleague',
    },
    {
      value: 'role',
      label: 'communication:addFields.recipientTypes.role',
    },
    {
      value: 'authorized',
      label: 'communication:addFields.recipientTypes.authorized',
    },
    { value: 'other', label: 'communication:addFields.recipientTypes.other' },
  ],
  applyBackgroundColor: true,
});

export const otherField: FormDefinitionField<EmailRecipientFormValuesType> = {
  name: 'other',
  type: fieldTypes.MULTI_VALUE_TEXT,
  format: 'email',
  allowMagicString: true,
  value: [],
  multiValue: true,
  placeholder: 'Aan:',
  getError: mergeErrors<EmailRecipientFormValuesType>('other'),
};

export const colleagueField: FormDefinitionField<
  EmailRecipientFormValuesType,
  ContactFinderConfigType
> = {
  name: 'colleague',
  type: fieldTypes.CONTACT_FINDER,
  value: [],
  multiValue: true,
  applyBackgroundColor: true,
  placeholder: 'communication:addFields.colleague',
  config: {
    subjectTypes: ['employee'],
    itemResolver: (contact, item) => {
      const {
        attributes: { name, email_address },
        id,
      } = contact;

      return email_address
        ? {
            ...item,
            label: name,
            subLabel: email_address,
            value: email_address,
            id,
          }
        : null;
    },
  },
};

export const getRoleField = (
  caseUuid: string
): FormDefinitionField<EmailRecipientFormValuesType> => ({
  name: 'role',
  type: fieldTypes.CASE_ROLE_FINDER,
  value: [],
  placeholder: 'communication:addFields.rolePlaceholder',
  applyBackgroundColor: true,
  multiValue: true,
  config: {
    valueResolver: caseRoleValueResolve,
    itemFilter: isNotPresetRequestor,
    caseUuid,
  },
});

export const getAuthorizedField = (
  caseUuid: string
): FormDefinitionField<EmailRecipientFormValuesType> => ({
  name: 'authorized',
  type: fieldTypes.CASE_ROLE_FINDER,
  value: [],
  placeholder: 'communication:addFields.authorizedPlaceholder',
  applyBackgroundColor: true,
  multiValue: true,
  config: {
    valueResolver: caseRoleValueResolve,
    itemFilter: (item: any) =>
      isNotPresetRequestor(item) && item.role.attributes.authorized,
    caseUuid,
  },
});

export const getRequestorField = (
  caseUuid: string
): FormDefinitionField<
  EmailRecipientFormValuesType,
  CaseRequestorConfigType
> => ({
  name: 'requestor',
  type: fieldTypes.CASE_REQUESTOR,
  value: null,
  applyBackgroundColor: true,
  config: {
    valueResolver: role => ({
      value: caseRoleValueResolve(role),
      label: createDisplayName(role),
      id: role.subject?.id,
    }),
    errorMessage: 'communication:addFields.caseRequestorError',
    itemFilter: role => {
      return caseRoleValueResolve(role) !== '' && isNotPresetRequestor(role);
    },
    caseUuid,
  },
});

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { ReactElement } from 'react';
import {
  FormDefinition,
  FormValuesType,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { Rule } from '@zaaksysteem/common/src/components/form/rules';
import { EmailTemplateDataType } from '../../../types/EmailIntegration.types';

export interface GenericMessageFormPropsType<Values = any> {
  save: (values: Values) => void;
  cancel: () => void;
  busy: boolean;
  enablePreview?: boolean;
  formDefinition: FormDefinition<Values>;
  emailTemplateData?: EmailTemplateDataType;
  formName: string;
  mapPreviewValues?: (values: FormValuesType<Values>) => FormValuesType<Values>;
  top?: ReactElement;
  caseUuid?: string;
  primaryButtonLabelKey?: string;
  rules?: Rule[];
  labels?: boolean;
}

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@mui/styles';

export const useMessageFormStyle = makeStyles({
  addFormWrapper: {
    maxWidth: '700px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    fontSize: '14px',
    width: '100%',
    padding: 30,
    margin: '0 auto',
  },
});

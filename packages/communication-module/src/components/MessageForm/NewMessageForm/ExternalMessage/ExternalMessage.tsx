// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Routes, Route, Navigate, useParams } from 'react-router-dom';

import {
  TYPE_PIP_MESSAGE,
  TYPE_EMAIL,
  TYPE_MIJN_OVERHEID,
  TYPE_POSTEX,
} from '../../../../library/communicationTypes.constants';
import TabsContainer from '../Tabs/TabsContainer';
import { GenericMessageFormPropsType } from '../../GenericMessageForm/GenericMessageForm.types';
import { SubTypeOfMessage } from '../../../../types/Message.types';
import PipMessageContainer from './Pip/PipContainer';
import EmailContainer from './Email/EmailContainer';
import MessageTabs from './MessageTabs/MessageTabs';
import MijnOverheidContainer from './MijnOverheid/MijnOverheidContainer';

export interface MessagePropsType
  extends Pick<GenericMessageFormPropsType, 'cancel'> {
  [key: string]: any;
}

type ExternalMessageParamsType = {
  subType: SubTypeOfMessage;
};

const ExternalMessage: React.ComponentType<MessagePropsType> = props => {
  const { subType } = useParams<
    keyof ExternalMessageParamsType
  >() as ExternalMessageParamsType;
  const components = {
    [TYPE_PIP_MESSAGE]: PipMessageContainer,
    [TYPE_EMAIL]: EmailContainer,
    [TYPE_POSTEX]: EmailContainer,
    [TYPE_MIJN_OVERHEID]: MijnOverheidContainer,
  };
  const ContainerElement = components[subType];
  const ConnectedTabs = TabsContainer(MessageTabs);
  const tabs = <ConnectedTabs subtype={subType} />;

  return (
    <React.Fragment>
      <ContainerElement {...props} top={tabs} />
    </React.Fragment>
  );
};

const ExternalMessageRoutes: React.ComponentType<MessagePropsType> = props => {
  return (
    <Routes>
      <Route
        path=""
        element={<Navigate to={props.defaultSubType} replace={true} />}
      />
      <Route path={':subType'} element={<ExternalMessage {...props} />} />;
    </Routes>
  );
};

export default ExternalMessageRoutes;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { SaveMessageFormValuesType } from '../../../../types/Message.types';
import {
  EmailExternalMessageForm,
  EmailExternalMessageFormProps,
} from '../../EmailExternalMessageForm/EmailExternalMessageForm';

export interface EmailReplyFormPropsType
  extends Pick<
    EmailExternalMessageFormProps,
    'cancel' | 'caseUuid' | 'busy' | 'initialValues' | 'enablePreview'
  > {
  caseUuid: string;
  doSave: (
    caseUuid: string,
    threadUuid: string,
    values: SaveMessageFormValuesType
  ) => void;
  threadUuid: string;
}

const EmailReplyForm: React.ComponentType<EmailReplyFormPropsType> = ({
  doSave,
  caseUuid,
  threadUuid,
  ...rest
}) => {
  return (
    <div>
      <EmailExternalMessageForm
        {...rest}
        caseUuid={caseUuid}
        selectedRecipientType="other"
        save={values => doSave(caseUuid, threadUuid, values)}
      />
    </div>
  );
};

export default EmailReplyForm;

// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Button from '@mintlab/ui/App/Material/Button';
import EmailReplyFormContainer from './Email/EmailReplyFormContainer';
import PipReplyContainer from './Pip/PipReplyContainer';

export interface ReplyFormPropsType {
  type: 'pip' | 'email' | 'postex';
  caseUuid: string;
}

const ReplyForm: React.ComponentType<ReplyFormPropsType> = ({
  type,
  caseUuid,
}) => {
  const [replyIsOpen, setReplyOpen] = useState(false);
  const [t] = useTranslation('communication');
  const closeReplyForm = () => setReplyOpen(false);

  return replyIsOpen ? (
    <div>
      {type === 'email' ? (
        <EmailReplyFormContainer caseUuid={caseUuid} cancel={closeReplyForm} />
      ) : (
        <PipReplyContainer caseUuid={caseUuid} cancel={closeReplyForm} />
      )}
    </div>
  ) : (
    <Button
      name="openMessageReply"
      icon="add"
      action={() => {
        setReplyOpen(true);
      }}
    >
      {t('replyForm.openButtonLabel')}
    </Button>
  );
};

export default ReplyForm;

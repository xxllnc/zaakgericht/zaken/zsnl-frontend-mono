// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import { PartialFormValuesType } from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { AJAX_STATE_PENDING } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import {
  saveMessage,
  setAddCommunicationPending,
} from '../../../../store/add/communication.add.actions';
import { CommunicationRootStateType } from '../../../../store/communication.reducer';
import {
  SaveMessageFormValuesType,
  ExternalMessageType,
} from '../../../../types/Message.types';
import { resolveMagicStrings } from '../../../../library/resolveMagicStrings';
import { PipReplyForm, PipReplyFormPropsType } from './PipReplyForm';

type PropsFromStateType = Pick<
  PipReplyFormPropsType,
  | 'busy'
  | 'initialValues'
  | 'contactUuid'
  | 'threadUuid'
  | 'context'
  | 'enablePreview'
>;

type PropsFromDispatchType = Pick<PipReplyFormPropsType, 'doSave'>;

const mapStateToProps = ({
  communication: {
    thread: { messages, id },
    add: { state },
    context: { context, contactUuid },
  },
}: CommunicationRootStateType): PropsFromStateType => {
  if (!messages) {
    return {
      contactUuid,
      context,
      threadUuid: id,
      busy: true,
      initialValues: {},
    };
  }

  const firstMessage = messages[0];
  const initialValues: PartialFormValuesType<SaveMessageFormValuesType> = {
    subject: (firstMessage as ExternalMessageType).subject
      ? `RE: ${(firstMessage as ExternalMessageType).subject}`
      : '',
  };

  return {
    contactUuid,
    context,
    initialValues,
    threadUuid: id,
    enablePreview: context !== 'pip',
    busy: state === AJAX_STATE_PENDING,
  };
};

const mapDispatchToProps = (dispatch: Dispatch): PropsFromDispatchType => ({
  async doSave(caseUuid, threadUuid, values) {
    /*
     * --- START TEMPORARY CODE ---
     * This is temporary until Python can resolve magic string
     */
    dispatch(setAddCommunicationPending());

    const resolvedValues = await resolveMagicStrings<SaveMessageFormValuesType>(
      values,
      values.case_uuid
    );

    /* --- END TEMPORARY CODE --- */

    // Todo: Fix Nullable type in FormValues shape
    const action = saveMessage({
      ...(resolvedValues as any),
      message_type: 'pip',
      thread_uuid: threadUuid,
      case_uuid: caseUuid,
    });
    dispatch(action as any);
  },
});

export default connect<
  PropsFromStateType,
  PropsFromDispatchType,
  {},
  CommunicationRootStateType
>(
  mapStateToProps,
  mapDispatchToProps
)(PipReplyForm);

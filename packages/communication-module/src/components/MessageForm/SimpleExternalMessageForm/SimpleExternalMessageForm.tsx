// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { useTranslation } from 'react-i18next';
import mapValuesToFormDefinition from '@zaaksysteem/common/src/components/form/library/mapValuesToFormDefinition';
import { asArray } from '@mintlab/kitchen-sink/source';
import GenericMessageForm from '../GenericMessageForm/GenericMessageForm';
import { SaveMessageFormValuesType } from '../../../types/Message.types';
import { CommunicationContextContextType } from '../../../types/Context.types';
import { GenericMessageFormPropsType } from '../GenericMessageForm/GenericMessageForm.types';
import { getSimpleExternalMessageFormDefinition } from './simpleExternalMessageForm.formDefinition';

export type SimpleExternalMessageFormProps<Values = any> = Pick<
  GenericMessageFormPropsType<Values>,
  'save' | 'busy' | 'cancel' | 'top' | 'enablePreview'
> & {
  caseUuid?: string;
  contactUuid?: string;
  initialValues?: Partial<SaveMessageFormValuesType>;
  context: CommunicationContextContextType;
};

const mapPreviewValues = (
  values: SaveMessageFormValuesType
): Omit<SaveMessageFormValuesType, 'requestor'> & {
  requestor: string | string[];
} => {
  const mappedValues = {
    ...values,
    requestor: values.requestor
      ? asArray(values.requestor).map(item => item.label)
      : values.requestor,
  };

  return mappedValues;
};

export const SimpleExternalMessageForm: React.ComponentType<
  SimpleExternalMessageFormProps
> = props => {
  const { caseUuid, contactUuid, context, initialValues: values } = props;
  const [t] = useTranslation('communication');
  const formDefinition = getSimpleExternalMessageFormDefinition({
    caseUuid,
    context,
    contactUuid,
  });
  const formDefinitionWithValues = values
    ? mapValuesToFormDefinition<SaveMessageFormValuesType>(
        values,
        formDefinition
      )
    : formDefinition;

  return (
    <GenericMessageForm
      {...props}
      mapPreviewValues={mapPreviewValues}
      formDefinition={formDefinitionWithValues}
      primaryButtonLabelKey={t('forms.send')}
      formName="message-form"
      labels={Boolean(context === 'pip')}
    />
  );
};

export default SimpleExternalMessageForm;

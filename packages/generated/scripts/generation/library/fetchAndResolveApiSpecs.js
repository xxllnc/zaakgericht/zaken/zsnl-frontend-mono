// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
/* eslint-disable no-console */

const fetch = require('node-fetch');
const $RefParser = require('json-schema-ref-parser');
const handle = require('../../library/promiseHandle');

function createNullableType(data) {
  if (data.type === 'object') {
    return {
      oneOf: [{ type: 'null' }, data],
    };
  } else {
    return {
      ...data,
      type: [data.type, 'null'],
    };
  }
}

/*
 * The nullable property of OpenAPI spec 3.0 is not supported by the parser,
 * manually converting it to be JSON schema 4.0 compliant
 */
function parseNullable(schema) {
  // eslint-disable-next-line complexity
  return Object.entries(schema).reduce((acc, [key, value]) => {
    if (key === 'example') return { ...acc, [key]: value };

    const parsedValue = value.nullable ? createNullableType(value) : value;

    if (typeof value === 'object') {
      // eslint-disable-next-line no-unused-vars
      Object.entries(value).forEach(([__, va]) => {
        Array.isArray(va) &&
          va.some(xyz => xyz.nullable) &&
          va.push({ type: 'null' });
      });
    }

    return {
      ...acc,
      [key]:
        typeof parsedValue === 'object' && !Array.isArray(parsedValue)
          ? parseNullable(parsedValue)
          : Array.isArray(parsedValue)
          ? parsedValue.map(val =>
              val && val.nullable ? parseNullable(val) : val
            )
          : value,
    };
  }, {});
}

function shouldRenameTitle(schema, name, value) {
  return (
    typeof schema.description === 'undefined' &&
    name === 'title' &&
    typeof value === 'string'
  );
}

function shouldRemoveTitle(schema, name, value) {
  return (
    typeof schema.description !== 'undefined' &&
    name === 'title' &&
    typeof value === 'string'
  );
}

/**
 * Rename nested title properties to description, because using the title
 * property will create a new type. And since titles are not unique we end
 * up with duplicated types. It's fairly safe to rename the title properties
 * to description properties since they seem to be used in that way.
 */
function renameTitleProperties(schema, remove = false) {
  return Object.entries(schema).reduce((acc, [name, value]) => {
    const shouldRename = shouldRenameTitle(schema, name, value) && remove;
    const shouldRemove = shouldRemoveTitle(schema, name, value) && remove;
    const propertyName = shouldRename ? 'description' : name;

    return shouldRemove
      ? acc
      : {
          ...acc,
          [propertyName]:
            !Array.isArray(value) && value === Object(value)
              ? renameTitleProperties(value, true)
              : value,
        };
  }, {});
}

/* eslint complexity: [2, 7] */
async function fetchAndResolveApiSpec(environmentUrl, baseUrl, fileName) {
  const url = `${environmentUrl}${baseUrl}${fileName}`;
  console.log('Reading spec file', url);
  const [response, responseError] = await handle(fetch(url));

  if (responseError)
    throw Error(`Error fetching schema ${url}: ${responseError}`);

  const [json, jsonError] = await handle(response.json());

  if (jsonError)
    throw Error(`Error parsing JSON for schema ${url}: ${responseError}`);

  const nullableSchema = parseNullable(json);
  const schemaWithoutTitles = renameTitleProperties(nullableSchema, true);

  return new Promise(resolve => {
    $RefParser.dereference(
      schemaWithoutTitles,
      {
        resolve: {
          file: {
            read(file, callback) {
              const [filename] = file.url.split('/').reverse();

              fetch(`${environmentUrl}${baseUrl}${filename}`)
                .then(fileResponse => fileResponse.json())
                .then(fileJson => {
                  const nullableRefSchema = parseNullable(fileJson);
                  const refSchemaWithoutTites = renameTitleProperties(
                    nullableRefSchema,
                    true
                  );

                  return callback(null, refSchemaWithoutTites);
                })
                .catch(error => callback(error));
            },
          },
        },
      },
      (err, data) => {
        if (err) {
          throw err;
        }

        resolve(data);
      }
    );
  });
}

async function fetchAndResolveApiSpecs(environmentUrl, schemas) {
  const schemaPromises = Object.entries(schemas).map(
    ([domain, { api_docs, entities, baseUrl }]) => {
      const entitiesPromise = Promise.all(
        entities.map(entity =>
          fetchAndResolveApiSpec(environmentUrl, baseUrl, entity)
        )
      );

      return Promise.all([
        Promise.resolve(domain),
        fetchAndResolveApiSpec(environmentUrl, baseUrl, api_docs),
        entitiesPromise,
      ]);
    }
  );

  const [specs, specsError] = await handle(Promise.all(schemaPromises));

  if (specsError)
    throw Error(`Error fetching/resolving api specs: ${specsError}`);

  return specs;
}

module.exports = fetchAndResolveApiSpecs;

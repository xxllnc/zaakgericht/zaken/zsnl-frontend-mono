// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
/* eslint-disable no-console */

const fs = require('fs');
const { promisify } = require('util');
const prettier = require('prettier');

const writeFileAsync = promisify(fs.writeFile);

function writeFile(output, outputPath) {
  return writeFileAsync(outputPath, output);
}

async function writeTypes(types, outputPath, environmentUrl) {
  types.forEach(async ([domain, api_docs, entities]) => {
    const banner = `// Generated on: ${new Date()}
// Environment used: ${environmentUrl}
//
// This file was automatically generated. DO NOT MODIFY IT BY HAND.
// Instead, rerun generation of ${domain} domain.

/* eslint-disable */
`;
    const allEntities = entities.reduce((acc, cur) => [...acc, ...cur], []);
    const allTypes = [...api_docs, ...allEntities];
    const output = allTypes.join('\n');
    const namespacedOutput = `${banner} export namespace API${domain} {${output}}`;

    const path = `${outputPath}API${domain}.types.ts`;
    const prettierOptions = {
      ...(await prettier.resolveConfig(__filename)),
      parser: 'typescript',
    };
    const formattedOutput = prettier.format(namespacedOutput, prettierOptions);
    writeFile(formattedOutput, path);

    console.log(`- written ${allTypes.length} types in ${path}`);
  });
}

module.exports = writeTypes;

# Project structure

## 🤷 Where is everything?

```tree
apps                      Source of our apps (based on Create React App).
  └── admin                 The Admin SPA. (/admin)
  └── main                  The Main SPA (/main)
  └── pip                   The PIP SPA (/pip)
ngnix                     Contains nginx development & production config, development config is used to allow proxy request to local running instance.
packages                  Source of our packages
  └── common                Common components and function used in apps/packages
  └── communication-module  CommunicationModule, used in pip and main app
  └── generated             Generated TypeScript interfaces based on OpenAPI spec files
  └── kitchen-sink          Common utility files
  └── ui                    Generic UI components
scripts                   Scripts not specific to a package/app
```

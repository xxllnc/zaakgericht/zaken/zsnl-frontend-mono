# Generated API Types

## 🌉 Bridging the gap between the UI and the API

> API types are complex and very error prone when written manually, thats why we generate it!

## OpenAPI 3.0

The API's are written by the `OpenAPI 3.0` standard, this standard dictates the use of a API spec file. This file includes all the request and response shapes for al the API calls. We read this spec file and generate `types` using the `json-schema-to-typescript` package.

## Generating fresh types

The generated `types` are part of the git repository, if a API spec get's updated/added/removed you'll need to regenerate the `types`.

To generate the types, simply run in the project root or Docker root:

```bash
yarn generate-types
```

You will be prompted to specify which domain you want to generate. Select at least one domain.

Last but not least specify the environment (hosting the API spec) which should be used to generate the `types`.

- **development**: https://development.zaaksysteem.nl
- **hotfix**: Enviroment deprecated
- **other**: Please specify a full domain e.g. https://dev.zaaksysteem.nl

## How to use the generated types?

Using the `types` from `@zaaksysteem/generated` is pretty straightforward. There are 3 different types generated:

- Request params
- Request body
- Response body

### Request params

A request params type can be recognized by the postfix `RequestParams`. This type can be used directly with the `buildUrl` util.

```typescript
import { APIDocument } from '@zaaksysteem/generated';
import { buildUrl } from '@mintlab/kitchen-sink/source';

const url = buildUrl<APIDocument.SearchDocumentRequestParams>(
  `/api/v2/document/search_document`,
  {
    case_uuid: 'abc',
  }
);
```

### Request body

A request body type can be recognized by the postfix `RequestBody`. You can use this `type` directly with the `createAjaxAction` util.

```typescript
import { createAjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { createAjaxConstants } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { ThunkActionWithPayload } from '@zaaksysteem/common/src/types/ActionWithPayload';
import { APICommunication } from '@zaaksysteem/generated';

const SAVE_COMMUNICATION = createAjaxConstants('SAVE_COMMUNICATION');
const saveAjaxAction = createAjaxAction(SAVE_COMMUNICATION);

export type SaveNotePayloadType = {
  values: Pick<APICommunication.CreateNoteRequestBody, 'content'>;
};

export const saveNoteAction = (
  payload: SaveNotePayloadType
): ThunkActionWithPayload<
  CommunicationRootStateType,
  APICommunication.CreateNoteRequestBody
> => (dispatch, getState) => {
  const {
    communication: { context },
  } = getState();

  const url = '/api/v2/communication/create_note';
  const { caseUuid, contactUuid } = context;
  const { content } = payload.values;

  return saveAjaxAction<
    SaveNotePayloadType,
    APICommunication.CreateNoteRequestBody
  >({
    url,
    method: 'POST',
    payload,
    data: {
      thread_uuid: uuidv4(),
      note_uuid: uuidv4(),
      content,
      ...(caseUuid
        ? { case_uuid: caseUuid }
        : { contact_uuid: contactUuid || '' }),
    },
  })(dispatch);
};
```

### Response body

A response body type can be recognized by the postfix `ResponseBody`.

#### Usage in a Redux `reducer`

```typescript
import { Reducer } from 'redux';
import { APICommunication } from '@zaaksysteem/generated';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { THREAD_FETCH } from './communication.thread.constants';
import { FetchThreadActionPayloadType } from './communication.thread.actions';

const handleFetchSuccess = (
  state: CommunicationThreadState,
  action: AjaxAction<
    APICommunication.GetMessageListResponseBody,
    FetchThreadActionPayloadType
  >
): CommunicationThreadState => {
  const { response } = action.payload;

  // return new state based on response
};

export const threadList: Reducer<
  CommunicationThreadState,
  AjaxAction<unknown>
> = (state = initialState, action) => {
  const { type } = action;

  switch (type) {
    case THREAD_FETCH.SUCCESS:
      return handleFetchSuccess(
        state,
        action as AjaxAction<
          APICommunication.GetMessageListResponseBody,
          FetchThreadActionPayloadType
        >
      );

    default:
      return state;
  }
};

export default threadList;
```

#### Usage with the `request` util

```typescript
import { request } from '@zaaksysteem/common/src/library/fetch';
import { APICommunication } from '@zaaksysteem/generated';

async function fetchCases(): Promise<APICommunication.GetCaseListForContactResponseBody> {
  const response = await request(
    'GET',
    `/api/v2/communication/get_case_list_for_contact`
  );
  return await response<APICommunication.GetCaseListForContactResponseBody>.json()
}
```

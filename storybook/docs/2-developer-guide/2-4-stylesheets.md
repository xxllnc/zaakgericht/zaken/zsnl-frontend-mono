# Stylesheets

## 🧐 Where are all the bloody `css` files?

> We are JSS beliebers

## Creating a components stylesheet

Each `Component` should have it's own stylesheet, isolating it from the bigger picture.

### Naming convention

A stylesheet is named after the component it is intended for postfixed by `style`, e.g. `MyComponent.style.ts`

### Stylesheet hook

Using a hook is the preferred way of creating and using a component stylesheet.

_MyComponent.style.ts_

```typescript
import { makeStyles } from '@mui/styles';

export const useMyComponentStyles = makeStyles() => ({
  wrapper: {
    backgroundColor: 'red',
  },
});
```

In some cases you will need style that is associated
with the context of a _Material-UI_ component. For
example, you might have two user defined components that
use the same _Material-UI_ component but it has to look
substantially different.

_MyComponent.style.ts_

```typescript
import { makeStyles } from '@mui/styles';

export const useMyComponentStyles = makeStyles(theme: any) => ({
  wrapper: {
    backgroundColor: 'red',
    borderRadius: theme.shape.borderRadius,
  },
});
```

Example usage:

```javascript
import Button from '@mui/material/Button';
import { useMyComponentStyles } from './MyComponent.style.js';

export const MyComponent = () => {
  const classes = useMyComponentStyles();

  return (
    <div className={classes.wrapper}>
      <Button>Click me!</Button>
    </div>
  );
};

export default MyComponent;
```

## Accessing the theme in a component

> Hooks, hooks, hooks! ;-)

The `useTheme` hook returns the `theme` when used in a component.

```javascript
import Button from '@mui/material/Button';
import { useMyComponentStyles } from './MyComponent.style.js';

export const MyComponent = () => {
  const classes = useMyComponentStyles();
  const theme = useTheme();

  // Do something with the theme

  return (
    <div className={classes.wrapper}>
      <Button>Click me!</Button>
    </div>
  );
};
```

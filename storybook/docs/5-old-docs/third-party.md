# Third party dependencies

- [_React_](https://reactjs.org/docs/react-api.html)
- [_Material-UI_](https://material-ui.com/)

## Configuration and infrastructure

- [Storybook](https://storybook.js.org/basics/introduction/)
- [webpack configuration](https://webpack.js.org/configuration/)
  - cf. `./storybook/webpack`
- [PLOP](https://plopjs.com/)

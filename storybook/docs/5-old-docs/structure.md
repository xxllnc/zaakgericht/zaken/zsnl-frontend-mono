# Global component source code structure

Components source code is organized into the directories

- `/Apps`
  - `/Abstract`
    - React components that do not directly render
      themselves but rather consume other components to
      produce enhanced rendering
  - `/Material`
    - implementations of Material Design components
      (currently mostly facades for `@material-ui`,
      but that might change)
  - `/Zaaksysteem`
    - components that are either not Material Design
      components at all or fairly specific
      composite components

This is to provide an easier overview both in the source code tree
and the _StoryBook_ UI _Story Panel_ sidebar.

The npm package distribution, however, exports flat named modules.
See the generated file `/App/index.js`.

To create a new component, run

    $ npm run plop

and use the interactive prompt.
This will also take care of name collisions.

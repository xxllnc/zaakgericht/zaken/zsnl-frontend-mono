export const removeOrderingIntegers = name => {
  const integersAtStartRegExp = /^[0-9\-\. ]+/;

  return name.replace(integersAtStartRegExp, '');
};

export const removeExtension = name => {
  const extensionRegExp = /.[a-z]{2,4}$/;

  return name.replace(extensionRegExp, '');
};

export const capitalizeName = name => {
  return removeOrderingIntegers(name)
    .split('-')
    .map(part => {
      return part
        .split('')
        .map((letter, letterIndex) =>
          letterIndex === 0 ? letter.toUpperCase() : letter
        )
        .join('');
    })
    .join(' ');
};

export const createStoryName = name => {
  const nameWithouExtension = removeExtension(name);

  return capitalizeName(nameWithouExtension);
};

export const createStoryPath = name => {
  const [fileName, ...pathReversed] = name
    .split('/')
    .filter(part => part !== '.')
    .reverse();

  return {
    fileName: createStoryName(fileName),
    path: pathReversed.reverse().map(capitalizeName).join('/'),
  };
};
